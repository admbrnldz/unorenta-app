<?php
    $class = '';
    if ($comment->author_id == $currentUser->employee->id) {
        $class = 'post-discussion-thread-entry-yours';
    }
?>
<li class="post-discussion-thread-entry {{ $class }} clearfix">
    <div class="pd-container">
        <div class="pd-dp">
            <div class="dp-handler" style="background:url({{ $comment->author->user->dp() }}) center center no-repeat;background-size:cover;">
                <a href="{{ route('employees.profile', ['employeeId' => $comment->author->id]) }}" title="{{ $comment->author->user->name() }}"></a>
            </div>
        </div>
        <div class="pd-content-wrap" title="Posted {{ processDate($comment->posted_at, true) }}">
            <div class="pd-content">
                {!! $comment->comment !!}
            </div>
        </div>
    </div>
</li>