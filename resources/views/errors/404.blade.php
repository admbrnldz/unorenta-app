@extends('layouts.app')

@section('title', 'Error 404: Page Not Found')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">
            
            <div class="page-error ta-center">
            	<h1>404</h1>
            	<h2>Page Not Found</h2>
            	<h4>Sorry, this page does not exist.</h4>
            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    

});
</script>
@endsection