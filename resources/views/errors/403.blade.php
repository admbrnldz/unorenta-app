@extends('layouts.app')

@section('title', 'Error 403: Access Denied')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">
            
            <div class="page-error ta-center">
            	<h1>403</h1>
            	<h2>Access Denied</h2>
            	<h4>You don't have permission to access this page.</h4>
            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    

});
</script>
@endsection