@forelse ($employee->dependents as $dependent)

    <li class="dependent-entry" id="dependent-entry-{{ $dependent->id }}">
        <small>{{ $dependent->relationship }}</small>
        <span>
            <a href="{{ route('dependent.update') }}/{{ $dependent->id }}" class="edit-dependent" data-target-form="#update-dependent-form">
                <strong>{{ $dependent->name() }}</strong>
            </a>
            <br />
            {{ processDate($dependent->birthdate) }}, {{ $dependent->birthplace }}
        </span>
    </li>

@empty

    <li class="no-dependent-entry">
        <small>No dependents yet</small>
    </li>

@endforelse