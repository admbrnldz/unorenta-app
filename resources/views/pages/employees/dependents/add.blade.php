<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('dependent.add') }}" method="post" autocomplete="off" id="add-dependent-form">
{!! csrf_field() !!}
<input type="hidden" name="employee_id" id="employee-id" value="{{ $employee->id }}" />
<div class="form-horizontal form-primary">
    
    <div class="form-group">
        <label for="first-name" class="col-xs-12 col-sm-3 control-label">Name</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="first_name" id="first-name" placeholder="First Name" value="{{ old('first_name') }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="middle_name" id="middle-name" placeholder="Middle Name" value="{{ old('middle_name') }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="last_name" id="last-name" placeholder="Last Name" value="{{ old('last_name') }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="contact-number" class="col-xs-12 col-sm-3 control-label">Birthdate</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control datepicker" name="birthdate" id="birthdate" value="{{ old('birthdate') }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="birthplace" class="col-xs-12 col-sm-3 control-label">Birthplace</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="birthplace" id="birthplace" value="{{ old('birthplace') }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="relationship" class="col-xs-12 col-sm-3 control-label">Relationship</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="relationship" id="relationship" value="{{ old('relationship') }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('#birthdate').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "{{ ($today->year - 21) }}:{{ $today->year }}",
        dateFormat: 'MM d, yy'
    });

});
</script>