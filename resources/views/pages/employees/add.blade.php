@extends('layouts.app')

@section('title', 'Add Employee')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Add Employee</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li>
                            <a href="{{ route('employees') }}">Employees</a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">           

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.errors')
                    </div>
                </div>

                <form method="post" autocomplete="off" enctype="multipart/form-data" id="add-employee-form">
                {!! csrf_field() !!}

                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Account Settings</h4>
                        </div>
                        <div class="panel-body">
                        
                            <div class="form-horizontal form-primary">

                                <div class="form-group">
                                    <label for="first-name" class="col-xs-12 col-sm-3 control-label">Display Photo</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">

                                                <div class="add-employee-dp">
                                                    <div class="profile-dp upload-profile" 
                                                        style="background:url({{ asset('assets/images/no-dp.png') }}) center center no-repeat;background-size:cover;"
                                                        data-target="#employee-dp"
                                                    ></div>
                                                </div>
                                                <div class="upload-dp-wrap">
                                                    <input type="file" name="display_photo" class="form-control-file" id="employee-dp" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="first-name" class="col-xs-12 col-sm-3 control-label">Name</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" name="first_name" id="first-name" placeholder="First Name" value="{{ old('first_name') }}" />
                                            </div>
                                        </div>
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" name="middle_name" id="middle-name" placeholder="Middle Name" value="{{ old('middle_name') }}" />
                                            </div>
                                        </div>
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" name="last_name" id="last-name" placeholder="Last Name" value="{{ old('last_name') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="username" class="col-xs-12 col-sm-3 control-label">Username</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control verify-field" name="username" id="username" placeholder="" value="{{ old('username') }}" data-url="{{ route('account.check.username') }}" data-user-id="" />
                                                <div class="username-verifier field-verifier"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-group-last">
                                    <label for="password" class="col-xs-12 col-sm-3 control-label">Password</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="password" id="password" placeholder="" />
                                                <a href="#" id="generate-password" data-target="password" class="btn btn-sm btn-primary" style="margin-top:10px;">Generate Password</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Personal Information</h4>    
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal form-primary">

                                <div class="form-group">
                                    <label for="nickname" class="col-xs-12 col-sm-3 control-label">Nickname</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="nickname" id="nickname" value="{{ old('nickname') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="address" class="col-xs-12 col-sm-3 control-label">Address</label>
                                    <div id="employee-address" class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" name="address" id="address" placeholder="Home No., Street, Subdivision/Village" value="{{ old('address') }}" />
                                            </div>
                                        </div>
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-5">
                                                <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ old('city') }}" />
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="province" id="province" placeholder="Province" value="{{ old('province') }}" />
                                            </div>
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" name="zip_code" id="zip-code" placeholder="Zip Code" value="{{ old('zip_code') }}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="mobile-number" class="col-xs-12 col-sm-3 control-label">Phone Numbers</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="mobile_number" id="mobile-number" placeholder="Mobile" value="{{ old('mobile_number') }}" />
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="home_number" id="home-number" placeholder="Home" value="{{ old('home_number') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email-address" class="col-xs-12 col-sm-3 control-label">Email</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="email" class="form-control" name="email_address" id="email-address" value="{{ old('email_address') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-group-separator"><span></span></div>

                                <div class="form-group">
                                    <label for="birthdate" class="col-xs-12 col-sm-3 control-label">Birthdate</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="birthdate" id="birthdate" value="{{ old('birthdate') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="birthplace" class="col-xs-12 col-sm-3 control-label">Birth Place</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="birthplace" id="birthplace" value="{{ old('birthplace') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{--*/ $sexOld = old('sex') /*--}}
                                    <label for="sex" class="col-xs-12 col-sm-3 control-label">Sex</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="sex" value="Male" 
                                                            <?php if ($sexOld == 'Male'): ?>checked<?php endif; ?>
                                                        />
                                                        <span>Male</span>
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="sex" value="Female" 
                                                            <?php if ($sexOld == 'Female'): ?>checked<?php endif; ?>
                                                        />
                                                        <span>Female</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{--*/ $civilStatusOld = old('civil_status') /*--}}
                                    <label for="civil-status" class="col-xs-12 col-sm-3 control-label">Civil Status</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">
                                                @foreach (getCivilStatus() as $civilStatus)
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="civil_status" value="{{ $civilStatus }}" 
                                                                <?php if ($civilStatusOld == $civilStatus): ?>checked<?php endif; ?>
                                                            />
                                                            <span>{{ $civilStatus }}</span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="citizenship" class="col-xs-12 col-sm-3 control-label">Citizenship</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-3">
                                                <select name="citizenship" id="citizenship" class="form-control">
                                                    <option value="Filipino">Filipino</option>
                                                    <option value="Others">Others</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-12 col-sm-9">
                                                <input type="text" class="form-control" name="citizenship_other" id="citizenship-other" placeholder="If others, please specify." value="{{ old('citizenship_other') }}" disabled="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="religion" class="col-xs-12 col-sm-3 control-label">Religion</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="religion" id="religion" value="{{ old('religion') }}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="height" class="col-xs-12 col-sm-3 control-label">Height/Weight</label>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-6">
                                                <input type="number" class="form-control" name="height" id="height" placeholder="Height" step="0.01" value="{{ old('height') }}" />
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="number" class="form-control" name="weight" id="weight" placeholder="Weight" step="0.01" value="{{ old('weight') }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <small style="display:block;padding:7px 0 0 0;"><em>*Foot/Kilo</em></small>
                                    </div>
                                </div>
                                               
                                <div class="form-group">
                                    <label for="build" class="col-xs-12 col-sm-3 control-label">Build/Eyes/Blood Type</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-3">
                                                <input type="text" class="form-control" name="build" id="build" placeholder="Build" value="{{ old('build') }}" />
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <input type="text" class="form-control" name="eyes" id="eyes" placeholder="Eyes" value="{{ old('eyes') }}" />
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <input type="text" class="form-control" name="blood_type" id="blood-type" placeholder="Blood Type" value="{{ old('blood_type') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-group-separator"><span></span></div>

                                <div class="form-group">
                                    <label for="sss" class="col-xs-12 col-sm-3 control-label">SSS Number</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="sss" id="sss" value="{{ old('sss') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="tin" class="col-xs-12 col-sm-3 control-label">TIN</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="tin" id="tin" value="{{ old('tin') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="philhealth" class="col-xs-12 col-sm-3 control-label">PhilHealth Number</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="philhealth" id="philhealth" value="{{ old('philhealth') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-group-last">
                                    <label for="pagibig" class="col-xs-12 col-sm-3 control-label">Pag-IBIG Number</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="pagibig" id="pagibig" value="{{ old('pagibig') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Employment Details</h4>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal form-primary">

                                <div class="form-group">
                                    {{--*/ $positionId = old('position_id') /*--}}
                                    <label for="position" class="col-xs-12 col-sm-3 control-label">Position</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">
                                                <select name="position_id" id="position" class="form-control">
                                                    <option value="">Select a Position</option>
                                                    @foreach ($positions as $position)
                                                        <option value="{{ $position->id }}"
                                                            <?php if ($position->id == $positionId): ?>selected<?php endif; ?>
                                                        >
                                                            {{ $position->name }} &mdash; {{ $position->department->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="basic-income" class="col-xs-12 col-sm-3 control-label">Basic Income</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-4">
                                                <input type="number" class="form-control" step="0.01" name="basic_income" id="basic-income" value="{{ old('basic_income') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="monthly-work-days" class="col-xs-12 col-sm-3 control-label">Monthly Work Days</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-4">
                                                <input type="number" name="monthly_work_days" step="0.0001" id="monthly-work-days" class="form-control" 
                                                    value="21.6667"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="date-hired" class="col-xs-12 col-sm-3 control-label">Date Hired</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="date_hired" id="date-hired" value="{{ old('date_hired', $today->format('F j, Y')) }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--*/ $employmentStatus = old('employment_status') /*--}}
                                <div id="status-wrap" class="form-group form-group-last">
                                    <label class="col-xs-12 col-sm-3 control-label">Status</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            @foreach (getEmploymentStatus() as $id => $es)
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" class="status" name="employment_status" value="{{ $id }}" 
                                                            <?php if ($employmentStatus == $id): ?>checked<?php endif; ?>
                                                        />
                                                        <span>{{ $es }}</span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="leave-type-wrap hidden">

                                    <div class="form-group form-group-separator"><span></span></div>

                                    <div class="form-group hidden-xs">
                                        <div class="col-xs-12 col-sm-3 col-sm-offset-3">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12 col-sm-6"><span class="label label-primary">Accounting Year</span></div>
                                                <div class="col-xs-12 col-sm-6"><span class="label label-primary">Credits</span></div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--*/ $count = count($leaveTypes) /*--}}
                                    {{--*/ $lcount = 1 /*--}}
                                    @foreach ($leaveTypes as $leaveType)
                                        @if ($count == $lcount)
                                        <div class="form-group form-group-last">
                                        @else
                                        <div class="form-group">
                                        @endif
                                            <label for="leave-type-{{ $leaveType->id }}" class="col-xs-12 col-sm-3 control-label">{{ $leaveType->name }}</label>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="leave[{{ $leaveType->id }}][year]" value="{{ old('leave[$leaveType->id][year]', $currentYear) }}" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        {{--*/ $lc = '' /*--}}
                                                        @if ($leaveType->name == 'Sick Leave' || $leaveType->name == 'Vacation Leave')
                                                            {{--*/ $lc = 15 /*--}}
                                                        @endif
                                                        <input type="text" class="form-control" name="leave[{{ $leaveType->id }}][credits]" id="leave-type-{{ $leaveType->id }}" value="{{ old('leave[$leaveType->id][credits]', $lc) }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--*/ $lcount++ /*--}}
                                    @endforeach

                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Direct Family</h4>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal form-primary">

                                <div class="form-group">
                                    <label for="father-name" class="col-xs-12 col-sm-3 control-label">Father's Name/Occupation</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-8">
                                                <input type="text" class="form-control" name="father_name" id="father-name" placeholder="Father's Name" value="{{ old('father_name') }}" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4">
                                                <input type="text" class="form-control" name="father_occupation" id="father-occupation" placeholder="Occupation" value="{{ old('father_occupation') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="mother-name" class="col-xs-12 col-sm-3 control-label">Mother's Name/Occupation</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-8">
                                                <input type="text" class="form-control" name="mother_name" id="mother-name" placeholder="Mother's Name" value="{{ old('mother_name') }}" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4">
                                                <input type="text" class="form-control" name="mother_occupation" id="mother-occupation" placeholder="Occupation" value="{{ old('mother_occupation') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-group-last">
                                    <label for="spouse-name" class="col-xs-12 col-sm-3 control-label">Spouse's Name/Occupation</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-8">
                                                <input type="text" class="form-control" name="spouse_name" id="spouse-name" placeholder="Spouse's Name" value="{{ old('spouse_name') }}" />
                                            </div>
                                            <div class="col-xs-12 col-sm-4">
                                                <input type="text" class="form-control" name="spouse_occupation" id="spouse-occupation" placeholder="Occupation" value="{{ old('spouse_occupation') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h4>In-case of Emergency</h4>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal form-primary">

                                <div class="form-group">
                                    <label for="cp" class="col-xs-12 col-sm-3 control-label">Contact Person</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-4">
                                                <select name="contact_person_opt" id="cp" class="form-control">
                                                    <option value=""></option>
                                                    <option value="mother">Mother</option>
                                                    <option value="father">Father</option>
                                                    <option value="spouse">Spouse</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-12 col-sm-8">
                                                <input type="text" class="form-control" name="contact_person" id="contact-person" placeholder="Contact Person's Name" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="contact-person-number" class="col-xs-12 col-sm-3 control-label">Contact Number</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-6">
                                                <input type="text" class="form-control" name="cp_number" id="contact-person-number" placeholder="" value="{{ old('contact_person_number') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="contact-person" class="col-xs-12 col-sm-3 control-label">Address</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row" id="contact-address-wrap">
                                            <div class="col-xs-12">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="cp_address" id="contact-address" placeholder="Complete Address" value="{{ old('cp_address') }}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" data-target="#contact-address" id="contact-same-address" />
                                                        <span>Same with employee's address</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-group-last">
                                    <label for="contact-person-language-spoken" class="col-xs-12 col-sm-3 control-label">Language Spoken</label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control" name="cp_language_spoken" id="contact-person-language-spoken" value="{{ old('contact_person_number') }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="panel-footer">
                            <div class="form-button-group clearfix">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ route('departments') }}" class="btn btn-default">Cancel</a>
                            </div>
                        </div>
                    </div>

                </form>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#mobile-number').mask('+63 999 999 9999');
    $('#contact-person-number').mask('+63 999 999 9999');
    $('#home-number').mask('+63 99 999 9999');
    $('#sss').mask('99-9999999-9');
    $('#tin').mask('999-999-999-999');
    $('#philhealth').mask('99-999999999-9');
    $('#pagibig').mask('9999-9999-9999');

    $('#date-hired').datepicker({
        dateFormat: 'MM d, yy'
    });

    $('#contact-same-address, #guardian-same-address').on('change', function() {
        if ($(this).prop('checked')) {
            if ($('#address').val() != '') {
                var employeeAddress = $('#address').val() + ', ' + $('#city').val() + ', ' + $('#province').val() + ', ' + $('#zip-code').val(),
                    target = $(this).attr('data-target');
                $(target).val(employeeAddress).prop('disabled', true);
            }
        } else {
            $(target).val('').prop('disabled', false); // clear
        }
    });

    $('.status').each(function() {
        $(this).on('change', function() {
            if ($(this).val() == 4) {
                $('#status-wrap').removeClass('form-group-last');
                $('.leave-type-wrap').removeClass('hidden');
            } else {
                $('#status-wrap').addClass('form-group-last');
                $('.leave-type-wrap').addClass('hidden');
            }
        });
    });

});
</script>
@endsection