<div class="profile-header">    
    <div class="profile-dp-td">
        <div class="profile-dp-wrap">
            <div id="e-dp" class="profile-dp <?php if ($currentUser->can('edit_employee', $currentUser) OR $currentUser->id == $employee->user->id): ?>upload-profile<?php endif; ?>" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;" data-target="#edit-dp"></div>

            @if (($currentUser->can('edit_employee', $currentUser) OR $currentUser->id == $employee->user->id) AND $employee->currentStatus(true) < 5)
                <div class="upload-dp-wrap">
                    <form action="{{ route('account.uploaddp') }}/{{ $employee->user->id }}" method="post" enctype="multipart/form-data" autocomplete="off" id="edit-dp-form">
                        {!! csrf_field() !!}
                        <input type="file" name="dp_file" class="form-control-file" id="edit-dp" data-target-form="#edit-dp-form" />
                        <input type="hidden" name="user_id" value="{{ $employee->user->id }}" />
                    </form>
                </div>
            @endif
        </div>
    </div>

    <div class="profile-meta">
        <h1>{{ $employee->user->name() }}</h1>
        <h2>{{ $employee->position->name }}</h2>
        {{--*/ $class = 'label-primary' /*--}}
        @if ($employee->currentStatus(true) > 4)
            {{--*/ $class = 'label-danger' /*--}}
        @endif
        <span class="label {{ $class }}">
            {{ $employee->currentStatus() }}
        </span>

        @if ($employee->mobile_number != '' OR $employee->home_number != '' OR $employee->email_address != '')
        <div class="profile-contacts">
            @if ($currentUser->can('edit_employee', $currentUser) OR $currentUser->id == $employee->user->id)
                @if ($employee->mobile_number != '')
                    <h5>
                        <i class="fa fa-mobile fa-fw"></i>{{ $employee->mobile_number }}
                    </h5>
                @endif                                    
                @if ($employee->home_number != '')
                    <h5>
                        <i class="fa fa-phone fa-fw"></i>{{ $employee->home_number }}
                    </h5>
                @endif
                @if ($employee->email_address != '')
                    <h5>
                        <i class="fa fa-envelope fa-fw"></i>{{ $employee->email_address }}
                    </h5>
                @endif
            @endif
        </div>
        @endif

    </div>
    @if ($currentUser->can('edit_employee', $currentUser) OR $employee->id == $currentUser->employee->id)
    <div class="profile-options">
        <div class="profile-opts-wrap">
            <button type="button" class="btn btn-primary btn-sm btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v fa-fw"></i>
            </button>
            <ul id="profile-opts-ul" class="dropdown-menu dropdown-menu-right">

                @if ($employee->user->is_active)
                    {{--*/ $hidden = '' /*--}}
                    @if ($employee->user->display_photo == 'no-dp.png')
                        {{--*/ $hidden = 'hidden' /*--}}
                    @endif
                    <li><a href="{{ route('account.removedp') }}/{{ $employee->user->id }}" id="dp-remove" class="{{ $hidden }}">Remove Photo</a></li>
                    <li><a href="#" id="dp-upload">Upload Photo</a></li>
                @endif

                @can ('edit_account_settings', $currentUser)
                    @if ($employee->user->is_active)
                        <li role="separator" class="divider"></li>
                    @endif
                    @if ($employee->user->is_active)
                        <li><a href="#" id="deactivate-account">Deactivate Account</a></li>
                        <form action="{{ route('account.deactivate') }}/{{ $employee->user->id }}" id="deactivate-account-form">
                            {!! csrf_field() !!}
                        </form>
                    @else
                        <li><a href="#" id="reactivate-account">Reactivate Account</a></li>
                        <form action="{{ route('account.reactivate') }}/{{ $employee->user->id }}" id="reactivate-account-form">
                            {!! csrf_field() !!}
                        </form>
                    @endif
                @endcan
            </ul>
        </div>
    </div>
    @endif
</div>