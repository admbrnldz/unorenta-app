<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('education.update') }}" method="post" autocomplete="off" id="update-education-form" data-delete="{{ route('education.delete') }}/{{ $education->id }}">
{!! csrf_field() !!}
<input type="hidden" name="employee_id" id="employee-id" value="{{ $education->employee->id }}" />
<input type="hidden" name="education_id" id="education-id" value="{{ $education->id }}" />
<div class="form-horizontal form-primary">
    
    <div class="form-group">
        <label for="relationship" class="col-xs-12 col-sm-3 control-label">Level</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select name="level" id="level" class="form-control">
                        <option value=""></option>
                        @foreach (getEducationalLevels() as $level)
                            <option value="{{ $level }}"
                                <?php if ($education->level == $level): ?>selected<?php endif; ?>
                            >
                                {{ $level }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="school-name" class="col-xs-12 col-sm-3 control-label">School Name</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="school_name" id="school-name" value="{{ $education->school_name }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-xs-12 col-sm-3 control-label">Address</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="school_address" id="address" value="{{ $education->school_address }}" />
                </div>
            </div>
        </div>
    </div>
    <?php if ($education->level == 'Elementary' || $education->level == 'High School'): ?>
    <div id="cd-field" class="form-group hidden">
    <?php else: ?>
    <div id="cd-field" class="form-group">
    <?php endif; ?>
        <label for="course-degree" class="col-xs-12 col-sm-3 control-label">Course/Degree</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="course_degree" id="course-degree" value="{{ $education->course_degree }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="year-graduated" class="col-xs-12 col-sm-3 control-label">Year Graduated</label>
        <div class="col-xs-12 col-sm-3">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="year_graduated" id="year-graduated" value="{{ $education->year_graduated }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('#level').on('change', function() {
        var cdField = $('#cd-field'),
            cdTextfield = $('#course-degree');
        if ($(this).val() == 'Elementary' || $(this).val() == 'High School') {
            cdField.addClass('hidden');
            cdTextfield.val('');
        } else {
            cdField.removeClass('hidden');
        }
    });

});
</script>