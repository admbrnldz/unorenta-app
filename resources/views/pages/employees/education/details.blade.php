@forelse ($employee->education as $education)

    <li>
        <small>{{ $education->level }}</small>
        <span>
            <strong>
                <a href="{{ route('education.update') }}/{{ $education->id }}" class="edit-education" data-target-form="#update-education-form">
                    {{ $education->school_name }}
                </a>    
            </strong>
            <br />
            {{ $education->school_address }}
            @if ($education->course_degree != '')
                <br /><em>{{ $education->course_degree }}</em>
            @endif
            <br />
            {{ $education->year_graduated }}
        </span>
    </li>

@empty

    <li>
        <small>No records yet</small>
    </li>

@endforelse