<?php
    $entitledLeaves = $employee->entitledLeave()
        ->groupBy('leave_type_id')
        ->select('id', 'leave_type_id')
        ->get();
?>
@foreach ($entitledLeaves as $entitledLeave)
    <li>        
        <span><strong>{{ $entitledLeave->leaveType->name }}</strong></span>
        <small>{{ $employee->checkLeaveCredits($entitledLeave->leaveType->id) }} Credits Remaining</small>
    </li>
@endforeach