<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('employees.update.direct-family') }}/{{ $family->id }}" method="post" autocomplete="off" id="update-family-form">
{!! csrf_field() !!}

<div class="form-horizontal form-primary">
    
    <div class="form-group form-group-last">
        <label for="name" class="col-xs-12 col-sm-3 control-label">{{ ucfirst($family->relationship) }}</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ old('name', $family->name) }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="occupation" id="occupation" placeholder="Occupation" value="{{ old('occupation', $family->occupation) }}" />
                </div>
            </div>
        </div>
    </div>

</div>

</form>