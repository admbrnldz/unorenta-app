@forelse ($employee->families()->orderBy('relationship')->get() as $family)
<li>
    <small>{{ ucfirst($family->relationship) }}</small>
    <span>
        <strong>
            <a href="{{ route('employees.update.direct-family') }}/{{ $family->id }}" 
                class="update-family" data-target="#update-family-form"
                data-delete="{{ route('employees.delete.direct-family') }}/{{ $family->id }}"
            >
                {{ $family->name }}
            </a>
        </strong>
    </span>
</li>
@empty
<li>
    <small>No family member encoded</small>
</li>
@endforelse