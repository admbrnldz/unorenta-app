<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('employees.add.direct-family') }}" method="post" autocomplete="off" id="add-family-form">
{!! csrf_field() !!}
<input type="hidden" name="employee_id" value="{{ $employee->id }}" />

<div class="form-horizontal form-primary">
    
    <div class="form-group form-group-last">
        <div class="col-xs-12">
            <div class="row input-field-wrap">
                <div class="col-xs-12 col-sm-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12 col-sm-6">
                    <input type="text" class="form-control" name="occupation" id="occupation" placeholder="Occupation" value="" />
                </div>
                <div class="col-xs-12 col-sm-6">
                    <select name="relationship" id="" class="form-control">
                        <option value=""></option>
                        @foreach ($opts as $opt)
                            <option value="{{ $opt }}">{{ ucfirst($opt) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

</div>

</form>