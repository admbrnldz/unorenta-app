<form action="{{ route('employees.update.direct-family') }}/{{ $employee->id }}" method="post" autocomplete="off" id="update-direct-family-form">
{!! csrf_field() !!}
<input type="hidden" id="employee-address" value="{{ $employee->address() }}" />
<div class="form-horizontal form-primary">
    
    <div class="form-group">
        <label for="father-name" class="col-xs-12 col-sm-3 control-label">Father</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="father_name" id="father-name" placeholder="Father's Name" value="{{ old('father_name', $employee->father_name) }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="father_occupation" id="father-occupation" placeholder="Father's Occupation" value="{{ old('father_occupation', $employee->father_occupation) }}" />
                </div>
            </div>
        </div>
    </div>

    
    <div class="form-group">
        <label for="mother-name" class="col-xs-12 col-sm-3 control-label">Mother</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="mother_name" id="mother-name" placeholder="Mother's Name" value="{{ old('mother_name', $employee->mother_name) }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="mother_occupation" id="mother-occupation" placeholder="Mother's Occupation" value="{{ old('mother_occupation', $employee->mother_occupation) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="spouse-name" class="col-xs-12 col-sm-3 control-label">Spouse</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="spouse_name" id="spouse-name" placeholder="Spouse' Name" value="{{ old('spouse_name', $employee->spouse_name) }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="spouse_occupation" id="spouseation" placeholder="Spouse' Occupation" value="{{ old('spouse_occupation', $employee->spouse_occupation) }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('#address-same-with-employee').on('change', function() {
        if ($(this).prop('checked')) {
            $('#guardian-address').prop('readonly', true).val($('#employee-address').val());
        } else {
            $('#guardian-address').prop('readonly', false);
        }
    });

});
</script>