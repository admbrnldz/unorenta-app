<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('employees.updaterate') }}/{{ $employee->id }}" method="post" autocomplete="off" id="update-rate-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">
    
    <div class="form-group">
        <label for="basic-income" class="col-xs-12 col-sm-4 control-label">Basic Income</label>
        <div class="col-xs-12 col-sm-4">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="number" class="form-control" name="basic_income" id="basic-income" value="{{ $employee->basic_income }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="monthly-work-days" class="col-xs-12 col-sm-4 control-label">Monthly Work Days</label>
        <div class="col-xs-12 col-sm-4">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="number" class="form-control" name="monthly_work_days" id="monthly-work-days" value="{{ $employee->monthly_work_days }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="erh" class="col-xs-12 col-sm-4 control-label">Estimated Rate/Hr</label>
        <div class="col-xs-12 col-sm-4">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="number" class="form-control" name="erh" id="erh" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    if ($('#basic-income').val() != '' && $('#monthly-work-days').val() != '') {
        var rph = $('#basic-income').val() / ($('#monthly-work-days').val() * 8);
        var rph = rph.toFixed(2);
        $('#erh').val(rph);
    }

    $('#basic-income, #monthly-work-days').on('keyup', function() {
        var rph = $('#basic-income').val() / ($('#monthly-work-days').val() * 8);
        var rph = rph.toFixed(2);
        $('#erh').val(rph);
    });

});
</script>