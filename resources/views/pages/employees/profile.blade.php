@extends('layouts.app')

@section('title', $employee->user->name())

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
{{--*/ $status = '' /*--}}
@if ($employee->currentStatus(true) > 4 || !$employee->user->is_active)
    {{--*/ $status = 'inactive' /*--}}
@endif
@if ($currentUser->can('edit_employee', $currentUser) OR $currentUser->id == $employee->user->id)
<section class="section-header with-menu {{ $status }}">
@else
<section class="section-header {{ $status }}">
@endif
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title clearfix">

                    @include('pages.employees.profile-header', $employee)

                </div>
                @if ($currentUser->can('edit_employee', $currentUser) OR $currentUser->id == $employee->user->id)
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="{{ activeWhen(['employees']) ? 'active' : '' }}">
                            <a href="{{ route('employees') }}">Profile</a>
                        </li>
                        <li class="{{ activeWhen(['departments']) ? 'active' : '' }}">
                            <a href="{{ route('account.settings') }}/{{ $employee->user->id }}">Account Settings</a>
                        </li>
                    </ul>
                </div>
                @endif

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="employee-profile">

    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                @if ($currentUser->can('edit_employee', $currentUser) OR $currentUser->id == $employee->user->id)
                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                @if ($currentUser->id == $employee->user->id AND !$currentUser->allowedTo('edit_employee'))
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-warning">
                            If you wish to update your information, please coordinate with your HR personal.
                        </div>
                    </div>
                </div>
                @endif

                @if ($employee->employement_status == 3)
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-warning">
                            You cannot edit any information of this employee because {{ $employee->sex == 'Female' ? 'she' : 'he' }} is inactive.
                        </div>
                    </div>
                </div>
                @endif
            
                <div id="profile-{{ $employee->id }}" class="profile row">

                    <div class="col-xs-12 col-sm-6 col-md-4">

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Personal Information</h4>
                                @can ('edit_employee', $currentUser)
                                    @if ($employee->currentStatus(true) < 5)
                                        <div class="panel-heading-edit">
                                            <a id="edit-personal-info" title="Edit Personal Information"
                                                href="{{ route('employees.update.personal-info') }}/{{ $employee->id }}"
                                                data-target-form="#update-personal-info-form"
                                                data-action-type="Update"
                                            >
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">

                                    @include('pages.employees.personal-info.details')

                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Direct Family</h4>
                                @can ('edit_employee', $currentUser)
                                    @if ($employee->currentStatus(true) < 5)
                                        <div class="panel-heading-edit">
                                            <a id="add-family" title="Add Family"
                                                href="{{ route('employees.add.direct-family') }}/{{ $employee->id }}"
                                                data-target="#add-family-form"
                                            >
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">

                                    @include('pages.employees.direct-family.details')

                                </ul>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>In-case of Emergency</h4>
                                @can ('edit_employee', $currentUser)
                                    @if ($employee->currentStatus(true) < 5)
                                        <div class="panel-heading-edit">
                                            <a id="edit-emergency" title="Edit Emergency Contact"
                                                href="{{ route('employees.update.emergency') }}/{{ $employee->id }}"
                                                data-target-form="#update-emergency-form"
                                                data-action-type="Update"
                                            >
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">

                                    @include('pages.employees.emergency.details')

                                </ul>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Employment Details</h4>
                                @can ('edit_employee', $currentUser)
                                    <div class="panel-heading-edit">
                                        <a id="update-employment-details" title="Update Employment Details"
                                            href="{{ route('employees.update.employmentdetails') }}/{{ $employee->id }}"
                                            data-target-form="#update-employment-details-form"
                                            data-action-type="Update"
                                        >
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">

                                    @include('pages.employees.employment-details.details')

                                </ul>
                            </div>
                        </div>
                        <div class="panel" id="signature-panel">
                            <div class="panel-heading">
                                <h4>Signature</h4>
                                @if ($employee->signature)
                                    <div class="panel-heading-edit">
                                        <a id="remove-signature" title="Remove Signature"
                                            href="{{ route('employees.removesignature') }}/{{ $employee->id }}"
                                        >
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </div>
                                @endif
                            </div>
                            <div class="panel-body">
                                @if ($employee->currentStatus(true) < 5)
                                    @if ($currentUser->can('edit_employee', $currentUser))
                                        @if ($employee->signature == null)
                                            @if ($employee->user->is_active)
                                                <div class="signature-holder no-signature">
                                                    Click to upload signature
                                                </div>
                                            @else
                                                <div class="signature-holder no-signature">
                                                    No signature
                                                </div>
                                            @endif
                                        @else
                                            <div class="signature-holder" style="background:url({{ $employee->signature() }}) center center no-repeat;background-size:contain;"></div>
                                        @endif
                                    @else
                                        <div class="signature-holder" style="background:url({{ $employee->signature() }}) center center no-repeat;background-size:contain;"></div>
                                    @endif
                                @else
                                    @if ($employee->signature == null)
                                        <div class="signature-holder no-signature">
                                            No signature
                                        </div>
                                    @else
                                        <div class="signature-holder" style="background:url({{ $employee->signature() }}) center center no-repeat;background-size:contain;"></div>
                                    @endif
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Dependents</h4>
                                @can ('edit_employee', $currentUser)
                                    @if ($employee->currentStatus(true) < 5)
                                        <div class="panel-heading-edit">
                                            <a id="add-dependent" title="Add Dependent"
                                                href="{{ route('dependent.add') }}/{{ $employee->id }}"
                                                data-target-form="#add-dependent-form"
                                                data-action-type="Add"
                                            >
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">

                                    @include('pages.employees.dependents.details')

                                </ul>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Educations</h4>
                                @can ('edit_employee', $currentUser)
                                    @if ($employee->currentStatus(true) < 5)
                                        <div class="panel-heading-edit">
                                            <a id="add-education" title="Add Educational Attainment"
                                                href="{{ route('education.add') }}/{{ $employee->id }}"
                                                data-target-form="#add-education-form"
                                                data-action-type="Add"
                                            >
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">

                                    @include('pages.employees.education.details')

                                </ul>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Entitled Leaves</h4>
                                @can ('entitle_leave_applications', $currentUser)
                                    @if ($employee->currentStatus(true) < 5)
                                        <div class="panel-heading-edit">
                                            <a href="{{ route('loa.entitle') }}/{{ $employee->id }}" 
                                                class="entitle-leave" title="Entitle Leave"
                                                data-target-form="#entitle-leave-form"
                                                data-where="profile"
                                            >
                                                <i class="fa fa-plus fa-fw"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">

                                    @include('pages.employees.entitled-leaves.details')

                                </ul>                                
                            </div>
                        </div>

                    </div>

                </div>
                @endif

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#add-family').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target');
        $.get( url ).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {
            
                            var form = $(targetForm),
                                data = form.serialize(),
                                url = form.attr('action');
                            $.post( url, data ).done(function(r) {
                                r = $.parseJSON(r);
                                window.location = r.redirect;
                            }).error(function(r) {
                                var errors = r.responseJSON;
                                var html = '';
                                $.each(errors, function(index, value) {
                                    for (var i = 0; i < value.length; i++) {
                                        html += '<li>' + value[i] + '</li>';
                                    }
                                });
                                $('#modal-error-handler').fadeIn().find('ul').html(html);
                                $('.bootbox').animate({ scrollTop: 0 }, 'slow');
                            });
                            return false;
                            
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });  
        })
    });

    $('#edit-personal-info, #edit-direct-family, #edit-emergency, #add-dependent, #add-education, #add-scholarship, #update-employment-details').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target-form'),
            actionType = $(this).attr('data-action-type');
        $.get({ url: url }).done(function(r) {
            r = $.parseJSON(r);
            var className = r.className;
            if (className == '' || className == null) {
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: actionType,
                            className: 'btn-primary',
                            callback: function() {

                                var form = $(targetForm),
                                    url = form.attr('action'),
                                    data = form.serialize();

                                $.post( url, data ).success(function(r) {
                                    r = $.parseJSON(r);
                                    bootbox.hideAll();
                                    setTimeout(function() {
                                        window.location = r.redirect;
                                    }, 500);                                
                                }).error(function(r) {
                                    var errors = r.responseJSON;
                                    var html = '';
                                    $.each(errors, function(index, value) {
                                        for (var i = 0; i < value.length; i++) {
                                            html += '<li>' + value[i] + '</li>';
                                        }
                                    });
                                    $('#modal-error-handler').fadeIn().find('ul').html(html);
                                    $('.bootbox').animate({ scrollTop: 0 }, 'slow');
                                });
                                return false;

                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            } else {
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    className: r.className,
                    buttons: {
                        'Okay' : {
                            className: 'btn-default'
                        }
                    }
                });
            }
        });
    });

    $('.edit-dependent, .edit-education, .edit-scholarship').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href'),
                targetForm = $(this).attr('data-target-form');
            $.get({ url: url }).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {
                                
                                var form = $(targetForm),
                                    url = form.attr('action'),
                                    data = form.serialize();

                                $.post( url, data ).success(function(r) {
                                    r = $.parseJSON(r);
                                    bootbox.hideAll();
                                    setTimeout(function() {
                                        window.location = r.redirect;
                                    }, 500);                                
                                }).error(function(r) {
                                    var errors = r.responseJSON;
                                    var html = '';
                                    $.each(errors, function(index, value) {
                                        for (var i = 0; i < value.length; i++) {
                                            html += '<li>' + value[i] + '</li>';
                                        }
                                    });
                                    $('#modal-error-handler').fadeIn().find('ul').html(html);
                                });
                                return false;

                            }
                        },
                        'Remove' : {
                            className: 'btn-danger',
                            callback: function() {
                                var deleteUrl = $(targetForm).attr('data-delete');
                                window.location = deleteUrl;
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    // regularize
    $('#employee-regularized').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target');
        $.ajax({
            url: url,
            type: 'GET',
        }).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Regularize',
                        className: 'btn-primary',
                        callback: function() {
                            $(targetForm).submit();
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $('#employee-resign').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        bootbox.dialog({
            title: 'Warning!',
            message: '<div class="ta-center">His/her account will be inactive at the same time.</div>',
            className: 'modal-warning',
            buttons: {
                success: {
                    label: 'Continue',
                    className: 'btn-danger',
                    callback: function() {
                        window.location = url;
                    }
                },
                'Cancel' : {
                    className: 'btn-default'
                }
            }
        });
    });

    $('.signature-holder').on('click', function(e) {
        e.preventDefault();
        $('#signature-file').click();
    });

    var id = '#remove-signature';
    $(document).on('click', id, function(e) {
        e.preventDefault();
        var url = $(id).attr('href');
        $.get( url ).done(function(r) {
            r = $.parseJSON(r);
            if (r.done) {
                var html = 'Click to upload signature';
                $('.signature-holder').addClass('no-signature').removeAttr('style').append(html);
                $(id).remove();
            }
        });
    });

});
</script>
@endsection