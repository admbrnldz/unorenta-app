<li>
    <small>Address</small>
    <span>
        @if ($employee->address != null)
            {{ $employee->address() }}
        @else
            -
        @endif
    </span>
</li>
@if ($currentUser->can('edit_employee', $currentUser) OR $currentUser->id == $employee->user->id)
<li>
    <small>Birthdate</small>
    <span>{{ $employee->birthdate  == null ? '-' : fullDate($employee->birthdate) }}</span>
</li>
<li>
    <small>Birthplace</small>
    <span>{{ $employee->birthplace }}</span>
</li>
<li>
    <small>Civil Status</small>
    <span>{{ $employee->civil_status }}</span>
</li>
<li>
    <small>Citizenship</small>
    <span>{{ $employee->citizenship }}</span>
</li>

<li>
    <small>Sex</small>
    <span>{{ $employee->sex }}</span>
</li>
<li>
    <small>Religion</small>
    <span>{{ $employee->religion }}</span>
</li>
<li>
    <small>Height</small>
    <span>{{ $employee->height }}</span>
</li>
<li>
    <small>Weight</small>
    <span>{{ $employee->weight }}</span>
</li>
<li>
    <small>Built</small>
    <span>{{ $employee->built }}</span>
</li>
<li>
    <small>Eyes</small>
    <span>{{ $employee->eyes }}</span>
</li>
<li>
    <small>Blood Type</small>
    <span>{{ $employee->blood_type }}</span>
</li>

<li>
    <small>SSS</small>
    <span>{{ $employee->sss }}</span>
</li>
<li>
    <small>TIN</small>
    <span>{{ $employee->tin }}</span>
</li>
<li>
    <small>PhilHealth</small>
    <span>{{ $employee->philhealth }}</span>
</li>
<li>
    <small>Pag-ibig</small>
    <span>{{ $employee->pagibig }}</span>
</li>
@endif