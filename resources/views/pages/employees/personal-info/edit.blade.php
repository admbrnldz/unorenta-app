<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('employees.update.personal-info') }}/{{ $employee->id }}" method="post" autocomplete="off" id="update-personal-info-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="position" class="col-xs-12 col-sm-3 control-label">Position</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select name="position_id" id="position" class="form-control">
                        <option value="">Select a Position</option>
                        @foreach ($positions as $position)
                        <option value="{{ $position->id }}"
                            <?php if ($position->id == $employee->position_id): ?>selected<?php endif; ?>
                        >
                            {{ $position->name }} &mdash; {{ $position->department->name }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="mobile-number" class="col-xs-12 col-sm-3 control-label">Contact Number</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="mobile_number" id="mobile-number" value="{{ old('mobile_number', $employee->mobile_number) }}" placeholder="Mobile Number" />
                </div>
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="home_number" id="home-number" value="{{ old('home_number', $employee->home_number) }}" placeholder="Home Number" />
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="email-address" class="col-xs-12 col-sm-3 control-label">Email Address</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="email_address" id="email-address" value="{{ old('email_address', $employee->email_address) }}" />
                </div>
            </div>
        </div>
    </div>

    <div class="form-group form-group-separator"><span></span></div>
    
    <div class="form-group">
        <label for="nickname" class="col-xs-12 col-sm-3 control-label">Nickname</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="nickname" id="nickname" value="{{ old('nickname', $employee->nickname) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-xs-12 col-sm-3 control-label">Address</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="address" id="address" placeholder="Street, Village, Barangay" value="{{ old('address', $employee->address) }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ old('city', $employee->city) }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-8">
                    <input type="text" class="form-control" name="province" id="province" placeholder="Province" value="{{ old('province', $employee->province) }}" />
                </div>
                <div class="col-xs-4">
                    <input type="text" class="form-control" name="zip_code" id="zip-code" placeholder="Zip Code" value="{{ old('zip_code', $employee->zip_code) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="contact-number" class="col-xs-12 col-sm-3 control-label">Birthdate</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control datepicker" name="birthdate" id="birthdate" value="{{ old('birthdate', formatDate($employee->birthdate, true)) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="birthplace" class="col-xs-12 col-sm-3 control-label">Birthplace</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="birthplace" id="birthplace" value="{{ old('birthplace', $employee->birthplace) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="civil-status" class="col-xs-12 col-sm-3 control-label">Civil Status</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    @foreach (getCivilStatus() as $civilStatus)
                    <div class="radio">
                        <label>
                            <input type="radio" name="civil_status" value="{{ $civilStatus }}" 
                                <?php if ($employee->civil_status == $civilStatus): ?>checked<?php endif; ?>
                            />
                            <span>{{ $civilStatus }}</span>
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="citizenship" class="col-xs-12 col-sm-3 control-label">Citizenship</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" class="citizenship" name="citizenship" value="Filipino" 
                                <?php if ($employee->citizenship == 'Filipino'): ?>checked<?php endif; ?>
                            />
                            <span>Filipino</span>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" class="citizenship" name="citizenship" value="" id="citizenship-other" 
                                <?php if ($employee->citizenship != 'Filipino'): ?>checked<?php endif; ?>
                            />
                            <span>Other</span>
                        </label>
                    </div>                                     
                </div>
            </div>            
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-3 control-label">&nbsp;</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="citizenship" id="citizenship-text-field" 
                        value="<?php if ($employee->citizenship != 'Filipino'): ?>{{ old('citizenship', $employee->citizenship) }}<?php endif; ?>" 
                        <?php if ($employee->citizenship == 'Filipino'): ?>disabled<?php endif; ?> 
                        placeholder="If other" 
                    />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-12 col-sm-3 control-label">Sex</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="sex" value="Male" 
                                <?php if ($employee->sex == 'Male'): ?>checked<?php endif; ?>
                            />
                            <span>Male</span>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="sex" value="Female" 
                                <?php if ($employee->sex == 'Female'): ?>checked<?php endif; ?>
                            />
                            <span>Female</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="religion" class="col-xs-12 col-sm-3 control-label">Religion</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="religion" id="religion" value="{{ old('religion', $employee->religion) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="height" class="col-xs-12 col-sm-3 control-label">Height/Weight</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-6">
                    <input type="number" class="form-control" name="height" id="height" value="{{ old('height', $employee->height) }}" />
                </div>
                <div class="col-xs-6">
                    <input type="number" class="form-control" name="weight" id="weight" value="{{ old('weight', $employee->weight) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="build" class="col-xs-12 col-sm-3 control-label">Build</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="build" id="build" value="{{ old('build', $employee->build) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="eyes" class="col-xs-12 col-sm-3 control-label">Eyes</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="eyes" id="eyes" value="{{ old('eyes', $employee->eyes) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="blood-type" class="col-xs-12 col-sm-3 control-label">Blood Type</label>
        <div class="col-xs-12 col-sm-3">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="blood_type" id="blood-type" value="{{ old('blood_type', $employee->blood_type) }}" />
                </div>
            </div>
        </div>
    </div>

    <div class="form-group form-group-separator"><span></span></div>

    <div class="form-group">
        <label for="sss" class="col-xs-12 col-sm-3 control-label">SSS Number</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="sss" id="sss" value="{{ old('sss', $employee->sss) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="tin" class="col-xs-12 col-sm-3 control-label">Tax ID Number</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="tin" id="tin" value="{{ old('tin', $employee->tin) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="philhealth" class="col-xs-12 col-sm-3 control-label">PhilHealth</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="philhealth" id="philhealth" value="{{ old('philhealth', $employee->philhealth) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="pagibig" class="col-xs-12 col-sm-3 control-label">Pag-ibig</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="pagibig" id="pagibig" value="{{ old('pagibig', $employee->pagibig) }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('#mobile-number').mask('+63 999 999 9999');
    $('#home-number').mask('+63 99 999 9999');
    $('#sss').mask('99-9999999-9');
    $('#tin').mask('999-999-999-999');
    $('#philhealth').mask('99-999999999-9');
    $('#pagibig').mask('9999-9999-9999');

    $('#birthdate').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '{{ $today->year - 40}}:{{ $today->year - 18}}',
        dateFormat: 'MM d, yy'
    });

    $('.citizenship').each(function() {
        $(this).on('change', function() {
            if ($(this).attr('value') != 'Filipino') {
                $('#citizenship-text-field').prop('disabled', false);
            } else {

                $('#citizenship-text-field').val('').prop('disabled', true);
            }
        });
    });

});
</script>