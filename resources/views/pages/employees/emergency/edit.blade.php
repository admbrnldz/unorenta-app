<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('employees.update.emergency') }}/{{ $employee->id }}" method="post" autocomplete="off" id="update-emergency-form">
{!! csrf_field() !!}
<input type="hidden" id="employee-address" value="{{ $employee->address() }}" />
<div class="form-horizontal form-primary">
    
    <div class="form-group">
        <label for="father-name" class="col-xs-12 col-sm-3 control-label">Contact Person</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <?php $matched = false; ?>
                    @foreach ($employee->families as $family)
                        <div class="radio">
                            <label>
                                <input type="radio" class="cperson-opt" name="contact_person" value="{{ $family->name }}" 
                                    <?php if ($employee->emergency->name == $family->name): ?>
                                        checked
                                        <?php $matched = true; ?>
                                    <?php endif; ?>
                                />
                                <span>{{ $family->name }} <em>({{ ucfirst($family->relationship) }})</em></span>
                            </label>
                        </div>
                    @endforeach
                    <div class="radio">
                            <label>
                                <input type="radio" class="cperson-opt" name="contact_person" value="" 
                                    <?php if (!$matched): ?>
                                        checked
                                    <?php endif; ?>
                                />
                                <span>Other Person</span>
                            </label>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="contact-person" class="col-xs-12 col-sm-3 control-label">&nbsp;</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="contact_person" id="contact-person" placeholder="If other person" 
                        <?php if ($matched): ?>disabled<?php else: ?>value="{{ $employee->emergency->name }}"<?php endif; ?>
                    />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="contact-number" class="col-xs-12 col-sm-3 control-label">Contact Number</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="cp_number" id="contact-number" placeholder="Contact Number" value="{{ old('cp_number', $employee->emergency->contact_number) }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="cp-address" class="col-xs-12 col-sm-3 control-label">Address</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="cp_address" id="cp-address"
                        value="{{ old('cp_address', $employee->emergency->address) }}" 
                        <?php if ($employee->address() == $employee->emergency->address): ?>readonly<?php endif; ?>
                        placeholder="Contact Person's Address" 
                    />
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 col-sm-offset-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="address-same-with-employee" 
                        <?php if ($employee->address() == $employee->emergency->address): ?>checked<?php endif; ?>
                    />
                    <span>Same address with employee</span>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="language-spoken" class="col-xs-12 col-sm-3 control-label">Language Spoken</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="cp_language_spoken" id="language-spoken" placeholder="Language Spoken" value="{{ old('cp_language_spoken', $employee->emergency->language_spoken) }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('#contact-number').mask('+63 999 999 9999');

    var prevContactPerson = "{{ $employee->contact_person }}";

    $('.cperson-opt').each(function() {
        $(this).on('change', function() {
            if ($(this).val() == '') {
                $('#contact-person').prop('disabled', false).val(prevContactPerson);
            } else {
                $('#contact-person').val('').prop('disabled', true);
            }
        });
    });

    $('#address-same-with-employee').on('change', function() {
        if ($(this).prop('checked')) {
            $('#cp-address').prop('readonly', true).val($('#employee-address').val());
        } else {
            $('#cp-address').prop('readonly', false).val('');
        }
    });

});
</script>