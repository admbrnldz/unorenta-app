<li>
    <small>Person to Contact</small>
    <span>
        @if ($employee->emergency)
            <strong>{{ $employee->emergency->name }}</strong><br />
            {{ $employee->emergency->contact_number }}<br />
            @if ($employee->emergency->address AND $employee->emergency->address != '')
                {{ $employee->emergency->address }}<br />
            @endif
            {{ $employee->emergency->languange_spoken }}
        @else
            -
        @endif
    </span>
</li>