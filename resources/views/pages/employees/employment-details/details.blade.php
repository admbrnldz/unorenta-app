<li>
    <small>Date Hired</small>
    <span>{{ fullDate($employee->date_hired) }}</span>
</li>

@foreach ($employee->statuses as $es)
    <li>
        <small>{{ getEmploymentStatus()[$es->employment_status] }}</small>
        <span>{{ fullDate($es->updated_at) }}</span>
    </li>
@endforeach