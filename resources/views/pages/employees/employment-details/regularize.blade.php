<form action="{{ route('employees.regularize') }}/{{ $employee->id }}" method="post" autocomplete="off" id="regularize-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">
    
    <div class="form-group hidden-xs">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="row input-field-wrap">
                <div class="col-xs-6"><span class="label label-default">Accounting Year</span></div>
                <div class="col-xs-6"><span class="label label-default">Credits</span></div>
            </div>
        </div>
    </div>
    
    {{--*/ $count = count($leaveTypes) /*--}}
    {{--*/ $lcount = 1 /*--}}
    @foreach ($leaveTypes as $leaveType)
    @if ($count == $lcount)
    <div class="form-group form-group-last">
    @else
    <div class="form-group">
    @endif
        <label for="leave-{{ $leaveType->id }}" class="col-xs-12 col-sm-3 control-label">{{ $leaveType->name }}</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="leave[{{ $leaveType->id }}][year]" placeholder="" value="{{ $currentYear }}" />
                </div>
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="leave[{{ $leaveType->id }}][credits]" id="leave-{{ $leaveType->id }}" placeholder="" 
                        <?php if ($leaveType->name == 'Sick Leave' || $leaveType->name == 'Vacation Leave'): ?>
                            value="15"
                        <?php endif; ?>
                    />
                </div>
            </div>
        </div>
    </div>
    {{--*/ $lcount++ /*--}}
    @endforeach

</div>
</form>
<script>
$(function() {

    

});
</script>