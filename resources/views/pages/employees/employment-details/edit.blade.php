<form action="{{ route('employees.update.employmentdetails') }}/{{ $employee->id }}" method="post" autocomplete="off" id="update-employment-details-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group form-group-last">
        <label for="status" class="col-xs-12 col-sm-4 control-label">Employment Status</label>
        <div class="col-xs-12 col-sm-4">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select name="status" id="status" class="form-control">
                        <option value=""></option>
                        @foreach ($employmentStatuses as $id => $es)
                            <option value="{{ $id }}">{{ $es }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-40:+0',
        dateFormat: 'MM d, yy'
    });

});
</script>