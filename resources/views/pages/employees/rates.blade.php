@extends('layouts.app')

@section('title', 'Employees')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Employees</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('employees') }}">Employees</a></li>
                        <li><a href="{{ route('departments') }}">Departments &amp; Positions</a></li>
                        @if ($currentUser->allowedTo('edit_employee_rate'))
                            <li class="active"><a href="{{ route('employees.rates') }}">Employees Rates</a></li>
                        @endif
                    </ul>
                </div>
                
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employees</th>
                                                <th>ID #</th>
                                                <th>Position</th>
                                                <th class="ta-right">Basic Income</th>
                                                <th class="ta-right">Monthly Work Days</th>
                                                <th class="ta-right">Rate Per Hour</th>
                                                @if ($currentUser->allowedTo('edit_employee_rate'))
                                                    <th></th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employees as $employee)
                                                <tr id="employee-{{ $employee->id }}"
                                                    <?php if (!$employee->user->is_active): ?>
                                                        class="inactive"
                                                    <?php endif; ?>
                                                >
                                                    <td class="table-dp-td">
                                                        <div class="dp-handler" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;">
                                                            <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}"></a>
                                                        </div>
                                                    </td>
                                                    <td class="table-highlight">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">
                                                            {{ $employee->name(true) }}
                                                        </a>
                                                    </td>
                                                    <td>{{ $employee->employee_uid }}</td>
                                                    <td>{{ $employee->position->name }}</td>
                                                    <td class="ta-right table-highlight">
                                                        <a href="{{ route('employees.updaterate') }}/{{ $employee->id }}" class="update-rate">
                                                            P {{ number_format($employee->basic_income, 2, '.', ',') }}
                                                        </a>
                                                    </td>
                                                    <td class="ta-right">{{ $employee->monthly_work_days }}</td>
                                                    <td class="ta-right">
                                                        {{--*/ $ratePerHour = 0 /*--}}
                                                        @if ($employee->basic_income > 0)
                                                            {{--*/ $ratePerHour = $employee->basic_income / ($employee->monthly_work_days * 8) /*--}}
                                                        @endif
                                                        P {{ number_format(round($ratePerHour, 2), 2, '.', ',') }}
                                                    </td>
                                                    @if ($currentUser->allowedTo('edit_employee_rate'))
                                                        <td class="ta-right">
                                                            <a href="{{ route('employees.updaterate') }}/{{ $employee->id }}" class="update-rate">
                                                                <i class="fa fa-pencil fa-fw"></i>
                                                            </a>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="row human-listing hidden">
                    @foreach ($employees as $employee)
                        {{--*/ $status = '' /*--}}
                        @if (!$employee->user->is_active)
                            {{--*/ $status = 'inactive' /*--}}
                        @endif
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="human-entry navigatable clearfix {{ $status }}" 
                                data-url="{{ route('employees.profile', ['employeeId' => $employee->id]) }}"
                            >
                                <div class="human-photo">
                                    <div class="human-photo-frame" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;"></div>
                                </div>
                                <div class="human-info">
                                    <h4>{{ $employee->name(true) }}</h4>
                                    <span class="human-meta">{{ $employee->position->name }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="human-entry human-entry-add" data-url="{{ route('employees.add') }}">
                            <a href="{{ route('employees.add') }}">Add Employee</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@can ('add_employee', $currentUser)
<div class="page-actions">
    <ul class="clearfix">
        <li>
            <a href="{{ route('employees.add') }}" id="menu-78" data-tooltip-msg="Add Employee" class="pa-primary">
                <i class="fa fa-plus"></i>
            </a>
        </li>
    </ul>
</div>
@endcan
@endsection

@section('footer-addon')
<script>
$(function() {

    $('.update-rate').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.get( url ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {
                
                                var url = $('#update-rate-form').attr('action'),
                                    data = $('#update-rate-form').serialize();
                                $.post( url, data ).done(function(r) {
                                    r = $.parseJSON(r);
                                    bootbox.hideAll();
                                    setTimeout(function() {
                                        window.location = r.redirect;
                                    }, 500);
                                }).error(function(r) {
                                    var errors = r.responseJSON;
                                    var html = '';
                                    $.each(errors, function(index, value) {
                                        html += '<li>' + value[0] + '</li>';
                                    });
                                    $('#modal-error-handler').fadeIn().find('ul').html(html);
                                });
                                return false;
                                
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });  
            })
        });
    });

});
</script>
@endsection