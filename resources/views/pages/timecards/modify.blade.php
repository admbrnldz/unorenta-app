<?php

    $timeIn = explode(':', \Carbon\Carbon::parse($timecard->time_in)->toTimeString());
    $timeOut = explode(':', \Carbon\Carbon::parse($timecard->time_out)->toTimeString());

?>
<form action="{{ route('timecards.modify') }}/{{ $timecard->id }}" method="post" autocomplete="off" id="modify-timecard-form">
{!! csrf_field() !!}

    <div class="form-horizontal form-primary">

        <div class="form-group">
            <label for="ti-hour" class="col-xs-12 col-sm-3 control-label">Time In</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-4">
                        <input type="number" min="1" max="12" name="ti_hour" id="ti-hour" class="form-control" value="{{ $timeIn['0'] > 12 ? $timeIn['0'] - 12 : $timeIn['0'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <input type="number" min="1" max="60" name="ti_minute" class="form-control" value="{{ $timeIn['1'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <select name="time_cycle_ti" id="time-cycle" class="form-control">
                            <option value="AM">AM</option>
                            <option value="PM" <?php if ($timeIn['0'] > 12) { echo 'selected'; } ?> >PM</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        @if ($timecard->time_out == null)
        <div class="form-group form-group-last">
        @else
        <div class="form-group">
        @endif
            <label for="shift-from-date" class="col-xs-12 col-sm-3 control-label">Late</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-4">
                        <input type="number" min="1" max="12" name="late_minutes" class="form-control" value="{{ $timecard->late_minutes }}" readonly />
                    </div>
                </div>
            </div>
        </div>

        @if ($timecard->time_out != null)
        <div class="form-group">
            <label for="to-hour" class="col-xs-12 col-sm-3 control-label">Time Out</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-4">
                        <input type="number" min="1" max="12" name="to_hour" id="to-hour" class="form-control" value="{{ $timeOut['0'] > 12 ? $timeOut['0'] - 12 : $timeOut['0'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <input type="number" min="1" max="60" name="to_minute" id="to-minute" class="form-control" value="{{ $timeOut['1'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <select name="time_cycle_to" id="time-cycle" class="form-control">
                            <option value="AM">AM</option>
                            <option value="PM" <?php if ($timeOut['0'] > 12) { echo 'selected'; } ?> >PM</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last">
            <label for="shift-from-date" class="col-xs-12 col-sm-3 control-label">Undertime</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-4">
                        <input type="number" min="1" max="12" name="undetime_minutes" class="form-control" value="{{ $timecard->undertime_minutes }}" readonly />
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>

</form>