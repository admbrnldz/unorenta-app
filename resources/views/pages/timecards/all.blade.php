@extends('layouts.app')

@section('title', 'For Approval Timecards')

@section('head-addon')
<style>
.modify-timecard, .shift-modify { cursor: pointer; }
</style>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>All Timecards</h3>
                </div>                
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('timecards.approval') }}">Monitoring</a></li>
                        <li class="active"><a href="{{ route('timecards.all') }}">All Timecards</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="timecards">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">

                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th>Time In</th>
                                                <th>Late</th>
                                                <th>Time Out</th>
                                                <th>Undertime</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($timecards as $timecard)
                                            <tr>
                                                <td class="table-dp-td">
                                                    <div class="dp-handler" style="background:url({{ $timecard->employee->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $timecard->employee->id]) }}"></a>
                                                    </div>
                                                </td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $timecard->employee->id]) }}">
                                                        {{ $timecard->employee->name() }}
                                                    </a>
                                                </td>
                                                @if ($timecard->is_absent)
                                                    <td colspan="4" class="ta-left">
                                                        <strong>Absent:</strong> <em>{{ $timecard->absent_reason }}</em>
                                                    </td>
                                                @else
                                                    <td>{{ processDate($timecard->time_in, true) }}</td>
                                                    <td>{{ $timecard->late_minutes }}</td>
                                                    <td>{{ processDate($timecard->time_out, true) }}</td>
                                                    <td>{{ $timecard->undertime_minutes }}</td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection