<?php

    $timeIn = explode(':', \Carbon\Carbon::parse($timecard->time_in)->toTimeString());

?>
<form action="{{ route('timecards.modifytimein') }}/{{ $timecard->id }}" method="post" autocomplete="off" id="modify-timein-form">
{!! csrf_field() !!}

    <div class="form-horizontal form-primary">

        @if ($lateMinutes > 10)
        <div class="form-group">
        @else
        <div class="form-group form-group-last">
        @endif
            <label for="ti-hour" class="col-xs-12 col-sm-3 control-label">Time In</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-4">
                        <input type="number" min="1" max="12" name="ti_hour" id="ti-hour" class="form-control" value="{{ $timeIn['0'] > 12 ? $timeIn['0'] - 12 : $timeIn['0'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <input type="number" min="1" max="60" name="ti_minute" id="ti-minute" class="form-control" value="{{ $timeIn['1'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <select name="time_cycle_ti" id="time-cycle" class="form-control">
                            <option value="AM">AM</option>
                            <option value="PM" <?php if ($timeIn['0'] > 12) { echo 'selected'; } ?> >PM</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        @if ($lateMinutes > 10)
        <div class="form-group">
            <label class="col-xs-12 col-sm-3 control-label">Late</label>
            <div class="col-xs-12 col-sm-3">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="late_minutes" value="{{ $lateMinutes }}" readonly />
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group form-group-last">
            <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="form-control" name="confirm_late" value="1" />
                                <span>Confirm late</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>

</form>