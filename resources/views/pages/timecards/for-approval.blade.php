@extends('layouts.app')

@section('title', 'For Approval Timecards')

@section('head-addon')
<style>
.modify-timecard, .shift-modify { cursor: pointer; }
</style>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Timecard Monitoring</h3>
                </div>                
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active"><a href="{{ route('timecards.approval') }}">Monitoring</a></li>
                        <li><a href="{{ route('timecards.all') }}">All Timecards</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="timecards">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Employees' Shift</h4>
                            </div>
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <table class="table" id="es-table">
                                        <thead>
                                            <th colspan="2">Employee</th>
                                            <th class="ta-right">Status</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($employeesShifts as $eShift)
                                            {{--*/ $employee = $eShift['employee'] /*--}}
                                            {{--*/ $shift = $eShift['shift'] /*--}}
                                            {{--*/ $nextShift = $eShift['nextShift'] /*--}}
                                            <tr id="es-{{ $employee->id }}">
                                                <td class="table-dp-td">
                                                    <div class="dp-handler" style="background:url({{ $employee->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}"></a>
                                                    </div>
                                                </td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">
                                                        {{ $employee->nickname }}
                                                    </a>
                                                </td>
                                                <td class="ta-right">
                                                    @if ($shift != null)
                                                        @if ($shift->timecard)
                                                            @if ($shift->timecard->is_absent)
                                                                <span class="label label-danger" title="{{ $shift->timecard->absent_reason }}">Absent</span>
                                                            @else
                                                                @if ($shift->timecard->time_out != null)
                                                                    <span class="label label-success modify-timecard"
                                                                        data-modify="{{ route('timecards.modify') }}/{{ $shift->timecard->id }}"
                                                                    >
                                                                        Time out: {{ processTime($shift->timecard->time_out) }}
                                                                    </span>
                                                                @else
                                                                    <span class="label label-success modify-timecard"
                                                                        data-modify="{{ route('timecards.modify') }}/{{ $shift->timecard->id }}"
                                                                    >
                                                                        Time in: {{ processTime($shift->timecard->time_in) }}
                                                                    </span>
                                                                @endif
                                                            @endif                                                            
                                                        @else
                                                            @if ($nextShift)
                                                                <span class="label label-warning" title="<?php if ($nextShift): ?>Next shift<?php endif; ?>">
                                                                    {{ processDate($shift->shift_from, true) }}
                                                                </span>
                                                            @else
                                                                <span class="label label-primary shift-modify" title="View Details"
                                                                    data-url="{{ route('shifts.details') }}/{{ $shift->id  }}"
                                                                    data-absent="{{ route('timecards.absent') }}/{{ $shift->id }}"
                                                                >
                                                                    {{ processDate($shift->shift_from, true) }}
                                                                </span>
                                                            @endif                                                            
                                                        @endif
                                                    @else
                                                        <span class="label label-default">RD</span>
                                                    @endif                                                    
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Time in</h4>
                            </div>
                            <div class="panel-body" style="overflow:hidden;">

                                <div class="table-responsive">
                                    <table class="table" id="ti-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th>Time In</th>
                                                <th class="ta-right">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($forApprovalTimeIn as $ti)
                                            <tr id="ti-{{ $ti->id }}">
                                                <td class="table-dp-td">
                                                    <div class="dp-handler" style="background:url({{ $ti->employee->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $ti->employee->id]) }}"></a>
                                                    </div>
                                                </td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $ti->employee->id]) }}">
                                                        {{ $ti->employee->nickname }}
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ processTime($ti->time_in) }}
                                                </td>
                                                <td class="ta-right">
                                                    <div class="table-actions clearfix btn-group">
                                                        <a href="{{ route('timecards.approvetimein') }}/{{ $ti->id }}" 
                                                            id="approve-ti" title="Click to approve" class="btn btn-sm btn-primary" 
                                                            data-timecard-id="{{ $ti->id }}"
                                                        >
                                                            Approve
                                                        </a>
                                                        <button class="btn btn-sm btn-warning btn-squared modify-ti" 
                                                            data-url="{{ route('timecards.modifytimein') }}/{{ $ti->id }}"
                                                            data-reject="{{ route('timecards.rejecttimein') }}/{{ $ti->id }}"
                                                            data-form="#modify-timein-form"
                                                        >
                                                            <i class="fa fa-pencil fa-fw"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="4" class="ta-center">No timecards</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Time out</h4>
                            </div>
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <table class="table" id="to-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th>Time In</th>
                                                <th class="ta-right">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($forApprovalTimeOut as $to)
                                            <tr id="to-{{ $to->id }}">
                                                <td class="table-dp-td">
                                                    <div class="dp-handler" style="background:url({{ $to->employee->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $to->employee->id]) }}"></a>
                                                    </div>
                                                </td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $to->employee->id]) }}">
                                                        {{ $to->employee->nickname }}
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ processTime($to->time_in) }}
                                                </td>
                                                <td class="ta-right">
                                                    <div class="table-actions clearfix btn-group">
                                                        <a href="{{ route('timecards.approvetimeout') }}/{{ $to->id }}" 
                                                            id="approve-to" title="Click to approve" class="btn btn-sm btn-primary" 
                                                            data-timecard-id="{{ $to->id }}"
                                                        >
                                                            Approve
                                                        </a>
                                                        <button class="btn btn-sm btn-warning btn-squared modify-to" 
                                                            data-url="{{ route('timecards.modifytimeout') }}/{{ $to->id }}"
                                                            data-reject="{{ route('timecards.rejecttimeout') }}/{{ $to->id }}"
                                                            data-form="#modify-timeout-form"
                                                        >
                                                            <i class="fa fa-pencil fa-fw"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="4" class="ta-center">No timecards</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#approve-ti, #approve-to').on('click', function(e) {
        e.preventDefault();
        loader.show();
        var url = $(this).attr('href'),
            timecardId = $(this).attr('data-timecard-id'),
            data = {
                'id': timecardId
            };

        $.post( url, data ).done(function(response) {
            response = $.parseJSON(response);
            console.log(response.timeDisplay);
            if (response.type == 'timeIn') {
                $('#timecard-ti-' + response.timecard.id).find('.fa').attr('class', 'fa fa-check fa-fw fc-primary');
                $('#timecard-late-' + response.timecard.id).html(response.timecard.late_minutes)                

                var tr = $('#ti-' + timecardId),
                    dp = tr.find('.table-dp-td').html(),
                    e = tr.find('.table-highlight').html(),
                    tdDp = '<td class="table-dp-td">' + dp + '</td>',
                    tdE = '<td class="table-highlight">' + e + '</td>';

                $('#es-table tbody').append('<tr>' + tdDp + tdE + '<td class="ta-right"><span class="label label-success">Time in: ' + response.timeDisplay + '</span></td></tr>');

                tr.remove();
                var trCount = $('#ti-table').find('tbody tr').length;
                if (trCount == 0) {
                    $('#ti-table').find('tbody').append('<tr><td colspan="4" class="ta-center">No timecards</td></tr>');
                }
            } else if (response.type == 'timeOut') {
                $('#timecard-to-' + response.timecard.id).find('.fa').attr('class', 'fa fa-check fa-fw fc-primary');
                $('#timecard-undertime-' + response.timecard.id).html(response.timecard.undertime_minutes);
                var tr = $('#to-' + timecardId),
                    dp = tr.find('.table-dp-td').html(),
                    e = tr.find('.table-highlight').html(),
                    tdDp = '<td class="table-dp-td">' + dp + '</td>',
                    tdE = '<td class="table-highlight">' + e + '</td>';

                $('#es-table tbody').append('<tr>' + tdDp + tdE + '<td class="ta-right"><span class="label label-success">Time out: ' + response.timeDisplay + '</span></td></tr>');
                    
                tr.remove();
                var trCount = $('#to-table').find('tbody tr').length;
                if (trCount == 0) {
                    $('#to-table').find('tbody').append('<tr><td colspan="4" class="ta-center">No timecards</td></tr>');
                }
            }
        });
        loader.hide();
    });

    $('#approve-timecard').on('click', function(e) {
        e.preventDefault();

        var data = { 
                'id': $(this).attr('data-timecard-id')
            },
            url = $(this).attr('href');

        $.post({
            url: url,
            data: data
        }).done(function(response) {
            response = $.parseJSON(response);
            if (response.type == 'timeIn') {
                $('#timein-record-' + response.timecard.id).attr('class', 'table-approved');
            } else if (response.type == 'timeOut') {
                $('#undertime-record-' + response.timecard.id).html(response.timecard.undertime_minutes + 'mins');
            }
        });
    });

    $('.shift-modify').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('data-url'),
                absent = $(this).attr('data-absent');
            $.get( url ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    closeButton: true,
                    buttons: {
                        'Absent' : {
                            className: 'btn-danger',
                            callback: function() {
                                this.modal('hide');
                                setTimeout(function() {

                                    $.get( absent ).done(function(r) {
                                        r = $.parseJSON(r);
                                        bootbox.dialog({
                                            title: r.title,
                                            message: r.view,
                                            className: 'modal-warning',
                                            buttons: {
                                                success: {
                                                    label: 'Absent',
                                                    className: 'btn-danger',
                                                    callback: function() {
                                                        var form = $('#absent-form'),
                                                            data = form.serialize(),
                                                            url = form.attr('action');
                                                        $.post( url, data ).done(function(r) {
                                                            r = $.parseJSON(r);
                                                            var target = $('#es-' + r.employeeId).find('span.label');
                                                            target.html('Absent');
                                                            target.attr('class', 'label label-danger');
                                                        });
                                                    }
                                                },
                                                'Cancel' : {
                                                    className: 'btn-default'
                                                }
                                            }
                                        });
                                    });
                                    
                                }, 500);
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    $('.modify-timecard').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('data-modify');
            $.get( url ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    closeButton: true,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {
                                
                                $('#modify-timecard-form').submit();

                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    $('.modify-to, .modify-ti').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('data-url'),
                form = $(this).attr('data-form'),
                reject = $(this).attr('data-reject');
            $.get( url ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    closeButton: true,
                    buttons: {
                        'Reject' : {
                            className: 'btn-danger',
                            callback: function() {
                                setTimeout(function() {
                                    bootbox.dialog({
                                        title: 'Hey!',
                                        message: '<div class="ta-center">Are you sure you want to reject this timecard?</div>',
                                        closeButton: true,
                                        className: 'modal-warning',
                                        buttons: {
                                            success: {
                                                label: 'Yes, Reject',
                                                className: 'btn-danger',
                                                callback: function() {
                                                    window.location = reject;
                                                }
                                            },
                                            'Cancel' : {
                                                className: 'btn-default'
                                            }
                                        }
                                    });
                                }, 500);                                
                            }
                        },
                        success: {
                            label: 'Submit',
                            className: 'btn-primary',
                            callback: function() {                           
                                $(form).submit();
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

});
</script>
@endsection