<?php

    $timeOut = explode(':', \Carbon\Carbon::parse($timecard->time_out)->toTimeString());

?>
<form action="{{ route('timecards.modifytimeout') }}/{{ $timecard->id }}" method="post" autocomplete="off" id="modify-timeout-form">
{!! csrf_field() !!}

    <div class="form-horizontal form-primary">

        @if ($overtimeMinutes > 60)
        <div class="form-group">
        @else
        <div class="form-group form-group-last">
        @endif
            <label for="to-hour" class="col-xs-12 col-sm-3 control-label">Time Out</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-4">
                        <input type="number" min="1" max="12" name="to_hour" id="to-hour" class="form-control" value="{{ $timeOut['0'] > 12 ? $timeOut['0'] - 12 : $timeOut['0'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <input type="number" min="1" max="60" name="to_minute" id="to-minute" class="form-control" value="{{ $timeOut['1'] }}" />
                    </div>
                    <div class="col-xs-4">
                        <select name="time_cycle_to" id="time-cycle" class="form-control">
                            <option value="AM">AM</option>
                            <option value="PM" <?php if ($timeOut['0'] > 12) { echo 'selected'; } ?> >PM</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        @if ($overtimeMinutes > 60)
        <div class="form-group">
            <label class="col-xs-12 col-sm-3 control-label">Overtime</label>
            <div class="col-xs-12 col-sm-3">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="overtime_minutes" value="{{ $overtimeMinutes }}" readonly />
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group form-group-last">
            <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="form-control" name="approve_overtime" value="1" />
                                <span>Approve overtime</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>

</form>