<div class="ta-center" style="margin-bottom: 28px;">Are you sure you want to mark absent {{ $shift->employee()->name() }}?</div>

<form action="{{ route('timecards.absent') }}/{{ $shift->id }}" method="post" autocomplete="off" id="absent-form">
{!! csrf_field() !!}

    <div class="form-horizontal form-primary">
        <div class="form-group form-group-last">
            <div class="col-xs-12">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <textarea name="reason" id="reason" rows="3" class="form-control autosize" placeholder="Reason for being absent"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
<script>
$(function() {

    autosize($('.autosize'));

});
</script>