@extends('layouts.app')

@section('title', 'Dashboard')

@section('head-addon')
<style>
    
</style>
@endsection

@section('content')
<section class="section-header">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="user-dashboard clearfix">
                            <div class="ud-dp">
                                <div class="profile-dp" style="background:url({{ $currentUser->dp() }}) center center no-repeat;background-size:cover;"></div>
                            </div>
                            <div class="ud-salute">
                                <h1>
                                    <a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}" title="View Profile">
                                        {{ $currentUser->name() }}
                                    </a>
                                </h1>
                                <h4>{{ $currentUser->employee->position->name }}</h4>
                            </div>
                        </div>

                        <?php
                            $disabled = true;
                            if ($shiftToday) {
                                $disabled = false;
                            }
                            if ($timecard) {
                                $disabled = true;
                            }
                        ?>

                        <form action="" method="post" autocomplete="off" id="bundy-clock-form">
                        {!! csrf_field() !!}
                        <input type="hidden" name="timecard_id" value="{{ $timecard != null ? $timecard->id : 0 }}" />
                        <input type="hidden" name="shift_id" value="{{ $shiftToday != null ? $shiftToday->id : 0 }}" />
                        <div id="bundy-clock" class="row">
                            <div class="col-xs-12 col-sm-4">
                                
                                <button id="time-in" class="btn btn-xl btn-primary btn-bundy-clock"
                                    data-url="{{ route('timecards.timein') }}"
                                    <?php if ($disabled): ?>disabled<?php endif; ?>
                                >
                                    <small>
                                        Time-in
                                    </small>
                                    <span id="time-in-clock">
                                        @if ($timecard)
                                            {{ processTime($timecard->time_in) }}
                                        @else
                                            00:00
                                        @endif
                                    </span>
                                    @if ($timecard)
                                        @if ($timecard->ti_signed_by != null)
                                            <i class="fa fa-check fa-fw" title="Approved"></i>   
                                        @else
                                            <i class="fa fa-circle-o-notch fa-spin fa-fw" title="Waiting for approval"></i>
                                        @endif
                                    @endif
                                    <!-- <i class="fa fa-circle-o-notch fa-spin fa-fw" title="Waiting for approval"></i> -->
                                </button>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                                {{--*/ $toDisabled = true /*--}}
                                @if ($timecard AND $timecard->time_out == null)
                                    {{--*/ $toDisabled = false /*--}}
                                @endif
                                <button id="time-out" class="btn btn-xl btn-primary btn-bundy-clock"
                                    data-url="{{ route('timecards.timeout') }}"
                                    <?php if ($toDisabled): ?>disabled<?php endif; ?>
                                    <?php if (Session::has('breaktime')): ?>disabled<?php endif; ?>
                                >
                                    <small>Time-out</small>
                                    <span id="time-out-clock">
                                        @if ($timecard)
                                            @if ($timecard->time_out != null)
                                                {{ processTime($timecard->time_out) }}
                                            @else
                                                00:00
                                            @endif
                                        @else
                                            00:00
                                        @endif
                                    </span>
                                    @if ($timecard)
                                        @if ($timecard->time_out != null)
                                            @if ($timecard->to_signed_by == null)
                                                <i class="fa fa-circle-o-notch fa-spin fa-fw" title="Waiting for approval"></i>
                                            @else
                                                <i class="fa fa-check fa-fw" title="Approved"></i>    
                                            @endif
                                        @endif
                                    @endif
                                </button>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                {{--*/ $width = '' /*--}}
                                {{--*/ $class = 'btn-success' /*--}}
                                @if ($timecard)
                                    {{--*/ $width = ($timecard->totalBreak() / 60) * 100 /*--}}
                                    @if ($width >= 75)
                                        {{--*/ $class = 'btn-warning' /*--}}
                                    @endif
                                    @if ($width >= 90)
                                        {{--*/ $class = 'btn-danger' /*--}}
                                    @endif
                                @endif
                                <button id="takebreak" class="btn btn-xl {{ $class }} btn-bundy-clock"
                                    <?php if ($toDisabled): ?>disabled<?php endif; ?>
                                    <?php if (Session::has('breaktime')): ?>disabled<?php endif; ?>
                                    <?php if ($timecard): ?>
                                    data-url="{{ route('timecards.break') }}/{{ $timecard->id }}"
                                    <?php endif; ?>
                                >
                                    <small>Break</small>
                                    @if (Session::has('breaktime'))
                                        <span>On break</span>                                        
                                        <div class="break-progress" style="width: {{$width}}%;"></div>
                                    @else
                                        @if ($timecard)
                                            <span>{{ $timecard->totalBreak() }}m</span>
                                            <div class="break-progress" style="width: {{$width}}%;"></div>
                                        @else
                                            <span>0m</span>
                                        @endif
                                    @endif
                                </button>
                            </div>
                        </div>
                        </form>

                    </div>
                </div>
                
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">


            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Leave Credits</h4>
                            </div>
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <table class="table table-with-footer">
                                        <thead>
                                            <tr>
                                                <th>Leave Type</th>
                                                <th class="ta-right">Remaining Credits</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($entitledLeaves as $entitledLeave)
                                                <tr>
                                                    <td class="table-highlight">{{ $entitledLeave->leaveType->name }}</td>
                                                    <td class="ta-right">{{ $currentUser->employee->checkLeaveCredits($entitledLeave->leaveType->id) }}</td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="2" class="ta-center">No entitled leave.</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="panel-footer">
                                <div class="form-button-group clearfix">
                                    <a href="{{ route('loa.file') }}" id="file-leave" class="btn btn-sm btn-primary" 
                                        data-form="#file-leave-form"
                                    >
                                        File Leave
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Shift Today</h4>
                            </div>
                            <div class="panel-body">

                                <div class="shift-time-wrap row">
                                    @if ($shiftToday)
                                    <div class="col-xs-6">
                                        <div class="shift-time shift-time-from">
                                            <small>{{ formatShortDate($shiftToday->shift_from) }}</small>
                                            <span>{{ processTime($shiftToday->shift_from, true) }}</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="shift-time shift-time-to">
                                            <small>{{ formatShortDate($shiftToday->shift_to) }}</small>
                                            <span>{{ processTime($shiftToday->shift_to, true) }}</span>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-xs-12">
                                        <div class="shift-time">
                                            <small>{{ formatShortDate($today) }}</small>
                                            <span>No Shift</span>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Recent Payslip</h4>
                            </div>
                            <div class="panel-body">
                                
                                @if ($latestPayslip)
                                <div class="latest-payslip">
                                    <small>
                                        {{ getMonths()[$latestPayslip->payrollBatch->month - 1] }} {{ $latestPayslip->payrollBatch->year }} / {{ $latestPayslip->payrollBatch->half }}{{ $latestPayslip->payrollBatch->half == 1 ? 'st' : 'nd' }} Half
                                    </small>
                                    <span>
                                        P {{ number_format($latestPayslip->basic_pay, 2, '.', ',') }}
                                    </span>
                                </div>
                                @else
                                <div class="latest-payslip">
                                    <small>
                                        No payslip yet
                                    </small>
                                    <span>
                                        P 00.00
                                    </span>
                                </div>
                                @endif                   

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>

@endsection

@section('footer-addon')
<script>
$(function() {


    
});
</script>
@endsection