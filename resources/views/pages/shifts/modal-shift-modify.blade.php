<form method="post" autocomplete="off" id="update-shift-form"    
    <?php if ($shift): ?>
        action="{{ route('shifts.update') }}/{{ $shift->id}}"
        data-shift-delete="{{ route('shifts.delete') }}/{{ $shift->id }}"
    <?php else: ?>
        action="{{ route('shifts.create') }}"
    <?php endif; ?>
>
    {{ csrf_field() }}
    <input type="hidden" name="date" value="{{ $date }}" />
    <input type="hidden" name="shift_brief_id" value="{{ $shiftBriefId }}" />
    <div class="form-horizontal form-primary">
        @if ($shift)
            <input type="hidden" name="shift_id" value="{{ $shift->id }}" />            
            <div class="form-group">
                <label for="shift-from-date" class="col-xs-12 col-sm-4 control-label">Shift From</label>
                <div class="col-xs-12 col-sm-8">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12 col-md-6">
                            <input type="hidden" name="shift_from_date" value="{{ $shift->shift_from != null ? processDateForDB($shift->shift_from) : '' }}" />
                            <input type="text" id="shift-from-date" class="form-control datepicker" 
                                value="{{ $shift->shift_from != null ? processDate($shift->shift_from) : '' }}" 
                                disabled 
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="shift-from-time" class="col-xs-12 col-sm-4 control-label">From</label>
                <div class="col-xs-12 col-sm-8">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12 col-md-6">
                            <select name="shift_from_time" id="shift-from-time" class="form-control">
                                @foreach (getTimeOptions() as $time)
                                <option value="{{ $time }}"
                                    <?php if (parseTime($shift->shift_from) == $time): ?>selected<?php endif; ?>
                                >
                                    {{ date('h:iA', strtotime($time)) }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="shift-to-time" class="col-xs-12 col-sm-4 control-label">To</label>
                <div class="col-xs-12 col-sm-8">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12 col-md-6">
                            <select name="shift_to_time" id="shift-to-time" class="form-control">
                                @foreach (getTimeOptions() as $time)
                                <option value="{{ $time }}"
                                    <?php if (parseTime($shift->shift_to) == $time): ?>selected<?php endif; ?>
                                >
                                    {{ date('h:iA', strtotime($time)) }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="form-group">
                <label for="shift-from-date" class="col-xs-12 col-sm-4 control-label">Shift Date</label>
                <div class="col-xs-12 col-sm-8">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12 col-md-6">
                            <input type="hidden" name="shift_from_date" value="{{ $date }}" />
                            <input type="text" id="shift-from-date" class="form-control" 
                                value="{{ date('M j, Y', strtotime($date)) }}" 
                                disabled 
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="shift-from-time" class="col-xs-12 col-sm-4 control-label">From</label>
                <div class="col-xs-12 col-sm-8">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12 col-md-6">
                            <select name="shift_from_time" id="shift-from-time" class="form-control">
                                @foreach (getTimeOptions() as $time)
                                <option value="{{ $time }}">{{ date('h:iA', strtotime($time)) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="shift-to-time" class="col-xs-12 col-sm-4 control-label">To</label>
                <div class="col-xs-12 col-sm-8">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12 col-md-6">
                            <select name="shift_to_time" id="shift-to-time" class="form-control">
                                @foreach (getTimeOptions() as $time)
                                <option value="{{ $time }}">{{ date('h:iA', strtotime($time)) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="form-group form-group-last">
            <label for="is-rd" class="col-xs-12 col-sm-4 control-label">Rest Day</label>
            <div class="col-xs-12 col-sm-8">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_rd" id="is-rd" value="1" 
                                    <?php if (!$shift): ?>
                                        checked
                                    <?php else: ?>
                                        <?php if ($shift->is_rd): ?>
                                            checked
                                        <?php endif; ?>
                                    <?php endif; ?>
                                />
                                <span>Check if yes</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script>
$(function() {

    $('#shift-from-time').on('change', function() {
        var shiftFrom = $(this).val(),
            shiftFromSpit = shiftFrom.split(':'),
            shiftFromHour = parseInt(shiftFromSpit[0]);
        if (shiftFrom == '') {
            $('#shift-to-time option:first').prop('selected', true);
        } else {
            $('#shift-to-time option').each(function() {
                var shiftTo = $(this).val(),
                    shiftToHour = parseInt(shiftFromHour + 8);
                shiftToHour = shiftToHour > 24 ? shiftToHour - 24 : shiftToHour;
                shiftToHour = shiftToHour < 10 ? '0' + shiftToHour : shiftToHour;
                var shiftToHourStr = shiftToHour + ':00:00';
                if (shiftTo == shiftToHourStr) {
                    $(this).prop('selected', true);
                    return;
                }                
            });
        }
    });

});
</script>