@extends('layouts.app')

@section('title', 'Shifts')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Generate Shift</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        @can ('add_shift', $currentUser)
                        <li><a href="{{ route('shifts.manage') }}">Manage Shifts</a></li>
                        @endcan
                        <li><a href="{{ route('shifts.list') }}/{{ $currentUser->employee->id }}">Your Shifts</a></li>
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            
            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <form action="{{ route('shifts.generate') }}" method="post" autocomplete="off" id="generate-shift-form">
                        {{ csrf_field() }}
                        <input type="hidden" name="url" value="{{ Request::url() }}" />
                            <div class="panel">
                                <div class="panel-body">
                                    
                                    <div class="form-horizontal form-primary">

                                        <div class="form-group">
                                            <label for="employees" class="col-xs-12 col-sm-3 control-label">Employees</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <select id="employees" name="employee_id[]" multiple="multiple" class="form-control">
                                                            @foreach ($employees as $employee)
                                                            <option value="{{ $employee->id }}"
                                                                <?php if (in_array($employee->id, $employeesId)): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $employee->user->name() }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="month" class="col-xs-12 col-sm-3 control-label">Month</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-8 col-sm-4">
                                                        <select name="month" id="month" class="form-control" data-url="{{ route('shifts.drawcalendar') }}">
                                                            @foreach (getMonths() as $index => $label)
                                                                @if (($index + 1) >= $today->month)
                                                                    <option value="{{ $index + 1 }}"
                                                                        <?php if (($index + 1) == $month): ?>selected<?php endif; ?>
                                                                    >
                                                                        {{ $label }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-2">
                                                        <select name="year" id="year" class="form-control" data-url="{{ route('shifts.drawcalendar') }}">
                                                            @foreach (getYears($year) as $yy)
                                                            <option value="{{ $yy }}"
                                                                <?php if ($yy == $year): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $yy }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="month" class="col-xs-12 col-sm-3 control-label">Time</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-8 col-sm-3">
                                                        <select name="shift_from" id="shift-from" class="form-control">
                                                            <option value=""></option>
                                                            @foreach (getTimeOptions() as $time)
                                                            <option value="{{ $time }}">
                                                                {{ date('h:iA', strtotime($time)) }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-3">
                                                        <select name="shift_to" id="shift-to" class="form-control">
                                                            <option value=""></option>
                                                            @foreach (getTimeOptions() as $time)
                                                            <option value="{{ $time }}">
                                                                {{ date('h:iA', strtotime($time)) }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-last">
                                            <label for="month" class="col-xs-12 col-sm-3 control-label">Select Rest Days</label>
                                            <div class="col-xs-12 col-sm-6">

                                                <div id="calendar-wrap">
                                                    <div class="loading-calendar ta-center" style="padding:12px 0;">
                                                        <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Loading Calendar
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    
                                    <div class="form-button-group clearfix">
                                        <button type="submit" id="generate-shift-btn" class="btn btn-primary">Generate Shift</button>
                                        <a href="{{ route('shifts.manage') }}" class="btn btn-default">Cancel</a>
                                    </div>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>          

        </div>
        </div>
    </div>
</section>
<div class="modal fade" id="manage-schedule" tabindex="-1" role="dialog" aria-labelledby="manage-schedule-title">
    <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-contentarea">
    </div>
    </div>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#employees').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5
    });

    $('#shift-from').on('change', function() {
        var shiftFrom = $(this).val(),
            shiftFromSpit = shiftFrom.split(':'),
            shiftFromHour = parseInt(shiftFromSpit[0]);
        if (shiftFrom == '') {
            $('#shift-to option:first').prop('selected', true);
        } else {
            $('#shift-to option').each(function() {
                var shiftTo = $(this).val(),
                    shiftToHour = parseInt(shiftFromHour + 8);
                shiftToHour = shiftToHour > 24 ? shiftToHour - 24 : shiftToHour;
                shiftToHour = shiftToHour < 10 ? '0' + shiftToHour : shiftToHour;
                var shiftToHourStr = shiftToHour + ':00:00';
                if (shiftTo == shiftToHourStr) {
                    $(this).prop('selected', true);
                    return;
                }
                
            });
        }
    });

    var month = $('#month').val(),
        year = $('#year').val(),
        url = $('#month').attr('data-url') + '/' + month + '-' + year;
    drawCalendar('#calendar-wrap', url);

    $('#month, #year').on('change', function() {
        $('#calendar-wrap').html('<div class="ta-center" style="padding:12px 0;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Loading Calendar</div>');
        var month = $('#month').val(),
            year = $('#year').val(),
            url = $(this).attr('data-url') + '/' + month + '-' + year;
        drawCalendar('#calendar-wrap', url);
    });

});
</script>
@endsection