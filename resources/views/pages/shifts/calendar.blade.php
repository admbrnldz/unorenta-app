<?php
    $running_day = date('w', mktime(0, 0, 0, $month, 1, $year));
    $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
    $days_in_this_week = 1;
    $day_counter = 0;
?>
<input type="hidden" name="days_count" value="{{ $days_in_month }}">
<div class="calendar-handler">
        <div class="table-responsive">
        <table class="table-calendar">
            <thead>
                <tr class="calendar-row">
                    <th class="calendar-day-head">
                        <?php echo implode('</th><th class="calendar-day-head">', getWeekLabels()); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="calendar-row">

                    <?php for ($x = 0; $x < $running_day; $x++): ?>
                        <td class="calendar-day-np"></td>
                    <?php $days_in_this_week++; endfor; ?>

                    <?php for ($list_day = 1; $list_day <= $days_in_month; $list_day++): ?>

                        <td class="calendar-day">
                            <div class="calendar-checkbox">
                                <label>
                                    <input type="checkbox" name="dayoff[]" id="" value="<?php echo $list_day; ?>" />
                                    <span><?php echo $list_day; ?></span>
                                </label>
                            </div>
                        </td>
                        <?php if ($running_day == 6): ?>
                            </tr>
                            <?php if(($day_counter + 1) != $days_in_month): ?>
                            <tr class="calendar-row">
                            <?php endif; $running_day = -1; $days_in_this_week = 0; ?>
                        <?php endif; ?>

                        <?php $days_in_this_week++; $running_day++; $day_counter++; ?>

                    <?php endfor; ?>

                    <?php if ($days_in_this_week > 1 AND $days_in_this_week < 8): ?>
                        <?php for ($x = 1; $x <= (8 - $days_in_this_week); $x++): ?>
                            <td class="calendar-day-np"></td>
                        <?php endfor; ?>
                    <?php endif; ?>

                </tr>
            </tbody>
        </table>
    </div>
</div>