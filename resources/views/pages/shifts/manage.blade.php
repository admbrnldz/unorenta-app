@extends('layouts.app')

@section('title', 'Shifts')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Manage Shifts</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        @can ('add_shift', $currentUser)
                        <li class="active"><a href="{{ route('shifts.manage') }}">Manage Shifts</a></li>
                        @endcan
                        <li><a href="{{ route('shifts.list') }}/{{ $currentUser->employee->id }}">Your Shifts</a></li>
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            @include('includes.status')

            <?php /*
            <div class="section-header">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="section-title">
                            <h3><i class="fa fa-calendar"></i>Manage Shifts</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="section-head-actions">
                            <button data-target="{{ route('shifts.generate') }}" id="generate-shift" class="btn btn-primary" disabled>Generate Shift</a>
                        </div>
                    </div>
                </div>
            </div>
            */ ?>
            
            <div class="section-content">

                <div class="panel">
                    <div class="panel-body">
                        
                        <div class="table-responsive">
                            <table class="table selectlist">
                                <thead>
                                    <tr>
                                        <th style="width:24px;">
                                            <input class="checkboxes sl-checkbox-main sl-checkboxes-ctrl" type="checkbox">
                                        </th> 
                                        <th colspan="2">Employee</th>
                                        <th>Position</th>
                                        <th>Shift Briefs</th>
                                        <th class="ta-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($employees as $employee)
                                    <tr>
                                        <td class="tbl-checkbox">
                                            <input class="employees-cb checkboxes sl-checkbox" value="{{ $employee->id }}" type="checkbox">
                                        </td>
                                        <td class="table-dp-td">
                                            <div class="dp-handler" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;">
                                                <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}"></a>
                                            </div>
                                        </td>
                                        <td class="table-highlight">
                                            <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">
                                                {{ $employee->user->name(true) }}
                                            </a>
                                        </td>
                                        <td>{{ $employee->position->name }}</td>
                                        <td>
                                            <?php
                                                $monthNow = $today->month;
                                                $shiftBriefs = $employee->shiftBriefs()
                                                    ->whereIn('month', [$monthNow - 1, $monthNow, $monthNow + 1])
                                                    ->where('year', $today->year)
                                                    ->get();
                                            ?>
                                            @foreach ($shiftBriefs as $shiftBrief)
                                                <span class="label label-primary">
                                                    <a href="{{ route('shifts.brief') }}/{{ $shiftBrief->id }}" title="View Detailed Shift">
                                                        {{ date('F', mktime(0, 0, 0, $shiftBrief->month, 1, $shiftBrief->year)) }}
                                                    </a>
                                                </span>&nbsp;
                                            @endforeach
                                        </td>
                                        <td class="ta-right">                                    
                                            <a href="{{ route('shifts.generate') }}/{{ $employee->id }}" title="Generate Shift"><i class="fa fa-calendar-check-o fa-fw"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </form>
                        </div>

                    </div>
                </div>

            </div>          

        </div>
        </div>
    </div>
</section>
<div class="modal fade" id="manage-schedule" tabindex="-1" role="dialog" aria-labelledby="manage-schedule-title">
    <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-contentarea">
    </div>
    </div>
</div>


<div class="page-actions">
    <ul class="clearfix">
        <li>
            <a data-target="{{ route('shifts.generate') }}" id="generate-shift" class="pa-primary hidden">
                <i class="fa fa-calendar-plus-o"></i>
            </a>
        </li>
    </ul>
</div>

@endsection

@section('footer-addon')
<script>
$(function() {

    $('.selectlist').selectlist({
        buttonClass: 'btn btn-primary btn-select-all'
    });

    $('#generate-shift').on('click', function(e) {
        e.preventDefault();

        var url = $(this).attr('data-target'),
            ids = '',
            count = 0;

        $('.sl-checkbox').each(function() {
            if ($(this).prop('checked') == true) {
                ids += ',' + $(this).val();
                count++;
            }
        });

        if (count < 1) {
            alert('Please select at least one employee');
            return false;
        }

        ids = ids.substring(1, ids.length);
        window.location = url + '/' +  ids;

    });

    $('.selectlist td, .sl-controller').on('click', function() {

        setTimeout(
            function() {
                var count = $('.selectlist').find('.sl-checkbox:checked').length;                
                if (count > 0) {
                    $('#generate-shift').removeClass('hidden');
                }
                else {
                    $('#generate-shift').addClass('hidden');
                }
            },
            250
        );

    });

});
</script>
@endsection