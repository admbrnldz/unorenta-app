@extends('layouts.app')

@section('title', $shiftBrief->employee->user->name())

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Shift Brief</h3>
                </div>                
                <div class="section-header-menu">
                    <ul class="clearfix">
                        @can ('add_shift', $currentUser)
                        <li><a href="{{ route('shifts.manage') }}">Manage Shifts</a></li>
                        @endcan
                        <li><a href="{{ route('shifts.list') }}/{{ $currentUser->employee->id }}">Your Shifts</a></li>
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="panel">
                            <div class="panel-heading">

                                <div class="panel-heading-profile clearfix">
                                    <div class="dp-wrapper">
                                        <div class="profile-dp" style="background:url({{ $shiftBrief->employee->user->dp() }}) center center no-repeat;background-size:cover;"></div>
                                    </div>
                                    <h1>
                                        <a href="{{ route('employees.profile', ['employeeId' => $shiftBrief->employee_id]) }}" title="View Profile">
                                            {{ $shiftBrief->employee->user->name() }}
                                        </a>
                                    </h1>
                                    <h3>{{ $shiftBrief->employee->position->name }}</h3>
                                </div>

                            </div>
                            <div class="panel-body">
                                
                                <div class="form-horizontal form-primary">

                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select name="" id="switch-shift-brief" class="form-control">
                                                        @foreach ($shiftBrief->employee->shiftBriefs as $sb)
                                                        <option value="{{ route('shifts.brief') }}/{{ $sb->id }}"
                                                            <?php if ($sb->id == $shiftBrief->id): ?>selected<?php endif; ?>
                                                            data-month="{{ $sb->month }}" data-year="{{ $sb->year }}" data-id="{{ $sb->id }}"
                                                        >
                                                            {{ date('F', mktime(0, 0, 0, $sb->month, 1, $sb->year)) }} {{ $sb->year }}
                                                        </option>
                                                        @endforeach
                                                    </select>                                          
                                                </div>
                                            </div>
                                        </div>
                                        <?php /*
                                        <div class="col-xs-12 col-sm-6 col-md-8">
                                            <div class="pull-right" style="margin-top: 4px;"> 
                                                <button class="btn btn-sm btn-danger" id="delete-shift-brief" type="button">
                                                    <i class="fa fa-trash-o fa-fw"></i> &nbsp; Delete All Shifts
                                                </button>
                                            </div>
                                        </div>
                                        */ ?>
                                    </div>

                                </div>

                                <div id="calendar-wrap">
                                    <div class="ta-center" style="padding:12px 0;">
                                        <i class="fa fa-circle-o-notch fa-spin fa-fw "></i> &nbsp; Loading Calendar
                                    </div>
                                </div>
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    drawShiftCalendar('#calendar-wrap', '{{ route("shifts.drawcalendar") }}/{{ $shiftBrief->month }}-{{ $shiftBrief->year }}', '{{ $shiftBrief->id }}');

    $('#switch-shift-brief').on('change', function() {
        window.history.pushState('', '', $(this).val());
        var month = $(this).find(':selected').attr('data-month'),
            year = $(this).find(':selected').attr('data-year'),
            id = $(this).find(':selected').attr('data-id');
        drawShiftCalendar('#calendar-wrap', '{{ route("shifts.drawcalendar") }}/' + month + '-' + year, id);
    });

    $('#delete-shift-brief').on('click', function(e) {
        e.preventDefault();
        bootbox.dialog({
            title: 'Hey!',
            message: 'Are you sure you want to delete all shift this month?',
            closeButton: true,
            buttons: {
                success: {
                    label: 'Yes',
                    className: 'btn-primary',
                    callback: function() {
                        var shiftBriefId = $('#switch-shift-brief').find(':selected').attr('data-id');
                        $.get({ 
                            url: "{{ route('shifts.brief.delete') }}/" + shiftBriefId 
                        }).done(function(response) {
                            response = $.parseJSON(response);
                            window.location = response.route;
                        });
                    }
                },
                'Cancel' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection