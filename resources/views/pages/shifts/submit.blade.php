<form action="{{ route('shifts.form') }}" id="submit-shift-form" autocomplete="off">
{{ csrf_field() }}
<input type="hidden" name="shift_id" value="<?php if ($shift): ?>{{ $shift->id }}<?php endif; ?>">
<input type="hidden" name="timecard_id" value="<?php if ($timecard): ?>{{ $timecard->id }}<?php endif; ?>">
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-info">
        <div class="modal-info-dp">
            <div class="dp-handler dp-handler-flex" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;">
                <a href="#"></a>
            </div>
        </div>
        <h2><a href="#">{{ $employee->user->name() }}</a></h2>
        <h4>{{ $employee->position->name }}</h4>
        <input type="hidden" name="employee_id" value="{{ $employee->id }}" />

    </div>
    @if ($yesterday)
        <div class="alert alert-warning">You cannot create a shift on days before today, sorry.</div>
    @else
        <div class="form-horizontal form-primary">

            <div class="form-group">
                <label for="date-shift-display" class="col-xs-12 col-sm-3 control-label">Date</label>
                <div class="col-xs-12 col-sm-9">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12">
                            <input type="hidden" name="date" value="{{ $fromDate }}" id="date-shift" />
                            <input type="text" name="date_preview" value="{{ date('F j, Y', strtotime($fromDate)) }}" class="form-control" id="date-shift-display" />
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $shiftFromToClass = 'modal-last-form-group';
                if ($timecard):
                    $shiftFromToClass = '';
                endif;
            ?>
            <div id="shift-from-to" class="form-group {{ $shiftFromToClass }}">
                <label for="shift-from" class="col-xs-12 col-sm-3 control-label">Shift</label>
                <div class="col-xs-12 col-sm-9">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12 col-sm-6">
                            <select name="shift_from" id="shift-from" class="form-control">
                                <option value=""></option>
                                @foreach (getTimeOptions() as $time)
                                <option value="{{ $time }}" 
                                    <?php if ($shift): ?>
                                    <?php if ($time == $shift->shift_from): ?>selected<?php endif; ?>
                                    <?php endif; ?>>
                                    {{ date('h:iA', strtotime($time)) }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <select name="shift_to" id="shift-to" class="form-control">
                                <option value=""></option>
                                @foreach (getTimeOptions() as $time)
                                <option value="{{ $time }}" 
                                    <?php if ($shift): ?>
                                    <?php if ($time == $shift->shift_to): ?>selected<?php endif; ?>
                                    <?php endif; ?>>
                                    {{ date('h:iA', strtotime($time)) }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $absentClass = 'hidden';
                $absentReason = '';
                if ($timecard):
                    if ($timecard->is_absent):
                        $absentClass = '';
                        $absentReason = $timecard->absent_reason;
                    endif;
                endif;
            ?>
            <div id="reason-absence" class="{{ $absentClass }} form-group modal-last-form-group">
                <label for="absent-reason" class="col-xs-12 col-sm-3 control-label">Reason of Absence</label>
                <div class="col-xs-12 col-sm-9">
                    <div class="row input-field-wrap">
                        <div class="col-xs-12">
                            <textarea name="absent_reason" id="absent-reason" rows="3" class="form-control">{{ $absentReason }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $lateClass = 'hidden';
                $lateMins = '';
                if ($timecard):
                    if ($timecard->late_minutes > 0):
                        $lateClass = '';
                        $lateMins = $timecard->late_minutes;
                    endif;
                endif;
            ?>
            <div id="minutes-late" class="{{ $lateClass }} form-group modal-last-form-group">
                <label for="late-minutes" class="col-xs-12 col-sm-3 control-label">Minutes Late</label>
                <div class="col-xs-12 col-sm-9">
                    <div class="row input-field-wrap">
                        <div class="col-xs-6">
                            <input type="text" name="late_minutes" class="form-control" id="late-minutes" value="{{ $lateMins }}" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endif
</div>
<div class="modal-footer">
    <div class="modal-footer-btn clearfix">
        @if ($shift)
        <div class="btn-group shift-btn-group" data-toggle="buttons">
            <label class="btn btn-sm btn-warning">
                <input type="radio" name="timecard_opts" id="timecard-late" autocomplete="off" data-target-url="{{ route('timecards.late') }}" />
                <span>Late</span>
            </label>
            <label class="btn btn-sm btn-danger">
                <input type="radio" name="timecard_opts" id="timecard-absent" autocomplete="off" data-target-url="{{ route('timecards.absent') }}" />
                <span>Absent</span>
            </label>
        </div>
        <button type="button" id="shift-present" data-target-url="{{ route('timecards.present') }}" class="btn btn-sm btn-primary">Present</button>
        @endif        
        <button type="submit" id="submit-shift" class="btn btn-sm btn-primary">Submit</button>
    </div>
</div>
</form>

<script>
$(function() {

    autosize($('textarea'));

    $('#date-shift-display').datepicker({ 
        dateFormat: 'MM dd, yy',
        minDate: 0
    });

    $('#shift-from').on('change', function() {
        var shiftFrom = $(this).val(),
            shiftFromSpit = shiftFrom.split(':'),
            shiftFromHour = parseInt(shiftFromSpit[0]);
        if (shiftFrom == '') {
            $('#shift-to option:first').prop('selected', true);
        } else {
            $('#shift-to option').each(function() {
                var shiftTo = $(this).val(),
                    shiftToHour = parseInt(shiftFromHour + 8);
                shiftToHour = shiftToHour > 24 ? shiftToHour - 24 : shiftToHour;
                shiftToHour = shiftToHour < 10 ? '0' + shiftToHour : shiftToHour;
                var shiftToHourStr = shiftToHour + ':00:00';
                if (shiftTo == shiftToHourStr) {
                    $(this).prop('selected', true);
                    return;
                }
                
            });
        }
    });

    $('#shift-present').on('click', function() {
        var form = $('#submit-shift-form'),
            url = $('#shift-present').attr('data-target-url');
            data = form.serialize();
        $.post(url, data, function(response) {
            $('#manage-schedule').modal('toggle');
            var source = '.' + $('#manage-schedule').attr('data-source');
            $(source).addClass('table-shift-day-' + response.className);
        }, 'json');
    });

    $('#submit-shift').on('click', function(e) {
        e.preventDefault();
        var form = $('#submit-shift-form'),
            url = form.attr('action');
            data = form.serialize();

        if ($('#timecard-absent').prop('checked')) {
            url = $('#timecard-absent').attr('data-target-url');
        }
        if ($('#timecard-late').prop('checked')) {
            url = $('#timecard-late').attr('data-target-url');
        }

        $.post(url, data, function(response) {
            $('#manage-schedule').modal('toggle');
            var source = '.' + $('#manage-schedule').attr('data-source');
            if (response.type == 'timecard') {
                $(source).addClass('table-shift-day-' + response.className);
                if (response.className == 'absent') {
                    $(source).find('span').html('Absent');
                } else if (response.className == 'absent') {
                    $(source).find('span').html('Present');
                }
            } else if (response.type == 'shift') {
                var shift = response.shift,
                    shiftFromArr = shift.shift_from.split(':'),
                    shiftFrom = parseInt(shiftFromArr['0']) > 12 ? (parseInt(shiftFromArr['0']) - 12) + 'pm' : parseInt(shiftFromArr['0']) + 'am',
                    shiftToArr = shift.shift_to.split(':'),
                    shiftTo = parseInt(shiftToArr['0']) > 12 ? (parseInt(shiftToArr['0']) - 12) + 'pm' : parseInt(shiftToArr['0']) + 'am',
                    shiftDate = new Date(shift.date),
                    shiftMonth = (shiftDate.getMonth() + 1) < 9 ? '0' + (shiftDate.getMonth() + 1) : (shiftDate.getMonth() + 1),
                    shiftDay = shiftDate.getDate() < 9 ? '0' + shiftDate.getDate() : shiftDate.getDate(),
                    formattedDate = shiftDate.getFullYear() + '-' + shiftMonth + '-' + shiftDay,
                    className = '.' + formattedDate + '--' + shift.employee_id;
                $(source).find('span').html('');
                $(className).find('span').html(
                    shiftFrom + '-' + shiftTo
                );
            }
        }, 'json');

    });

    $('#date-shift-display').on('change', function() {
        var date = new Date($(this).val()),
            shiftMonth = (date.getMonth() + 1) < 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1),
            shiftDay = date.getDate() < 9 ? '0' + date.getDate() : date.getDate();
        $('#date-shift').val(date.getFullYear() + '-' + shiftMonth + '-' + shiftDay);
    });

    $('#timecard-absent').on('change', function() {
        if ($('#shift-from-to').hasClass('modal-last-form-group')) {
            $('#shift-from-to').removeClass('modal-last-form-group');
        }
        if ($('#reason-absence').hasClass('hidden')) {
            $('#minutes-late').addClass('hidden').find('.form-control');
            $('#reason-absence').removeClass('hidden');
        } else {
            $('#reason-absence, #minutes-late').addClass('hidden').find('.form-control');
            $('#shift-from-to').addClass('modal-last-form-group')
        }
    });

    $('#timecard-late').on('change', function() {
        if ($('#shift-from-to').hasClass('modal-last-form-group')) {
            $('#shift-from-to').removeClass('modal-last-form-group');
        }
        if ($('#minutes-late').hasClass('hidden')) {
            $('#reason-absence').addClass('hidden').find('.form-control');
            $('#minutes-late').removeClass('hidden');
        } else {
            $('#reason-absence, #minutes-late').addClass('hidden').find('.form-control');
            $('#shift-from-to').addClass('modal-last-form-group')
        }
    })

})
</script>