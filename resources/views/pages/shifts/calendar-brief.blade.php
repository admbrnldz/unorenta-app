<?php
    $running_day = date('w', mktime(0, 0, 0, $month, 1, $year));
    $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
    $days_in_this_week = 1;
    $day_counter = 0;
?>
<input type="hidden" name="days_count" value="{{ $days_in_month }}">
<div class="calendar-handler">
    <div class="table-responsive">
        <table class="table-calendar">
            <thead>
                <tr class="calendar-row">
                    <th class="calendar-day-head">
                        <?php echo implode('</th><th class="calendar-day-head">', getWeekLabels()); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="calendar-row">

                    <?php for ($x = 0; $x < $running_day; $x++): ?>
                        <td class="calendar-day-np"></td>
                    <?php $days_in_this_week++; endfor; ?>

                    <?php for ($list_day = 1; $list_day <= $days_in_month; $list_day++): ?>

                        <td class="calendar-day">
                            <?php 
                                $date = \Carbon\Carbon::create($year, $month, $list_day, 0);
                                // $date = date('Y-m-d', mktime(0, 0, 0, $month, $list_day, $year));
                                $shift = $shiftBrief->shifts()
                                    ->whereBetween('shift_from', [$date, $date->copy()->addDay()->subMinute()])
                                    ->first();
                            ?>
                            <div class="calendar-day-min">{{ $list_day }}</div>
                            <div class="calendar-shift-details clearfix">
                                <?php
                                    $loas = \UnoRenta\Models\LeaveApplication::query()
                                        ->whereRaw("'$date' BETWEEN date_from AND date_to")
                                        ->where('employee_id', $shiftBrief->employee->id)
                                        ->get();
                                ?>
                                @foreach ($loas as $loa)                                    
                                    @if ($loa->leave_type_id > 0)
                                        <span class="label label-danger" title="{{ $loa->reason }}">
                                            {{ $loa->leaveType->name }}
                                        </span>
                                    @else
                                        <span class="label label-danger" title="Leave without pay: {{ $loa->reason }}">
                                            LWOP
                                        </span>
                                    @endif                                    
                                @endforeach
                                @if (count($loas) > 0) <br clear="all"/> @endif
                                @if ($shift)
                                    {{--*/ $class = 'label-primary' /*--}}
                                    @if ($shift->is_rd)
                                        {{--*/ $class = 'label-success' /*--}}
                                    @endif
                                    <span class="label {{ $class }} edit-shift" 
                                        data-shift-id="{{ $shift->id }}" 
                                        data-shift-date="{{ processDateForDB($date) }}"
                                        data-shift-from="{{ $shift->shift_from }}"
                                        data-shift-to="{{ $shift->shift_to }}"
                                        data-employee-id="{{ $shiftBrief->employee_id }}"
                                        data-sb-id="{{ $shiftBrief->id }}"
                                    >
                                        {{ processTime($shift->shift_from, false) }}-{{ processTime($shift->shift_to, false) }}
                                    </span>
                                    @if ($shift->timecard)
                                        {{--*/ $class = 'label-default hidden' /*--}}
                                        @if ($shift->timecard->late_minutes > 0 || $shift->timecard->undertime_minutes > 0)
                                            {{--*/ $class ='label-warning' /*--}}
                                        @endif
                                        <span class="label {{ $class }}" title="Late:{{ $shift->timecard->late_minutes }} / Undertime:{{ $shift->timecard->undertime_minutes }}">
                                            LT:{{ $shift->timecard->late_minutes }} / UT:{{ $shift->timecard->undertime_minutes }}
                                        </span>
                                    @endif
                                @else
                                    <span class="label label-default edit-shift" 
                                        data-shift-id="RD"
                                        data-shift-date="{{ processDateForDB($date) }}"
                                        data-employee-id="{{ $shiftBrief->employee_id }}"
                                        data-sb-id="{{ $shiftBrief->id }}"
                                    >
                                        RD
                                    </span>
                                @endif
                                <?php
                                    $holidays = \UnoRenta\Models\Holiday::where('date', $date)->get();
                                ?>
                                @foreach ($holidays as $holiday)
                                    <span class="label label-info" title="{{ $holiday->category->name }} Holiday - {{ $holiday->name }}">
                                        {{ $holiday->name }}
                                    </span>
                                @endforeach
                            </div>
                        </td>
                        <?php if ($running_day == 6): ?>
                            </tr>
                            <?php if(($day_counter + 1) != $days_in_month): ?>
                            <tr class="calendar-row">
                            <?php endif; $running_day = -1; $days_in_this_week = 0; ?>
                        <?php endif; ?>

                        <?php $days_in_this_week++; $running_day++; $day_counter++; ?>

                    <?php endfor; ?>

                    <?php if ($days_in_this_week > 1 AND $days_in_this_week < 8): ?>
                        <?php for ($x = 1; $x <= (8 - $days_in_this_week); $x++): ?>
                            <td class="calendar-day-np"></td>
                        <?php endfor; ?>
                    <?php endif; ?>

                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="calendar-meta">
    <small>Generated by: <a href="{{ route('employees.profile', ['employeeId' => $shiftBrief->created_by]) }}">{{ $shiftBrief->createdBy->user->name() }}</a>, {{ processDate($shiftBrief->created_at, true) }}</small>
</div>

@can ('edit_shifts', $currentUser)
<script>
$(function() {

    $('.edit-shift').each(function() {
        $(this).on('click', function(e) {
            var shiftId = $(this).attr('data-shift-id'),
                shiftFrom = $(this).attr('data-shift-from'),
                shiftTo = $(this).attr('data-shift-to'),
                employeeId = $(this).attr('data-employee-id'),
                sbId = $(this).attr('data-sb-id'),
                date = $(this).attr('data-shift-date'),
                hidden = '';

            if (shiftId == 'RD') { 
                shiftId = 0; 
                hidden = 'hidden';
            }
            var url = "{{ route('shifts.getinfo') }}/" + shiftId + "/" + employeeId + "/" + sbId + "?d=" + date;
            $.get( url ).done(function(response) {
                response = $.parseJSON(response);
                if (response.past) {
                    bootbox.dialog({
                        title: response.title,
                        message: response.view,
                        className: response.className,
                        buttons: {
                            'Okay' : {
                                className: 'btn-default'
                            }
                        }
                    });
                } else {
                    bootbox.dialog({
                        title: response.title,
                        message: response.view,
                        buttons: {
                            success: {
                                label: 'Update',
                                className: 'btn-primary',
                                callback: function(result) {
                                    var form = $('#update-shift-form'),
                                        data = form.serialize(),
                                        url = form.attr('action');
                                    $.post({
                                        url: url, data: data
                                    }).done(function(response) {
                                        response = $.parseJSON(response);
                                        window.location = response.redirect;
                                    });
                                }
                            },
                            'Rest Day' : {
                                className: 'btn-success ' + hidden,
                                callback: function() {
                                    var form = $('#update-shift-form'),
                                        data = form.serialize(),
                                        url = form.attr('data-shift-delete');
                                    $.post({
                                        url: url, data: data
                                    }).done(function(response) {
                                        response = $.parseJSON(response);
                                        window.location = response.redirect;
                                    });
                                }
                            },
                            'Cancel' : {
                                className: 'btn-default'
                            }
                        }
                    });
                }                
            });
        });
    });

});
</script>
@endcan