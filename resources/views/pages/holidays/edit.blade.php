<form action="{{ route('holidays.edit') }}/{{ $holiday->id }}" method="post" 
    autocomplete="off" id="edit-holiday-form"
>
    {{ csrf_field() }}
    <div class="form-horizontal form-primary">

        <div class="form-group">
            <label for="date" class="col-xs-12 col-sm-3 control-label">Date</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" id="date" name="date" class="form-control datepicker" value="{{ $holiday->date }}" />
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label">Name</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" id="name" name="name" class="form-control" value="{{ $holiday->name }}" />
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last">
            <label for="description" class="col-xs-12 col-sm-3 control-label">Description</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <textarea name="description" id="description" rows="3" class="form-control autosize">{{ $holiday->description }}</textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>
<script>
$(function() {

    autosize($('.autosize'));

    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd'
    });

});
</script>