<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('holidays.add') }}" method="post" autocomplete="off" id="add-holiday-form">
{{ csrf_field() }}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="date" class="col-xs-12 col-sm-3 control-label">Date</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" id="date" name="date" class="form-control datepicker" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-xs-12 col-sm-3 control-label">Name</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" id="name" name="name" class="form-control" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-xs-12 col-sm-3 control-label">Description</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <textarea name="description" id="description" rows="3" class="form-control autosize"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="category" class="col-xs-12 col-sm-3 control-label">Category</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select name="category_id" id="category" class="form-control">
                        @foreach ($categories as $cat)
                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    autosize($('.autosize'));

    $('.datepicker').datepicker({
        dateFormat: 'MM d, yy',
        changeYear: true,
        changeMonth: true,
        yearRange: 'c-2:c+2'
    });

});
</script>