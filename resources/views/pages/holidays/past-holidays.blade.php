@extends('layouts.app')

@section('title', 'Holidays')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Holidays</h3>
                </div>
                {{--*/ $active = ['holidays'] /*--}}
                @include('pages.loa.menu', $active)

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="holidays">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">         

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                @if (count($holidays) > 0 AND $showViewButton)
                <div class="row">
                    <div class="col-xs-12">

                        <div class="listing-header-actions">
                            <a href="{{ route('holidays.dashboard') }}" class="btn btn-sm btn-default">View Upcoming Holidays</a>
                        </div>

                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-xs-12 col-sm-8">                       

                        <div class="holidays-list row">
                            
                            @forelse ($holidays as $holiday)
                            <div class="col-xs-12">
                                <div class="panel">
                                    <div class="panel-body">
                                        <div class="holiday-entry">
                                            <div class="holiday-date">
                                                <em>{{ $holiday->month() }}</em>
                                                <span>{{ $holiday->day() }}</span>
                                            </div>
                                            <div class="holiday-details">
                                                <h3>
                                                    <a href="{{ route('holidays.details', ['holidayId' => $holiday->id]) }}">
                                                        {{ $holiday->name }}
                                                    </a>
                                                </h3>
                                                <span class="label label-primary">
                                                    {{ $holiday->category->name }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @empty
                            <div class="col-xs-12">
                                <div class="no-holiday">
                                    @can ('add_holiday', $currentUser)
                                        <a href="{{ route('holidays.add') }}" class="add-holiday">Add Holiday</a>
                                    @else
                                        <a href="#">No holidays yet.</a>
                                    @endcan
                                </div>
                            </div>
                            @endforelse

                        </div>  

                    </div>

                    <div class="col-xs-12 col-sm-4">

                        <div class="panel">
                            <ul class="list-group">
                                <a href="{{ route('holidays.dashboard') }}" 
                                    <?php if ($activeCat == 0): ?>
                                        class="list-group-item active"
                                    <?php else: ?>
                                        class="list-group-item"
                                    <?php endif; ?>
                                >
                                    All
                                </a>
                                @foreach ($categories as $cat)
                                    <a href="{{ route('holidays.past') }}?cat={{ $cat->id }}" 
                                        <?php if ($activeCat == $cat->id): ?>
                                            class="list-group-item active"
                                        <?php else: ?>
                                            class="list-group-item"
                                        <?php endif; ?>
                                    >
                                        {{ $cat->name }}
                                    </a>
                                @endforeach
                            </ul>
                        </div>
                        
                    </div>
                </div>             

            </div>

        </div>
        </div>
    </div>
</section>

@can ('add_holiday', $currentUser)
<div class="page-actions">
    <ul class="clearfix">
        <li>
            <a href="{{ route('holidays.add') }}" class="pa-primary add-holiday" 
                data-form="#add-holiday-form"
            >
                <i class="fa fa-plus"></i>
            </a>
        </li>
    </ul>
</div>
@endcan
@endsection

@section('footer-addon')
<script>
$(function() {

    

});
</script>
@endsection