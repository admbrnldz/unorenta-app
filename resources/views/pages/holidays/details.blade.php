@extends('layouts.app')

@section('title', $holiday->name)

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $holiday->name }}</h1>
                    <div class="details-page-meta">
                        <ul class="clearfix">
                            <li><i class="fa fa-calendar-o fa-fw"></i> &nbsp; {{ processDate($holiday->date) }}</li>
                            <li><i class="fa fa-comments fa-fw"></i> &nbsp; 
                                <a href="#holiday-discussions-thread">
                                    {{--*/ $commentCount = $holiday->comments->count() /*--}}
                                    {{ $commentCount }} {{ $commentCount == 1 ? 'Comment' : 'Comments' }} 
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('holidays.dashboard') }}">Holidays</a></li>
                        @if ($holiday->date > $today)
                            @can ('edit_holiday', $currentUser)
                            <li>
                                <a href="{{ route('holidays.edit', ['holidayId' => $holiday->id]) }}"
                                    id="edit-holiday" data-target="#edit-holiday-form"
                                >
                                    Edit
                                </a>
                            </li>
                            @endcan
                        @endif
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="holidays">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel">
                            <div class="panel-body">
                                <article>
                                    @if ($holiday->description != '')
                                        {{ $holiday->description }}
                                    @else
                                        No description
                                    @endif
                                </article>
                            </div>
                            <div class="panel-footer">
                                <div id="holiday-discussions-thread" class="post-discussion holiday-discussions">
                                    <h4><i class="fa fa-comments fa-fw"></i> &nbsp; Discussions</h4>
                                    <ul class="post-discussion-thread clearfix">
                                        
                                        @foreach ($holiday->comments as $comment)
                                        <?php
                                            $class = '';
                                            if ($comment->author_id == $currentUser->employee->id) {
                                                $class = 'post-discussion-thread-entry-yours';
                                            }
                                        ?>
                                        <li class="post-discussion-thread-entry {{ $class }} clearfix" id="holiday-comment-{{ $comment->id }}">
                                            <div class="pd-container">
                                                <div class="pd-dp">
                                                    <div class="dp-handler" style="background:url({{ $comment->author->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $comment->author->id]) }}" title="{{ $comment->author->name() }}"></a>
                                                    </div>
                                                </div>
                                                <div class="pd-content-wrap" title="Posted {{ processDate($comment->created_at, true) }}">
                                                    <div class="pd-content">
                                                        {!! $comment->comment !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach

                                    </ul>
                                    <div id="write-comment" class="post-discussion-write-comment">
                                        <form action="{{ route('holidays.comment.add') }}" method="post" id="post-holiday-comment-form">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="holiday_id" value="{{ $holiday->id }}" />
                                        <div class="pd-wc-container">
                                            <div class="pd-wc-dp">
                                                <div class="dp-handler" style="background:url({{ $currentUser->dp() }}) center center no-repeat;background-size:cover;">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}" title="You"></a>
                                                </div>
                                            </div>
                                            <div class="pd-wc-textbox">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <textarea name="comment" id="comment" rows="3" class="form-control"></textarea>
                                                        <div class="write-comment-post-button">
                                                            <button class="btn btn-primary btn-sm" 
                                                                id="post-comment" disabled
                                                                data-taget-form="#post-holiday-comment-form"
                                                            >
                                                                Post
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div><!-- / post-discussion -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    var interval = 0,
        currentUserId = {{ $currentUser->id }},
        requestUrl = "{{ route('longpoll.holidaycomments') }}/{{ $holiday->id }}";
    function checkComment(url, timestamp)
    {
        if (timestamp > 0) {
            interval = 1000;
        }
        setTimeout(function() {
            $.ajax({
                type: 'GET',
                url: url + '?ts=' + timestamp,
                async: true,
                success: function(r) {
                    r = $.parseJSON(r);
                    $('.post-discussion-thread').append(r.view);
                    if (r.userId != currentUserId) {
                        $.playSound("{{ asset('assets/tones') }}/" + tone);
                    }
                    checkComment(r.url, r.timestamp);
                },
                error: function(jqXhr, exception) {
                    checkComment(requestUrl, timestamp);  
                }
            });
        }, interval);
    }
    checkComment(requestUrl, 0);

    $('#post-comment').on('click', function(e) {
        e.preventDefault();
        $(this).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;Post');
        $('#post-comment').prop('disabled', true);

        if ($('#comment').val() == '') {
            $(this).html('<i class="fa fa-close fa-fw"></i>&nbsp;Post');
            $(this).removeClass('btn-primary').addClass('btn-danger');
            return false;
        }

        var form = $(this).attr('data-taget-form'),
            url = $(form).attr('action'),
            data = $(form).serialize();
        $.post({
            url: url,
            data: data
        }).done(function(r) {
            r = $.parseJSON(r);
            $('#post-comment').find('.fa').removeClass('fa-circle-o-notch fa-spin')
                .addClass('fa-check');
            $('#comment').val('');
            setTimeout(function() {
                $('#post-comment').html('Post');
                $(this).addClass('btn-primary').removeClass('btn-danger');
            }, 3000);
        });
    });

    $('#edit-holiday').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target');
        $.get(url).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Update',
                        className: 'btn-primary',
                        callback: function() {
                            $(targetForm).submit();
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

});
</script>
@endsection