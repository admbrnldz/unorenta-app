@extends('layouts.app')

@section('title', 'Payslip Timecards')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $payslip->employee->name() }}</h1>
                    <div class="details-page-meta">
                        <ul class="clearfix">
                            <li><i class="fa fa-code-fork fa-fw"></i> &nbsp; {{ sprintf('%05d', $payslip->payrollBatch->id) }}</li>
                            <li><i class="fa fa-calendar fa-fw"></i> &nbsp; {{ formatDate($payslip->payrollBatch->from) }} - {{ formatDate($payslip->payrollBatch->to) }}</li>
                            <li><i class="fa fa-money fa-fw"></i> &nbsp; P {{ number_format($payslip->getNetPay(), 2, '.', ',') }}</li>
                        </ul>
                    </div>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('payslips.details') }}/{{ $payslip->id }}">Payslip</a></li>
                        <li class="active"><a href="{{ route('payslips.timecards') }}/{{ $payslip->id }}">Timecards</a></li>
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="holidays">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">

                        <div class="panel">
                            <div class="panel-body">

                                {{--*/ $totalHours = 0 /*--}}
                                {{--*/ $totalLate = 0 /*--}}
                                {{--*/ $totalUndertime = 0 /*--}}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Time In</th>
                                                <th>Time Out</th>
                                                <th style="width:80px;">Hours</th>
                                                <th title="Late" style="width:80px;">L</th>
                                                <th title="Undertime" style="width:80px;">U</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($payslip->timecards as $tc)
                                            <tr>
                                                <td>{{ processDate($tc->time_in, true) }}</td>
                                                <td>{{ processDate($tc->time_out, true) }}</td>
                                                <td>
                                                    @if ($tc->totalHoursWorked() >= 8)
                                                        8
                                                        {{--*/ $totalHours += 8 /*--}}
                                                    @else
                                                        {{ $tc->totalHoursWorked() }}
                                                        {{--*/ $totalHours += $tc->totalHoursWorked() /*--}}
                                                    @endif                                                    
                                                </td>
                                                <td>{{ $tc->late_minutes }}</td>
                                                <td>{{ $tc->undertime_minutes }}</td>
                                            </tr>                                            
                                            {{--*/ $totalLate += $tc->late_minutes /*--}}
                                            {{--*/ $totalUndertime += $tc->undertime_minutes /*--}}
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr class="total">
                                                <td colspan="2"><strong>Total</strong></td>
                                                <td><strong>{{ $totalHours }}h</strong></td>
                                                <td><strong>{{ $totalLate }}m</strong></td>
                                                <td><strong>{{ $totalUndertime }}m</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

   

});
</script>
@endsection