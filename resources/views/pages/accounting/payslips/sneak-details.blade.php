<div class="sneak-payslip-details">
    
    <div class="sneak-meta">
        <h5>
            <span class="label label-primary">P {{ number_format($payslip->getNetPay(), 2, '.', ',') }}</span>
        </h5>
        <h1>{{ $payslip->employee->name() }}</h1>
        <h4>
            {{ getMonths()[$payslip->payrollBatch->month - 1] }} {{ $payslip->payrollBatch->year }} / {{ $payslip->payrollBatch->half }}{{ $payslip->payrollBatch->half == 1 ? 'st' : 'nd' }} Half
        </h4>        
    </div>

    <div class="sneak-tabbing">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#payslip-details" aria-controls="payslip-details" role="tab" data-toggle="tab">Payslip</a></li>
            <li role="presentation"><a href="#payslip-timecards" aria-controls="payslip-timecards" role="tab" data-toggle="tab">Timecards</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="payslip-details">

                <div class="table-responsive">
                    <table class="table table-payslip">
                        <thead>
                            <tr>
                                <th colspan="3">Taxable Income</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>Basic Pay</strong></td>
                                <td class="ta-right">{{ number_format(round($payslip->basic_pay, 2), 2, '.', ',') }}</td>
                                <td style="width:160px;"></td>
                            </tr>
                            @if ($payslip->ndPayment() > 0)
                                <tr>
                                    <td>Night Differential</td>
                                    <td class="ta-right">{{ number_format(round($payslip->ndPayment(), 2), 2, '.', ',') }}</td>
                                    <td style="width:160px;"></td>
                                </tr>
                            @endif
                            @if ($payslip->rdothPayment() > 0)
                                <tr>
                                    <td>RD/OT/Holiday Pay</td>
                                    <td class="ta-right">{{ number_format(round($payslip->rdothPayment(), 2), 2, '.', ',') }}</td>
                                    <td style="width:160px;"></td>
                                </tr>
                            @endif
                            @if ($payslip->leaveWithPayDaysCount() > 0)
                                <tr>
                                    <td>LWP ({{ $payslip->leaveWithPayDaysCount() }})</td>
                                    <td class="ta-right">{{ number_format(round($payslip->leaveWithPayAmount(), 2), 2, '.', ',') }}</td>
                                    <td style="width:160px;"></td>
                                </tr>
                            @endif
                            <tr class="sub-total">
                                <td><strong>Gross Pay</strong></td>
                                <td colspan="2" class="ta-right">
                                    <strong>{{ number_format(round($payslip->getGrossPay(), 2), 2, '.', ',') }}</strong><br />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-payslip">
                        <thead>
                            <tr>
                                <th colspan="3">Less: Deductions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payslip->deductions as $deduction)
                                @if ($deduction->deduction_type == 1)
                                    <tr>
                                        <td>{{ $deduction->name }}</td>
                                        <td class="ta-right">{{ number_format(round($deduction->amount, 2), 2, '.', ',') }}</td>
                                        <td style="width:160px;"></td>
                                    </tr>
                                @endif
                            @endforeach
                            @if ($payslip->ltDeduction() > 0)
                                <tr>
                                    <td>Late/Tardiness</td>
                                    <td class="ta-right">{{ number_format(round($payslip->ltDeduction(), 2), 2, '.', ',') }}</td>
                                    <td style="width:160px;"></td>
                                </tr>
                            @endif
                            @if ($payslip->abDeduction() > 0)
                                <tr>
                                    <td>Absences</td>
                                    <td class="ta-right">{{ number_format(round($payslip->abDeduction(), 2), 2, '.', ',') }}</td>
                                    <td style="width:160px;"></td>
                                </tr>
                            @endif
                            @if ($payslip->utDeduction() > 0)
                                <tr>
                                    <td>Undertime</td>
                                    <td class="ta-right">{{ number_format(round($payslip->utDeduction(), 2), 2, '.', ',') }}</td>
                                    <td style="width:160px;"></td>
                                </tr>
                            @endif
                            @if ($payslip->leaveWithoutPayDaysCount() > 0)
                                <tr>
                                    <td>LWOP ({{ $payslip->leaveWithoutPayDaysCount() }})</td>
                                    <td class="ta-right">{{ number_format(round($payslip->leaveWithoutPayAmount(), 2), 2, '.', ',') }}</td>
                                    <td style="width:160px;"></td>
                                </tr>
                            @endif
                            <tr class="sub-total">
                                <td><strong>Total Deductions Before Tax</strong></td>
                                <td colspan="2" class="ta-right">
                                    <strong>{{ number_format(round($payslip->getDeductionsBeforeTax(), 2), 2, '.', ',') }}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-payslip">
                        <tbody>
                            <tr class="semi-total">
                                <td><strong>Total Taxable Income</strong></td>
                                <td colspan="2" class="ta-right">
                                    <strong>{{ number_format(round($payslip->getTaxableIncome(), 2), 2, '.', ',') }}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-payslip">
                        <thead>
                            <tr>
                                <th colspan="3">Deductions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Withholding Tax</td>
                                <td class="ta-right">{{ number_format(round($payslip->getWithholdingTax(), 2), 2, '.', ',') }}</td>
                                <td style="width:160px;"></td>
                            </tr>
                            @foreach ($payslip->deductions as $deduction)
                                @if ($deduction->deduction_type == 2)
                                    <tr>
                                        <td>
                                            {{ $deduction->name }} &nbsp; &mdash; &nbsp; 
                                            @if ($deduction->scheduled_deduction_id != null)
                                                <em>
                                                    {{--*/ $count = $deduction->scheduledDeduction->months_payable /*--}}
                                                    @if ($deduction->scheduledDeduction->term == 'semi-monthly')
                                                        {{--*/ $count = $deduction->scheduledDeduction->months_payable * 2 /*--}}
                                                    @endif
                                                    {{ $deduction->scheduledDeduction->payments()->count() }} of {{ $count }}
                                                </em>
                                            @endif
                                        </td>
                                        <td class="ta-right">{{ number_format(round($deduction->amount, 2), 2, '.', ',') }}</td>
                                        <td style="width:160px;"></td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr class="sub-total">
                                <td><strong>Total Deductions</strong></td>
                                <td colspan="2" class="ta-right">
                                    <strong>{{ number_format(round($payslip->getTotalDeductionsAfterTax(), 2), 2, '.', ',') }}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive">
                    <table class="table table-payslip" style="margin-bottom:0;">
                        <tfoot>
                            <tr class="total">
                                <td><strong>Net Pay</strong></td>
                                <td colspan="2" class="ta-right">
                                    <strong>{{ number_format(round($payslip->getNetPay(), 2), 2, '.', ',') }}</strong>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="payslip-timecards">

                {{--*/ $totalHours = 0 /*--}}
                {{--*/ $totalLate = 0 /*--}}
                {{--*/ $totalUndertime = 0 /*--}}
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Time In</th>
                                <th>Time Out</th>
                                <th title="Late" style="width:80px;">L</th>
                                <th title="Undertime" style="width:80px;">U</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payslip->timecards as $tc)
                            <tr>
                                <td>{{ processDate($tc->time_in, true) }}</td>
                                <td>{{ processDate($tc->time_out, true) }}</td>
                                <td>{{ $tc->late_minutes }}</td>
                                <td>{{ $tc->undertime_minutes }}</td>
                            </tr>                                            
                            {{--*/ $totalLate += $tc->late_minutes /*--}}
                            {{--*/ $totalUndertime += $tc->undertime_minutes /*--}}
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr class="total">
                                <td colspan="2"><strong>Total</strong></td>
                                <td><strong>{{ $totalLate }}m</strong></td>
                                <td><strong>{{ $totalUndertime }}m</strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>

    </div>

</div>