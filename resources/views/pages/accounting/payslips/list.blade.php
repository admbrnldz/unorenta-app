@extends('layouts.app')

@section('title', 'Payslips')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Your Payslips</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active"><a href="{{ route('payslips') }}">Your Payslips</a></li>
                        <li><a href="{{ route('deductions') }}">Your Scheduled Deductions</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Batch ID</th>
                                                <th>Period</th>
                                                <th style="width:90px;">LOA<em>*</em></th>
                                                <th style="width:60px;">LT<em>*</em></th>
                                                <th style="width:60px;">AB<em>*</em></th>
                                                <th style="width:60px;">UT<em>*</em></th>
                                                <th style="width:60px;">ND<em>*</em></th>
                                                <th style="width:60px;">RD/OT<em>*</em></th>
                                                <th style="width:60px;">SH<em>*</em></th>
                                                <th style="width:60px;">SHRD<em>*</em></th>
                                                <th style="width:60px;">RH<em>*</em></th>
                                                <th style="width:60px;">RHRD<em>*</em></th>
                                                <th style="width:60px;">DH<em>*</em></th>
                                                <th style="width:60px;">DHRD<em>*</em></th>
                                                <th class="ta-right">Net Pay</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{--*/ $total = 0 /*--}}
                                            @forelse ($payslips as $ps)
                                            <tr>
                                                <td class="table-sneak">
                                                    <a href="{{ route('payslips.details') }}/{{ $ps->id }}" class="sneak">
                                                        <i class="fa fa-dedent fa-fw"></i>
                                                    </a>
                                                </td>
                                                <td>{{ sprintf('%05d', $ps->payrollBatch->id) }}</td>
                                                <td>
                                                    <strong>{{ getMonths()[$ps->payrollBatch->month - 1] }} {{ $ps->payrollBatch->year }} / {{ $ps->payrollBatch->half }}{{ $ps->payrollBatch->half == 1 ? 'st' : 'nd' }} Half</strong>
                                                    <br />
                                                    <em>{{ $ps->payrollBatch->dateCovered() }}</em>
                                                </td>
                                                <td>
                                                    WP: {{ $ps->leaveWithPayDaysCount() }}<br />
                                                    WOP: {{ $ps->leaveWithoutPayDaysCount() }}
                                                </td>
                                                <td>{{ $ps->lt }}</td>
                                                <td>{{ $ps->ab }}</td>
                                                <td>{{ $ps->ut }}</td>
                                                <td>{{ $ps->nd }}</td>
                                                <td>{{ $ps->rdot }}</td>
                                                <td>{{ $ps->sh }}</td>
                                                <td>{{ $ps->shrd }}</td>
                                                <td>{{ $ps->rh }}</td>
                                                <td>{{ $ps->rhrd }}</td>
                                                <td>{{ $ps->dh }}</td>
                                                <td>{{ $ps->dhrd }}</td>
                                                <td class="ta-right">P {{ number_format(round($ps->getNetPay(), 2), 2, '.', ',') }}</td>
                                            </tr>
                                            {{--*/ $total += $ps->getNetPay() /*--}}
                                            @empty
                                            <tr>
                                                <td colspan="15" class="ta-center">No payslips yet</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                        @if (count($payslips) > 0)
                                        <tfoot>
                                            <tr class="total">
                                                <td colspan="15" class="table-highlight">
                                                    Total Pay
                                                </td>
                                                <td class="table-highlight ta-right">
                                                    P {{ number_format($total, 2, '.', ',') }}
                                                </td>
                                            </tr>
                                        </tfoot>
                                        @endif
                                    </table>
                                    <p style="padding:12px 28px 0 28px;margin-bottom:0;">
                                        <small><i><span style="color: #00a8a0;">*</span> in minutes</i></small>
                                    </p>
                                    <div class="legend clearfix">
                                        <em>*</em> LOA - Leave of Absence, <em>*</em> LT - Late/Tardiness, <em>*</em> AB - Absences, <em>*</em> UT - Undertime, <em>*</em> ND - Night Differential, <em>*</em> RD/OT - Rest Day/Overtime, <em>*</em> SH - Special Holiday, <em>*</em> SHRD - Special Holiday + Rest Day/Overtime, <em>*</em> RH - Rest Holiday, <em>*</em> RHRD - Rest Holiday + Rest Day/Overtime, <em>*</em> DH - Double Holiday, <em>*</em> DHRD - Double Holiday + Rest Day/Overtime
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection