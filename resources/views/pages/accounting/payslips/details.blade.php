@extends('layouts.app')

@section('title', 'Payslip Details')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $payslip->employee->name() }}</h1>
                    <div class="details-page-meta">
                        <ul class="clearfix">
                            <li><i class="fa fa-code-fork fa-fw"></i> &nbsp; {{ sprintf('%05d', $payslip->payrollBatch->id) }}</li>
                            <li><i class="fa fa-calendar fa-fw"></i> &nbsp; {{ formatDate($payslip->payrollBatch->from) }} - {{ formatDate($payslip->payrollBatch->to) }}</li>
                            <li><i class="fa fa-money fa-fw"></i> &nbsp; P {{ number_format($payslip->getNetPay(), 2, '.', ',') }}</li>
                        </ul>
                    </div>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active"><a href="{{ route('payslips.details') }}/{{ $payslip->id }}">Payslip</a></li>
                        <li><a href="{{ route('payslips.timecards') }}/{{ $payslip->id }}">Timecards</a></li>
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="holidays">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">

                        <div class="panel">
                            <div class="panel-body panel-payslip">

                                {{--*/ $grossPay = 0 /*--}}
                                <div class="table-responsive">
                                    <table class="table table-payslip">
                                        <thead>
                                            <tr>
                                                <th colspan="3">Taxable Income</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><strong>Basic Pay</strong></td>
                                                <td class="ta-right">{{ number_format(round($payslip->basic_pay, 2), 2, '.', ',') }}</td>
                                                <td style="width:160px;"></td>
                                                {{--*/ $grossPay += $payslip->basic_pay /*--}}
                                            </tr>
                                            @if ($payslip->nd > 0)
                                                {{--*/ $ndPay = ($payslip->nd / 60) * $payslip->ratePerHour() /*--}}
                                                <tr>
                                                    <td>Night Differential</td>
                                                    <td class="ta-right">{{ number_format(round($ndPay, 2), 2, '.', ',') }}</td>
                                                    <td style="width:160px;"></td>
                                                    {{--*/ $grossPay += $ndPay /*--}}
                                                </tr>
                                            @endif
                                            <tr>
                                                <td>RD/OT/Holiday Pay</td>
                                                <td></td>
                                                <td style="width:160px;"></td>
                                            </tr>
                                            @if (count($payslip->holidaysCovered()) > 0)
                                                {{--*/ $minutes = 0 /*--}}
                                                @foreach ($payslip->holidaysCovered() as $h)
                                                    {{--*/ $minutes += $h /*--}}
                                                @endforeach
                                                <tr>
                                                    <td>Holiday</td>
                                                    <td class="ta-right">{{ number_format(round(calculateHolidayPay($minutes, $payslip->ratePerHour()), 2), 2, '.', ',') }}</td>
                                                    <td style="width:160px;"></td>
                                                    {{--*/ $grossPay += $payslip->holiday() /*--}}
                                                </tr>
                                            @endif
                                            <tr class="sub-total">
                                                <td><strong>Gross Pay</strong></td>
                                                <td colspan="2" class="ta-right">
                                                    <strong>{{ number_format(round($grossPay, 2), 2, '.', ',') }}</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                {{--*/ $deductionsBeforeTax = 0 /*--}}
                                <div class="table-responsive">
                                    <table class="table table-payslip">
                                        <thead>
                                            <tr>
                                                <th colspan="3">Less: Deductions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($payslip->lateMinutes() > 0)
                                                <tr>
                                                    <td>Late/Tardiness</td>
                                                    <td class="ta-right">{{ number_format(round($payslip->late(), 2), 2, '.', ',') }}</td>
                                                    <td style="width:160px;"></td>
                                                    {{--*/ $deductionsBeforeTax += $payslip->late() /*--}}
                                                </tr>
                                            @endif
                                            @if ($payslip->undertimeMinutes() > 0)
                                                <tr>
                                                    <td>Undertime</td>
                                                    <td class="ta-right">{{ number_format(round($payslip->undertime(), 2), 2, '.', ',') }}</td>
                                                    <td style="width:160px;"></td>
                                                    {{--*/ $deductionsBeforeTax += 0 /*--}}
                                                </tr>
                                            @endif
                                            @foreach ($payslip->deductions as $deduction)
                                                @if ($deduction->deduction_type == 1)
                                                    <tr>
                                                        <td>{{ $deduction->name }}</td>
                                                        <td class="ta-right">{{ number_format(round($deduction->amount, 2), 2, '.', ',') }}</td>
                                                        <td style="width:160px;"></td>
                                                        {{--*/ $deductionsBeforeTax += $deduction->amount /*--}}
                                                    </tr>
                                                @endif
                                            @endforeach
                                            <tr class="sub-total">
                                                <td><strong>Total Deductions Before Tax</strong></td>
                                                <td colspan="2" class="ta-right">
                                                    <strong>{{ number_format(round($deductionsBeforeTax, 2), 2, '.', ',') }}</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                {{--*/ $taxableIncome = $grossPay - $deductionsBeforeTax /*--}}
                                <div class="table-responsive">
                                    <table class="table table-payslip">
                                        <tbody>
                                            <tr class="semi-total">
                                                <td><strong>Total Taxable Income</strong></td>
                                                <td colspan="2" class="ta-right">
                                                    <strong>{{ number_format(round($taxableIncome, 2), 2, '.', ',') }}</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                {{--*/ $deductionsAfterTax = 0 /*--}}
                                <div class="table-responsive">
                                    <table class="table table-payslip">
                                        <thead>
                                            <tr>
                                                <th colspan="3">Deductions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Withholding Tax</td>
                                                <td class="ta-right">{{ number_format(round($payslip->getWithholdingTax(), 2), 2, '.', ',') }}</td>
                                                <td style="width:160px;"></td>
                                                {{--*/ $deductionsAfterTax += $payslip->getWithholdingTax() /*--}}
                                            </tr>
                                            @foreach ($payslip->deductions as $deduction)
                                                @if ($deduction->deduction_type == 2)
                                                    <tr>
                                                        <td>
                                                            {{ $deduction->name }} &nbsp; &mdash; &nbsp; 
                                                            @if ($deduction->scheduled_deduction_id != null)
                                                                <em>
                                                                    {{ $deduction->scheduledDeduction->payments()->count() }} of {{ $deduction->scheduledDeduction->payment_count }}
                                                                </em>
                                                            @endif
                                                        </td>
                                                        <td class="ta-right">{{ number_format(round($deduction->amount, 2), 2, '.', ',') }}</td>
                                                        <td style="width:160px;"></td>
                                                        {{--*/ $deductionsAfterTax += $deduction->amount /*--}}
                                                    </tr>
                                                @endif
                                            @endforeach
                                            <tr class="sub-total">
                                                <td><strong>Total Deductions</strong></td>
                                                <td colspan="2" class="ta-right">
                                                    <strong>{{ number_format(round($deductionsAfterTax, 2), 2, '.', ',') }}</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                {{--*/ $netPay = $taxableIncome - $deductionsAfterTax /*--}}
                                <div class="table-responsive">
                                    <table class="table table-payslip">
                                        <tfoot>
                                            <tr class="total">
                                                <td><strong>Net Pay</strong></td>
                                                <td colspan="2" class="ta-right">
                                                    <strong>{{ number_format(round($netPay, 2), 2, '.', ',') }}</strong>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

   

});
</script>
@endsection