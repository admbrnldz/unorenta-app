<div class="sneak-deduction">
    
    <div class="sneak-meta">
        <h5>
            <span class="label label-primary">P {{ number_format($deduction->amount, 2, '.', ',') }}</span>
        </h5>
        <h1>{{ $deduction->name }}</h1>
        <h4>
            <a href="{{ route('employees.profile', ['employeeId' => $deduction->employee->id]) }}">
                {{ $deduction->employee->name() }}
            </a>
        </h4>        
    </div>

    <div class="sneak-tabbing">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#deductions-details" aria-controls="payslip-details" role="tab" data-toggle="tab">Payments</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="deductions-details">

                {{--*/ $total = 0 /*--}}
                {{--*/ $balance = $deduction->amount /*--}}
                <table class="table">
                    <thead>
                        <tr>
                            <td style="width:80px;">#</td>
                            <th>Payslip</th>
                            <th class="ta-right">Amount</th>
                            <th class="ta-right">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{--*/ $count = 1 /*--}}
                        @forelse ($deduction->payments as $payment)
                            <tr>
                                <td>{{ $count }}</td>
                                <td>
                                    {{ formatDate($payment->payslip->created_at, true) }}
                                </td>
                                <td class="ta-right">{{ number_format($payment->amount, 2, '.', ',') }}</td>
                                <td class="ta-right"></td>
                                {{--*/ $total += $payment->amount /*--}}
                                {{--*/ $count++ /*--}}
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="ta-center">No payments made yet</td>
                            </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        @if ($total > 0)
                        <tr class="total">
                            <td colspan="2">Total Amount Paid</td>
                            <td class="ta-right">P {{ number_format($total, 2, '.', ',') }}</td>
                            <td class="ta-right"></td>
                        </tr>
                        @endif
                        <tr class="sub-total">
                            <td colspan="2">Remaining Balance</td>
                            <td class="ta-right">&nbsp;</td>
                            <td class="ta-right">P {{ number_format($balance - $total, 2, '.', ',') }}</td>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>

    </div>

</div>