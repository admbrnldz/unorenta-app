@extends('layouts.app')

@section('title', 'Scheduled Deductions')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Your Scheduled Deductions</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('payslips') }}">Your Payslips</a></li>
                        <li class="active"><a href="{{ route('deductions') }}">Your Scheduled Deductions</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Deduction</th>
                                                <th class="ta-right">Amount</th>
                                                <th>Payment Count / Term</th>
                                                <th class="ta-right">Per Payment</th>
                                                <th class="ta-right">Payments Made</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($deductions as $ded)
                                                {{--*/ $class = '' /*--}}
                                                @if ($ded->paid())
                                                    {{--*/ $class = 'success' /*--}}
                                                @endif
                                                <tr class="{{ $class }}">
                                                    <td class="table-sneak">
                                                        <a href="{{ route('payroll.deductions.detils') }}/{{ $ded->id }}" class="sneak">
                                                            <i class="fa fa-dedent fa-fw"></i>
                                                        </a>
                                                    </td>
                                                    <td class="table-highlight">{{ $ded->name }}</td>
                                                    <td class="ta-right">P {{ $ded->amount }}</td>
                                                    <td>
                                                        {{ $ded->months_payable }} {{ $ded->months_payable == 1 ? 'month' : 'months'}} to pay
                                                        @if ($ded->term == 'monthly')
                                                            / {{ $ded->term }} : {{ $ded->half}}half
                                                        @else
                                                            / {{ $ded->term }}
                                                        @endif
                                                    </td>
                                                    <td class="ta-right">
                                                        @if ($ded->term == 'monthly')
                                                            P {{ $ded->amount / $ded->months_payable }}
                                                        @else
                                                            P {{ $ded->amount / ($ded->months_payable * 2) }} 
                                                        @endif
                                                    </td>
                                                    <td class="ta-right">{{ $ded->payments()->count() }}</td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="8" class="ta-center">No deductions yet</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection