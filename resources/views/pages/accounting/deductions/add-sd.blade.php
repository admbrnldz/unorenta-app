<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>

<form action="{{ route('payroll.deductions.add') }}" method="post" autocomplete="off" id="add-scheduled-deductions-form">
{!! csrf_field() !!}

    <div class="form-horizontal form-primary">

        <div class="form-group">
            <label for="employees" class="col-xs-12">Employees</label>
            <div class="col-xs-12">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <select id="employees" name="employees[]" multiple="multiple" class="form-control">
                            @foreach ($employees as $employee)
                                <option value="{{ $employee->id }}">{{ $employee->name() }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label">Name</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" name="name" id="name" class="form-control" placeholder="e.g. Vaccine" />
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="amount" class="col-xs-12 col-sm-3 control-label">Amount</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="number" name="amount" id="amount" class="form-control" />
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="months-payable" class="col-xs-12 col-sm-3 control-label">Months to pay</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="number" name="months_payable" id="months-payable" class="form-control" value="1" />
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="before_tax" class="col-xs-12 col-sm-3 control-label">Before Tax</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="before_tax" value="1" />
                                <span>Check if yes</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="term" class="col-xs-12 col-sm-3 control-label">Term</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <div class="radio">
                            <label>
                                <input type="radio" name="term" value="semi-monthly" class="term" checked />
                                <span>Semi-Monthly</span>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="term" value="monthly" class="term" />
                                <span>Monthly</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="semi-options" class="form-group hidden">
            <label for="half" class="col-xs-12 col-sm-3 control-label">Half</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <div class="radio">
                            <label>
                                <input type="radio" name="half" value="1" />
                                <span>First Half</span>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="half" value="2" />
                                <span>Second Half</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last">
            <label for="amount-per-payment" id="app-label" class="col-xs-12 col-sm-3 control-label">Per payroll</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" name="amount_per_payment" id="amount-per-payment" class="form-control" readonly />
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</form>
<script>
var count = '',
    amount = '';
$(function() {

    $('#employees').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5
    });

    $('#amount, #months-payable').on('keyup', function() {
        count = $('#months-payable').val();
        amount = $('#amount').val();
        if ($('#amount').val() != '' && $('#months-payable').val() != '') {
            var term = '';
            $('.term').each(function() {
                if ($(this).prop('checked')) {
                    term = $(this).val();
                }
            });
            if (term == 'semi-monthly') {
                $('#amount-per-payment').val(amount / (count * 2));
            } else if (term == 'monthly') {
                $('#amount-per-payment').val(amount / count);
            }
        }
    });

    $('.term').each(function() {
        $(this).on('change', function() {
            if ($(this).val() == 'monthly') {
                $('#app-label').html('Per month');
                $('#semi-options').removeClass('hidden');
                $('#amount-per-payment').val(amount / count);
            } else if ($(this).val() == 'semi-monthly') {
                $('#app-label').html('Per payroll');
                $('#semi-options').addClass('hidden');
                $('#amount-per-payment').val(amount / (count * 2));
                var opts = $('#semi-options').find('input[type="radio"]');
                opts.each(function() {
                    $(this).prop('checked', false);
                })
            }
        });
    });

});
</script>