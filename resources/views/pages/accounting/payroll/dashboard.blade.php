@extends('layouts.app')

@section('title', 'Applicants')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Payroll Batch</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active">
                            <a href="#">Payroll Batch</a>
                        </li>
                        <li><a href="{{ route('payroll.generate') }}">Generate Payroll</a></li>
                        <li><a href="#">Payroll Inputs</a></li>
                        <li><a href="#">Mandatory Deductions</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Batch Number</th>
                                                <th>Payroll Batch</th>
                                                <th>Employees Paid</th>
                                                <th>Total Pay</th>
                                                <th class="ta-right">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i=0; $i < 5; $i++): ?>
                                            <tr>
                                                <td>{{ printf('%04d', rand(4, 20)) }}</td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('payroll.batch.details') }}/1">
                                                        August 24 - September 10
                                                    </a>
                                                </td>
                                                <td>{{ rand(30, 50) }}</td>
                                                <td>P 76,{{ rand(100, 999) }}.00</td>
                                                <td class="ta-right">
                                                    <a href="{{ route('payroll.batch.details') }}/1">
                                                        <i class="fa fa-file-text-o fa-fw"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endfor; ?>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection