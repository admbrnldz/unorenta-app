@extends('layouts.app')

@section('title', 'Payroll Batches')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Payroll Batch</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active">
                            <a href="{{ route('payroll.batch') }}">Payroll Batch</a>
                        </li>
                        <li><a href="{{ route('payroll.generate') }}">Generate Payslip</a></li>
                        <li><a href="{{ route('payroll.deductions') }}">Other Deductions</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Batch Number</th>
                                                <th>Payroll Batch</th>
                                                <th>Period</th>
                                                <th>Employees Paid</th>
                                                <th class="ta-right">Total Pay</th>
                                                <th class="ta-right">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($payrollBatches as $pb)
                                            <tr>
                                                <td>{{ sprintf('%05d', $pb->id) }}</td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('payroll.batch.details') }}/{{ $pb->id }}">
                                                        {{ getMonths()[$pb->month - 1] }} {{ $pb->year }} / {{ $pb->half }}{{ $pb->half == 1 ? 'st' : 'nd' }} Half
                                                    </a>
                                                </td>
                                                <td>{{ $pb->dateCovered() }}</td>
                                                <td>{{ $pb->payslips()->count() }}</td>
                                                <td class="ta-right">P {{ number_format($pb->totalPay(), 2, '.', ',') }}</td>
                                                <td class="ta-right">
                                                    <a href="{{ route('payroll.batch.details') }}/{{ $pb->id }}">
                                                        <i class="fa fa-file-text-o fa-fw"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="5" class="ta-center">No payroll batches</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection