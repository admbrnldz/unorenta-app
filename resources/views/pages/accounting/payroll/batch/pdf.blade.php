<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $filename }}</title>
    <style>
    body {
        font-family: 'Helvetica';
        line-height: 1.5;
        font-size: 7pt;
    }
    .page-break {
        page-break-after: always;
    }
    .table-entry {
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 5pt;
    }
    .table-entry tr td {
        border:.2pt solid #000000;
        padding: 5pt;
        vertical-align: top;
    }
    .inner-table {
        width: 100%;
        border-collapse: collapse;
    }
    .inner-td {
        border:0!important;
        padding-bottom: 2pt!important;
        padding-left: 0!important;
        padding-top: 2pt!important;
        padding-right: 0!important;
        line-height: 10pt;
    }
    .ta-right {
        text-align: right;
    }
    .page-break {
        page-break-after: always!important;
    }
    .fw-700 {
        font-weight: 700;
    }
    span {
        padding: 0;
    }
    .inner-td h3 {
        margin: 0 0 2pt 0;
        padding: 0;
        line-height: 1.3;
    }
    </style>
</head>
<body>
    
    {{--*/ $count = 1; /*--}}
    @foreach ($payslips as $payslip)
    <table class="table-entry">
        <tr>
            <td colspan="3" style="background-color:#eeeeee;">
                <table class="inner-table"> 
                    <tr>
                        <td class="inner-td" style="width: 50pt;">
                            <span>{{ $payslip->employee->employee_uid }}</span>
                        </td>
                        <td class="inner-td">
                            <h3 class="fw-700">{{ $payslip->employee->name(true) }}</h3>
                            {{ $payslip->employee->position->name }}
                        </td>
                        <td class="inner-td ta-right">
                            <span>{{ $payslip->payrollBatch->dateCovered() }}</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:33%;">
                <table class="inner-table">
                    <tr>
                        <td class="inner-td">Basic Pay</td>
                        <td class="inner-td ta-right">P {{ number_format(round($payslip->basic_pay, 2), 2, '.', ',') }}</td>
                    </tr>
                    @if ($payslip->ndPayment() > 0)
                    <tr>
                        <td class="inner-td">Night Differential</td>
                        <td class="inner-td ta-right">{{ number_format(round($payslip->ndPayment(), 2), 2, '.', ',') }}</td>
                    </tr>
                    @endif
                    @if ($payslip->rdothPayment() > 0)
                    <tr>
                        <td class="inner-td">RD/OT/Holiday Pay</td>
                        <td class="inner-td ta-right">{{ number_format(round($payslip->rdothPayment(), 2), 2, '.', ',') }}</td>
                    </tr>
                    @endif
                    @if ($payslip->leaveWithPayDaysCount() > 0)
                        <tr>
                            <td class="inner-td">LWP ({{ $payslip->leaveWithPayDaysCount() }})</td>
                            <td class="inner-td ta-right">
                                {{ number_format(round($payslip->leaveWithPayAmount(), 2), 2, '.', ',') }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td class="inner-td" colspan="2" style="border-top:.2pt dashed #000000!important;"></td>
                    </tr>
                    <tr>
                        <td class="inner-td">Gross Pay</td>
                        <td class="inner-td ta-right">P {{ number_format(round($payslip->getGrossPay(), 2), 2, '.', ',') }}</td>
                    </tr>
                </table>                
            </td>
            <td style="width:33%;">
                <table class="inner-table">
                    @foreach ($payslip->deductions as $deduction)
                        @if ($deduction->deduction_type == 1)
                            <tr>
                                <td class="inner-td">{{ $deduction->name }}</td>
                                <td class="inner-td ta-right">P {{ number_format(round($deduction->amount, 2), 2, '.', ',') }}</td>
                            </tr>
                        @endif
                    @endforeach
                    @if ($payslip->ltDeduction() > 0)
                        <tr>
                            <td class="inner-td">Late/Tardiness</td>
                            <td class="inner-td ta-right">{{ number_format(round($payslip->ltDeduction(), 2), 2, '.', ',') }}</td>
                        </tr>
                    @endif
                    @if ($payslip->utDeduction() > 0)
                        <tr>
                            <td class="inner-td">Undertime/Absences</td>
                            <td class="inner-td ta-right">{{ number_format(round($payslip->utDeduction(), 2), 2, '.', ',') }}</td>
                        </tr>
                    @endif
                    @if ($payslip->leaveWithoutPayDaysCount() > 0)
                        <tr>
                            <td class="inner-td">LWOP ({{ $payslip->leaveWithoutPayDaysCount() }})</td>
                            <td class="inner-td ta-right">
                                {{ number_format(round($payslip->leaveWithoutPayAmount(), 2), 2, '.', ',') }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td class="inner-td" colspan="2" style="border-top:.2pt dashed #000000!important;"></td>
                    </tr>
                    <tr>
                        <td class="inner-td">Total Deductions Before Tax</td>
                        <td class="inner-td ta-right">P {{ number_format(round($payslip->getDeductionsBeforeTax(), 2), 2, '.', ',') }}</td>
                    </tr>
                </table>
            </td>
            <td style="width:33%;">
                <table class="inner-table">
                    <tr>
                        <td class="inner-td">Taxable Income</td>
                        <td class="inner-td ta-right">P {{ number_format(round($payslip->getTaxableIncome(), 2), 2, '.', ',') }}</td>
                    </tr>
                    <tr>
                        <td class="inner-td">Withholding Tax</td>
                        <td class="inner-td ta-right">{{ number_format(round($payslip->getWithholdingTax(), 2), 2, '.', ',') }}</td>
                    </tr>
                    @foreach ($payslip->deductions as $deduction)
                        @if ($deduction->deduction_type == 2)
                            <tr>
                                <td class="inner-td">
                                    {{ $deduction->name }} &nbsp; &mdash; &nbsp; 
                                    @if ($deduction->scheduled_deduction_id != null)
                                        <em>
                                            {{--*/ $count = $deduction->scheduledDeduction->months_payable /*--}}
                                            @if ($deduction->scheduledDeduction->term == 'semi-monthly')
                                                {{--*/ $count = $deduction->scheduledDeduction->months_payable * 2 /*--}}
                                            @endif
                                            {{ $deduction->scheduledDeduction->payments()->count() }} of {{ $count }}
                                        </em>
                                    @endif
                                </td>
                                <td class="inner-td ta-right">{{ number_format(round($deduction->amount, 2), 2, '.', ',') }}</td>
                            </tr>
                        @endif
                    @endforeach                    
                    <tr>
                        <td class="inner-td" colspan="2" style="border-top:.2pt dashed #000000!important;"></td>
                    </tr>
                    <tr>
                        <td class="inner-td"><strong>Net Pay</strong></td>
                        <td class="inner-td ta-right"><strong>P {{ number_format(round($payslip->getNetPay(), 2), 2, '.', ',') }}</strong></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @if ($count % 5 == 0)
    <div class="page-break"></div>
    @endif
    {{--*/ $count++ /*--}}
    @endforeach    

</body>
</html>