@extends('layouts.app')

@section('title', 'Generate Payslips')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Payroll Batch</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('payroll.batch') }}">Payroll Batch</a></li>
                        <li class="active">
                            <a href="{{ route('payroll.generate') }}">Generate Payslip</a>
                        </li>
                        <li><a href="{{ route('payroll.deductions') }}">Other Deductions</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Employees</h4>
                            </div>
                            <div class="panel-filter">
                                
                                <form action="{{ route('payroll.fetch-timecards') }}" action="get" autocomplete="off" id="fetch-timecards-form">
                                <div class="form-horizontal form-primary">

                                    <div class="form-group form-group-last">
                                        <div class="col-xs-12 col-sm-9 col-md-2">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select name="year" id="year" class="form-control">
                                                        @foreach (getYears() as $year)
                                                            <option value="{{ $year }}"
                                                                <?php if ($year == $today->year): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $year }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-3">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select name="month" id="month" class="form-control">
                                                        @foreach (getMonths() as $key => $month)
                                                            <option value="{{ $key + 1 }}"
                                                                <?php if (($key + 1) == $today->month): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $month }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-2">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select name="half" id="half" class="form-control">
                                                        <option value="1">1st Half</option>
                                                        <option value="2">2nd Half</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-3">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select name="department" id="department" class="form-control">
                                                        <option value="0">All</option>
                                                        @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-2">
                                            <button class="btn btn-primary btn-fw" id="fetch-timecards"
                                                data-form="#fetch-timecards-form"
                                            >
                                                Update List
                                            </button>
                                        </div>
                                    </div>

                                </div>
                                </form>

                            </div>
                            <div id="the-panel" class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table selectlist">
                                        <thead>
                                            <tr>
                                                <th style="width:24px;">
                                                    <input class="checkboxes sl-checkbox-main sl-checkboxes-ctrl" type="checkbox">
                                                </th> 
                                                <th colspan="2">Employee</th>
                                                <th>Position</th>
                                                <th class="ta-right">Timecards</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" class="ta-center">No data</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <div id="the-footer" class="panel-footer hidden">
                                <div class="form-button-group clearfix">
                                    <button class="btn btn-primary" id="generate-payroll">Generate</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
var employees = {},
    batchId = 0,
    year = 0,
    mont = 0,
    half = 0;
$(function() {

    $('#fetch-timecards').on('click', function(e) {
        e.preventDefault();
        loader.show();
        var targetForm = $(this).attr('data-form');
        $.get( $(targetForm).attr('action'), $(targetForm).serialize() ).done(function(r) {
            r = $.parseJSON(r);
            $('#the-panel .table-responsive').html(r.table);
            if (!r.noData) {
                $('#the-footer').removeClass('hidden');
            }            
            $('.selectlist').selectlist();
            loader.hide();
        });
    });

    $('#date-from, #date-to').datepicker({
        dateFormat: 'MM d, yy'
    });

    $('.selectlist').selectlist({
        buttonClass: 'btn btn-primary btn-select-all'
    });

    $('#generate-payroll').on('click', function(e) {
        e.preventDefault();
        year = $('#year').val();
        month = $('#month').val();
        half = $('#half').val();

        var innercount = 0;

        $('.employees-id').each(function() {
            if ($(this).prop('checked') == true) {
                employees[innercount] = $(this).val();
                innercount++;
            }
        });

        if (innercount == 0) {
            bootbox.dialog({
                title: 'Hey!',
                message: '<div class="ta-center">Please select atleast one (1) employee from the list.</div>',
                className: 'modal-warning',
                buttons: {
                    'Close' : {
                        className: 'btn-default'
                    }
                }
            });
            return false;
        }

        loader.showLoading();
        $('.loading-caption').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Generating payroll batch...');
        $('.loading-bar').css({
            'width': '50%'
        });        
        
        // generate payroll batch
        var url = "{{ route('payroll.batch.create') }}",
            data = { 
                year: year,
                month: month,
                half: half,
                firstEmployee: employees[0] 
            };
        $.post( url, data ).done(function(r) {
            r = $.parseJSON(r);
            batchId = r.payrollBatch;           

            $('.loading-caption').html('<i class="fa fa-check fa-fw"></i> Payroll batch successfully created!');
            $('.loading-bar').css({
                'width': '100%'
            });

            setTimeout(function() {
                $('.loading-caption').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Generating payslip of ' + r.firstEmployee + '...');
                $('.loading-bar').css({
                    'width': '0%'
                });
                generate(employees[0], 1);
            }, 750);

        });

    });

});

function generate(employeeId, count) {
    var generateUrl = "{{ route('payroll.generate') }}",
        data = {
            employees: employees,
            employeeId: employeeId,
            year: year,
            month: month,
            half: half,
            batchId: batchId,
            count: count
        };
    setTimeout(function() {
        $.post( generateUrl, data ).done(function(r) {
            r = $.parseJSON(r);
            if (r.done) {
                $('.loading-caption').html('<i class="fa fa-check fa-fw"></i> Done!');
                $('.loading-bar').css({
                    'width': '100%'
                });
                setTimeout(function() {
                    // loader.hide();
                    window.location = r.redirect;
                }, 750);
            } else {
                $('.loading-caption').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Generating payslip of ' + r.employeeName + '...');
                $('.loading-bar').css({
                    'width': r.percent + '%'
                });
                generate(r.next, r.count);
            }
        });
    }, 250);
}

</script>
@endsection