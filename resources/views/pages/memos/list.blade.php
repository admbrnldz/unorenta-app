@extends('layouts.app')

@section('title', 'Memoranda')

@section('head-addon')
<style>
    
</style>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Memo</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active">
                            <a href="{{ route('memos') }}">
                                List 
                                @if ($currentUser->employee->forSignatureMemo()->count() > 0)
                                    ({{ $currentUser->employee->forSignatureMemo()->count() }})
                                @endif
                            </a>
                        </li>
                        @can ('broadcast_memo', $currentUser)
                        <li><a href="{{ route('memos.broadcast') }}">Broadcast Memo</a></li>
                        @endcan
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                    </div>
                </div>

                <div class="listing-page memos row">

                    {{--*/ $count = 0 /*--}}
                    @foreach ($memos as $memo)

                        @if (($memo->isSigned() AND $memo->isRecipient()) || $currentUser->can('broadcast_memo', $currentUser) || $memo->isSignatory())
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="memo-entry">
                                <div class="listing-header">                                    
                                    <h3><a href="{{ route('memos.details') }}/{{ $memo->id }}">{{ $memo->title }}</a></h3>
                                    @if (!$memo->isSigned())
                                        <span class="label label-danger">For Signature</span>
                                    @endif
                                    <div class="listing-meta">
                                        <ul class="clearfix">
                                            <li><i class="fa fa-calendar-o fa-fw"></i> &nbsp; {{ processDate($memo->created_at) }}</li>
                                            <li><i class="fa fa-bullhorn fa-fw"></i> &nbsp; To 
                                                <a href="{{ route('memos.details') }}/{{ $memo->id }}#conform">
                                                    {{ $memo->recipients->count() }} {{ $memo->recipients->count() == 1 ? 'Employee' : 'Employees'}}
                                                </a>
                                            </li>
                                            @if ($memo->allow_discussions)
                                                <li><i class="fa fa-comments fa-fw"></i> &nbsp; 
                                                    <a href="{{ route('memos.details') }}/{{ $memo->id }}#memo-discussions-thread">
                                                        {{ $memo->comments->count() }} {{ $memo->comments->count() == 1 ? 'Comment' : 'Comments' }}
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="listing-content">
                                    {!! $memo->content !!}
                                </div>
                                <div class="memo-entry-overlay">
                                    <a href="{{ route('memos.details') }}/{{ $memo->id }}" class="btn btn-sm btn-primary">View Full</a>
                                    <a href="{{ route('memos.details') }}/{{ $memo->id }}?sneak=true" 
                                        class="btn btn-sm btn-squared btn-default sneak"
                                    >
                                        <i class="fa fa-dedent fa-14"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        {{--*/ $count++ /*--}}
                        @endif

                    @endforeach

                    @if ($count == 0)
                    <div class="col-xs-12">
                        <div class="no-memo">
                            @can ('broadcast_memo', $currentUser)
                                <a href="{{ route('memos.broadcast') }}">Broadcast your first memorandum</a>
                            @else
                                <span>No memorandum posted yet.</span>
                            @endcan
                        </div>
                    </div>
                    @endif

                </div>
                
            </div>

        </div>
        </div>
    </div>
</section>

@can ('broadcast_memo', $currentUser)
<div class="page-actions">
    <ul class="clearfix">
        <li>
            <a href="{{ route('memos.broadcast') }}" id="menu-66" class="pa-primary add-applicant">
                <i class="fa fa-bullhorn"></i>
            </a>
        </li>
    </ul>
</div>
@endcan

@endsection

@section('footer-addon')
<script>
$(function() {

    

});
</script>
@endsection