@extends('layouts.app')

@section('title', 'Broadcast Memorandum')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $memo->title }}</h1>
                    <div class="details-page-meta">
                        <ul class="clearfix">
                            <li><i class="fa fa-calendar-o fa-fw"></i> &nbsp; {{ processDate($memo->created_at) }}</li>
                            <li><i class="fa fa-bullhorn fa-fw"></i> &nbsp; To 
                                <a href="#">
                                    {{ $memo->recipients->count() }} {{ $memo->recipients->count() == 1 ? 'Employee' : 'Employees'}}
                                </a>
                            </li>
                            @if ($memo->allow_discussions)
                            <li><i class="fa fa-comments fa-fw"></i> &nbsp; 
                                <a href="#memo-discussions-thread">
                                    {{ $memo->comments->count() }} {{ $memo->comments->count() == 1 ? 'Comment' : 'Comments' }}
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('memos.details') }}/{{ $memo->id }}">Details</a></li>
                        @can ('edit_memo', $currentUser)
                        <li class="active"><a href="{{ route('memos.edit') }}/{{ $memo->id }}">Edit</a></li>
                        @endcan
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">

            @include('includes.errors')

            <div class="section-content">
                
                <form method="post" autocomplete="off">
                {{ csrf_field() }}
                <div class="panel">
                    <div class="panel-body">                        
                        <div class="form-horizontal form-primary">

                            <div class="form-group">
                                <label for="title" class="col-xs-12 col-sm-3 control-label">Title</label>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row input-field-wrap">
                                        <div class="col-xs-12">
                                            <input type="text" class="form-control" name="title" id="title" placeholder="" value="{{ old('title', $memo->title) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                                    <div class="row input-field-wrap">
                                        <div class="col-xs-12">
                                            <textarea name="content" id="content" rows="6" class="form-control">{!! old('content', $memo->content) !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="signatories-wrap">
                                {{--*/ $count = 1 /*--}}
                                @foreach ($memo->signatories as $signatory)
                                <div id="signatory-{{ $count }}" class="signatory-entry form-group">
                                    <label for="signatory-{{ $count }}-cc" class="col-xs-12 col-sm-3 control-label">
                                        Signatory {{ $count }}
                                        @if ($count > 1)
                                        &nbsp; <a href="#" class="remove-signatory" data-target="#signatory-{{ $count }}"><i class="fa fa-close fa-fw"></i></a>
                                        @else
                                        &nbsp; <a href="#" class="clear-signatory" data-target="#signatory-{{ $count }}" title="clear"><i class="fa fa-eraser fa-fw"></i></a>
                                        @endif
                                    </label>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="row input-field-wrap">
                                            <div class="col-xs-12 col-sm-5">
                                                <input type="text" class="form-control sig-cc" name="signatories[{{ $count}}][comp_closing]" id="signatory-{{ $count }}-cc" placeholder="Complimentary Closing" value="{{ $signatory->complimentary_closing }}" />
                                            </div>
                                            <div class="col-xs-12 col-sm-7">
                                                <select id="signatory-{{ $count }}-name" name="signatories[{{ $count }}][employeeid]" multiple="multiple" class="form-control signatories sig-name">
                                                    @foreach ($employees as $employee)
                                                    <option value="{{ $employee->id }}"
                                                        <?php if ($employee->id == $signatory->employee_id): ?>selected<?php endif; ?>
                                                    >
                                                        {{ $employee->user->name() }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--*/ $count++ /*--}}
                                @endforeach
                            </div>
                            <div class="form-group" style="margin-top:-12px;">
                                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                    <a href="#" class="btn btn-sm btn-primary" id="add-signatory">Add Signatory</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="recipients" class="col-xs-12 col-sm-3 control-label">Recipients</label>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row input-field-wrap">
                                        <div class="col-xs-12">
                                            <select id="recipients" name="recipients[]" multiple="multiple" class="form-control">
                                                @foreach ($employees as $employee)
                                                <option value="{{ $employee->id }}"
                                                    <?php if (!$memo->to_all AND $memo->isRecipient($employee->id) AND !$memo->isSignatory($employee->id)): ?>selected<?php endif; ?>
                                                >
                                                    {{ $employee->user->name() }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-last">
                                <label for="name" class="col-xs-12 col-sm-3 control-label">&nbsp;</label>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row input-field-wrap">
                                        <div class="col-xs-12">
                                            <div class="checkbox styled">
                                                <label>
                                                    <input type="checkbox" name="to_all" id="broadcast-all" value="1" 
                                                        <?php if ($memo->to_all): ?>checked<?php endif; ?>
                                                    />
                                                    <span>Broadcast to all</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="checkbox styled">
                                                <label>
                                                    <input type="checkbox" name="allow_discussions" id="" value="1" 
                                                        <?php if ($memo->allow_discussions): ?>checked<?php endif; ?>
                                                    />
                                                    <span>Allow Discussion</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-button-group clearfix">
                            <button type="submit" class="btn btn-primary">Update Memo</button>
                            <a href="{{ route('memos.details') }}/{{ $memo->id }}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
                </form>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    CKEDITOR.replace('content', {
        contentsCss : [
            '{{ asset("assets/css/ckeditor.css") }}',
            '{{ asset("assets/css/fonts.css") }}'
        ],
        customConfig : '',
        toolbar: [
            { name: 'basicstyles', items: ['Bold', 'Italic'] },
            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] }
        ],
        extraPlugins: 'autogrow',
        autoGrow_maxHeight : 1000
    });

    CKEDITOR.addCss("" + 
        ".cke_editable {font-family: 'latoregular'; font-size: 14px;line-height: 22px;margin:15px;}" +
        "::-moz-selection {background: #00a8a0;color: #ffffff;text-shadow: none;}" +
        "::selection {background: #00a8a0;color: #ffffff;text-shadow: none;}"
    );

    $('#recipients').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5
    });

    $('#broadcast-all').on('click', function() {
        if ($(this).prop('checked') == true) {
            $('#recipients').tokenize().clear().disable();
        } else {
            $('#recipients').tokenize().enable();
        }
    });

    var tokenizeParam = {
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5,
        maxElements: 1
    };
    $('.signatories').tokenize(tokenizeParam);

    $('#add-signatory').on('click', function(e) {
        e.preventDefault();
        var id = $('.signatory-entry:last').attr('id').split('-'),
            idNumber = id[1],
            newId = parseInt(idNumber) + 1;
        // console.log(id);
        $('.signatory-entry:last').clone()
            .remove('.Tokenize')
            .attr('id', 'signatory-' + newId)
            .appendTo($('.signatories-wrap'));

        $('#signatory-' + newId)
            .find('label')
            .attr('for', 'signatory-' + newId + '-cc')
            .html('Signatory ' + newId + '&nbsp; <a href="#" class="remove-signatory" data-target="#signatory-' + newId + '"><i class="fa fa-close"></i></a>');
        $('#signatory-' + newId)
            .find('.Tokenize')
            .remove();
        $('#signatory-' + newId)
            .find('.sig-cc')
            .attr('name', 'signatories[' + newId + '][comp_closing]')
            .attr('id', 'signatory-' + newId + '-cc')
            .val('');
        $('#signatory-' + newId)
            .find('.sig-name')
            .attr('name', 'signatories[' + newId + '][employeeid]')
            .attr('id', 'signatory-' + newId + '-name')
            .val('')
            .attr('style', '');

        $('#signatory-' + newId +  ' .signatories').tokenize(tokenizeParam);
        $('.remove-signatory').each(function() {
            $(this).on('click', function(e) {
                e.preventDefault();
                $($(this).attr('data-target')).remove();
            });
        });
    });

    $('.remove-signatory').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            $($(this).attr('data-target')).remove();
        });
    });

    $('.clear-signatory').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var target = $($(this).attr('data-target'));
            target.find('.sig-name').tokenize().clear();
            target.find('.sig-cc').val('').focus();
        });
    });

});
</script>
@endsection