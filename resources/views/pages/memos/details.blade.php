@extends('layouts.app')

@section('title', $memo->title)

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $memo->title }}</h1>
                    <div class="details-page-meta">
                        <ul class="clearfix">
                            <li><i class="fa fa-calendar-o fa-fw"></i> &nbsp; {{ processDate($memo->created_at) }}</li>
                            <li><i class="fa fa-bullhorn fa-fw"></i> &nbsp; To 
                                <a href="#">
                                    {{ $memo->recipients->count() }} {{ $memo->recipients->count() == 1 ? 'Employee' : 'Employees'}}
                                </a>
                            </li>
                            @if ($memo->allow_discussions)
                            <li><i class="fa fa-comments fa-fw"></i> &nbsp; 
                                <a href="#memo-discussions-thread">
                                    <span id="comments-count-2">{{ $memo->comments->count() }}</span> {{ $memo->comments->count() == 1 ? 'Comment' : 'Comments' }}
                                </a>
                            </li>
                            @endif
                            @if ($currentUser->can('broadcast_memo', $currentUser) || $memo->isSignatory())
                            <li>
                                <i class="fa fa-bar-chart-o fa-fw"></i> &nbsp; <a href="#memo-stats">View Statistics</a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active"><a href="{{ route('memos.details') }}/{{ $memo->id }}">Details</a></li>
                        @can ('edit_memo', $currentUser)
                        <li><a href="{{ route('memos.edit') }}/{{ $memo->id }}">Edit</a></li>
                        @endcan
                        @if (!$memo->isSigned())
                            <li><a href="{{ route('memos.delete') }}/{{ $memo->id }}" id="delete-memo">Delete</a></li>
                        @endif
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                        <div class="panel">

                            <div class="panel-body">

                                <article>
                                    {!! $memo->content !!}
                                </article>

                                <div class="details-page-signatories row">

                                    @foreach ($memo->signatories as $signatory)
                                    <div class="col-xs-12 col-sm-6">
                                        <?php $style = $signatory->date_signed != null ? 'background:url(' . $signatory->employee->signature() . ') left center no-repeat;background-size:auto 60px;' : ''; ?>
                                        <span class="signatory-complimentary-closing">{{ $signatory->complimentary_closing }}</span>
                                        <span class="signatory-signature" id="signature-pad-{{ $signatory->employee_id }}" style="{{ $style }}">

                                            @if ($signatory->employee_id == $currentUser->employee->id)
                                                @if ($signatory->date_signed == null)
                                                <form action="{{ route('memos.sign') }}/{{ $memo->id }}" method="post" autocomplete="off" id="sign-memo-form">
                                                    {!! csrf_field() !!}
                                                </form>
                                                <a href="#" id="sign-memo">
                                                    <span><i class="fa fa-pencil fa-fw"></i> Click to Sign</span>
                                                </a>
                                                @endif
                                            @endif

                                        </span>                                    

                                        <span class="signatory-name">{{ $signatory->employee->user->name() }}</span>
                                        <span class="signatory-position">{{ $signatory->employee->position->name }}</span>
                                    </div>
                                    @endforeach

                                </div>

                            </div>

                            @if ($memo->isSigned())
                            {{--*/ $allowDiscussion = '' /*--}}
                            @if ($memo->allow_discussions)
                                {{--*/ $allowDiscussion = 'with_discussion' /*--}}
                            @endif
                            <div class="panel-footer post-footer {{ $allowDiscussion }}">
                                <div id="conform" class="post-feedback memo-conforme">
                                    <ul class="clearfix">
                                        @if ($memo->isRecipient())
                                            @if ($memo->conformed($currentUser->employee->id))
                                                <li>
                                                    <div class="dp-handler" style="background:url({{ $currentUser->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}" title="You"></a>
                                                    </div>
                                                </li>
                                            @else
                                                <li>
                                                    <form action="{{ route('memos.conform') }}" method="post" id="conform-form">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="memo_id" value="{{ $memo->id }}" />
                                                    <a href="#" class="btn btn-primary btn-sm" id="conform-memo">Conforme</a>
                                                    </form>
                                                </li>
                                            @endif
                                        @endif
                                        {{--*/ $conformedEmployees = $memo->conformedEmployees(12) /*--}}
                                        @foreach ($conformedEmployees['list'] as $conformedEmployee)
                                            <li>                                        
                                                <div class="dp-handler" style="background:url({{ $conformedEmployee->employee->user->dp() }}) center center no-repeat;background-size:cover;">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $conformedEmployee->employee->id]) }}" 
                                                        title="{{ $conformedEmployee->employee->user->name() }}"
                                                    ></a>
                                                </div>
                                            </li>
                                        @endforeach
                                        @if ($conformedEmployees['offset'] > 0)
                                            <li class="post-feedback-other">+{{ $conformedEmployees['offset'] }} other</li>
                                        @endif
                                    </ul>
                                </div>

                                @if ($memo->allow_discussions)
                                <div id="memo-discussions-thread" class="post-discussion memo-discussions">
                                    <h4><i class="fa fa-comments fa-fw"></i> &nbsp; Discussion</h4>
                                    <ul class="post-discussion-thread clearfix">
                                        
                                        @foreach ($memo->comments as $comment)
                                        <?php
                                            $class = '';
                                            if ($comment->author_id == $currentUser->employee->id) {
                                                $class = 'post-discussion-thread-entry-yours';
                                            }
                                        ?>
                                        <li class="post-discussion-thread-entry {{ $class }} clearfix">
                                            <div class="pd-container">
                                                <div class="pd-dp">
                                                    <div class="dp-handler" style="background:url({{ $comment->author->user->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $comment->author->id]) }}" title="{{ $comment->author->user->name() }}"></a>
                                                    </div>
                                                </div>
                                                <div class="pd-content-wrap" title="Posted {{ processDate($comment->posted_at, true) }}">
                                                    <div class="pd-content">
                                                        {!! $comment->comment !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach

                                    </ul>
                                    <div id="write-comment" class="post-discussion-write-comment">
                                        <form action="{{ route('memos.comment.add') }}" method="post" id="post-memo-comment-form">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="memo_id" value="{{ $memo->id }}" />
                                        <div class="pd-wc-container">
                                            <div class="pd-wc-dp">
                                                <div class="dp-handler" style="background:url({{ $currentUser->dp() }}) center center no-repeat;background-size:cover;">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}" title="You"></a>
                                                </div>
                                            </div>
                                            <div class="pd-wc-textbox">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <textarea name="comment" id="comment" rows="3" class="form-control"></textarea>
                                                        <div class="write-comment-post-button">
                                                            <button class="btn btn-primary btn-sm" id="post-comment" disabled>Post</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div><!-- / post-discussion -->
                                @endif
                            </div>
                            @endif

                        </div>

                    </div>

                    @if ($currentUser->can('broadcast_memo', $currentUser) || $memo->isSignatory())
                    <div class="col-xs-12">
                        
                        <div id="memo-stats" class="panel">
                            <div class="panel-heading">
                                <h4>Memo Statistics</h4>
                            </div>
                            <div class="panel-stat">
                                <div class="row stats memo-stats">
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="stat-entry">
                                            <h5>{{ $memo->recipients()->count() }}</h5>
                                            <span>Recipients</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="stat-entry">
                                            <h5 id="conform-count">{{ $memo->recipients()->where('conform', 1)->count() }}</h5>
                                            <span>Conform</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="stat-entry">
                                            <h5 id="seen-count">{{ $memo->recipients()->where('seen', 1)->count() }}</h5>
                                            <span>Seen</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="stat-entry">
                                            {{--*/ $commentCount = $memo->comments()->count() /*--}}
                                            <h5 id="comments-count">{{ $commentCount }}</h5>
                                            <span id="comments-count-label">{{ $commentCount == 1 ? 'Comment' : 'Comments' }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">

                                <div class="table-responsive">
                                    
                                    <table class="table" id="recipients-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th>Conform</th>
                                                <th>Last Seen</th>
                                                <th class="ta-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($recipients as $recipient)
                                            <tr id="r-{{ $recipient->id }}">
                                                <td class="table-dp-td">
                                                    <div class="dp-handler" style="background:url({{ $recipient->employee->user->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $recipient->employee->id]) }}"></a>
                                                    </div>
                                                </td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $recipient->employee->id]) }}">
                                                        {{ $recipient->employee->name() }}
                                                    </a>
                                                </td>
                                                <td class="conform-date">
                                                    @if ($recipient->date_conform)
                                                        {{ processDate($recipient->date_conform, true) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="seen-date">
                                                    @if ($recipient->date_last_seen)
                                                        {{ processDate($recipient->date_last_seen, true) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="ta-right">
                                                    <a href="{{ route('memos.removerecipient') }}/{{ $recipient->id }}" class="remove-recipient">
                                                        <i class="fa fa-close fa-fw"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <div class="panel-footer post-footer">

                                <form action="{{ route('memos.addrecipient') }}/{{ $memo->id }}" method="post" autocomplete="off" id="add-recipient-form">
                                {!! csrf_field() !!}
                                <div class="form-primary">

                                    <div class="form-group form-group-last">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-9 col-sm-10">
                                                        <select id="recipients" name="recipients[]" multiple="multiple" class="form-control">
                                                            @foreach ($employees as $employee)
                                                            <option value="{{ $employee->id }}"
                                                                <?php if ($employee->id == $signatory->employee_id): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $employee->user->name() }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-3 col-sm-2">
                                                        <button class="btn btn-primary" id="add-recipients" data-target="#add-recipient-form">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                    
                                </div>
                                </form>
                                
                            </div>
                        </div>

                    </div>
                    @endif

                    <div class="col-xs-12">
                        <div class="listing-footer-actions">
                            <a href="#" class="btn btn-sm btn-primary btp">Back to top</a>
                        </div>
                    </div>

                </div>

            </div>           

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    var interval1 = 0,
        currentUserId = {{ $currentUser->id }},
        requestUrl = "{{ route('longpoll.memocomments') }}/{{ $memo->id }}";
    function checkComment(url, timestamp)
    {
        if (timestamp > 0) {
            interval1 = 300;
        }
        setTimeout(function() {
            $.ajax({
                type: 'GET',
                url: url + '?ts=' + timestamp,
                async: true,
                success: function(r) {
                    r = $.parseJSON(r);
                    $('#comments-count').html(r.newCommentCount);
                    $('#comments-count-2').html(r.newCommentCount);
                    if (r.newCommentCount == 1) {
                        $('#comments-count-label').html('Comment');
                    } else {
                        $('#comments-count-label').html('Comments');
                    }
                    $('.post-discussion-thread').append(r.view);
                    if (r.userId != currentUserId) {
                        $.playSound("{{ asset('assets/tones') }}/" + tone);
                    }
                    checkComment(r.url, r.timestamp);
                },
                error: function(jqXhr, exception) {
                    checkComment(requestUrl, timestamp);  
                }
            });
        }, interval1);
    }
    checkComment(requestUrl, 0);

    var interval2 = 0,
        requestUrl2 = "{{ route('longpoll.memoseencount') }}/{{ $memo->id }}";
    function recountSeen(url, theCount)
    {
        if (theCount > 0) {
            interval2 = 300;
        }
        setTimeout(function() {
            $.ajax({
                type: 'GET',
                url: url + '?sc=' + theCount,
                async: true,
                success: function(r) {
                    r = $.parseJSON(r);
                    $('#seen-count').html(r.newCount);
                    $('#r-' + r.employeeId).find('.seen-date').html(r.lastSeenDate);
                    recountSeen(r.url, r.newCount);
                },
                error: function(jqXhr, exception) {
                    recountSeen(requestUrl2, theCount);  
                }
            });
        }, interval2);
    }
    recountSeen(requestUrl2, 0);

    var interval3 = 0,
        requestUrl3 = "{{ route('longpoll.memoconformcount') }}/{{ $memo->id }}";
    function recountConform(url, theCount)
    {
        if (theCount > 0) {
            interval3 = 300;
        }
        setTimeout(function() {
            $.ajax({
                type: 'GET',
                url: url + '?cc=' + theCount,
                async: true,
                success: function(r) {
                    r = $.parseJSON(r);
                    $('#conform-count').html(r.newCount);
                    $('#r-' + r.employeeId).find('.conform-date').html(r.lastConformDate);
                    recountConform(r.url, r.newCount);
                },
                error: function(jqXhr, exception) {
                    recountConform(requestUrl3, theCount);  
                }
            });
        }, interval3);
    }
    recountConform(requestUrl3, 0);

    $('#delete-memo').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div class="ta-center">Are you sure you want to delete this memo?</div>',
            className: 'modal-warning',
            buttons: {
                success: {
                    label: 'Yes, Please',
                    className: 'btn-danger',
                    callback: function() {
        
                        window.location = url;
                        
                    }
                },
                'Cancel' : {
                    className: 'btn-default'
                }
            }
        });  
    })

    $('#recipients').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5
    });

    $('#conform-memo').on('click', function(e) {
        e.preventDefault();        
        $(this).prepend('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;');

        var form = $('#conform-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post({
            url: url,
            data: data
        }).done(function(response) {            
            response = $.parseJSON(response);

            $('#conform-memo').find('.fa').removeClass('fa-circle-o-notch fa-spin')
                .addClass('fa-check');

            setTimeout(function() {
                var html = '<div class="dp-handler" style="background:url(' + response.dp + ') center center no-repeat;background-size:cover;">' +
                    '<a href="' + response.profileUrl + '" title="You"></a>' +
                    '</div>';
                $('.memo-conforme li:first-child').html(html);
                var currentCount = $('#memo-notifyer').html();
                if (currentCount != '') {
                    if (currentCount > 1) {
                        $('#memo-notifyer').html(currentCount - 1);
                    } else {
                        $('#memo-notifyer').remove();
                    }                
                }
            }, 2000);
        });
    });

    $('#sign-memo').on('click', function(e) {
        e.preventDefault();
        $(this).remove('span').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');

        var form = $('#sign-memo-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post( url, data ).done(function(response) {
            response = $.parseJSON(response);

            var target = $('#signature-pad-' + response.id),
                style = 'background:url(' + response.signature + ') left center no-repeat;background-size:auto 60px;';

            target.html('');
            target.attr('style', style);            
        });
    });

    $('#post-comment').on('click', function(e) {
        e.preventDefault();
        $(this).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;Post');
        $('#post-comment').prop('disabled', true);

        if ($('#comment').val() == '') {
            $(this).html('<i class="fa fa-close fa-fw"></i>&nbsp;Post');
            $(this).removeClass('btn-primary').addClass('btn-danger');
            return false;
        }

        var form = $('#post-memo-comment-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post( url, data ).done(function(r) {
            r = $.parseJSON(r);
            $('#post-comment').find('.fa').removeClass('fa-circle-o-notch fa-spin')
                .addClass('fa-check');
            $('#comment').val('');
            setTimeout(function() {
                $('#post-comment').html('Post');
                $(this).addClass('btn-primary').removeClass('btn-danger');
            }, 3000);
        });
    });
    
    $('#add-recipients').on('click', function(e) {
        e.preventDefault();
        var targetForm = $(this).attr('data-target'),
            form = $(targetForm),
            url = form.attr('action'),
            data = form.serialize();
        $.post( url, data ).done(function(r) {
            r = $.parseJSON(r);
            var count = r.r.length;
            for (var i = 0; i < count; i++) {
                $('#recipients option[value="' + r.r[i] + '"]').remove();
            }
            $('#recipients').tokenize().clear();
            $('#recipients-table').append(r.view);
        });
    });

    $(document).on('click', '.remove-recipient', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.get( url ).done(function(r) {
            r = $.parseJSON(r);
            $('#r-' + r.id).remove();
            $('#recipients').append(r.option);
        });
    });
    
});
</script>
@endsection