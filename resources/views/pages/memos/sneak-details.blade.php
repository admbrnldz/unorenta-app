<div class="sneak-memo-details">

    <div class="section-title">
        <h1>
            <a href="{{ route('memos.details') }}/{{ $memo->id }}">
                {{ $memo->title }}
            </a>
        </h1>
        <div class="details-page-meta">
            <ul class="clearfix">
                <li><i class="fa fa-calendar-o fa-fw"></i> &nbsp; {{ processDate($memo->created_at) }}</li>
                <li><i class="fa fa-bullhorn fa-fw"></i> &nbsp; To {{ $memo->recipients->count() }} {{ $memo->recipients->count() == 1 ? 'Employee' : 'Employees'}}</li>
                @if ($memo->allow_discussions)
                <li><i class="fa fa-comments fa-fw"></i> &nbsp; {{ $memo->comments->count() }} {{ $memo->comments->count() == 1 ? 'Comment' : 'Comments' }}</li>
                @endif
            </ul>
        </div>
    </div>

    <div style="margin-top:40px;">
        <article>
            {!! $memo->content !!}
        </article>
    </div>

    <div class="details-page-signatories row">

        @foreach ($memo->signatories as $signatory)
        <div class="col-xs-12 col-sm-6">
            <span class="signatory-complimentary-closing">{{ $signatory->complimentary_closing }}</span>
            <span class="signatory-signature" style="background:url({{ $signatory->employee->signature() }}) left center no-repeat;background-size:auto 60px;"></span>
            <span class="signatory-name">{{ $signatory->employee->user->name() }}</span>
            <span class="signatory-position">{{ $signatory->employee->position->name }}</span>
        </div>
        @endforeach

    </div>

</div>