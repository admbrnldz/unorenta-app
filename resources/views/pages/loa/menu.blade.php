<div class="section-header-menu">
    <ul class="clearfix">
        <li class="{{ $active[0] == 'dashboard' ? 'active' : '' }}"><a href="{{ route('loa.dashboard') }}">Leave Applications</a></li>
        @if (Auth::user()->isApprover())
        <li class="{{ $active[0] == 'signed-leaves' ? 'active' : '' }}"><a href="{{ route('loa.signed') }}">Signed Leaves</a></li>
        @endif
        @can ('entitle_leave_applications', $currentUser)
        <li class="{{ $active[0] == 'entitlements' ? 'active' : '' }}"><a href="{{ route('loa.entitlements') }}">Leave Entitlements</a></li>
        @endcan
        <li class="{{ $active[0] == 'approvers' ? 'active' : '' }}">
            <a href="{{ route('loa.approvers') }}">
                Leave Approvers
            </a>
        </li>
        <li class="{{ $active[0] == 'holidays' ? 'active' : '' }}"><a href="{{ route('holidays.dashboard') }}">Holidays</a></li>
    </ul>
</div>