<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('loa.file') }}" method="post" autocomplete="off" id="file-leave-form">
{{ csrf_field() }}
<input type="hidden" name="employee_id" id="employee-id" value="{{ $currentUser->employee->id }}" />
<input type="hidden" name="days_count" id="days-count" value="" />

    <div class="form-horizontal form-primary">

        <div class="form-group">
            <label for="leave-type" class="col-xs-12 col-sm-3 control-label">Leave Type</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <select name="leave_type_id" id="leave-type" class="form-control" data-check-url="{{ route('loa.checkcredits') }}">
                            <option value=""></option>
                            {{--*/ $entitledLeaves = $currentUser->employee->entitledLeave()->where('year', $currentYear)->get() /*--}}
                            @if (count($entitledLeaves) > 0)
                                @foreach ($entitledLeaves as $entitledLeave)
                                    <option value="{{ $entitledLeave->leaveType->id }}"
                                        <?php if ($entitledLeave->leaveType->id == old('leave_type_id')): ?>selected<?php endif; ?>
                                    >
                                        {{ $entitledLeave->leaveType->name }}
                                    </option>
                                @endforeach
                            @endif
                            <option value="0">Leave Without Pay</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div id="leave-period-wrap" class="form-group <?php if (old('leave_type_id') == null): ?>hidden<?php endif; ?>">
            <label for="leave-period" class="col-xs-12 col-sm-3 control-label">Leave Period</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <div class="option-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="leave_period" id="leave-period-halfday" value="Half Day" />
                                    <span>Half Day</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="leave_period" id="leave-period-wholeday" value="Whole Day" />
                                    <span>Whole Day</span>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="time-cycle-wrap" class="form-group hidden">
            <label for="time-cycle" class="col-xs-12 col-sm-3 control-label">Time Cycle</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">

                        <div class="option-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="time_cycle" id="time-cycle-am" value="AM" />
                                    <span>AM</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="time_cycle" id="time-cycle-pm" value="PM" />
                                    <span>PM</span>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="number-of-days-wrap" class="form-group hidden">
            <label for="number-of-days" class="col-xs-12 col-sm-3 control-label">Number of Days</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12 col-sm-6">
                        <select name="number_of_days" id="number-of-days" class="form-control">
                            <?php if (old('days_count')): ?>
                                <option value=""></option>
                                <?php for ($i=1; $i <= old('days_count'); $i++): ?>
                                    <option value="{{ $i }}">{{ $i }} {{ $i > 1 ? 'days' : 'day'}}</option>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div id="date-wrap" class="form-group hidden">
            <label for="date-from" class="col-xs-12 col-sm-3 control-label">Date</label>
            <div class="col-xs-12 col-sm-9">
                <div id="date-from-to-wrap" class="row input-field-wrap">
                    <div class="col-xs-6">
                        <input type="text" name="date_from" id="date-from" class="form-control datepicker-leave" placeholder="From" disabled />
                    </div>
                    <div class="col-xs-6">
                        <input type="text" name="date_to" id="date-to" class="form-control datepicker-leave" placeholder="To" disabled />
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="reason" class="col-xs-12 col-sm-3 control-label">Reason</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <textarea name="reason" id="reason" rows="3" class="form-control autosize">{{ old('reason') }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="first-approver" class="col-xs-12 col-sm-3 control-label">First Approver</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <select id="first-approver" name="first_approver" multiple="multiple" class="form-control">
                            @foreach ($approvers as $approver)
                                <option value="{{ $approver->employee_id }}">
                                    {{ $approver->employee->name() }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last">
            <label for="final-approver" class="col-xs-12 col-sm-3 control-label">Final Approver</label>
            <div class="col-xs-12 col-sm-6">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <select id="final-approver" name="final_approver" multiple="multiple" class="form-control">
                            @foreach ($approvers as $approver)
                                <option value="{{ $approver->employee_id }}">
                                    {{ $approver->employee->name() }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>
<script>
$(function() {

    $('.datepicker-leave').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $('#first-approver').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5,
        maxElements: 1,
        onAddToken: function(value, text, e) {
            $('#final-approver').find('option[value="' + value + '"]').remove();
        },
        onRemoveToken: function(value, e) {
            var name = $('#first-approver').find('option[value="' + value + '"]').html();
            $('#final-approver').prepend('<option value="' + value + '">' + name + '</option>');
        }
    });

    $('#final-approver').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5,
        maxElements: 1,
        onAddToken: function(value, text, e) {
            $('#first-approver').find('option[value="' + value + '"]').remove();
        },
        onRemoveToken: function(value, e) {
            var name = $('#final-approver').find('option[value="' + value + '"]').html();
            $('#first-approver').prepend('<option value="' + value + '">' + name + '</option>');
        }
    });

    $('#leave-period-halfday').on('change', function() {
        $('#date-wrap').removeClass('hidden');
        $('#date-from').attr('disabled', false);
        $('#date-to').val($('#date-from').val());
        if ($(this).prop('checked') == true) {
            $('#time-cycle-wrap').removeClass('hidden');
            $('#number-of-days-wrap').addClass('hidden');
        }
    });
    $('#leave-period-wholeday').on('change', function() {
        $('#date-wrap').removeClass('hidden');
        $('#date-from').attr('disabled', true);
        $('#date-to').val('');
        if ($(this).prop('checked') == true) {
            $('#number-of-days-wrap').removeClass('hidden');
            $('#time-cycle-wrap').addClass('hidden');
        }
    });

    $('#number-of-days').on('change', function() {
        if ($(this).val() == '') {
            $('#date-from').attr('disabled', true);
            $('#date-to').attr('disabled', true);
        } else if ($(this).val() == 1) {
            $('#date-from').attr('disabled', false);
            $('#date-to').attr('disabled', true).val($('#date-from').val());
        } else {
            $('#date-from').attr('disabled', false);
            $('#date-to').attr('disabled', false).val('');
            var d = $('#date-from').val().split('-'),
                mind = new Date(d[0], (parseInt(d[1]) - 1), d[2]),
                newd = new Date(mind);
            newd.setDate(newd.getDate() + parseInt($('#number-of-days').val() - 1));
            var maxd = new Date(newd);
            $('#date-to').val('').datepicker('option', {
                maxDate: maxd
            });
        }
    });

    $('#date-from').on('change', function() {
        if ($('#number-of-days').val() <= 1) {
            $('#date-to').val($(this).val());
            console.log($('#date-to').val());
        } else {
            var d = $(this).val().split('-'),
                mind = new Date(d[0], (parseInt(d[1]) - 1), d[2]),
                newd = new Date(mind);
            newd.setDate(newd.getDate() + parseInt($('#number-of-days').val() - 1));
            var maxd = new Date(newd);
            $('#date-to').val('').datepicker('option', {
                minDate: mind,
                maxDate: maxd
            });
        }
    });

    $('#leave-type').on('change', function() {
        
        var url = $(this).attr('data-check-url'),
            data = {
                'employee_id': $('#employee-id').val(),
                'leave_type_id': $(this).val()
            };
        $.post(url, data).done(function(r) {
            r = $.parseJSON(r);
            $('#days-count').val(r.remainingLeave);
            $('#number-of-days').html('');
            for (var i = 0; i <= r.remainingLeave; i++) {
                if (i == 0) {
                    $('#number-of-days').append('<option value=""></option>');    
                } else {
                    var label = i > 1 ? 'days' : 'day';
                    $('#number-of-days').append('<option value="' + i + '">' + i + ' ' + label + '</option>');
                }
            }            
        });

        if ($(this).val() != '') {
            $('#leave-period-wrap').removeClass('hidden');
        } else {
            $('#leave-period-wrap').addClass('hidden');
        }

    });

});
</script>