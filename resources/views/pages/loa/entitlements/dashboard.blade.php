@extends('layouts.app')

@section('title', 'Leave Entitlements')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Leave Entitlements</h3>
                </div>
                {{--*/ $active = ['entitlements'] /*--}}
                @include('pages.loa.menu', $active)

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-8">

                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th>Leave Types</th>
                                                <th class="ta-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employees as $employee)
                                                @if ($employee->currentStatus(true) < 5)
                                                <tr>
                                                    <td class="table-dp-td ">
                                                        <div class="dp-handler" 
                                                            style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;"
                                                        >
                                                            <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}"></a>
                                                        </div>
                                                    </td>
                                                    <td class="table-highlight">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">
                                                            {{ $employee->name(true) }}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $entitledLeaves = $employee->entitledLeave()
                                                                ->where('year', $currentYear)
                                                                ->select('id', 'leave_type_id')
                                                                ->get();
                                                        ?>
                                                        <div class="table-labels">
                                                            @foreach ($entitledLeaves as $leave)
                                                                <span class="label label-primary">
                                                                    {{ $leave->leaveType->name }} ({{ $employee->checkLeaveCredits($leave->leave_type_id) }})
                                                                </span>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                    <td class="ta-right">
                                                        <a href="{{ route('loa.entitlements.list') }}/{{ $employee->id }}"
                                                            class="entitlements-list"
                                                        >
                                                            <i class="fa fa-file-text-o fa-fw"></i>
                                                        </a>
                                                        <a href="{{ route('loa.entitle') }}/{{ $employee->id }}" 
                                                            class="entitle-leave"
                                                            data-target-form="#entitle-leave-form"
                                                            data-where="entitlement"
                                                        >
                                                            <i class="fa fa-plus fa-fw"></i>
                                                        </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-md-4">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Leave Types</h4>
                                <div class="panel-heading-edit">
                                    <a id="add-leave-types" title="Add Leave Types"
                                        href="{{ route('loa.type.add') }}"
                                        data-target-form="#add-leave-type-form"
                                    >
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list">

                                    @foreach ($leaveTypes as $lt)
                                    <li>
                                        <a class="update-leave-type" href="{{ route('loa.type.edit') }}/{{ $lt->id }}"
                                            data-delete="{{ route('loa.type.delete') }}/{{ $lt->id }}"
                                            data-target-form="#edit-leave-type-form"
                                        >
                                            {{ $lt->name }}
                                        </a>
                                    </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#add-leave-types').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target-form');
        $.get({ url: url }).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {

                            var form = $(targetForm),
                                url = form.attr('action'),
                                data = form.serialize();

                            $.post( url, data ).success(function(r) {
                                r = $.parseJSON(r);
                                bootbox.hideAll();
                                setTimeout(function() {
                                    window.location = r.redirect;
                                }, 500);                                
                            }).error(function(r) {
                                var errors = r.responseJSON;
                                var html = '';
                                $.each(errors, function(index, value) {
                                    html += '<li>' + value[0] + '</li>';
                                });
                                $('#modal-error-handler').fadeIn().find('ul').html(html);
                            });
                            return false;
                            
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $('.update-leave-type').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var editUrl = $(this).attr('href'),
                deleteUrl = $(this).attr('data-delete'),
                targetForm = $(this).attr('data-target-form');
            $.get({ url: editUrl }).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {
                                $(targetForm).submit();
                            }
                        },
                        'Delete' : {
                            className: 'btn-danger',
                            callback: function() {

                                bootbox.dialog({
                                    title: 'Are you sure you want to delete this?',
                                    message: '<div class="ta-center">All datas related in this leave type will also be deleted.</div>',
                                    buttons: {
                                        success: {
                                            label: 'Delete',
                                            className: 'btn-danger',
                                            callback: function() {
                                                window.location = deleteUrl;
                                            }
                                        },
                                        'Cancel' : {
                                            className: 'btn-default'
                                        }
                                    }
                                });

                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    $('.entitlements-list').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');

            $.get( url ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    className: 'modal-with-table',
                    buttons: {
                        'Close' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

});
</script>
@endsection