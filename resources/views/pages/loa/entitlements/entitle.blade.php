<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('loa.entitle') }}/{{ $employee->id }}" method="post" id="entitle-leave-form" autocomplete="off">
{!! csrf_field() !!}
<input type="hidden" name="where" value="{{ $redirect }}" />
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="school-name" class="col-xs-12 col-sm-3 control-label">Leave Type</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select name="leave_type_id" id="" class="form-control">
                        <option value=""></option>
                        @foreach ($leaveTypes as $leaveType)
                            <option value="{{ $leaveType->id }}">{{ $leaveType->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="year" class="col-xs-12 col-sm-3 control-label">Year</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="year" id="year" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="credits" class="col-xs-12 col-sm-3 control-label">Credits</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="credits" id="credits" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>