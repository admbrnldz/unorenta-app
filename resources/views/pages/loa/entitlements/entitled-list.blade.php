<table class="table" style="margin-bottom:0;">
    <thead>
        <tr>
            <th>Leave Type</th>
            <th>Year</th>
            <th>Credits</th>
            <th>Date Entitled</th>
            <th class="ta-right">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($employee->entitledLeave as $entitledLeave)
        <tr>
            <td class="table-highlight">{{ $entitledLeave->leaveType->name }}</td>
            <td>{{ $entitledLeave->year }}</td>
            <td>{{ $entitledLeave->credits }}</td>
            <td>{{ processDate($entitledLeave->created_at) }}</td>
            <td class="ta-right">
                <a href="{{ route('loa.remove-entitled-leave') }}/{{ $entitledLeave->id }}" class="delete-entitled-leave">
                    <i class="fa fa-close fa-fw"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script>
$(function() {

    // $('.delete-entitled-leave').each(function() {
    //     $(this).on('click', function(e) {
    //         e.preventDefault();
    //         var url = $(this).attr('href');

    //     });
    // });

});
</script>