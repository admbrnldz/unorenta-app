@extends('layouts.app')

@section('title', 'Create New Department')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Leave of Absence</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('loa.dashboard') }}">Leave Applications</a></li>
                        <li><a href="{{ route('loa.signed') }}">Signed Leaves</a></li>
                        <li><a href="{{ route('loa.entitlements') }}">Leave Entitlements</a></li>
                        <li><a href="{{ route('holidays.dashboard') }}">Holidays</a></li>
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="loa-details">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12">

                        <div class="panel">
                            <div class="panel-body">
                                
                                <ul class="panel-body-list">
                                    <li>
                                        <span style="font-size:20px;line-height:1.4;"><strong>
                                            <a href="{{ route('employees.profile', ['employeeId' => $leave->employee->id]) }}">
                                                {{ $leave->employee->name() }}
                                            </a>
                                        </strong></span>
                                        <span>{{ $leave->employee->position->name }}</span>
                                    </li>
                                    <li>
                                        <small>Leave Type</small>
                                        @if ($leave->leave_type_id > 0)
                                            <span>{{ $leave->leaveType->name }}</span>
                                        @else
                                            <span>Leave Without Pay</span>
                                        @endif                                        
                                    </li>
                                    <li>
                                        <small>Number of Days</small>
                                        <span>{{ $leave->number_of_days }}</span>
                                    </li>                                    
                                    <li>
                                        <small class="table-label">Time Cycle</small>
                                        <span>{{ $leave->time_cycle or '-' }}</span>
                                    </li>
                                    <li>
                                        <small class="table-label">Period</small>
                                        <span>
                                            {{ processDate($leave->date_from) }}
                                            @if ($leave->number_of_days > 1)
                                            &nbsp;&mdash;&nbsp; {{ processDate($leave->date_to) }}
                                            @endif
                                        </span>
                                    </li>
                                    <li>
                                        <small class="table-label">Reason</small>
                                        <span>{{ $leave->reason }}</span>
                                    </li>
                                </ul>

                                <div class="details-page-signatories row">
                                    <div class="col-xs-12 col-sm-6">
                                        <?php 
                                            $bg = '';
                                            if ($leave->is_first_approved == 0) {
                                                $bg = '/assets/images/x.png';
                                            } elseif ($leave->is_first_approved == 1) {
                                                $bg = $leave->firstApprover->signature();
                                            }
                                        ?>
                                        <span class="signatory-complimentary-closing">First Approver</span>
                                        <span class="signatory-signature" id="signature-pad-first" style="background:url({{ $bg }}) left center no-repeat;background-size:auto 60px;">
                                            @if ($leave->first_approver == $currentUser->employee->id)
                                                @if ($leave->is_first_approved < 0)
                                                <form method="post" autocomplete="off" id="sign-first-loa-form">
                                                    {!! csrf_field() !!}                                            
                                                </form>
                                                <a href="{{ route('loa.sign') }}/first/a/{{ $leave->id }}" class="sign-pad sign-pad-loa sign-pad-approve" id="sign-first-loa">
                                                    <span><i class="fa fa-pencil fa-fw"></i> Click to Approve</span>
                                                </a>
                                                <a href="{{ route('loa.sign') }}/first/d/{{ $leave->id }}" class="sign-pad sign-pad-loa sign-pad-disapprove" id="sign-first-loa">
                                                    <span><i class="fa fa-close fa-fw"></i></span>
                                                </a>
                                                @endif
                                            @endif
                                        </span>
                                        <span class="signatory-name">{{ $leave->firstApprover->user->name() }}</span>
                                        <span class="signatory-position">{{ $leave->firstApprover->position->name }}</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <?php 
                                            $bg = '';
                                            if ($leave->is_final_approved == 0) {
                                                $bg = asset('assets/images/x.png');
                                            } elseif ($leave->is_final_approved == 1) {
                                                $bg = $leave->finalApprover->signature();
                                            }
                                        ?>
                                        <span class="signatory-complimentary-closing">Final Approver</span>
                                        <span class="signatory-signature" id="signature-pad-final" style="background:url({{ $bg }}) left center no-repeat;background-size:auto 60px;">
                                            @if ($leave->final_approver == $currentUser->employee->id)
                                                @if ($leave->is_final_approved < 0)
                                                <form method="post" autocomplete="off" id="sign-final-loa-form">
                                                    {!! csrf_field() !!}
                                                </form>
                                                <a href="{{ route('loa.sign') }}/final/a/{{ $leave->id }}" class="sign-pad sign-pad-loa sign-pad-approve" id="sign-final-loa">
                                                    <span><i class="fa fa-pencil fa-fw"></i> Click to Approve</span>
                                                </a>
                                                <a href="{{ route('loa.sign') }}/final/d/{{ $leave->id }}" class="sign-pad sign-pad-loa sign-pad-disapprove" id="sign-final-loa">
                                                    <span><i class="fa fa-close fa-fw"></i></span>
                                                </a>
                                                @endif
                                            @endif
                                        </span>
                                        <span class="signatory-name">{{ $leave->finalApprover->user->name() }}</span>
                                        <span class="signatory-position">{{ $leave->finalApprover->position->name }}</span>
                                    </div>
                                </div>

                            </div>
                            <div class="panel-footer post-footer">
                                
                                <div id="loa-discussions-thread" class="post-discussion loa-discussions">
                                    <h4><i class="fa fa-comments fa-fw"></i> &nbsp; Discussion</h4>

                                    <ul class="post-discussion-thread clearfix">
                                        
                                        @foreach ($leave->comments as $comment)
                                        <?php
                                            $class = '';
                                            if ($comment->author_id == $currentUser->employee->id) {
                                                $class = 'post-discussion-thread-entry-yours';
                                            }
                                        ?>
                                        <li class="post-discussion-thread-entry {{ $class }} clearfix">
                                            <div class="pd-container">
                                                <div class="pd-dp">
                                                    <div class="dp-handler" style="background:url({{ $comment->author->dp() }}) center center no-repeat;background-size:cover;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $comment->author->id]) }}" title="{{ $comment->author->name() }}"></a>
                                                    </div>
                                                </div>
                                                <div class="pd-content-wrap" title="Posted {{ processDate($comment->posted_at, true) }}">
                                                    <div class="pd-content">
                                                        {!! $comment->comment !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach

                                    </ul>

                                    <div id="write-comment" class="post-discussion-write-comment">
                                        <form action="{{ route('loa.comment.add') }}" method="post" id="post-loa-comment-form">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="leave_application_id" value="{{ $leave->id }}" />
                                        <div class="pd-wc-container">
                                            <div class="pd-wc-dp">
                                                <div class="dp-handler" style="background:url({{ $currentUser->dp() }}) center center no-repeat;background-size:cover;">
                                                    <a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}" title="You"></a>
                                                </div>
                                            </div>
                                            <div class="pd-wc-textbox">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <textarea name="comment" id="comment" rows="3" class="form-control"></textarea>
                                                        <div class="write-comment-post-button">
                                                            <button class="btn btn-primary btn-sm" id="post-comment" disabled>Post</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    var interval = 0,
        currentUserId = {{ $currentUser->id }},
        requestUrl = "{{ route('longpoll.leavecomments') }}/{{ $leave->id }}";
    function checkComment(url, timestamp)
    {
        if (timestamp > 0) {
            interval = 1000;
        }
        setTimeout(function() {
            $.ajax({
                type: 'GET',
                url: url + '?ts=' + timestamp,
                async: true,
                success: function(r) {
                    r = $.parseJSON(r);
                    $('.post-discussion-thread').append(r.view);
                    if (r.userId != currentUserId) {
                        $.playSound("{{ asset('assets/tones') }}/" + tone);
                    }
                    checkComment(r.url, r.timestamp);
                },
                error: function(jqXhr, exception) {
                    checkComment(requestUrl, timestamp);  
                }
            });
        }, interval);
    }
    checkComment(requestUrl, 0);

    $('.datepicker-leave').datepicker();

    $('#sign-first-loa, #sign-final-loa').on('click', function(e) {
        e.preventDefault();
        var targetFormId = '#' + $(this).attr('id') + '-form';

        $(this).remove('span').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
        var form = $(targetFormId),
            url = $(this).attr('href'),
            data = form.serialize();

        $.post( url, data ).done(function(r) {
            r = $.parseJSON(r);

            var img = '';
            if (r.status == 1) {
                img = r.signature;
            } else if (r.status == 0) {
                img = '/assets/images/x.png';
                $('#comment').val('Reason for disapproval: ').focus();
            }

            var target = $('#signature-pad-' + r.who),
                style = 'background:url(' + img + ') left center no-repeat;background-size:auto 60px;';

            target.html('');
            target.attr('style', style);
        });
    });

    $('#post-comment').on('click', function(e) {
        e.preventDefault();
        $(this).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;Post');
        $('#post-comment').prop('disabled', true);

        if ($('#comment').val() == '') {
            $(this).html('<i class="fa fa-close fa-fw"></i>&nbsp;Post');
            $(this).removeClass('btn-primary').addClass('btn-danger');
            return false;
        }

        var form = $('#post-loa-comment-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post( url, data ).done(function(r) {
            r = $.parseJSON(r);

            $('#post-comment').find('.fa').removeClass('fa-circle-o-notch fa-spin')
                .addClass('fa-check');
            $('#comment').val('');
            setTimeout(function() {
                $('#post-comment').html('Post');
                $(this).addClass('btn-primary').removeClass('btn-danger');
            }, 3000);
        });
    });

});
</script>
@endsection