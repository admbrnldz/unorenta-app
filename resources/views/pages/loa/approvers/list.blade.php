@extends('layouts.app')

@section('title', 'Leave and Holidays')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Leave Approvers</h3>
                </div>
                {{--*/ $active = ['approvers'] /*--}}
                @include('pages.loa.menu', $active)

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Approvers</th>
                                                <th>Departments</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($approvers as $approver)
                                                <tr>
                                                    <td class="table-dp-td">
                                                        <div class="dp-handler" style="background:url({{ $approver->employee->user->dp() }}) center center no-repeat;background-size:cover">
                                                            <a href="{{ route('employees.profile', ['employeeId' => $approver->employee->id]) }}"></a>
                                                        </div>
                                                    </td>
                                                    <td class="table-highlight" style="white-space: nowrap;">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $approver->employee->id]) }}">
                                                            {{ $approver->employee->name(true) }}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <div class="table-labels clearfix">
                                                            @foreach ($approver->departments($approver->employee_id) as $department)
                                                                <span class="label label-primary">{{ $department->name }}</span>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="3" class="ta-center">No approvers yet</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>

<div class="page-actions">
    <ul class="clearfix">
        <li>
            <a href="{{ route('loa.approvers.add') }}" id="add-approver" class="pa-primary" 
                data-form="#add-approver-form"
            >
                <i class="fa fa-file-text-o"></i>
            </a>
        </li>
    </ul>
</div>

@endsection

@section('footer-addon')
<script>
$(function() {

    $('#add-approver').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-form');
        $.get(url).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {
                            var form = $(targetForm),
                                url = form.attr('action'),
                                data = form.serialize();

                            $.post( url, data ).success(function(r) {
                                r = $.parseJSON(r);
                                bootbox.hideAll();
                                setTimeout(function() {
                                    window.location = r.redirect;
                                }, 500);                                
                            }).error(function(r) {
                                var errors = r.responseJSON;
                                var html = '';
                                $.each(errors, function(index, value) {
                                    html += '<li>' + value[0] + '</li>';
                                });
                                $('#modal-error-handler').fadeIn().find('ul').html(html);
                            });
                            return false;
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

});
</script>
@endsection