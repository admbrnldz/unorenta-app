<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('loa.approvers.add') }}" method="post" autocomplete="off" id="add-approver-form">
{{ csrf_field() }}

    <div class="form-horizontal form-primary">

        <div class="form-group">
            <label for="leave-type" class="col-xs-12 col-sm-3 control-label">Approver</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <select id="approver" name="approver" multiple="multiple" class="form-control">
                            @foreach ($employees as $employee)
                            <option value="{{ $employee->id }}">{{ $employee->name() }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last">
            <label for="leave-type" class="col-xs-12 col-sm-3 control-label">Departments</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <select id="departments" name="departments[]" multiple="multiple" class="form-control">
                            @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>
<script>
$(function() {

    $('#approver').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5,
        maxElements: 1,
    });

    $('#departments').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5,
    });

});
</script>