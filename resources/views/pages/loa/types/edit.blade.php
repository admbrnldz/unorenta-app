<form action="{{ route('loa.type.edit') }}/{{ $leaveType->id }}" method="post" autocomplete="off" id="edit-leave-type-form">
{{ csrf_field() }}
<div class="form-horizontal form-primary">

    <div class="form-group form-group-last">
        <label for="name" class="col-xs-12 col-sm-4 control-label">Leave Type</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ $leaveType->name }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>