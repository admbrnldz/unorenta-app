<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('loa.type.add') }}" method="post" autocomplete="off" id="add-leave-type-form">
{{ csrf_field() }}
<div class="form-horizontal form-primary">

    <div class="form-group form-group-last">
        <label for="name" class="col-xs-12 col-sm-4 control-label">Leave Type</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>