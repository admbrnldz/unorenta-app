@extends('layouts.app')

@section('title', 'Leave and Holidays')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>{{ $title }}</h3>
                </div>
                {{--*/ $active = ['signed-leaves'] /*--}}
                @include('pages.loa.menu', $active)

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>For Your Approval</h4>
                            </div>
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    <form action="" method="post" id="leave-form">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="approver_id" value="{{ Auth::user()->employee->id }}" />
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th>Leave Type</th>
                                                <th>Days</th>
                                                <th>Date</th>
                                                <th>First Approver</th>
                                                <th>Final Approver</th>
                                                <th class="ta-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                @forelse ($leaveApplications as $leaveApp)
                                                <tr class="">
                                                    <td class="table-dp-td">
                                                        <div class="dp-handler" style="background:url({{ $leaveApp->employee->user->dp() }}) center center no-repeat;background-size:cover">
                                                            <a href="{{ route('employees.profile', ['employeeId' => $leaveApp->employee->id]) }}"></a>
                                                        </div>
                                                    </td>
                                                    <td class="table-highlight">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $leaveApp->employee->id]) }}">
                                                            {{ $leaveApp->employee->id == $currentUser->employee->id ? 'Me' : $leaveApp->employee->user->name() }}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        @if ($leaveApp->leave_type_id == 0)
                                                            Leave Without Pay
                                                        @else
                                                            {{ $leaveApp->leaveType->name }}
                                                        @endif
                                                    </td>
                                                    <td>{{ $leaveApp->number_of_days }}</td>
                                                    <td>
                                                        <a href="{{ route('loa.details', ['loaId' => $leaveApp->id]) }}">
                                                            {{ processDate($leaveApp->date_from) }}
                                                            @if ($leaveApp->time_cycle != '')
                                                                ({{ $leaveApp->time_cycle }})
                                                            @endif
                                                            @if ($leaveApp->number_of_days > 1)
                                                            &nbsp;&rarr;&nbsp; {{ processDate($leaveApp->date_to) }}
                                                            @endif
                                                        </a>
                                                    </td>
                                                    <td id="leave-{{ $leaveApp->id }}-{{ $leaveApp->first_approver }}">
                                                        @if ($leaveApp->is_first_approved == 1)
                                                            <i class="fa fa-check fa-fw fc-primary"></i> &nbsp;
                                                        @elseif ($leaveApp->is_first_approved == 0)
                                                            <i class="fa fa-close fa-fw fc-danger" title="Disapproved"></i> &nbsp;
                                                        @else
                                                            <i class="fa fa-circle-o-notch fa-spin fa-fw fc-gray" title="Waiting for approval"></i> &nbsp;
                                                        @endif
                                                        <a href="{{ route('employees.profile', ['employeeId' => $leaveApp->firstApprover->id]) }}">
                                                            <strong>{{ $leaveApp->first_approver == $currentUser->employee->id ? 'Me' : $leaveApp->firstApprover->user->name() }}</strong>
                                                        </a>
                                                    </td>
                                                    <td id="leave-{{ $leaveApp->id }}-{{ $leaveApp->final_approver }}">
                                                        @if ($leaveApp->is_final_approved == 1)
                                                            <i class="fa fa-check fa-fw fc-primary"></i> &nbsp;
                                                        @elseif ($leaveApp->is_final_approved == 0)
                                                            <i class="fa fa-close fa-fw fc-danger" title="Disapproved"></i> &nbsp;
                                                        @else
                                                            <i class="fa fa-circle-o-notch fa-spin fa-fw fc-gray" title="Waiting for approval"></i> &nbsp;
                                                        @endif
                                                        <a href="{{ route('employees.profile', ['employeeId' => $leaveApp->finalApprover->id]) }}">
                                                            <strong>{{ $leaveApp->final_approver == $currentUser->employee->id ? 'Me' : $leaveApp->finalApprover->user->name() }}</strong>
                                                        </a>
                                                    </td>
                                                    <td id="leave-actions-{{ $leaveApp->id }}" class="ta-right">
                                                        @if ($leaveApp->isApprover($currentUser->employee->id) AND !$leaveApp->hasApproved($currentUser->employee->id))
                                                        <div class="table-actions clearfix">
                                                            <button class="btn btn-sm btn-primary loa-approve"
                                                                data-url="{{ route('loa.approve', ['loaId' => $leaveApp->id]) }}"
                                                            >
                                                                Approve
                                                            </button>
                                                            <button class="btn btn-sm btn-danger btn-squared loa-disapprove" 
                                                                data-url="{{ route('loa.disapprove', ['loadId' => $leaveApp->id]) }}"
                                                            >
                                                                <i class="fa fa-close"></i>
                                                            </button>
                                                        </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @empty
                                                <tr>
                                                    <td colspan="8" class="ta-center">No leave applications yet.</td>
                                                </tr>
                                                @endforelse
                                        </tbody>
                                    </table>
                                    </form>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>

@if ($currentUser->employee->canFileLeave())
<div class="page-actions">
    <ul class="clearfix">
        <li>
            <a href="{{ route('loa.file') }}" id="file-leave" class="pa-primary" 
                data-form="#file-leave-form"
            >
                <i class="fa fa-file-text-o"></i>
            </a>
        </li>
    </ul>
</div>
@endif

@endsection

@section('footer-addon')
<script>
$(function() {

    $('.loa-approve').each(function() {

        $(this).on('click', function(e) {
            e.preventDefault();

            var form = $('#leave-form'),
                data = form.serialize(),
                url = $(this).attr('data-url');

            $.post( url, data ).done(function(r) {
                r = $.parseJSON(r);
                var DOM = $('#leave-' + r.leaveId + '-' + r.approverId).find('i.fa');

                switch(r.status) {
                    case 'disapproved':
                        DOM.attr('class', 'fa fa-close fa-fw fc-danger');
                        DOM.attr('title', 'Disapproved');
                        break;
                    case 'approved':
                        DOM.attr('class', 'fa fa-check fa-fw fc-primary');
                        DOM.attr('title', 'Approved');
                        break;
                }

                $('#leave-actions-' + r.leaveId).html('-');
            });

        });

    });

    $('.loa-disapprove').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            loader.show();

            var url = $(this).attr('data-url');

            $.get( url ).done(function(r) {
                loader.hide();
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Disapproved',
                            className: 'btn-danger',
                            callback: function() {

                                var form = $('#disapprove-form'),
                                    url = form.attr('action'),
                                    data = form.serialize();

                                $.post( url, data ).done(function(r) {
                                    r = $.parseJSON(r);
                                    var DOM = $('#leave-' + r.leaveId + '-' + r.approverId).find('i.fa');

                                    switch(r.status) {
                                        case 'disapproved':
                                            DOM.attr('class', 'fa fa-close fa-fw fc-danger');
                                            DOM.attr('title', 'Disapproved');
                                            break;
                                        case 'approved':
                                            DOM.attr('class', 'fa fa-check fa-fw fc-primary');
                                            DOM.attr('title', 'Approved');
                                            break;
                                    }

                                    $('#leave-actions-' + r.leaveId).html('-');
                                });

                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

});
</script>
@endsection