<form action="{{ route('loa.disapprove') }}/{{ $leave->id }}" method="post" autocomplete="off" id="disapprove-form">
{!! csrf_field() !!}
<input type="hidden" name="approver_id" value="{{ $currentUser->employee->id }}" />
<input type="hidden" name="leave_application_id" value="{{ $leave->id }}">

    <div class="form-horizontal form-primary">

        <div class="form-group form-group-last">
            <label for="comment" class="col-xs-12">Reason for disapproval</label>
            <div class="col-xs-12">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <textarea name="comment" id="comment" rows="3" class="form-control autosize">Reason for disapproval: </textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>
<script>
$(function() {

    autosize($('.autosize'));

});
</script>