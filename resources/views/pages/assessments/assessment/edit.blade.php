<form action="{{ route('assessments.assessmentguide.edit') }}/{{ $question->id }}" method="post" autocomplete="off" id="questions-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="question" class="col-xs-12 col-sm-3 control-label">Question</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <textarea name="question" id="question" rows="2" class="form-control autosize">{{ $question->question }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="category" class="col-xs-12 col-sm-3 control-label">Category</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select name="category_id" id="category" class="form-control">
                        <option value=""></option>
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}"
                            <?php if ($category->id == $question->assessment_category_id): ?>selected<?php endif; ?>
                        >
                            {{ $category->name }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    autosize($('.autosize'));

})
</script>