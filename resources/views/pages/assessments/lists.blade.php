@extends('layouts.app')

@section('title', 'Interview Questions')

@section('head-addon')
<styles>

</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
                
                <div class="section-title">
                    <h3>Assessment Guides</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('applicants') }}">Applicants</a></li>
                        <li class="active">
                            <a href="{{ route('assessments') }}">Assessment Guides</a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content" id="nestable-container">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')  
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Assessment Guides</h4>
                            </div>
                            <div class="panel-body">
                                <form action="{{ route('assessments.assessmentguide.update') }}" method="post" autocomplete="off" id="update-assessment-guide-form">
                                {!! csrf_field() !!}
                                <div class="dd assessment-guide" id="assessment-guide-nestable">
                                    <ol class="dd-list">
                                        @foreach ($categories as $category)
                                            <li class="dd-item" data-cat-id="{{ $category->id }}" data-cat-name="{{ $category->name }}">
                                                <div class="dd-handle">
                                                    {{ $category->name }}
                                                    <div class="question-options dd-nodrag">
                                                        <div class="dropdown">
                                                            <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                <i class="fa fa-ellipsis-v fa-fw"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                @can ('edit_interview_question', $currentUser)
                                                                    <span>
                                                                        <a href="{{ route('assessments.category.edit') }}/{{ $category->id }}" 
                                                                            class="edit-entry dd-nodrag" title="Edit"
                                                                        >
                                                                            <i class="fa fa-pencil fa-fw"></i> Edit Category
                                                                        </a>
                                                                    </span>
                                                                @endcan
                                                                @can ('delete_interview_question', $currentUser)
                                                                    <span>
                                                                        <a href="{{ route('assessments.category.delete') }}/{{ $category->id }}" 
                                                                            class="delete-entry dd-nodrag" title="Edit"
                                                                        >
                                                                            <i class="fa fa-remove fa-fw"></i> Delete Category
                                                                        </a>
                                                                    </span>
                                                                @endcan
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if (count($category->assessments) > 0)
                                                <ol class="dd-list">
                                                    @foreach ($category->assessments()->orderBy('order')->get() as $assessment)
                                                    <li class="dd-item" data-id="{{ $assessment->id }}" data-question="{{ $assessment->question }}">
                                                        <div class="dd-handle">
                                                            {{ $assessment->question }}
                                                            <div class="question-options dd-nodrag">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                        <i class="fa fa-ellipsis-v fa-fw"></i>
                                                                    </button>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        @can ('edit_interview_question', $currentUser)
                                                                        <span>
                                                                            <a href="{{ route('assessments.assessmentguide.edit') }}/{{ $assessment->id }}" 
                                                                                class="edit-entry dd-nodrag" title="Edit"
                                                                            >
                                                                                <i class="fa fa-pencil fa-fw"></i> Edit Guide
                                                                            </a>
                                                                        </span>
                                                                        @endcan
                                                                        @can ('delete_interview_question', $currentUser)
                                                                        <span>
                                                                            <a href="{{ route('assessments.assessmentguide.delete') }}/{{ $assessment->id }}" 
                                                                                class="delete-entry dd-nodrag" title="Edit"
                                                                            >
                                                                                <i class="fa fa-remove fa-fw"></i> Delete Guide
                                                                            </a>
                                                                        </span>
                                                                        @endcan
                                                                    </div>
                                                                </div>
                                                            </div>                                            
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ol>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ol>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Post-Assessment Guides</h4>
                            </div>
                            <div class="panel-body">
                                <form action="{{ route('assessments.postassessmentguide.update') }}" method="post" autocomplete="off" id="update-post-assessment-guide-form">
                                {!! csrf_field() !!}
                                <div class="dd post-assessment-guide" id="post-assessment-guide-nestable">
                                    <ol class="dd-list">
                                        @foreach ($postAssessmentGuides as $pag)
                                            <li class="dd-item" data-pag-id="{{ $pag->id }}" data-pag-question="{{ $pag->question }}">
                                                <div class="dd-handle">
                                                    {{ $pag->question }}
                                                    <div class="question-options dd-nodrag">
                                                        <div class="dropdown">
                                                            <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                <i class="fa fa-ellipsis-v fa-fw"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                @can ('edit_interview_question', $currentUser)
                                                                    <span>
                                                                        <a href="{{ route('assessments.postassessmentguide.edit') }}/{{ $pag->id }}" 
                                                                            class="edit-entry dd-nodrag" title="Edit"
                                                                        >
                                                                            <i class="fa fa-pencil fa-fw"></i> Edit Guide
                                                                        </a>
                                                                    </span>
                                                                @endcan
                                                                @can ('delete_interview_question', $currentUser)
                                                                    <span>
                                                                        <a href="{{ route('assessments.postassessmentguide.delete') }}/{{ $pag->id }}" 
                                                                            class="delete-entry dd-nodrag" title="Edit"
                                                                        >
                                                                            <i class="fa fa-remove fa-fw"></i> Delete Guide
                                                                        </a>
                                                                    </span>
                                                                @endcan
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ol>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>                    
               
        </div>
        </div>
    </div>
</section>
@can ('add_interview_questions', $currentUser)
<div class="page-actions">
    <ul class="clearfix">
        <li class="dropup">
            <a href="#" class="pa-primary pa-pop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropup">
                <i class="fa fa-plus"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right pa-submenu" aria-labelledby="dropup">
                <li>
                    <a href="{{ route('assessments.category.add') }}" class="pa-primary" id="add-assessment-category">
                        <span>Assessment Category</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('assessments.assessmentguide.add') }}" class="pa-primary" id="add-assessment-guide">
                        <span>Assessment Guide</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('assessments.postassessmentguide.add') }}" class="pa-primary" id="add-post-assessment-guide">
                        <span>Post-Assessment Guide</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
@endcan
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/nestable.js') }}"></script>
<script>
$(function() {

    $('#assessment-guide-nestable').nestable({
        maxDepth: 2
    }).on('change', function() {
        var form = $('#update-assessment-guide-form'),
            url = form.attr('action'),
            data = window.JSON.stringify($('#assessment-guide-nestable').nestable('serialize'));

        loader.show();
        $.post({
            url: url,
            data: {
                'data': data
            },
            dataType: 'json',
        }).done(function(r) {
            loader.hide();
        });
    });

    $('#post-assessment-guide-nestable').nestable({
        maxDepth: 1
    }).on('change', function() {
        var form = $('#update-post-assessment-guide-form'),
            url = form.attr('action'),
            data = window.JSON.stringify($('#post-assessment-guide-nestable').nestable('serialize'));

        loader.show();
        $.post({
            url: url,
            data: {
                'data': data
            },
            dataType: 'json',
        }).done(function(r) {
            loader.hide();
        });
    });

    $('#add-assessment-category, #add-assessment-guide, #add-post-assessment-guide').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.get({ url: url }).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {
                            var form = $('#questions-form');
                            form.submit();
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $('.edit-entry').each(function() {
        $(this).on('click', function(e) {            
            e.preventDefault();
            var url = $(this).attr('href');
            $.get({ url: url }).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {
                                var form = $('#questions-form');
                                form.submit();
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    $('.delete-entry').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
                message = 'All data related to this entry will also be deleted.';
            bootbox.dialog({
                title: 'Are you sure you want to delete this?',
                message: '<div class="ta-center">' + message + '</div>',
                buttons: {
                    success: {
                        label: 'Delete',
                        className: 'btn-danger',
                        callback: function() {
                            window.location = url;
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        })
    });

});
</script>
@endsection