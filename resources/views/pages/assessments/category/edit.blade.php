<form action="{{ route('assessments.category.edit') }}/{{ $category->id }}" method="post" autocomplete="off" id="questions-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group form-group-last">
        <label for="category-name" class="col-xs-12 col-sm-3 control-label">Category Name</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="category_name" id="category-name" value="{{ $category->name }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>