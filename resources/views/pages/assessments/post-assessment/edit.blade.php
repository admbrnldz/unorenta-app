<form action="{{ route('assessments.postassessmentguide.edit') }}/{{ $postAssessmentGuide->id }}" method="post" autocomplete="off" id="questions-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group form-group-last">
        <label for="question" class="col-xs-12 col-sm-3 control-label">Guide Question</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <textarea name="question" id="question" rows="2" class="form-control autosize">{{ $postAssessmentGuide->question }}</textarea>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    autosize($('.autosize'));
    
})
</script>