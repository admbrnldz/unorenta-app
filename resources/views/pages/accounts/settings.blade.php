@extends('layouts.app')

@section('title', $user->name())

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
{{--*/ $status = '' /*--}}
@if ($user->employee->status == 3 AND !$user->is_active)
    {{--*/ $status = 'inactive' /*--}}
@endif
<section class="section-header with-menu {{ $status }}">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title clearfix">

                    {{--*/ $employee = $user->employee /*--}}
                    @include('pages.employees.profile-header', $employee)

                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li>
                            <a href="{{ route('employees.profile', ['employeeId' => $user->employee->id]) }}">Profile</a>
                        </li>
                        <li class="active">
                            <a href="{{ route('account.settings') }}/{{ $user->id }}">Account Settings</a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="employee-profile">
    
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                @include('includes.status')
            
                <div id="profile-{{ $user->employee->id }}" class="profile row">

                    <div class="col-xs-12 col-md-6">

                        <form action="{{ route('account.settings.save') }}" method="post" autocomplete="off" id="account-settings-form">
                        {!! csrf_field() !!}
                        <input type="hidden" name="user_id" value="{{ $user->id }}" />

                            <div class="panel">
                                <div class="panel-heading">
                                    <h4>Account Settings</h4>
                                </div>
                                <div class="panel-body">
                                        
                                    <div class="form-horizontal form-primary">

                                        @can ('edit_account_settings', $currentUser)
                                        <div class="form-group">
                                            <label for="first-name" class="col-xs-12 col-sm-3 control-label">Name</label>
                                            <div class="col-xs-12 col-sm-9">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="first_name" id="first-name" placeholder="First Name" value="{{ old('first_name', $user->first_name) }}" />
                                                    </div>
                                                </div>
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="middle_name" id="middle-name" placeholder="Middle Name" value="{{ old('middle_name', $user->middle_name) }}" />
                                                    </div>
                                                </div>
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="last_name" id="last-name" placeholder="Last Name" value="{{ old('last_name', $user->last_name) }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endcan
                                        <div class="form-group">
                                            <label for="username" class="col-xs-12 col-sm-3 control-label">Username</label>
                                            <div class="col-xs-12 col-sm-9">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control verify-field" name="username" id="username" placeholder="" value="{{ old('username', $user->username) }}" data-url="{{ route('account.check.username') }}" data-user-id="{{ $user->id }}" />
                                                        <div class="username-verifier field-verifier"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @can ('edit_account_settings', $currentUser)
                                        <div class="form-group">
                                        @else
                                        <div class="form-group form-group-last">
                                        @endcan
                                            <label for="password" class="col-xs-12 col-sm-3 control-label">Password</label>
                                            <div class="col-xs-12 col-sm-9">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="password" id="password" placeholder="Don't put anything to remain unchanged" />
                                                        <a href="#" id="generate-password" data-target="password" class="btn btn-sm btn-primary" style="margin-top:10px;">Generate Password</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @can ('edit_account_settings', $currentUser)
                                        <div class="form-group form-group-last">
                                            <label for="password" class="col-xs-12 col-sm-3 control-label">Status</label>
                                            <div class="col-xs-12 col-sm-8">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="is_active" id="" value="1" 
                                                                    <?php if ($user->employee->user->is_active == 1): ?>checked<?php endif; ?>
                                                                />
                                                                <span>Active</span>
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="is_active" id="not_active" value="0" 
                                                                    <?php if ($user->employee->user->is_active == 0): ?>checked<?php endif; ?>
                                                                />
                                                                <span>Inactive</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endcan

                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <div class="form-button-group clearfix">
                                        <button type="submit" class="btn btn-primary" id="update-account">Update Account</button>
                                    </div>
                                </div>
                            </div>
                        
                        </form>

                    </div>
                    <div class="col-xs-12 col-md-6">

                        @can ('edit_account_settings', $currentUser)
                        <form action="{{ route('account.update.permissions') }}/{{ $user->id }}" method="post">
                        {!! csrf_field() !!}
                        @endcan
                        <div class="panel">
                            <div class="panel-heading">
                                @can ('edit_account_settings', $currentUser)
                                <h4>User Permissions</h4>
                                @else
                                <h4>Things you can</h4>
                                @endcan
                            </div>
                            <div class="panel-body">
                                @can ('edit_permissions', $currentUser)
                                    @foreach ($categories as $category)
                                        <div class="category-wrap">
                                            <h4>{{ $category->name}}</h4>
                                            @if (count($category->permissions) > 0)

                                                <div class="row permissions-list">
                                                    @foreach ($category->permissions as $permission)
                                                        <div class="col-xs-12 col-sm-6">
                                                            <div class="checkbox">
                                                                <label for="{{ $permission->key }}">
                                                                    <input type="checkbox" name="permissions[]" id="{{ $permission->key }}" 
                                                                        value="{{ $permission->id }}"
                                                                        <?php if (in_array($permission->id, $userPermissions)): ?>checked<?php endif; ?>
                                                                    />
                                                                    <span>{{ $permission->name }}</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>

                                            @endif
                                        </div>
                                    @endforeach
                                @else
                                    <div class="category-wrap">
                                        <div class="row permissions-view-only permissions-list">                                            
                                            @foreach ($user->permissions as $permission)
                                                <div class="col-xs-12">
                                                    <div class="checkbox">
                                                        <label for="{{ $permission->permission->key }}">
                                                            <input type="checkbox" id="{{ $permission->permission->key }}" checked disabled />
                                                            <span>{{ $permission->permission->name }}</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endcan
                            </div>
                            @can ('edit_permissions', $currentUser)
                                @if ($user->is_active AND $user->employee->status != 3)
                                    <div class="panel-footer">
                                        <div class="form-button-group clearfix">
                                            <button type="submit" class="btn btn-primary">Update Permissions</button>
                                        </div>
                                    </div>
                                @endif
                            @endcan
                        </div>
                        @can ('edit_account_settings', $currentUser)
                        </form>
                        @endcan

                    </div>

                </div>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#birthdate').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-40:+0',
        dateFormat: 'yy-mm-dd'
    });

    $('.profile-info table tr').each(function() {
        $(this).on('click', function() {
            var id = $(this).attr('id'),
                target = $('#' + id + '-form');
            $(this).html(target.html());
        });
    });

    $('#add-dependent').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');

        $.get(url, function(response) {
            $('#add-dependent').hide();
            $('#add-dependent-form-wrap').append(response);
        });
    });

    $('#update-account').on('click', function(e) {
        e.preventDefault();
        var form = $('#account-settings-form');
        if ($('#not_active').prop('checked')) {
            bootbox.dialog({
                title: 'Warning!',
                message: '<div class="ta-center">Setting this account to <strong><u>inactive</u></strong> will set his/her position into <strong><i>"Unassigned"</i></strong>.<br />Are you sure you want to deactive this account?</div>',
                className: 'modal-warning',
                buttons: {
                    success: {
                        label: 'Deactive',
                        className: 'btn-danger',
                        callback: function() {
                            $(form).submit();
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        } else {
            $(form).submit();
        }
    });

});
</script>
@endsection