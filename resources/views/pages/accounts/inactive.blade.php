<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>UnoRenta | Inactive</title>

<link rel="shortcut icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon" />
<link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon" />

<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/rwd.css') }}" />

</head>

<body class="inactive">

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            
            <div class="inactive-account">
                <h1>Hello, {{ $currentUser->employee->nickname }}!</h1>
                <p>Sorry, your account is inactive.</p>
                <a href="/logout" class="btn btn-primary btn-sm">Log out</a>
            </div>

        </div>
    </div>
</div>

<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

</body>
</html>
