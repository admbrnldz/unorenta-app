@extends('pages.docs.master')
@section('title', 'Docs')
@section('content')
	<div class="article-wrap">
		<div class="doc-article">
			<h1>Dashboard</h1>	
			<!-- onpage-links -->
			<ul class="onpage-links">
				<li>
					<a href="#" class="root-item">Dashboard</a>
					<ul>
						<li><a href="#time-in-approval">Time-in + approval</a></li>
						<li><a href="#break-approval">Break + approval</a></li>
						<li><a href="#time-out-approval">Time-out + approval</a></li>
					</ul>
				</li>
			</ul>

			
			<!-- Time in + approval -->
			<div id="time-in-approval" class="article-item">
				<h2>Time in + Approval</h2>
					<!-- time-in request -->
					<h3>Time in | Request</h3>
					<p class="image-instruction">
						If an employee/agent has a schedued shift for the current date, the time-in button on the dashboard will be enabled and upon clicking the button, it will then send an approval request to TLs, OMs, HR officer and the Admin head.
					</p>
					<div class="screenshot" style="background-image: url(../assets/images/docs_screenshots/dashboard_timeinrequest.png);" ></div>
					<p class="image-instruction">
						After the request has been made, it will then reflect on the TLs/OMs/HR officer/Admin head's end and any of them can approve the time-in log. The time that will be recorded on the employees timecard is the actual time the employee pressed the button. After approval, the time-out button and the break button will then be enabled.
					</p>
					<div class="screenshot" style="background-image: url(../assets/images/docs_screenshots/dashboard_timeinapproval.png);" ></div>
					
					<!-- end time-in request -->
					<!-- employee details -->
					<h3>Employee Details</h3>
					<p class="image-instruction">
						To view each employee's details, simply click a specific employee's icon on the list.
					</p>
					<div class="screenshot prtsc-view-emp2"></div>
					<p class="image-instruction">
						Once you're in the employee details, data can be modified by clicking on action buttons and a dialog box will popup.
					</p>
					<div class="screenshot prtsc-view-emp3"></div>
					<p class="image-instruction">
						After modifying the data, it can then be saved by clicking the submit buttons on the lower part of the panel.
					</p>
					<div class="screenshot prtsc-view-emp4"></div>
					<!-- end of employee details -->		
			</div><!-- end of view/update employee -->
			

			<!-- add/resign -->
			<div id="emp-add-resign" class="article-item">
				<h2>Add/Resign</h2>
					<!-- add employee -->
					<h3>Add Employee</h3>
					<p class="image-instruction">
						To add an employee, simply click the quick-access button on the lower right corner on the employee list page.
					</p>
					<div class="screenshot prtsc-add-emp1"></div>
					<p class="image-instruction">
						A dialog box with a form be displayed and you may fill-in and save the data for new employee by clicking the submit button on the bottom part of the form.
					</p>
					<div class="screenshot prtsc-add-emp2"></div>
					<div class="screenshot prtsc-add-emp3"></div>
					<!-- end of add employee -->
					<!-- resign employee -->
					<h3>Resign Employee</h3>
					<p class="image-instruction">
						To resign an employee, simply proceed to employee details <a href="#emp-view-update">(see employee module)</a> and click resign employee button on the "Employment Details" panel. A warning popup will display then click "Continue". Please take note that since each employee has an account (credentials) for using the system, once an employee resigns, its account will also be deactivated.
					</p>
					<div class="screenshot prtsc-resign-emp"></div>
					<div class="screenshot prtsc-resign-emp2"></div>
			</div>	
			<!-- end of add/resign -->

		</div>
	</div>
@endsection