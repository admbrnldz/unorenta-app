@extends('layouts.app')

@section('title', 'Hire ' . $applicant->first_name)

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Hire {{ $applicant->first_name }}</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li>
                            <a href="{{ route('applicants.interview.details') }}/{{ $applicant->interview->id }}">
                                Applicant Information &amp; Interview Result
                            </a>
                        </li>
                        <li class="active">
                            <a href="{{ route('applicants.hire') }}/{{ $applicant->id }}">
                                Hire {{ $applicant->first_name }}
                            </a>
                        </li>
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">            

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.errors')
                    </div>
                </div>

                <form action="" method="post" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-xs-12">
                            
                            <div id="hire-personal-info" class="panel">
                                <div class="panel-heading">
                                    <h4>Personal Information</h4>
                                </div>
                                <div class="panel-body">
                                    
                                    <div class="form-horizontal form-primary">

                                        <div class="form-group">
                                            <label for="first-name" class="col-xs-12 col-sm-3 control-label">Name</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="first_name" id="first-name" placeholder="" value="{{ old('first_name', $applicant->first_name) }}">
                                                    </div>
                                                </div>
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="middle_name" id="middle-name" placeholder="" value="{{ old('middle_name', $applicant->middle_name) }}">
                                                    </div>
                                                </div>
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="last_name" id="last-name" placeholder="" value="{{ old('last_name', $applicant->last_name) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nickname" class="col-xs-12 col-sm-3 control-label">Nickname</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="nickname" id="nickname" placeholder="" value="{{ old('nickname') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="username" class="col-xs-12 col-sm-3 control-label">Username</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="username" id="username" placeholder="" value="{{ old('username', $username) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="password" class="col-xs-12 col-sm-3 control-label">Password</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="password" class="form-control" name="password" 
                                                            id="password" placeholder="If blank, password will be 12345"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-separator"><span></span></div>

                                        <div class="form-group">
                                            <label for="position" class="col-xs-12 col-sm-3 control-label">Position</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <select name="position_id" id="position" class="form-control auto">
                                                            <option value="">Select a Position</option>
                                                            @foreach ($positions as $position)
                                                            <option value="{{ $position->id }}">
                                                                {{ $position->name }} &mdash; {{ $position->department->name }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="status" class="col-xs-12 col-sm-3 control-label">Employment Status</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <select name="status" id="status" class="form-control auto">
                                                            <option value=""></option>
                                                            @foreach (getEmploymentStatus([5, 6]) as $id => $es)
                                                                <option value="{{ $id }}">{{ $es }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="basic-income" class="col-xs-12 col-sm-3 control-label">Basic Income</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-4">
                                                        <input type="number" name="basic_income" step="0.01" id="basic-income" class="form-control" 
                                                            value="{{ old('basic_income')}}"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="monthly-work-days" class="col-xs-12 col-sm-3 control-label">Monthly Work Days</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-4">
                                                        <input type="number" name="monthly_work_days" step="0.0001" id="monthly-work-days" class="form-control" 
                                                            value="21.6667"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-separator"><span></span></div>

                                        <div class="form-group">
                                            <label for="username" class="col-xs-12 col-sm-3 control-label">Address</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control" name="address" id="address" placeholder="Home No., Street, Subdivision/Village" value="{{ old('address') }}" />
                                                    </div>
                                                </div>
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-5">
                                                        <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ old('city') }}" />
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <input type="text" class="form-control" name="province" id="province" placeholder="Province" value="{{ old('province') }}" />
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input type="text" class="form-control" name="zip_code" id="zip-code" placeholder="Zip Code" value="{{ old('zip_code') }}"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="mobile-number" class="col-xs-12 col-sm-3 control-label">Phone Numbers</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="mobile_number" id="mobile-number" placeholder="Mobile" value="{{ old('mobile_number', $applicant->contact_number) }}" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="home_number" id="home-number" placeholder="Home" value="{{ old('home_number') }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="email-address" class="col-xs-12 col-sm-3 control-label">Email</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="email_address" id="email-address" placeholder="" value="{{ old('email_address', $applicant->email) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-separator"><span></span></div>

                                        <div class="form-group">
                                            <label for="birthdate" class="col-xs-12 col-sm-3 control-label">Birthdate</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="birthdate" id="birthdate" placeholder="" value="{{ old('birthdate') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="birthplace" class="col-xs-12 col-sm-3 control-label">Birthplace</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="birthplace" id="birthplace" placeholder="" value="{{ old('birthplace') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="sex" class="col-xs-12 col-sm-3 control-label">Sex</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="sex" value="Male" />
                                                                <span>Male</span>
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="sex" value="Female" />
                                                                <span>Female</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="civil-status" class="col-xs-12 col-sm-3 control-label">Civil Status</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12">
                                                        @foreach (getCivilStatus() as $civilStatus)
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="civil_status" value="{{ $civilStatus }}" />
                                                                    <span>{{ $civilStatus }}</span>
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="citizenship" class="col-xs-12 col-sm-3 control-label">Citizenship</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-4">
                                                        <select name="citizenship" id="citizenship" class="form-control">
                                                            <option value="Filipino">Filipino</option>
                                                            <option value="Others">Others</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-8">
                                                        <input type="text" class="form-control" name="citizenship_other" id="citizenship-other" placeholder="If others, please specify." value="{{ old('citizenship_other') }}" disabled="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="religion" class="col-xs-12 col-sm-3 control-label">Religion</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="religion" id="religion" placeholder="" value="{{ old('religion') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="height" class="col-xs-12 col-sm-3 control-label">Height/Weight</label>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-6">
                                                        <input type="number" class="form-control" name="height" id="height" step="0.01" placeholder="Height" value="{{ old('height') }}">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <input type="number" class="form-control" name="weight" id="weight" step="0.01" placeholder="Weight" value="{{ old('weight') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <small style="display:block;padding:7px 0 0 0;"><em>*Foot/Kilo</em></small>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="build" class="col-xs-12 col-sm-3 control-label">Build/Eyes/Blood Type</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-4">
                                                        <input type="text" class="form-control" name="build" id="build" placeholder="Build" value="{{ old('build') }}">
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <input type="text" class="form-control" name="eyes" id="eyes" placeholder="Eyes" value="{{ old('eyes') }}">
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <input type="text" class="form-control" name="blood_type" id="blood_type" placeholder="Blood Type" value="{{ old('blood_type') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-separator"><span></span></div>

                                        <div class="form-group">
                                            <label for="sss" class="col-xs-12 col-sm-3 control-label">SSS Number</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="sss" id="sss" placeholder="" value="{{ old('sss') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="tin" class="col-xs-12 col-sm-3 control-label">TIN</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="tin" id="tin" placeholder="" value="{{ old('tin') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="philhealth" class="col-xs-12 col-sm-3 control-label">PhilHealth Number</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="philhealth" id="philhealth" placeholder="" value="{{ old('philhealth') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-last">
                                            <label for="pagibig" class="col-xs-12 col-sm-3 control-label">Pag-IBIG Number</label>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="row input-field-wrap">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <input type="text" class="form-control" name="pagibig" id="pagibig" placeholder="" value="{{ old('pagibig') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <div class="form-button-group clearfix">
                                        <button type="submit" class="btn btn-primary">Hire</button>
                                        <a href="{{ route('applicants.interview.details') }}/{{ $applicant->interview->id }}" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#mobile-number').mask('+63 999 999 9999');
    $('#home-number').mask('+63 99 999 9999');
    $('#sss').mask('99-9999999-9');
    $('#tin').mask('999-999-999-999');
    $('#philhealth').mask('99-999999999-9');
    $('#pagibig').mask('9999-9999-9999');

    $('#guardian-same-address, #contact-same-address').on('change', function() {
        var applicantAddress = $('#address').val() + ', ' + $('#city').val() + ', ' + $('#province').val() + ' ' + $('#zip-code').val(),
            target = $(this).attr('data-target');
        if ($(this).prop('checked')) {            
            $(target).val(applicantAddress);
        }
    });

    $('#add-dependent-entry').on('click', function(e) {
        e.preventDefault();
        var lastChild = $('.dependent-entry:last-child'),
            entryNumber = lastChild.attr('data-count');
        console.log(lastChild.attr('class'));
        addDependentEntry((parseInt(entryNumber) + 1), '.dependent-entries-wrap');
    });

    $(document).on('click', '.remove-me', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-target-entry');
        $(target).remove();
    });

});
function addDependentEntry(entryNumber, dom) {
    var html = '<div class="dependent-entry" id="de-' + entryNumber + '" data-count="' + entryNumber + '">' +
        '<div class="form-group form-group-separator"><span></span></div><div class="form-group">' +
        '<label for="dependent-first-name-' + entryNumber + '" class="col-xs-12 col-sm-3 control-label">Name</label>' +
        '<div class="col-xs-12 col-sm-9"><div class="row" id="contact-address-wrap"><div class="col-xs-12"><div class="row input-field-wrap"><div class="col-xs-12">' +
        '<input type="text" class="form-control" name="dependent[' + entryNumber + '][first_name]" id="dependent-first-name-' + entryNumber + '" placeholder="First Name" />' +
        '</div></div><div class="row input-field-wrap"><div class="col-xs-12">' +
        '<input type="text" class="form-control" name="dependent[' + entryNumber + '][middle_name]" id="dependent-middle-name-' + entryNumber + '" placeholder="Middle Name" />' +
        '</div></div><div class="row input-field-wrap"><div class="col-xs-12">' +
        '<input type="text" class="form-control" name="dependent[' + entryNumber + '][first_name]" id="dependent-last-name-' + entryNumber + '" placeholder="Last Name" />' +
        '</div></div></div></div></div></div><div class="form-group">' +
        '<label for="dependent-birthdate-' + entryNumber + '" class="col-xs-12 col-sm-3 control-label">Birthdate</label>' + 
        '<div class="col-xs-12 col-sm-6"><div class="row" id="contact-address-wrap"><div class="col-xs-12"><div class="row input-field-wrap"><div class="col-xs-12">' +
        '<input type="text" class="form-control" name="dependent[' + entryNumber + '][birthdate]" id="dependent-birthdate-' + entryNumber + '" placeholder="" />' +
        '</div></div></div></div></div></div><div class="form-group">' +
        '<label for="dependent-birthplace-' + entryNumber + '" class="col-xs-12 col-sm-3 control-label">Birthplace</label>' +
        '<div class="col-xs-12 col-sm-6"><div class="row" id="contact-address-wrap"><div class="col-xs-12"><div class="row input-field-wrap"><div class="col-xs-12">' +
        '<input type="text" class="form-control" name="dependent[' + entryNumber + '][birthplace]" id="dependent-birthplace-' + entryNumber + '" placeholder="" />' +
        '</div></div></div></div></div></div><div class="form-group">' +
        '<label for="dependent-relationship-' + entryNumber + '" class="col-xs-12 col-sm-3 control-label">Relationship</label>' +
        '<div class="col-xs-12 col-sm-6"><div class="row" id="contact-address-wrap"><div class="col-xs-12"><div class="row input-field-wrap"><div class="col-xs-12">' +
        '<input type="text" class="form-control" name="dependent[' + entryNumber + '][relationship]" id="dependent-relationship-' + entryNumber + '" placeholder="" />' +
        '</div></div></div></div></div></div><div class="form-group"><div class="col-xs-12 col-sm-6 col-sm-offset-3">'+
        '<button class="remove-me btn btn-sm btn-warning" data-target-entry="#de-' + entryNumber + '"><i class="fa fa-close fa-fw"></i> &nbsp;Remove</button></div></div></div>';
    $(dom).append(html);
}
</script>
@endsection