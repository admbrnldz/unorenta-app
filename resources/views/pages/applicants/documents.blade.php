@extends('layouts.app')

@section('title', 'Interview Details: ' . $applicant->name(true))

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
@if ($applicant->is_flagged)
<section class="section-header with-menu inactive">
@else
<section class="section-header with-menu">
@endif
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $applicant->name(true) }}</h1>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        @if ($applicant->interview)
                            <li><a href="{{ route('applicants.details') }}/{{ $applicant->id }}">Applicant Information &amp; Interview Result</a></li>
                        @else
                            <li><a href="{{ route('applicants.details') }}/{{ $applicant->id }}">Applicant Information</a></li>
                        @endif
                        <li class="active">
                            <a href="{{ route('applicants.documents') }}/{{ $applicant->id }}">
                                Documents ({{ $applicant->documents->count() }})
                            </a>
                        </li>
                        @if (!$applicant->is_hired AND $applicant->interview AND !$applicant->is_flagged)
                            <li><a href="{{ route('applicants.hire') }}/{{ $applicant->id }}">Hire {{ $applicant->first_name }}</a></li>
                        @endif
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">            

            <div class="section-content">
                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                    </div>
                </div>

                @if ($applicant->is_hired)
                {{--*/ $gender = $employee->sex == 'Female' ? 'her' : 'his'; /*--}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-warning">
                                <strong>{{ $applicant->name() }}</strong> was already hired. 
                                <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">Click here to view {{ $gender }} profile</a>.
                            </div>
                        </div>
                    </div>
                @endif

                @if (!$applicant->is_flagged)
                    <div class="row">
                        <div class="col-xs-12">

                            <div class="row" style="margin-bottom: 28px;">
                                <div class="col-xs-12">
                                    <a id="upload-document" title="Upload Document" href="{{ route('applicants.documents.upload') }}/{{ $applicant->id }}"
                                        data-target="#upload-document-form" class="btn btn-sm btn-primary" 
                                    >
                                        Upload Document
                                    </a>
                                </div>
                            </div>

                            <div class="row documents-wrapper">
                                @forelse ($applicant->documents as $document)
                                <div class="col-xs-12 col-sm-6 col-md-4">                                
                                    <div class="document-entry" data-url="{{ $document->file() }}">                                    
                                        @if (strposa(strtolower($document->filename), ['.png', '.jpeg', '.jpg', '.gif'], 0))
                                            <i class="fa fa-pakgenern fa-file-image-o fa-fw"></i>
                                        @elseif (strposa(strtolower($document->filename), ['.doc', '.docx'], 0))
                                            <i class="fa fa-pakgenern fa-file-word-o fa-fw"></i>
                                        @elseif (strposa(strtolower($document->filename), ['.pdf'], 0))
                                            <i class="fa fa-pakgenern fa-file-pdf-o fa-fw"></i>
                                        @else
                                            <i class="fa fa-pakgenern fa-file-o fa-fw"></i>
                                        @endif
                                        @if ($document->description == null)
                                            {{ $document->filename }}
                                        @else
                                            {{ $document->description }}
                                        @endif
                                        <a href="{{ route('applicants.documents.edit') }}/{{ $document->id }}" data-target="#edit-document-form" data-delete="{{ route('applicants.documents.remove') }}/{{ $document->id }}" class="edit-description"><i class="fa fa-pencil fa-fw"></i></a><br />
                                        <em><small>{{ processDate($document->created_at, true) }}</small></em>
                                    </div>
                                    <a href="{{ route('applicants.documents.remove') }}/{{ $document->id }}" class="remove-document"><i class="fa fa-close"></i></a>
                                </div>
                                @empty
                                <div class="col-xs-12">
                                    <div class="no-documents">
                                        Click to upload document
                                    </div>
                                </div>
                                @endforelse
                            </div>

                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-danger">
                                This applicant is marked as flagged. If you wish to upload or remove any document, please mark this applicant as <a href="{{ route('applicants.unflag') }}/{{ $applicant->id }}" id="unflag-applicant">unflagged</a> first.
                            </div>
                        </div>
                    </div>
                @endif

            </div>           

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {
 
    $('#upload-document').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target');
        $.get( url ).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Upload',
                        className: 'btn-primary',
                        callback: function() {
                            
                            $(targetForm).submit();

                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $('.document-entry').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            window.open($(this).attr('data-url'), '_blank');
        });
    });

    $('.no-documents').on('click', function() {
        $('#upload-document').click();
    });

    $('.edit-description').each(function() {
        $(this).on('click', function(e) {            
            e.preventDefault();            
            e.stopPropagation();
            var url = $(this).attr('href'),
                target = $(this).attr('data-target'),
                deleteurl = $(this).attr('data-delete');
            $.get( url ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {                                
                                $(target).submit();
                            }
                        },
                        'Remove' : {
                            className: 'btn-danger',
                            callback: function() {
                                window.location = deleteurl;
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

});
</script>
@endsection