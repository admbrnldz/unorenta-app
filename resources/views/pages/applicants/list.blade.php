@extends('layouts.app')

@section('title', 'Applicants')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Applicants</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="{{ $activeMenu1 }}"><a href="{{ route('applicants') }}">Applicants</a></li>
                        <li class="{{ $activeMenu2 }}"><a href="{{ route('applicants') }}?a=flagged">Flagged ({{ $flaggedCount }})</a></li>
                        <li class="{{ $activeMenu3 }}"><a href="{{ route('applicants') }}?a=hired">Hired ({{ $hiredCount }})</a></li>
                        <li><a href="{{ route('assessments') }}">Assessment Guides</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.errors')
                        @include('includes.status')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Applicant Name</th>
                                                <th>Contact Number</th>
                                                <th>Email</th>
                                                <th>Documents</th>
                                                <th class="ta-right">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($applicants as $applicant)
                                            @if ($applicant->isInterviewed())
                                                <tr class="tr-primary">
                                            @else
                                                <tr>
                                            @endif
                                                <td class="table-sneak">
                                                    <a href="{{ route('applicants.quickview') }}/{{ $applicant->id }}" class="sneak">
                                                        <i class="fa fa-dedent fa-fw"></i>
                                                    </a>
                                                </td>
                                                <td class="table-highlight">
                                                    <a href="{{ route('applicants.details') }}/{{ $applicant->id }}">
                                                        {{ $applicant->name(true) }}
                                                    </a>                                                    
                                                </td>
                                                <td>{{ $applicant->contact_number }}</td>
                                                <td>
                                                    <a href="mailto:{{ $applicant->email }}" class="email-applicant">
                                                        {{ $applicant->email }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('applicants.documents') }}/{{ $applicant->id }}">
                                                        {{ $applicant->documents()->count() }}
                                                    </a>
                                                </td>
                                                <td class="ta-right">
                                                    @if ($applicant->isInterviewed() AND !$applicant->is_hired)
                                                        <span class="label label-primary">Interviewed</span>
                                                    @else
                                                        @if ($applicant->is_hired)
                                                            <span class="label label-success">Hired</span>
                                                        @else
                                                            <span class="label label-default">Waiting for Interview</span>
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="row human-listing applicants-list hidden">
                    @foreach ($applicants as $applicant)
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="human-entry clearfix applicant-entry" 
                                data-url="{{ route('applicants.details') }}/{{ $applicant->id }}"
                                <?php if ($applicant->interview): ?>
                                    data-interviewed="1"
                                    data-interview-page="{{ route('applicants.interview.details') }}/{{ $applicant->interview->id}}"
                                <?php else: ?>
                                    data-interviewed="0"
                                <?php endif; ?>
                                <?php if ($applicant->is_flagged): ?>
                                    title="<?php echo ($applicant->duration - $applicant->flagDays()); ?> days left"
                                <?php endif; ?>
                            >
                                {{--*/ $class = '' /*--}}
                                @if ($applicant->interview) 
                                    {{--*/ $class = 'interviewed' /*--}}
                                @endif
                                @if ($applicant->is_hired)
                                    {{--*/ $class = 'hired' /*--}}
                                @endif
                                @if (!$applicant->is_flagged)
                                    <div class="applicant-status-indicator {{ $class }}" title="{{ ucfirst($class) }}"><span></span></div>
                                @endif
                                <div class="human-info">
                                    <h4>{{ $applicant->name(true) }}</h4>
                                    @if ($applicant->is_flagged)
                                        <span class="human-meta">
                                            <?php echo ($applicant->duration - $applicant->flagDays()); ?> days left<br />
                                            <a href="{{ route('applicants.unflag') }}/{{ $applicant->id }}">Unflag</a>
                                        </span>
                                    @else
                                        <span class="human-meta">
                                            {{ $applicant->contact_number }}<br />
                                            <a href="mailto:{{ $applicant->email }}" class="email-applicant">{{ $applicant->email }}</a>
                                        </span>
                                    @endif
                                </div>
                                @if ($applicant->is_flagged)
                                    <div class="flag-loader"><div class="flag-loading" style="width:{{ ($applicant->flagDays() / $applicant->duration) * 100 }}%;"></div></div>
                                @endif
                            </div>
                        </div>
                    @endforeach                    
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="human-entry human-entry-add">
                            <a href="{{ route('applicants.add') }}" class="add-applicant">Add Applicant</a>
                        </div>
                    </div>
                </div>
            </div>
               
        </div>
        </div>
    </div>
</section>
<div class="page-actions">
    <ul class="clearfix">
        <li>
            <a href="{{ route('applicants.add') }}" class="pa-primary add-applicant">
                <i class="fa fa-plus"></i>
            </a>
        </li>
    </ul>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('.email-applicant').each(function() {
        $(this).on('click', function(e) {
            e.stopPropagation();
            window.location.href = $(this).attr('href');
        });
    });

    $('.add-applicant').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.get({ url: url }).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Add',
                            className: 'btn-primary',
                            callback: function() {
                                var form = $('#add-applicant-form');
                                form.submit();
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    $('.applicant-entry').each(function() {
        $(this).on('click', function(e) {
            e.stopPropagation();
            var url = $(this).attr('data-url')
                interviewed = $(this).attr('data-interviewed');

            window.location = url;

            // if (interviewed == 1) {
            //     window.location = $(this).attr('data-interview-page');
            // } else {
            //     $.get({ url: url }).done(function(r) {
            //         r = $.parseJSON(r);
            //         bootbox.dialog({
            //             title: r.title,
            //             message: r.view,
            //             buttons: {
            //                 success: {
            //                     label: 'Update',
            //                     className: 'btn-primary',
            //                     callback: function() {
            //                         $('#edit-applicant-form').submit();
            //                     }
            //                 },
            //                 'Remove' : {
            //                     className: 'btn-danger',
            //                     callback: function() {
            //                         var deleteURL = $('#edit-applicant-form').attr('data-delete');
            //                         window.location = deleteURL;
            //                     }
            //                 },
            //                 'Cancel' : {
            //                     className: 'btn-default'
            //                 }
            //             }
            //         });
            //     });
            // }

        });
    });

    $('.delete-applicant').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            bootbox.dialog({
                title: 'Delete Applicant',
                message: '<div class="ta-center">Are you sure you want to delete this applicant?</div>',
                buttons: {
                    success: {
                        label: 'Yes',
                        className: 'btn-primary',
                        callback: function() {
                            window.location = url;
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $('.edit-applicant').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.get({ url: url }).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {
                                $('#edit-applicant-form').submit();
                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

});
</script>
@endsection