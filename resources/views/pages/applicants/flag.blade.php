<form action="{{ route('applicants.flag') }}/{{ $applicant->id }}" method="post" autocomplete="off" id="flag-applicant-form">
{!! csrf_field() !!}

    <div class="ta-center" style="margin-bottom:24px;">
        Please specify when will be the flagging starts and the duration <em>(in days)</em>.
    </div>

    <div class="form-horizontal form-primary">

        <div class="form-group form-group-last">
            <div class="col-xs-12 col-sm-8">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" name="flag_start" id="flag-start" class="form-control" placeholder="Flag start" value="{{ formatDate($today, true) }}" />
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" name="duration" id="duration" class="form-control" placeholder="Duration" value="90" />
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>
<script>
$(function() {

    $('#flag-start').datepicker({
        dateFormat: 'MM d, yy'
    });

});
</script>