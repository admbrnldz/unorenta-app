<form action="{{ route('applicants.documents.upload') }}/{{ $applicant->id }}" method="post" id="upload-document-form" enctype="multipart/form-data" autocomplete="off">   
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <div class="col-xs-12">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="file" name="document" id="document" class="form-control form-control-file" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <div class="col-xs-12">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <textarea name="description" id="description" rows="2" class="form-control autosize" placeholder="Document Description"></textarea>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    var count = 1;
    $('.form-control-file').each(function() {
        $(this).attr('data-parent', 'fcfw-' + count);
        $(this).wrap('<div id="fcfw-' + count + '" class="form-control-file-wrapper"></div>');
        $(this).after('<div class="form-file-placeholder" data-target="#fcfw-' + count + '"><i class="fa fa-paperclip"></i><span>Choose file</span></div>');
        count++;
    });

    $('.form-control-file').each(function() {
        $(this).on('change', function() {
            var filename = $(this).val().split('\\');
            console.log(filename[2]);
            $('div[data-target="#' + $(this).attr('data-parent') + '"').find('span').html(filename[2]);
        });
    });

    autosize($('.autosize'));

});
</script>