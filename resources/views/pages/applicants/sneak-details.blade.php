<div class="sneak-applicant <?php echo $applicant->is_flagged == true ? 'inactive' : ''; ?>">
    
    <div class="sneak-meta">
        <h1>{{ $applicant->name(true) }}</h1>
        <h4></h4>        
    </div>

    <div class="sneak-tabbing">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#contact-details" aria-controls="payslip-details" role="tab" data-toggle="tab">Contact Info</a></li>
            @if ($applicant->interview)
            <li role="presentation"><a href="#interview-assessment" aria-controls="interview-assessment" role="tab" data-toggle="tab">Assessment</a></li>
            <li role="presentation"><a href="#interview-post-assessment" aria-controls="interview-post-assessment" role="tab" data-toggle="tab">Post-Assessment</a></li>
            @endif
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="contact-details">

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><em>Contact Number</em></td>
                                <td class="ta-right table-highlight">
                                    {{ $applicant->contact_number }}
                                </td>
                            </tr>
                            <tr>
                                <td><em>Email Address</em></td>
                                <td class="ta-right table-highlight">
                                    <a href="mailto:{{ $applicant->email }}">
                                        {{ $applicant->email }}
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td><em>Skype</em></td>
                                <td class="ta-right table-highlight">
                                    {{ $applicant->skype }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="sneak-action">
                    @if (!$applicant->interview AND !$applicant->is_flagged)
                        <a href="{{ route('applicants.interview') }}/{{ $applicant->id }}" class="btn btn-success">Start Interview</a>
                    @endif
                    @if ($applicant->interview)
                        <a href="{{ route('applicants.hire') }}/{{ $applicant->id }}" class="btn btn-primary">Hire {{ $applicant->first_name }}</a>
                    @endif
                    @if ($applicant->is_flagged)
                        <a href="{{ route('applicants.unflag') }}/{{ $applicant->id }}" class="btn btn-danger" id="unflag-applicant">Unflag</a>
                    @else
                        <a href="{{ route('applicants.flag') }}/{{ $applicant->id }}" class="btn btn-danger" id="flag-applicant">Flag</a>
                    @endif
                </div>

            </div>
            @if ($applicant->interview)
            <div role="tabpanel" class="tab-pane" id="interview-assessment">
                
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            {{--*/ $assessments = $applicant->interview->assessments /*--}}
                            @foreach ($assessments as $assessment)
                            <tr>
                                <td>
                                    <strong>{{ $assessment->assessmentGuide->question }}</strong><br />
                                    <div class="the-rating">
                                        <ul class="clearfix">
                                            @for ($i = 1; $i <= 5; $i++)
                                                <li>
                                                    @if ($i <= $assessment->rating)
                                                        <i class="fa fa-star"></i>
                                                    @else
                                                        <i class="fa fa-star-o"></i>
                                                    @endif
                                                </li>
                                            @endfor
                                        </ul>
                                    </div>
                                    <em>
                                        @if ($assessment->comments != '')
                                            {{ $assessment->comments }}
                                        @else
                                            No comment.
                                        @endif
                                    </em>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="sneak-action">
                    <a href="{{ route('applicants.hire') }}/{{ $applicant->id }}" class="btn btn-primary">Hire {{ $applicant->first_name }}</a>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="interview-post-assessment">

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            {{--*/ $postAssessments = $applicant->interview->postAssessments /*--}}
                            @foreach ($postAssessments as $postAssessment)
                            <tr>
                                <td>
                                    <strong>{{ $postAssessment->postAssessmentGuide->question }}</strong><br />
                                    <em>
                                        @if ($postAssessment->comments != '')
                                            {{ $postAssessment->comments }}
                                        @else
                                            No comment.
                                        @endif
                                    </em>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="sneak-action">
                    <a href="{{ route('applicants.hire') }}/{{ $applicant->id }}" class="btn btn-primary">Hire {{ $applicant->first_name }}</a>
                </div>

            </div>
            @endif
        </div>

    </div>

</div>