@extends('layouts.app')

@section('title', 'Interview Details: ' . $applicant->name(true))

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
@if ($applicant->is_flagged)
<section class="section-header with-menu inactive">
@else
<section class="section-header with-menu">
@endif
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $applicant->name(true) }}</h1>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        @if ($applicant->interview)
                            <li class="active"><a href="{{ route('applicants.details') }}/{{ $applicant->id }}">Applicant Information &amp; Interview Result</a></li>
                        @else
                            <li class="active"><a href="{{ route('applicants.details') }}/{{ $applicant->id }}">Applicant Information</a></li>
                        @endif
                        <li>
                            <a href="{{ route('applicants.documents') }}/{{ $applicant->id }}">
                                Documents ({{ $applicant->documents->count() }})
                            </a>
                        </li>
                        @if (!$applicant->is_hired AND $applicant->interview AND !$applicant->is_flagged)
                            <li><a href="{{ route('applicants.hire') }}/{{ $applicant->id }}">Hire {{ $applicant->first_name }}</a></li>
                        @endif
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">
                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                    </div>
                </div>

                @if ($applicant->is_hired)
                {{--*/ $gender = $employee->sex == 'Female' ? 'her' : 'his'; /*--}}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-warning">
                                <strong>{{ $applicant->name() }}</strong> was already hired. 
                                <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">Click here to view {{ $gender }} profile</a>.
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-xs-12 col-md-4">

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Applicant Information</h4>
                                @can ('edit_applicant', $currentUser)
                                    @if (!$applicant->is_hired AND !$applicant->is_flagged)
                                        <div class="panel-heading-edit">
                                            <a id="edit-applicant-info" title="Edit Applicant Information"
                                                href="{{ route('applicants.edit') }}/{{ $applicant->id }}"
                                                data-target-form="#edit-applicant-form"
                                            >
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">
                                    
                                    @include('pages.applicants.applicant-info')
                                    
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-xs-12 col-md-8">

                        @if ($applicant->interview)
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4>Assessment Guide</h4>
                                </div>
                                <div class="panel-body">

                                    <ol class="assessment-guides">
                                        @foreach ($applicant->interview->assessments as $assessment)
                                        <li class="assessment-result">
                                            <h4 class="the-question">{{ $assessment->assessmentGuide->question }}</h4>
                                            <div class="the-rating">
                                                <ul class="clearfix">
                                                    @for ($i = 1; $i <= 5; $i++)
                                                        <li>
                                                            @if ($i <= $assessment->rating)
                                                                <i class="fa fa-star"></i>
                                                            @else
                                                                <i class="fa fa-star-o"></i>
                                                            @endif
                                                        </li>
                                                    @endfor
                                                </ul>
                                            </div>
                                            <div class="the-comment">
                                                @if ($assessment->comments != '')
                                                    {{ $assessment->comments }}
                                                @else
                                                    No comment.
                                                @endif
                                            </div>
                                        </li>
                                        @endforeach
                                    </ol>

                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading">
                                    <h4>Post-Assessmeng Guide</h4>
                                </div>
                                <div class="panel-body">

                                    <ol class="assessment-guides post-assessment-guides">
                                        @foreach ($applicant->interview->postAssessments as $postAssessment)
                                        <li class="assessment-result">
                                            <h4 class="the-question">{{ $postAssessment->postAssessmentGuide->question }}</h4>
                                            <div class="the-comment">
                                                @if ($postAssessment->comments != '')
                                                    {{ $postAssessment->comments }}
                                                @else
                                                    No comment.
                                                @endif
                                            </div>
                                        </li>
                                        @endforeach
                                    </ol>

                                </div>
                                @if (!$applicant->is_hired)
                                    <div class="panel-footer">
                                        <div class="form-button-group clearfix">

                                            @if ($applicant->is_flagged)
                                                <a href="{{ route('applicants.unflag') }}/{{ $applicant->id }}" class="btn btn-danger" id="unflag-applicant">Unflag</a>
                                            @else
                                                <a href="{{ route('applicants.hire') }}/{{ $applicant->id }}" class="btn btn-primary">Hire {{ $applicant->first_name }}</a>
                                                <a href="{{ route('applicants.flag') }}/{{ $applicant->id }}" class="btn btn-danger" id="flag-applicant">Flag</a>
                                            @endif

                                        </div>
                                    </div>
                                @endif
                            </div>
                        @else
                            @can ('interview_applicants', $currentUser)
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-button-group clearfix">                     

                                                @if ($applicant->is_flagged)
                                                    <a href="{{ route('applicants.unflag') }}/{{ $applicant->id }}" class="btn btn-danger" id="unflag-applicant">Unflag</a>
                                                @else
                                                    <a href="{{ route('applicants.interview') }}/{{ $applicant->id }}" class="btn btn-success">Start Interview</a>
                                                    <a href="{{ route('applicants.flag') }}/{{ $applicant->id }}" class="btn btn-danger" id="flag-applicant">Flag</a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endcan
                        @endif

                    </div>
                </div>
            </div>           

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {
 
    $('#edit-applicant-info').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target-form');
        $.get(url).done(function(r) {
            r = $.parseJSON(r);
            var hideDelete = '';
            if (r.interviewed) {
                hideDelete = 'hidden';
            }
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Update',
                        className: 'btn-primary',
                        callback: function() {
                            $('#edit-applicant-form').submit();
                        }
                    },
                    'Remove' : {
                        className: 'btn-danger ' + hideDelete,
                        callback: function() {
                            var deleteURL = $(targetForm).attr('data-delete');
                            window.location = deleteURL;
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

});
</script>
@endsection