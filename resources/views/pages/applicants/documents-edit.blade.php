<form action="{{ route('applicants.documents.edit') }}/{{ $document->id }}" method="post" id="edit-document-form" enctype="multipart/form-data" autocomplete="off">   
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group form-group-last">
        <div class="col-xs-12">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <textarea name="description" id="description" rows="2" class="form-control autosize" placeholder="Document Description">{{ $document->description }}</textarea>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    autosize($('.autosize'));

});
</script>