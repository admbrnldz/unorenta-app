@extends('layouts.app')

@section('title', 'Interview ' . $applicant->name(true))

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $applicant->name(true) }}</h1>
                    <div class="details-page-meta">
                        <ul class="clearfix">
                            <li><i class="fa fa-calendar-o fa-fw"></i> &nbsp; {{ processDate($startTime) }}</li>
                            <li><i class="fa fa-commenting fa-fw"></i> &nbsp; {{ $currentUser->name() }}</li>
                        </ul>
                    </div>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">

            @include('includes.status')

            <div class="section-content">
                <div class="row">
                    <div class="col-xs-12">

                        <form action="{{ route('applicants.interview') }}/{{ $applicant->id }}" method="post" autocomplete="off">
                        {!! csrf_field() !!}
                        <input type="hidden" name="date_interview" value="{{ $startTime }}" />

                            <div class="panel">
                                <div class="panel-heading">
                                    <h4>Assessment Guide</h4>
                                </div>
                                <div class="panel-body">

                                    <ol class="assessment-guides">
                                    @foreach ($categories as $category)
                                        <li class="category-entry">
                                            <div class="category-title">
                                                <h4>{{ $category->name }}</h4>
                                            </div>
                                            @if (count($category->assessments) > 0)
                                            <ul class="assessment-guides">
                                                @foreach ($category->assessments()->orderBy('order')->get() as $assessmentGuide)
                                                <li>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <label class="the-question" for="ag-{{ $assessmentGuide->id }}">{{ $assessmentGuide->question }}</label>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="rating">
                                                                <input type="hidden" name="assessment[{{ $assessmentGuide->id }}][rating]" value="0" id="rating-question-" />
                                                                <ul class="clearfix">
                                                                    <li data-rating="1"><i class="fa fa-star-o"></i></li>
                                                                    <li data-rating="2"><i class="fa fa-star-o"></i></li>
                                                                    <li data-rating="3"><i class="fa fa-star-o"></i></li>
                                                                    <li data-rating="4"><i class="fa fa-star-o"></i></li>
                                                                    <li data-rating="5"><i class="fa fa-star-o"></i></li>
                                                                </ul>
                                                            </div>            
                                                        </div>
                                                        <div class="col-xs-12 input-field-wrap">
                                                            <textarea name="assessment[{{ $assessmentGuide->id }}][comments]" rows="2" id="ag-{{ $assessmentGuide->id }}" class="form-control autosize"></textarea>
                                                        </div>
                                                    </div>
                                                </li>

                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                    </ol>

                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-heading">
                                    <h4>Post-Assessmeng Guide</h4>
                                </div>
                                <div class="panel-body">
                                    
                                    <ol class="assessment-guides post-assessment-guides">
                                        @foreach ($postAssessmentGuides as $pag)
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label class="the-question" for="pag-{{ $pag->id }}">{{ $pag->question }}</label>  
                                                </div>                                            
                                                <div class="col-xs-12 input-field-wrap">
                                                    <textarea name="postassessment[{{ $pag->id }}][comments]" rows="2" id="pag-{{ $pag->id }}" class="form-control autosize"></textarea>
                                                </div>
                                            </div>                                        
                                        </li>
                                        @endforeach
                                    </ol>

                                </div>
                                <div class="panel-footer">
                                    <div class="form-button-group clearfix">
                                        <button type="submit" class="btn btn-primary">Done Interview</button>
                                        <a href="{{ route('applicants') }}" class="btn btn-default">Cancel</a>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>           

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    autosize($('.autosize'));

    $('#conform-memo').on('click', function(e) {
        e.preventDefault();        
        $(this).prepend('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;');

        var form = $('#conform-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post({
            url: url,
            data: data
        }).done(function(response) {            
            response = $.parseJSON(response);

            $('#conform-memo').find('.fa').removeClass('fa-circle-o-notch fa-spin')
                .addClass('fa-check');

            setTimeout(function() {
                var html = '<div class="dp-handler" style="background:url(' + response.dp + ') center center no-repeat;background-size:cover;">' +
                    '<a href="' + response.profileUrl + '" title="You"></a>' +
                    '</div>';
                $('.memo-conforme li:first-child').html(html);
            }, 2000);
        });
    });

    $('#sign-memo').on('click', function(e) {
        e.preventDefault();
        $(this).remove('span').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');

        var form = $('#sign-memo-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post({
            url: url,
            data: data
        }).done(function(response) {
            response = $.parseJSON(response);

            var target = $('#signature-pad-' + response.id),
                style = 'background:url(' + response.signature + ') left center no-repeat;background-size:auto 60px;';

            target.html('');
            target.attr('style', style);
        });
    });

    $('#post-comment').on('click', function(e) {
        e.preventDefault();
        $(this).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>&nbsp;Post');
        $('#post-comment').prop('disabled', true);

        if ($('#message').val() == '') {
            $(this).html('<i class="fa fa-close fa-fw"></i>&nbsp;Post');
            $(this).removeClass('btn-primary').addClass('btn-danger');
            return false;
        }

        var form = $('#post-memo-comment-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post({
            url: url,
            data: data
        }).done(function(response) {
            $('#post-comment').find('.fa').removeClass('fa-circle-o-notch fa-spin')
                .addClass('fa-check');
            $('#message').val('');
            $('.post-discussion-thread').append(response);
            setTimeout(function() {
                $('#post-comment').html('Post');
                $(this).addClass('btn-primary').removeClass('btn-danger');
            }, 3000);
        });
    });
    

});
</script>
@endsection