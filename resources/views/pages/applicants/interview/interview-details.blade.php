@extends('layouts.app')

@section('title', 'Interview Details: ' . $interview->applicant->name(true))

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h1>{{ $interview->applicant->name(true) }}</h1>
                    <div class="details-page-meta">
                        <ul class="clearfix">
                            <li><i class="fa fa-calendar-o fa-fw"></i> &nbsp; {{ processDate($interview->date_interview) }}</li>
                            <li><i class="fa fa-commenting fa-fw"></i> &nbsp; {{ $interview->theInterviewer->name() }}</li>
                        </ul>
                    </div>                    
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active"><a href="{{ route('applicants.interview.details') }}/{{ $interview->id }}">Applicant Information &amp; Interview Result</a></li>
                        @if (!$interview->applicant->is_hired)
                            <li><a href="{{ route('applicants.hire') }}/{{ $interview->applicant->id }}">Hire {{ $interview->applicant->first_name }}</a></li>
                        @endif
                    </ul>
                </div>
            
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">            

            <div class="section-content">
                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                    </div>
                </div>

                @if ($interview->applicant->is_hired)
                <?php
                    $gender = 'his';
                    if ($employee->sex == 'Female') {
                        $gender = 'her';
                    }
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-warning">
                            <strong>{{ $interview->applicant->name() }}</strong> was already hired. 
                            <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">Click here to view {{ $gender }} profile</a>.
                        </div>
                    </div>
                </div>
                @endif

                <div class="row">
                    <div class="col-xs-12 col-md-4">

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Applicant Information</h4>
                                @can ('edit_applicant', $currentUser)
                                    @if (!$interview->applicant->is_hired)
                                        <div class="panel-heading-edit">
                                            <a id="edit-applicant-info" title="Edit Applicant Information"
                                                href="{{ route('applicants.edit') }}/{{ $interview->applicant->id }}"
                                                data-target-form="#edit-applicant-form"
                                            >
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="panel-body">
                                <ul class="panel-body-list profile-details">
                                    <li>
                                        <small>Name</small>
                                        <span>{{ $interview->applicant->name() }}</span>
                                    </li>
                                    <li>
                                        <small>Contact Number</small>
                                        <span>{{ $interview->applicant->contact_number }}</span>
                                    </li>
                                    <li>
                                        <small>Email</small>
                                        <span>{{ $interview->applicant->email }}</span>
                                    </li>
                                    <li>
                                        <small>Skype ID</small>
                                        <span>{{ $interview->applicant->skype }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-xs-12 col-md-8">

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Assessment Guide</h4>
                            </div>
                            <div class="panel-body">

                                <ol class="assessment-guides">
                                    @foreach ($interview->assessments as $assessment)
                                    <li class="assessment-result">
                                        <h4 class="the-question">{{ $assessment->assessmentGuide->question }}</h4>
                                        <div class="the-rating">
                                            <ul class="clearfix">
                                                @for ($i = 1; $i <= 5; $i++)
                                                    <li>
                                                        @if ($i <= $assessment->rating)
                                                            <i class="fa fa-star"></i>
                                                        @else
                                                            <i class="fa fa-star-o"></i>
                                                        @endif
                                                    </li>
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="the-comment">
                                            @if ($assessment->comments != '')
                                                {{ $assessment->comments }}
                                            @else
                                                No comment.
                                            @endif
                                        </div>
                                    </li>
                                    @endforeach
                                </ol>

                            </div>
                        </div>

                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Post-Assessmeng Guide</h4>
                            </div>
                            <div class="panel-body">

                                <ol class="assessment-guides post-assessment-guides">
                                    @foreach ($interview->postAssessments as $postAssessment)
                                    <li class="assessment-result">
                                        <h4 class="the-question">{{ $postAssessment->postAssessmentGuide->question }}</h4>
                                        <div class="the-comment">
                                            @if ($postAssessment->comments != '')
                                                {{ $postAssessment->comments }}
                                            @else
                                                No comment.
                                            @endif
                                        </div>
                                    </li>
                                    @endforeach
                                </ol>

                            </div>
                            @if (!$interview->applicant->is_hired)
                                <div class="panel-footer">
                                    <div class="form-button-group clearfix">
                                        <a href="{{ route('applicants.hire') }}/{{ $interview->applicant->id }}" class="btn btn-primary">Hire {{ $interview->applicant->first_name }}</a>
                                        <a href="{{ route('applicants.flag') }}/{{ $interview->applicant->id }}" class="btn btn-danger" id="flag-applicant">Flag</a>
                                    </div>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>           

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {
 
    $('#edit-applicant-info').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-target-form');
        $.get(url).done(function(r) {
            r = $.parseJSON(r);
            var hideDelete = '';
            if (r.interviewed) {
                hideDelete = 'hidden';
            }
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Update',
                        className: 'btn-primary',
                        callback: function() {
                            $('#edit-applicant-form').submit();
                        }
                    },
                    'Remove' : {
                        className: 'btn-danger ' + hideDelete,
                        callback: function() {
                            var deleteURL = $(targetForm).attr('data-delete');
                            window.location = deleteURL;
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        })
    });

});
</script>
@endsection