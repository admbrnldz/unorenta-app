<li>
    <small>Name</small>
    <span>{{ $applicant->name() }}</span>
</li>
<li>
    <small>Contact Number</small>
    <span>{{ $applicant->contact_number }}</span>
</li>
<li>
    <small>Email</small>
    <span>{{ $applicant->email }}</span>
</li>
<li>
    <small>Skype ID</small>
    <span>{{ $applicant->skype }}</span>
</li>
@if ($applicant->interview)
<li>
    <small>Date Interviewed</small>
    <span>{{ processDate($applicant->interview->date_interview) }}</span>
</li>
<li>
    <small>Interviewer</small>
    <span>
        <a href="{{ route('employees.profile', ['employeeId' => $applicant->interview->theInterviewer->id]) }}">
            {{ $applicant->interview->theInterviewer->name() }}
        </a>
    </span>
</li>
@endif