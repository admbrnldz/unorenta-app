<form method="post" autocomplete="off" id="edit-applicant-form"
    action="{{ route('applicants.edit') }}/{{ $applicant->id }}" 
    data-delete="{{ route('applicants.delete') }}/{{ $applicant->id }}"
>
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="first-name" class="col-xs-12 col-sm-3 control-label">Name</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="first_name" id="first-name" placeholder="First Name" value="{{ $applicant->first_name}}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="middle_name" id="middle-name" placeholder="Middle Name" value="{{ $applicant->middle_name}}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="last_name" id="last-name" placeholder="Last Name" value="{{ $applicant->last_name}}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="contact-number" class="col-xs-12 col-sm-3 control-label">Contact Number</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="contact_number" id="contact-number" value="{{ $applicant->contact_number }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-xs-12 col-sm-3 control-label">Email Address</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="email" id="email" value="{{ $applicant->email }}" />
                </div>
            </div>
        </div>
    </div>
    @if ($applicant->interview)
    <div class="form-group form-group-last">
    @else
    <div class="form-group">
    @endif
        <label for="skype" class="col-xs-12 col-sm-3 control-label">Skype ID</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="skype" id="skype" value="{{ $applicant->skype }}" />
                </div>
            </div>
        </div>
    </div>
    @if (!$applicant->interview AND $currentUser->can('interview_applicants', $currentUser))
    <div class="form-group form-group-last">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="form-button-group clearfix">
                <a href="{{ route('applicants.interview') }}/{{ $applicant->id }}" class="btn btn-sm btn-success">
                    Start Interview
                </a>
            </div>
        </div>
    </div>
    @endif

</div>
</form>