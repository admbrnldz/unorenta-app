<form action="{{ route('applicants.add') }}" method="post" autocomplete="off" id="add-applicant-form">
{!! csrf_field() !!}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="first-name" class="col-xs-12 col-sm-3 control-label">Name</label>
        <div class="col-xs-12 col-sm-9">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="first_name" id="first-name" placeholder="First Name" value="{{ old('first_name') }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="middle_name" id="middle-name" placeholder="Middle Name" value="{{ old('middle_name') }}" />
                </div>
            </div>
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="last_name" id="last-name" placeholder="Last Name" value="{{ old('last_name') }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="contact-number" class="col-xs-12 col-sm-3 control-label">Contact Number</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="contact_number" id="contact-number" value="{{ old('contact_number') }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-xs-12 col-sm-3 control-label">Email Address</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="email" id="email" value="{{ old('email') }}" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="skype" class="col-xs-12 col-sm-3 control-label">Skype ID</label>
        <div class="col-xs-12 col-sm-6">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="skype" id="skype" value="{{ old('skype') }}" />
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('#contact-number').mask('+63 999 999 9999');

});
</script>