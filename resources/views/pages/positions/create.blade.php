<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('positions.add') }}" method="post" autocomplete="off" id="add-position-form">
{{ csrf_field() }}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="name" class="col-xs-12 col-sm-4 control-label">Position Name</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ $errors->has('name') ? '' : old('name') }}">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="position-id" class="col-xs-12 col-sm-4 control-label">Department</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select name="department_id" id="position-id" class="form-control">
                        <option value=""></option>
                        @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <?php /*
    <div class="form-group">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="form-button-group clearfix">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('departments') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
    */ ?>

</div>
</form>