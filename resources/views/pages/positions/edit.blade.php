<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('positions.edit') }}/{{ $position->id }}" method="post" autocomplete="off" id="edit-position-form">
{{ csrf_field() }}

    <div class="form-horizontal form-primary">
        
        <div class="form-group">
            <label for="name" class="col-xs-12 col-sm-3 control-label">Position Name</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ $errors->has('name') ? old('name') : $position->name }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-group-last">
            <label for="position-id" class="col-xs-12 col-sm-3 control-label">Department</label>
            <div class="col-xs-12 col-sm-9">
                <div class="row input-field-wrap">
                    <div class="col-xs-12">
                        <select name="department_id" id="position-id" class="form-control">
                            <option value=""></option>
                            @foreach ($departments as $department)
                            <option value="{{ $department->id }}"
                                <?php if ($department->id == $position->department_id): ?>selected<?php endif; ?>>{{ $department->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>