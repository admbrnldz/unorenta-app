<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('departments.edit') }}/{{ $department->id }}" method="post" autocomplete="off" id="edit-department-form">
{{ csrf_field() }}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="name" class="col-xs-12 col-sm-4 control-label">Department Name</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="name" id="name" value="{{ $department->name }}">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="schedulers" class="col-xs-12 col-sm-4 control-label">Schedulers</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select id="schedulers" name="schedulers[]" multiple="multiple" class="form-control">
                        @foreach ($employees as $employee)
                        <option value="{{ $employee->id }}"
                            <?php if ($department->schedulers()->where('employee_id', $employee->id)->where('department_id', $department->id)->count() > 0): ?>selected<?php endif; ?>
                        >
                            {{ $employee->user->name() }}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
$(function() {

    $('#schedulers').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5
    });

});
</script>