<?php /*
@if (count($department->employees()) > 0)
<div class="row human-listing" style="margin-bottom: -28px;">

    @foreach ($department->employees() as $employee)
        <div class="col-xs-12 col-sm-6">
            <div class="human-entry navigatable clearfix" data-url="{{ route('employees.profile', ['employeeId' => $employee->user->employee->id]) }}">
                <div class="human-photo">
                    <div class="human-photo-frame" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;"></div>
                </div>
                <div class="human-info">
                    <h4>{{ $employee->name(true) }}</h4>
                    <span class="human-meta">{{ $employee->position->name }}</span>
                </div>
            </div>
        </div>
    @endforeach

</div>
@else
<div class="row">
    <div class="col-xs-12 ta-center">
        There's no employees under this department :)
    </div>
</div>
@endif
*/ ?>

<div class="table-responsive">
    
    <table class="table" style="margin-bottom: 0;">
        <thead>
            <tr>
                <th>Position</th>
                <th class="ta-right">Employees</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($department->positions as $position)
            <tr>
                <td class="table-highlight">
                    {{ $position->name }}
                </td>
                <td>
                    <div class="table-employees-avatar">
                        <ul>
                            @foreach ($position->employees as $employee)
                                <li>
                                    <div class="dp-handler" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover">
                                        <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}" title="{{ $employee->name() }}"></a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>                    
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>