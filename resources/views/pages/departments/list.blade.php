@extends('layouts.app')

@section('title', 'Departments')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">

                <div class="section-title">
                    <h3>Departments &amp; Positions</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        @can ('view_employees', $currentUser)
                        <li><a href="{{ route('employees') }}">Employees</a></li>
                        @endcan
                        <li class="active"><a href="{{ route('departments') }}">Departments &amp; Positions</a></li>
                        @if ($currentUser->allowedTo('edit_employee'))
                            <li><a href="{{ route('employees.rates') }}">Employees Rates</a></li>
                        @endif
                    </ul>
                </div>
                
            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">

                                <div id="departments-and-positions">
                                    @foreach ($departments as $department)
                                    <div class="dap-entry">
                                        <div class="dap-control" role="tab">
                                            <button data-toggle="collapse" data-parent="#departments-and-positions" aria-expanded="false" 
                                                data-target="#d--{{ $department->id }}" aria-controls="d--{{ $department->id }}" class="collapsed"
                                            >
                                                {{ $department->name }}
                                            </button>
                                            @can ('edit_department_position', $currentUser)
                                                <a href="#" class="edit-department" 
                                                    data-edit-url="{{ route('departments.edit') }}/{{ $department->id }}"
                                                    data-target-form="#edit-department-form"
                                                >
                                                    <i class="fa fa-pencil fa-fw"></i>
                                                </a>
                                            @endcan
                                        </div>
                                        <div class="collapse" id="d--{{ $department->id }}">
                                            <div class="dap-content">
                                                
                                                @if (count($department->positions) > 0)
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Position</th>
                                                                    <th class="ta-right">Employees</th>
                                                                    @can ('edit_department_position', $currentUser)
                                                                        <th style="width: 70px;">&nbsp;</th>
                                                                    @endcan
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($department->positions as $position)
                                                                    <tr>
                                                                        <td style="white-space: nowrap;"><em>{{ $position->name }}</em></td>
                                                                        <td>
                                                                            <div class="table-employees-avatar">
                                                                                <ul>
                                                                                    @foreach ($position->employees as $employee)
                                                                                        <li>
                                                                                            <div class="dp-handler" style="background:url({{ $employee->dp() }}) center center no-repeat;background-size:cover">
                                                                                                <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}" title="{{ $employee->name() }}"></a>
                                                                                            </div>
                                                                                        </li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>
                                                                        </td>
                                                                        @can ('edit_department_position', $currentUser)
                                                                        <td class="ta-right">
                                                                            <a href="#" class="edit-position" 
                                                                                data-edit-url="{{ route('positions.edit') }}/{{ $position->id }}" 
                                                                                data-target-form="#edit-position-form"
                                                                            >
                                                                                <i class="fa fa-pencil fa-fw"></i>
                                                                            </a>
                                                                        </td>
                                                                        @endcan
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                @else
                                                    <div class="ta-center">
                                                        No positions under this department.
                                                    </div>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>

@can ('add_department_position', $currentUser)
<div class="page-actions">
    <ul class="clearfix">
        <li class="dropup">
            <a href="#" class="pa-primary pa-pop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropup">
                <i class="fa fa-plus"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right pa-submenu" aria-labelledby="dropup">
                <li>
                    <a href="{{ route('departments.create') }}" class="pa-primary" id="add-department" 
                        data-form-targer="#add-department-form"
                    >
                        <span>Department</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('positions.add') }}" class="pa-primary" id="add-position" data-form-targer="#add-position-form">
                        <span>Position</span>
                        <i class="fa fa-plus"></i>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
@endcan

@endsection

@section('footer-addon')
<script>
$(function() {

    $('#add-department, #add-position').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-form-targer');
        $.get({ url: url }).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {

                            var form = $(targetForm),
                                url = form.attr('action'),
                                data = form.serialize();

                            $.post( url, data ).success(function(r) {
                                r = $.parseJSON(r);
                                bootbox.hideAll();
                                setTimeout(function() {
                                    window.location = r.redirect;
                                }, 500);
                            }).error(function(r) {
                                var errors = r.responseJSON;
                                var html = '';
                                $.each(errors, function(index, value) {
                                    html += '<li>' + value[0] + '</li>';
                                });
                                $('#modal-error-handler').fadeIn().find('ul').html(html);
                            });
                            return false;

                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $('.group-entry').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();

            var url = $(this).attr('data-modable');
            $.get({ url: url }).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    className: 'modal-wide modal-with-table',
                    title: r.title,
                    message: r.view,
                    buttons: {
                        'Close' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    $('.edit-department, .edit-position').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var url = $(this).attr('data-edit-url')
                targetForm = $(this).attr('data-target-form');

            $.get({ url: url }).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {

                                var form = $(targetForm),
                                    formUrl = form.attr('action'),
                                    data = form.serialize();

                                $.post( formUrl, data ).success(function(r) {
                                    r = $.parseJSON(r);
                                    bootbox.hideAll();
                                    setTimeout(function() {
                                        window.location = r.redirect;
                                    }, 500);
                                }).error(function(r) {
                                    var errors = r.responseJSON;
                                    var html = '';
                                    $.each(errors, function(index, value) {
                                        html += '<li>' + value[0] + '</li>';
                                    });
                                    $('#modal-error-handler').fadeIn().find('ul').html(html);
                                });
                                return false;

                            }
                        },
                        'Close' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

});
</script>
@endsection