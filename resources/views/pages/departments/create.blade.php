<div id="modal-error-handler" class="alert alert-danger" role="alert" style="display:none;">
    <ul class="alert-errors"></ul>
</div>
<form action="{{ route('departments.create') }}" method="post" autocomplete="off" id="add-department-form">
{{ csrf_field() }}
<div class="form-horizontal form-primary">

    <div class="form-group">
        <label for="name" class="col-xs-12 col-sm-4 control-label">Department Name</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group form-group-last">
        <label for="schedulers" class="col-xs-12 col-sm-4 control-label">Schedulers</label>
        <div class="col-xs-12 col-sm-8">
            <div class="row input-field-wrap">
                <div class="col-xs-12">
                    <select id="schedulers" name="schedulers[]" multiple="multiple" class="form-control">
                        @foreach ($employees as $employee)
                        <option value="{{ $employee->id }}">{{ $employee->user->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <?php /*
    <div class="form-group form-group-last">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="form-button-group clearfix">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('departments') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
    */ ?>

</div>
</form>
<script>
$(function() {

    $('#schedulers').tokenize({
        autosize: true, 
        displayDropdownOnFocus: false, 
        newElements: false,
        nbDropdownElements: 5
    });

});
</script>