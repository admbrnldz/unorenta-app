<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $filename }}</title>
    <style>
    body {
        font-family: 'Helvetica';
        line-height: 1.5;
        font-size: 9pt;
    }
    table {
        width: 100%;
        border-collapse: collapse;
    }
    .header {
        margin-bottom: 20pt;
    }
    .the-header {
        font-size: 20pt;
        line-height: 1.4;
        margin: 0 0 0 0;
        padding: 0;
    }
    table tr td,
    table tr th {
        padding: 6pt 4pt;
        border-bottom: .25pt solid #000000;
    }
    table tbody tr:nth-child(odd) td {
        background-color: #fbfbfb;
    }
    .ta-left { text-align: left; }
    .ta-right { text-align: right; }
    .page-break {
        page-break-after: always!important;
    }
    </style>
</head>
<body>

    <div class="header">
        <h3>{{ $year }} Withholding Tax</h3>
        <h1 class="the-header">{{ $employee->name(true) }}</h1>
        Tax Identification Number: {{ $employee->tin }}
    </div>    
    
    <table class="table">
        <thead>
            <tr>
                <th class="ta-left">Month</th>
                <th class="ta-left">Half</th>
                <th class="ta-right">Amount</th>
                <th class="ta-right">Payment Date</th>
            </tr>
        </thead>
        <tbody>
            {{--*/ $total = 0 /*--}}
            @forelse ($payslips as $payslip)
                <tr>
                    <td class="table-highlight">
                        {{ getMonths()[$payslip->payrollBatch->month - 1] }}
                    </td>
                    <td>
                        {{ $payslip->payrollBatch->half }}
                    </td>
                    {{--*/ $wht = $payslip->getWithholdingTax() /*--}}
                    <td class="ta-right">{{ number_format($wht, 2, '.', ',') }}</td>
                    <td class="ta-right">{{ processDate($payslip->created_at) }}</td>
                </tr>
                {{--*/ $total += $wht /*--}}
            @empty
                <tr>
                    <td colspan="5" class="ta-center">No payments found.</td>
                </tr>
            @endforelse
        </tbody>
        <tfoot>
            <tr class="total">
                <td colspan="2"><strong>Total Tax Paid</strong></td>                                         
                <td class="ta-right">
                    <strong>P {{ number_format(round($total, 2), 2, '.', ',') }}</strong>
                </td>
                <td></td>
            </tr>
        </tfoot>
    </table>

</body>
</html>