@extends('layouts.app')

@section('title', 'Contributions Report')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>{{ $employee->name() }}</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('reports.dashboard') }}"><i class="fa fa-home fa-fw"></i></a></li>
                        <li><a href="{{ route('reports.withholdingtax') }}">Withholding Tax</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="reports">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Withholding Tax Report</h4>
                            </div>
                            <div class="panel-filter">
                                
                                <div class="form-horizontal form-primary">

                                    <div class="form-group form-group-last">
                                        <div class="col-xs-12 col-sm-9 col-md-2">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select id="year" class="form-control">
                                                        {{--*/ $selectedYear = $year /*--}}
                                                        @foreach ($years as $year)
                                                            <option value="{{ route('reports.contributions.details', ['premiumType' => 'sss', 'employeeId' => $employee->id]) }}?y={{ $year->year }}"
                                                                <?php if ($selectedYear == $year->id): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $year->year }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="table-sneak"></th>
                                                <th>Month</th>
                                                <th>Half</th>
                                                <th style="width:160px;" class="ta-right">Amount</th>
                                                <th style="width:200px;"  class="ta-right">Payment Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{--*/ $total = 0 /*--}}
                                            @forelse ($payslips as $payslip)
                                                <tr>
                                                    <td>
                                                        <a href="{{ route('payslips.details') }}/{{ $payslip->id }}" class="sneak">
                                                            <i class="fa fa-dedent fa-fw"></i>
                                                        </a>
                                                    </td>
                                                    <td class="table-highlight">
                                                        <a href="{{ route('payslips.details') }}/{{ $payslip->id }}" class="sneak">
                                                            {{ getMonths()[$payslip->payrollBatch->month - 1] }}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {{ $payslip->payrollBatch->half }}
                                                    </td>
                                                    {{--*/ $wht = $payslip->getWithholdingTax() /*--}}
                                                    <td class="ta-right">{{ number_format($wht, 2, '.', ',') }}</td>
                                                    <td class="ta-right">{{ processDate($payslip->created_at) }}</td>
                                                </tr>
                                                {{--*/ $total += $wht /*--}}
                                            @empty
                                                <tr>
                                                    <td colspan="5" class="ta-center">No payments found.</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                        <tfoot>
                                            <tr class="total">
                                                <td colspan="3"><strong>Total Tax Paid</strong></td>                                         
                                                <td class="ta-right">
                                                    <strong>P {{ number_format(round($total, 2), 2, '.', ',') }}</strong>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>

                            </div>
                            <div class="panel-footer">
                                <div class="form-button-group clearfix">
                                    <a href="{{ route('reports.withholdingtax.pdf', ['year' => $selectedYear, 'employeeId' => $employee->id]) }}" 
                                        class="btn btn-danger" target="_blank"
                                    >
                                        <i class="fa fa-file-pdf-o"> &nbsp; </i>Export to PDF
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#year').on('change', function(e) {
        window.location = $(this).val();
    });

});
</script>
@endsection