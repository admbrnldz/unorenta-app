@extends('layouts.app')

@section('title', 'Reports Dashboard')

@section('head-addon')
<style>
    
</style>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Reports Dashboard</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li class="active"><a href="{{ route('reports.dashboard') }}"><i class="fa fa-home fa-fw"></i></a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="reports">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">


            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                        <div class="reports-widget row">
                        </div>
                        
                        <div class="reports-widget row">
                            
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="report-entry">
                                    <h3>
                                        <a href="{{ route('reports.contributions') }}">
                                            Contributions
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="report-entry">
                                    <h3>
                                        <a href="{{ route('reports.withholdingtax') }}">
                                            Withholding Tax
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <?php /*
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="report-entry">
                                    <h3>
                                        <a href="#">
                                            Employees' Earnings
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="report-entry">
                                    <h3>
                                        <a href="#">
                                            Employees' Location
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            */ ?>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="report-entry">
                                    <h3>
                                        <a href="{{ route('reports.tardiness') }}">
                                            Tardiness
                                        </a>
                                    </h3>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="report-entry">
                                    <h3>
                                        <a href="{{ route('reports.absences') }}">
                                            Absences
                                        </a>
                                    </h3>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>

@endsection

@section('footer-addon')
<script>
$(function() {


    
});
</script>
@endsection