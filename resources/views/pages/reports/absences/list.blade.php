@extends('layouts.app')

@section('title', 'Absences Report')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Absences Report</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('reports.dashboard') }}"><i class="fa fa-home fa-fw"></i></a></li>
                        <li class="active"><a href="{{ route('reports.absences') }}">Absences</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="reports">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Employees</h4>
                            </div>
                            <div class="panel-filter">
                                
                                <div class="form-horizontal form-primary">

                                    <form action="{{ route('reports.absences') }}" method="get">
                                    <div class="form-group form-group-last">
                                        <div class="col-xs-12 col-sm-5">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select id="year" name="y" class="form-control">
                                                        @foreach (getYears() as $year)
                                                            <option value="{{ $year }}"
                                                                <?php if ($year == $theYear): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $year }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-5">
                                            <div class="row input-field-wrap">
                                                <div class="col-xs-12">
                                                    <select id="month" name="m" class="form-control">
                                                        @foreach (getMonths() as $i => $month)
                                                            <option value="{{ $i + 1 }}"
                                                                <?php if (($i + 1) == $theMonth): ?>selected<?php endif; ?>
                                                            >
                                                                {{ $month }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-2">
                                            <button class="btn btn-primary btn-fw" id="refresh-list">Update List</button>
                                        </div>
                                    </div>
                                    </form>

                                </div>

                            </div>
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th class="ta-right">Absent Count</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employees as $employee)
                                                <?php
                                                    $timecards = $employee->timecards()
                                                        ->whereRaw("time_in BETWEEN '$dateFrom' AND '$dateTo'")
                                                        ->get();
                                                    $count = 0;
                                                    foreach ($timecards as $timecard) {
                                                        if ($timecard->is_absent) {
                                                            $count++;
                                                        }                                                        
                                                    }
                                                ?>
                                                <tr>
                                                    <td class="table-dp-td">
                                                        <div class="dp-handler" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;">
                                                            <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}"></a>
                                                        </div>
                                                    </td>
                                                    <td class="table-highlight">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">
                                                            {{ $employee->name(true) }}
                                                        </a>
                                                    </td>
                                                    <td class="ta-right">
                                                        {{ $count }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <div class="panel-footer">
                                <div class="form-button-group clearfix">
                                    <a href="{{ route('reports.absences.pdf', ['year' => $theYear, 'month' => $theMonth]) }}" 
                                        class="btn btn-danger" target="_blank"
                                    >
                                        <i class="fa fa-file-pdf-o"> &nbsp; </i>Export to PDF
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#fetch-data').on('click', function(e) {
        e.preventDefault()
    });

});
</script>
@endsection