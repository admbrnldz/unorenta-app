<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $filename }}</title>
    <style>
    body {
        font-family: 'Helvetica';
        line-height: 1.5;
        font-size: 9pt;
    }
    table {
        width: 100%;
        border-collapse: collapse;
    }
    .header {
        margin-bottom: 20pt;
    }
    .the-header {
        font-size: 20pt;
        line-height: 1.4;
        margin: 0 0 0 0;
        padding: 0;
    }
    table tr td,
    table tr th {
        padding: 6pt 4pt;
        border-bottom: .25pt solid #000000;
    }
    table tbody tr:nth-child(odd) td {
        background-color: #fbfbfb;
    }
    .ta-left { text-align: left; }
    .ta-right { text-align: right; }
    .page-break {
        page-break-after: always!important;
    }
    .nf {
        display: block;
        text-align: center;
        padding-top: 15pt;
    }
    </style>
</head>
<body>

    <div class="header">
        <h3>Absences Report</h3>
        <h1 class="the-header">{{ getMonths()[$month - 1] }} {{ $year }}</h1>
    </div>    
    
    <table>
        <thead>
            <tr>
                <th class="ta-left">Employee</th>
                <th class="ta-right">Absent Count</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
                <?php
                    $timecards = $employee->timecards()
                        ->whereRaw("time_in BETWEEN '$dateFrom' AND '$dateTo'")
                        ->get();
                    $count = 0;
                    foreach ($timecards as $timecard) {
                        if ($timecard->is_absent) {
                            $count++;
                        }                                                        
                    }
                ?>
                <tr>
                    <td class="table-highlight">{{ $employee->name(true) }}</td>
                    <td class="ta-right">{{ $count }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="nf">
        -- Nothing follows --
    </div>

    <div style="margin-top:40pt;width:120pt;text-align: center;">
        <strong>{{ $andy->name() }}</strong><br />
        {{ $andy->position->name }}
    </div>

</body>
</html>