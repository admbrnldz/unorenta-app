@extends('layouts.app')

@section('title', 'Contributions Report')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header with-menu">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Contributions Report</h3>
                </div>
                <div class="section-header-menu">
                    <ul class="clearfix">
                        <li><a href="{{ route('reports.dashboard') }}"><i class="fa fa-home fa-fw"></i></a></li>
                        <li class="active"><a href="{{ route('reports.contributions') }}">Contributions</a></li>
                    </ul>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="reports">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>Employees' Contribution</h4>
                            </div>
                            <div class="panel-body">
                                
                                <div class="table-responsive">
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Employee</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employees as $employee)
                                                <tr>
                                                    <td class="table-dp-td">
                                                        <div class="dp-handler" style="background:url({{ $employee->user->dp() }}) center center no-repeat;background-size:cover;">
                                                            <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}"></a>
                                                        </div>
                                                    </td>
                                                    <td class="table-highlight">
                                                        <a href="{{ route('employees.profile', ['employeeId' => $employee->id]) }}">
                                                            {{ $employee->name(true) }}
                                                        </a>
                                                    </td>
                                                    <td class="ta-right">
                                                        <a href="{{ route('reports.contributions.details', ['premiumType' => 'sss', 'employeeId' => $employee->id]) }}" 
                                                            class="btn btn-xs btn-info"
                                                        >
                                                            SSS
                                                        </a>
                                                        <a href="{{ route('reports.contributions.details', ['premiumType' => 'philhealth', 'employeeId' => $employee->id]) }}" 
                                                            class="btn btn-xs btn-success"
                                                        >
                                                            PhilHealth
                                                        </a>
                                                        <a href="{{ route('reports.contributions.details', ['premiumType' => 'pagibig', 'employeeId' => $employee->id]) }}" 
                                                            class="btn btn-xs btn-warning"
                                                        >
                                                            Pag-ibig
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
               
        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#fetch-data').on('click', function(e) {
        e.preventDefault()
    });

});
</script>
@endsection