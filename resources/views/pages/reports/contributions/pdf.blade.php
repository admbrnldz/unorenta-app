<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $filename }}</title>
    <style>
    body {
        font-family: 'Helvetica';
        line-height: 1.5;
        font-size: 9pt;
    }
    table {
        width: 100%;
        border-collapse: collapse;
    }
    .header {
        margin-bottom: 20pt;
    }
    .the-header {
        font-size: 20pt;
        line-height: 1.4;
        margin: 0 0 0 0;
        padding: 0;
    }
    table tr td,
    table tr th {
        padding: 6pt 4pt;
        border-bottom: .25pt solid #000000;
    }
    table tbody tr:nth-child(odd) td {
        background-color: #fbfbfb;
    }
    .ta-left { text-align: left; }
    .ta-right { text-align: right; }
    .page-break {
        page-break-after: always!important;
    }
    </style>
</head>
<body>

    <div class="header">
        <h3>{{ $year }} {{ strtoupper($premiumType) }} Contribution</h3>
        <h1 class="the-header">{{ $employee->name(true) }}</h1>
        @if ($premiumType == 'sss')
            SSS Number: {{ $employee->sss }}
        @elseif ($premiumType == 'philhealth')
            PhilHealth Number: {{ $employee->philhealth }}
        @elseif ($premiumType == 'pagibig')
            Pagibig Number: {{ $employee->pagibig }}
        @endif
    </div>    
    
    <table>
        <thead>
            <tr>
                <th class="ta-left">Month</th>
                @if ($premiumType == 'pagibig')
                <th class="ta-right">Contribution</th>
                @else
                <th class="ta-right">ER</th>
                <th class="ta-right">EE</th>
                @endif
                <th class="ta-right">Total</th>
                <th class="ta-right">Payment Date</th>
            </tr>
        </thead>
        <tbody>
            {{--*/ $total = 0 /*--}}
            @forelse ($payments as $payment)
                {{--*/ $subTotal = 0 /*--}}
                <tr>
                    <td class="table-highlight">
                        {{ getMonths()[$payment->payslip->payrollBatch->month - 1] }}
                    </td>
                    @if ($premiumType !== 'pagibig')
                        {{--*/ $er = getER($payment->amount, $premiumType) /*--}}
                        <td class="ta-right">{{ number_format($er, 2, '.', ',') }}</td>
                        {{--*/ $subTotal += $er /*--}}
                    @endif
                    <td class="ta-right">{{ number_format($payment->amount, 2, '.', ',') }}</td>
                    {{--*/ $subTotal += $payment->amount /*--}}
                    <td class="ta-right">{{ number_format($subTotal, 2, '.', ',') }}</td>
                    <td class="ta-right">{{ processDate($payment->created_at) }}</td>
                </tr>
                {{--*/ $total += $subTotal /*--}}
            @empty
                <tr>
                    <td colspan="5" class="ta-center">No payments found.</td>
                </tr>
            @endforelse
        </tbody>
        <tfoot>
            <tr class="total">
                @if ($premiumType !== 'pagibig')
                    <td colspan="3"><strong>Total Contributions</strong></td>
                @else
                    <td colspan="2"><strong>Total Contributions</strong></td>
                @endif                                                
                <td class="ta-right">
                    <strong>P {{ number_format(round($total, 2), 2, '.', ',') }}</strong>
                </td>
                <td></td>
            </tr>
        </tfoot>
    </table>

</body>
</html>