@extends('layouts.app')

@section('title', 'Home')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<div class="login-page">
    <div class="centerfy">
        
        <div class="login-form-wrap">
            
            <div class="login-logo">
                <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo-white.png') }}" alt="" /></a>
            </div>

            <div class="alert alert-login ta-center">
                Please ask the company's HR Personnel if you wish to have an account, thank you!
            </div>

            <div class="ta-center" style="margin-top: 14px;">
                <a href="{{ url('/login') }}"><em>Click here to log in.</em></a>
            </div>

        </div>

    </div>    
</div> 
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection