@extends('layouts.app')

@section('title', 'Home')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section" id="dashboard">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="user-dashboard clearfix">
                
                <div class="ud-dp imglink" style="background:url({{ asset('assets/dp/dp4.jpg') }}) center center no-repeat;background-size:cover;">
                    <a href="#"></a>
                </div>
                <div class="ud-salute">
                    <h1>Good morning, Eric!</h1>
                    <h4>Today is Tuesday, July 11.</h4>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>


<section class="section" id="employee-list">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="section-title">
                            <h3><i class="fa fa-users"></i>Employees</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="section-head-actions">
                            <a href="#" class="btn btn-primary">Add Employee</a>
                            <a href="#" class="btn btn-success btn-squared"><i class="fa fa-search"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-content">
                <div class="table-responsive">
                    
                    <table class="table">
                        <thead>
                            <tr>
                                <th>DP</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Department</th>
                                <th>Contact Number</th>
                                <th>Date Hired</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="dp-handler" style="background:url({{ asset('assets/dp/dp12.jpg') }}) center center no-repeat;background-size:cover;">
                                        <a href="#"></a>
                                    </div>
                                </td>
                                <td class="table-highlight">
                                    <a href="#">
                                        Cabral, Mercedez S.
                                    </a>
                                </td>
                                <td>Front-end Develop</td>
                                <td>Web Development Dept</td>
                                <td>0925 222 3212</td>
                                <td>May 29, 2016</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="dp-handler" style="background:url({{ asset('assets/dp/dp5.jpg') }}) center center no-repeat;background-size:cover;">
                                        <a href="#"></a>
                                    </div>
                                </td>
                                <td class="table-highlight">
                                    <a href="#">
                                        Bernaldez, Adam Vincent
                                    </a>
                                </td>
                                <td>Student</td>
                                <td>Computing Education</td>
                                <td>0917 321 3235</td>
                                <td>Jun 16, 2016</td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="dp-handler" style="background:url({{ asset('assets/dp/dp5.jpg') }}) center center no-repeat;background-size:cover;">
                                        <a href="#"></a>
                                    </div>
                                </td>
                                <td class="table-highlight">
                                    <a href="#">
                                        Palaca, John Baby
                                    </a>
                                </td>
                                <td>Technical Head</td>
                                <td>Technical Department</td>
                                <td>0918 251 7841</td>
                                <td>Jul 02, 2016</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="dp-handler" style="background:url({{ asset('assets/dp/dp6.jpg') }}) center center no-repeat;background-size:cover;">
                                        <a href="#"></a>
                                    </div>
                                </td>
                                <td class="table-highlight">
                                    <a href="#">
                                        Amigo, Gene Eric
                                    </a>
                                </td>
                                <td>Standing</td>
                                <td>Homestead</td>
                                <td>0923 921 5821</td>
                                <td>Jun 04, 2016</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="dp-handler" style="background:url({{ asset('assets/dp/dp9.jpg') }}) center center no-repeat;background-size:cover;">
                                        <a href="#"></a>
                                    </div>
                                </td>
                                <td class="table-highlight">
                                    <a href="#">
                                        Mendiola, Jessy
                                    </a>
                                </td>
                                <td>Usherette</td>
                                <td>Rich and Famous Corp</td>
                                <td>0922 621 6922</td>
                                <td>May 30, 2016</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
        </div>
    </div>
</section>


<section class="section" id="elements">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="alert alert-success">
                    <strong>Success</strong> - Solaque dei rem quamvis uti <a href="#">homines</a> aequare patitur caetera hae.
                </div>
                <div class="alert alert-warning">
                    <strong>Warning</strong> - At in ob <a href="#">argutari</a> efficere formalem competit existere augeatur vi.
                </div>
                <div class="alert alert-danger">
                    <strong>Danger</strong> - Occurrit si concipio si veritate <a href="#">nihildum</a> credamus dependet et im.
                </div>

                <br />
                <br />
                
                <a href="#" class="btn btn-default">Default</a>
                <a href="#" class="btn btn-primary">Primary</a>
                <a href="#" class="btn btn-success">Success</a>
                <a href="#" class="btn btn-info">Info</a>
                <a href="#" class="btn btn-warning">Warning</a>
                <a href="#" class="btn btn-danger">Danger</a>

            </div>
        </div>
    </div>
</section>



<section class="section" id="form">
    <div class="container">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title">
                            <h3><i class="fa fa-file-text-o"></i>File Leave of Absence</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-content">

                <form autocomplete="off">
                <div class="form-horizontal form-primary">

                    <div class="form-group">
                        <label for="targetid" class="col-xs-12 col-sm-3 control-label">Label</label>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12 col-sm-8">
                                    <input type="text" class="form-control" id="targetid" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="targetid" class="col-xs-12 col-sm-3 control-label">Label</label>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12 col-sm-6">
                                    <input type="text" class="form-control" id="targetid" placeholder="">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input type="text" class="form-control" id="targetid" placeholder="Sample po">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12">
                                    <textarea name="" id="" rows="10" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="targetid" class="col-xs-12 col-sm-3 control-label">Label</label>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="targetid" placeholder="">
                                </div>
                                <div class="col-xs-12 col-sm-5">
                                    <input type="text" class="form-control" id="targetid" placeholder="">
                                </div>
                                <div class="col-xs-12 col-sm-2">
                                    <select name="" id="" class="form-control">
                                        <option value=""></option>
                                        <option value="">Laki</option>
                                        <option value="">Bayi</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="targetid" class="col-xs-12 col-sm-3 control-label">Label</label>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12 col-sm-6">
                                    <input type="text" class="form-control" id="date-from" placeholder="Select Date">
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input type="text" class="form-control" id="date-to" placeholder="Select Date">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="targetid" class="col-xs-12 col-sm-3 control-label">Label</label>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12">
                                    <textarea name="" id="" rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12">
                                    <div class="radio styled">
                                        <label>
                                            <input type="radio" name="name" id="" />
                                            <span>Broadcast to all</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="radio styled">
                                        <label>
                                            <input type="radio" name="name" id="" />
                                            <span>Allow discussion</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                            <div class="row input-field-wrap">
                                <div class="col-xs-12">
                                    <div class="checkbox styled">
                                        <label>
                                            <input type="checkbox" name="11" id="" />
                                            <span>Broadcast to all</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="checkbox styled">
                                        <label>
                                            <input type="checkbox" name="22" id="" />
                                            <span>Allow discussion</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                            <div class="form-button-group clearfix">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                                <a href="#" class="btn btn-default">Cancel</a>
                            </div>
                        </div>
                    </div>

                </div>
                </form>

            </div>

        </div>
        </div>
    </div>
</section>
@endsection

@section('footer-addon')
<script>
$(function() {

    $('#date-from, #date-to').datepicker();

});
</script>
@endsection