@extends('layouts.app')

@section('title', 'Testing Groud')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<section class="section-header">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">

            <div class="section-header-content">
            
                <div class="section-title">
                    <h3>Testing Groud</h3>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>
<section class="section" id="dashboard">
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">


            <div class="section-content">

                <div class="row">
                    <div class="col-xs-12">
                        @include('includes.status')
                        @include('includes.errors')
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        
                        <div class="panel">
                            <div class="panel-body">

                                <?php var_dump(getTones()); ?>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        </div>
    </div>
</section>

@endsection

@section('footer-addon')
<script>
$(function() {


    
});
</script>
@endsection