@extends('layouts.app')

@section('title', 'Home')

@section('head-addon')
<styles>
    
</styles>
@endsection

@section('content')
<div class="login-page">
    <div class="centerfy">
        
        <div class="login-form-wrap">
            
            <div class="login-logo">
                <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo-white.png') }}" alt="" /></a>
            </div>

            @if (count($errors) > 0)
            <div class="alert alert-warning">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" autocomplete="off">
            {{ csrf_field() }}
            <div class="login-form">                
                <div class="login-input login-input-username">
                    <input type="text" name="username" id="username" value="{{ old('username') }}" placeholder="username" />
                </div>
                <div class="login-input login-input-password">
                    <input type="password" name="password" id="password" placeholder="password" />
                </div>
                <div class="login-submit">
                    <button type="submit" class="btn btn-white">Log In</button>
                </div>
            </div>
            </form>

        </div>

    </div>    
</div> 
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection