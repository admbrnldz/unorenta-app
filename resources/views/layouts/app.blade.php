<!DOCTYPE html>
<html lang="en">
<head>

@include('layouts.fragments.head')
@yield('head-addon')

</head>

<body class="@if (!Auth::check()) out @endif">

@include('layouts.fragments.nav')
@include('layouts.fragments.onbreak')

@yield('content')

@include('layouts.fragments.scripts')
@yield('footer-addon')

</body>
</html>
