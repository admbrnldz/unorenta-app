@if (Auth::check())
<div id="aside-alt">
    <button id="aside-trigger">
        <i class="fa fa-ellipsis-h"></i>
    </button>
    <div class="user-dp-wrap">
        <div class="user-dp" style="background:url({{ Auth::user()->dp() }}) center center no-repeat;background-size:cover;">&nbsp;</div>
    </div>
</div>
<aside>

    <div id="upanel">
        <div class="upanel-dp-wrap">
            <div class="upanel-dp" 
                style="background:url({{ Auth::user()->dp() }}) center center no-repeat;background-size:cover;"
                data-toggle="modal" data-target="#user-dp-modal"
            >
                &nbsp;
            </div>
        </div>
    </div>

    <div id="main-nav">
        <ul class="clearfix">

            <li class="{{ activeWhen(['dashboard']) ? 'active' : '' }}">
                <a href="{{ route('dashboard') }}" id="menu-68">
                    <i class="fa fa-dashboard fa-fw"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            @can ('view_employees', $currentUser)
                <li class="{{ activeWhen(['employees', 'departments', 'account']) ? 'active' : '' }}">
                    <a href="{{ route('employees') }}" id="menu-69">
                        <i class="fa fa-users fa-fw"></i>
                        <span>Employees</span>
                    </a>
                </li>
            @endcan

            @can ('view_departments_positions', $currentUser)
                @can ('view_employees', $currentUser)
                @else
                <li class="{{ activeWhen(['departments']) ? 'active' : '' }}">
                    <a href="{{ route('departments') }}" id="menu-82">
                        <i class="fa fa-object-group fa-fw"></i>
                        <span>Departments</span>
                    </a>
                </li>
                @endcan
            @endcan

            <li class="{{ activeWhen(['loa', 'holidays']) ? 'active' : '' }}">
                <a href="{{ route('loa.dashboard') }}" id="menu-76">
                    {{--*/ $forApprovalLeavesCount = count($currentUser->employee->forApprovalLeaves()) /*--}}
                    @if ($forApprovalLeavesCount > 0)
                        <div class="notifyer">{{ $forApprovalLeavesCount }}</div>
                    @endif
                    <i class="fa fa-paper-plane fa-fw"></i>
                    <span>Leave</span>
                </a>
            </li>

            @can ('view_memos', $currentUser)
                <li class="{{ activeWhen(['memos']) ? 'active' : '' }}">                    
                    <a href="{{ route('memos') }}" id="menu-77">
                        {{--*/ $memoNotificationCount = $currentUser->employee->memoNotificationCount() /*--}}
                        @if ($memoNotificationCount > 0)
                            <div id="memo-notifyer" class="notifyer">{{ $memoNotificationCount }}</div>
                        @endif
                        <i class="fa fa-bullhorn fa-fw"></i>
                        <span>Memo</span>
                    </a>
                </li>
            @endcan            
            
            <li class="{{ activeWhen(['shifts']) ? 'active' : '' }}">
                @can ('add_shift', $currentUser)
                    <a href="{{ route('shifts.manage') }}" id="menu-83">
                        <i class="fa fa-calendar-check-o fa-fw"></i>
                        <span>Shifts</span>
                    </a>
                @else
                    <a href="{{ route('shifts.list') }}/{{ $currentUser->employee->id }}" id="menu-83">
                        <i class="fa fa-calendar fa-fw"></i>
                        <span>Shifts</span>
                    </a>
                @endcan
            </li>

            @can ('approve_attendance', $currentUser)
                <li class="{{ activeWhen(['timecards']) ? 'active' : '' }}">
                    <a href="{{ route('timecards.approval') }}" id="menu-84">
                        <i class="fa fa-clock-o fa-fw"></i>
                        <span>Timecards</span>
                    </a>
                </li>
            @endcan

            @can ('view_applicants', $currentUser)
                <li class="{{ activeWhen(['applicants', 'assessments']) ? 'active' : '' }}">
                    <a href="{{ route('applicants') }}" id="menu-65">
                        <i class="fa fa-user-secret fa-fw"></i>
                        <span>Applicants</span>
                    </a>
                </li>
            @endcan

            @if ($currentUser->position(['Accounting Staff']))
                <li class="{{ activeWhen(['accounting']) ? 'active' : '' }}">
                    <a href="{{ route('payroll.batch') }}" id="menu-80">
                        <i class="fa fa-bar-chart-o fa-fw"></i>
                        <span>Payroll</span>
                    </a>
                </li>
            @endif

            <li class="{{ activeWhen(['payslips']) ? 'active' : '' }}">
                <a href="{{ route('payslips') }}" id="menu-89">
                    <i class="fa fa-wpforms fa-fw"></i>
                    <span>Payslips</span>
                </a>
            </li>

            @if ($currentUser->allowedTo('view_reports'))
            <li class="{{ activeWhen(['reports']) ? 'active' : '' }}">
                <a href="{{ route('reports.dashboard') }}">
                    <i class="fa fa-area-chart fa-fw"></i>
                    <span>Reports</span>
                </a>
            </li>
            @endif

        </ul>
    </div>

    <?php /*
    <div id="upanel">
        <ul class="navbar-right">
            <li id="notifications-dropdown" class="dropdown">
                <a id="notifications-drop" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-bell"></i>
                </a>
                <ul id="notifications-list" class="dropdown-menu" aria-labelledby="notifications-drop">
                    <li><a href="#">No notifications yet</a></li>
                </ul>
            </li>
            <li id="user-opts-dropdown" class="dropdown">
                <a id="user-opts-drop" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="user-dp" style="background:url({{ Auth::user()->dp() }}) center center no-repeat;background-size:cover;">&nbsp;</div>
                </a>
                <ul class="dropdown-menu" aria-labelledby="user-opts-drop">

                    <li class="user-opts-details">
                        <a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}">
                            <h4>{{ $currentUser->name() }}</h4>
                            <h5>{{ $currentUser->employee->position->name }}</h5>
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>

                    <li><a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}">Profile</a></li>
                    <li><a href="{{ route('account.settings', ['userId' => $currentUser->id]) }}">Settings</a></li>

                    <li role="separator" class="divider"></li>

                    @if (Auth::user()->employee->canFileALeave())
                        <li><a href="{{ route('loa.file') }}">File Leave</a></li>
                    @endif

                    <li role="separator" class="divider"></li>

                    <li><a href="{{ url('/logout') }}">Log Out</a></li>

                </ul>
            </li>
        </ul>
    </div>
    */ ?>

</aside>
<div class="modal fade" id="user-dp-modal" tabindex="-1" role="dialog" aria-labelledby="user-dp">
    <div class="modal-dialog" role="document">        
        <div class="modal-content">
            <div class="modal-dp-wrap">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-dp" 
                    style="background:url({{ Auth::user()->dp() }}) center center no-repeat;background-size:cover;"
                    data-target="#dp-file"
                >
                    &nbsp;
                </div>
                <form action="{{ route('account.uploaddp') }}/{{ $currentUser->id }}" method="post" enctype="multipart/form-data" autocomplete="off" id="upload-dp-form">
                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $currentUser->id }}" />
                    <input type="file" name="dp_file" id="dp-file" class="form-control-file" data-form="#upload-dp-form" accept="image/*" />
                </form>
            </div>
            <div class="modal-user-info">
                <h4>{{ $currentUser->name() }}</h4>
                <h5>{{ $currentUser->employee->position->name }}</h5>
                {{--*/ $class = 'label-primary' /*--}}
                @if ($currentUser->employee->currentStatus(true) > 4)
                    {{--*/ $class = 'label-danger' /*--}}
                @endif
                <span class="label {{ $class }}">
                    {{ $currentUser->employee->currentStatus() }}
                </span>
                <div class="modal-user-action">
                    <a href="{{ route('employees.profile', ['employeeId' => $currentUser->employee->id]) }}" class="btn btn-sm btn-default">Profile</a>
                    <a href="{{ url('/logout') }}" class="btn btn-sm btn-default">Log Out</a>
                </div>
            </div>
            <div class="modal-footer ta-left">
                <a role="button" data-toggle="collapse" href="#tones-selection" aria-expanded="false" aria-controls="tones-selection">
                    <i class="fa fa-bell fa-fw"></i> &nbsp; Select Notification Tone
                </a>
                <div class="collapse" id="tones-selection">
                    <div class="tones-options">
                        <form action="{{ route('account.updatetone') }}/{{ $currentUser->id }}" method="post" id="update-tone-form">
                            {!! csrf_field() !!}
                            @foreach (getTones() as $key => $fileName)
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tone" id="{{ $key }}" value="{{ $key }}" class="tone-option"
                                            <?php if ($currentUser->notification_tone == $key): ?>checked<?php endif; ?>
                                        />
                                        <span>{{ $fileName }}</span>
                                    </label>
                                </div>
                            @endforeach
                            <div class="radio">
                                <label>
                                    <input type="radio" name="tone" id="none" value="" class="tone-option"
                                        <?php if ($currentUser->notification_tone == ''): ?>checked<?php endif; ?>
                                    />
                                    <span>Mute</span>
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif