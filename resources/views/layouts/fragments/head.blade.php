<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>UnoRenta | @yield('title')</title>

<link rel="shortcut icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon" />
<link rel="icon" href="{{ asset('assets/favicon.png') }}" type="image/x-icon" />

<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/rwd.css') }}" />

<!-- other css -->
<link rel="stylesheet" href="{{ asset('assets/css/timepicker.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/selectlist.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/ckeditor.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/tokenize.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/perfectscroll.css') }}" />