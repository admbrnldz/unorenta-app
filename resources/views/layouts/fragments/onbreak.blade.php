<div id="break-nofitication-wrap">
    @if (Session::has('breaktime'))
    <section class="break-notification" data-url="{{ route('timecards.break') }}/{{ Session::get('breaktime')->id }}">
        You're on break! Click here once you get back.
    </section>
    @endif
</div>