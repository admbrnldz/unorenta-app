<script src="{{ asset('assets/js/jq.js') }}"></script>

<script>
var loader = {
    html: '<div class="loader-wrap"><i class="fa fa-loader fa-circle-o-notch fa-spin"></i></div>',
    loadinghtml: '<div class="loader-wrap"><div class="loading-wrap"><div class="loading-bar"></div></div><div class="loading-caption"></div></div>',
    show: function() {
        $('body').append(this.html);
        setTimeout(
            function() {
                $('.loader-wrap').addClass('in');
            }, 
            100
        );
    },
    hide: function() {
        $('.loader-wrap').removeClass('in');
        setTimeout(
            function() {
                $('.loader-wrap').remove();
            }, 
            600
        );
    },
    showLoading: function() {
        $('body').append(this.loadinghtml);
        setTimeout(
            function() {
                $('.loader-wrap').addClass('in');
            }, 
            100
        );
    }
};
var sneak = {
    html: '<div class="sneak-wrap"><div class="sneak-content"></div></div><div class="sneak-backdrop"></div>',
    preloader: '<div class="sneak-preloader"><i class="fa fa-loader fa-circle-o-notch fa-spin"></i></div>',
    go: function(url) {
        $('body').addClass('open-sneak').append(this.html);
        $('.sneak-content').append(this.preloader);

        $.get( url ).done(function(r) {
            r = $.parseJSON(r);
            $('.sneak-content').fadeOut(100);
            setTimeout(function() {
                $('.sneak-content').html(r.view).fadeIn(100);
            }, 300);
        });

        setTimeout(
            function() {
                $('.sneak-wrap, .sneak-backdrop').addClass('in');
            }, 
            100
        );
        $('.sneak-backdrop').on('click', function() {
            sneak.out();
        });
    },
    out: function() {
        $('.sneak-wrap').css({ 'right': '-800px' });
        $('.sneak-wrap, .sneak-backdrop').fadeOut(300);
        setTimeout(function() {
            $('.sneak-wrap, .sneak-backdrop').remove();
        }, 500);
        $('body').removeClass('open-sneak');
    }
};
</script>

<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/jq-ui.js') }}"></script>
<script src="{{ asset('assets/js/autoresize-ta.js') }}"></script>
<script src="{{ asset('assets/js/timepicker.js') }}"></script>
<script src="{{ asset('assets/js/selectlist.js') }}"></script>
<script src="{{ asset('assets/js/bootbox.min.js') }}"></script>
<script src="{{ asset('assets/js/perfectscroll.js') }}"></script>

@if (Auth::check())
<script src="{{ asset('assets/js/sound.js') }}"></script>
<script>var tone = '{{ Auth::user()->notification_tone }}';</script>
<script src="{{ asset('assets/js/polling.js') }}"></script>
<script src="{{ asset('assets/js/bundyclock.js') }}"></script>
<script src="{{ asset('assets/js/keypress.js') }}"></script>
@endif

<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="{{ asset('assets/js/tokenize.js') }}"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="{{ asset('assets/js/inputmask.js') }}"></script>