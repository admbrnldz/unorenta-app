<?php

namespace UnoRenta\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use UnoRenta\Models\User;
use UnoRenta\Models\Permission;
use UnoRenta\Models\UserPermission;

class UserPolicy
{
    use HandlesAuthorization;

    public function view_employees(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function view_profile(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function add_employee(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_employee(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function view_departments_positions(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function add_department_position(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_department_position(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function delete_department_position(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function view_holidays(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function add_holiday(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_holiday(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function delete_holiday(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function view_memos(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function broadcast_memo(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_memo(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function delete_memo(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_permissions(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function view_leave_applications(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function entitle_leave_applications(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function view_shifts(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function add_shift(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_shifts(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function view_applicants(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function add_applicant(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function delete_applicant(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_applicant(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function interview_applicants(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_account_settings(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function approve_attendance(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function add_interview_questions(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function edit_interview_question(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }
    public function delete_interview_question(User $current, User $user)
    {
        return $this->execute($user, str_replace('_', '-', __FUNCTION__));
    }

    protected function execute(User $user, $key)
    {
        $permission = Permission::where('key', $key)->first();

        if ($permission) {
            if (UserPermission::where('permission_id', $permission->id)
                ->where('user_id', $user->id)->count() > 0)
            {
                return true;
            }
        }
        return false;
    }

}
