<?php

namespace UnoRenta\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MemoRecipient extends Model
{
	
	public $timestamps = false;

    protected $fillable = [
        'memo_id',
        'employee_id',
        'seen',
        'date_last_seen',
        'conform',
        'date_conform'
    ];

    public function memo()
    {
        return $this->belongsTo(Memo::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function seen()
    {
        $this->seen = 1;
        $this->date_last_seen = Carbon::now();
        $this->save();

        return $this;
    }

    public static function conform($request)
    {
        $memoRecipient = MemoRecipient::query()
            ->where('memo_id', $request->memo_id)
            ->where('employee_id', Auth::user()->employee->id)
            ->first();

        if ($memoRecipient) {
            $conformData = [
                'conform' => 1,
                'date_conform' => Carbon::now()
            ];
            $memoRecipient->update($conformData);
        }

        return $memoRecipient;
    }

}
