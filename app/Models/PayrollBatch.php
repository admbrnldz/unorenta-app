<?php

namespace UnoRenta\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PayrollBatch extends Model
{

    protected $fillable = [
        'year',
        'month',
        'half',
        'generated_by'
    ];

    public function payslips()
    {
        return $this->hasMany(Payslip::class);
    }

    public function dateCovered()
    {
        $dayFrom = $this->half == 1 ? 21 : 6;
        $dayTo = $this->half == 1 ? 5 : 20;
        $monthForm = $this->half == 1 ? $this->month - 1 : $this->month;
        $dateFrom = Carbon::create($this->year, $monthForm, $dayFrom);
        $dateTo = Carbon::create($this->year, $this->month, $dayTo);

        return processDate($dateFrom) . ' &mdash; ' . processDate($dateTo);
    }

    public function totalPay()
    {
        $totalPay = 0;
        foreach ($this->payslips as $ps) {
            $totalPay += $ps->getNetPay();
        }

        return $totalPay;
    }

    public function from()
    {
        $date = $this->half == 1 ? 21 : 6;
        $from = Carbon::create($this->year, $this->month, $date);

        return $from->startOfDay();
    }

    public function to()
    {
        $date = $this->half == 1 ? 5 : 21;
        $to = Carbon::create($this->year, $this->month, $date);

        return $to->endOfDay();
    }

}
