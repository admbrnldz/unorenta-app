<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    
    protected $fillable = [
        'name'
    ];

}
