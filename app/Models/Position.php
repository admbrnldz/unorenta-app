<?php

namespace UnoRenta\Models;

use UnoRenta\Models\Position;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{

	protected $fillable = [
        'name', 'department_id'
    ];
    
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function previewEmployees($count = 10)
    {
        return Employee::where('position_id', $this->id)->take($count)->get();
    }

    public function unseenEmployees($count = 10)
    {
        return Employee::where('position_id', $this->id)->count() - count($this->previewEmployees($count));
    }

    public static function addPosition($request)
    {
        $position = new Position;
        $position->name = $request->name;
        $position->department_id = $request->department_id;

        return $position;
    }

    public function updatePosition($request)
    {
        $this->name = $request->name;
        $this->department_id = $request->department_id;
        $this->save();

        return $this;
    }
	
}
