<?php

namespace UnoRenta\Models;

use Auth;
use Carbon\Carbon;
use UnoRenta\Models\Shift;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Timecard;
use UnoRenta\Models\WithholdingTax;
use UnoRenta\Models\EmployeeStatus;
use UnoRenta\Models\LeaveEntitlement;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $fillable = [
        'employee_uid', 
        'user_id', 
        'position_id', 
        'nickname', 
        'birthdate', 
        'birthplace', 
        'sex', 
        'address', 
        'city', 
        'province', 
        'zip_code', 
        'mobile_number', 
        'home_number', 
        'email_address', 
        'civil_status', 
        'citizenship', 
        'religion', 
        'height', 
        'weight', 
        'build', 
        'eyes', 
        'blood_type', 
        'sss', 
        'tin', 
        'philhealth', 
        'pagibig', 
        'father_name', 
        'father_occupation', 
        'mother_name', 
        'mother_occupation',
        'spouse_name', 
        'spouse_occupation', 
        'contact_person', 
        'cp_number', 
        'cp_address', 
        'cp_language_spoken', 
        'date_hired', 
        'date_probationary', 
        'date_regular', 
        'date_resigned', 
        'status',
        'basic_income',
        'monthly_work_days',
        'checker', 
        'signature'
    ];
    
	public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function families()
    {
        return $this->hasMany(EmployeeFamily::class);
    }

    public function emergency()
    {
        return $this->hasOne(EmployeeEmergency::class);
    }

    public function name($full = false)
    {
        return $this->user->name($full);
    }

    public function dp()
    {
        return $this->user->dp();
    }

    public function statuses()
    {
        return $this->hasMany(EmployeeStatus::class);
    }

    public function currentStatus($id = false)
    {
        $status = $this->statuses()->orderBy('id', 'desc')->first();
        if ($id) {
            return $status->employment_status;
        }        
        return getEmploymentStatus()[$status->employment_status];
    }

    public function address()
    {
        $address = $this->address . ', ' . $this->city . ', ' . 
            $this->province . ', ' . $this->zip_code;
        return $address;
    }

    public function dependents()
    {
        return $this->hasMany(Dependent::class);
    }

    public function education()
    {
        return $this->hasMany(EmployeeEducation::class);
    }

    public function scholarships()
    {
        return $this->hasMany(EmployeeScholarship::class);
    }

    public function shiftBriefs()
    {
        return $this->hasMany(ShiftBrief::class);
    }

    public function managedDepartments()
    {
        return $this->hasMany(Scheduler::class);
    }

    public function leaveApplications()
    {
        return $this->hasMany(LeaveApplication::class);
    }

    public function entitledLeave()
    {
        return $this->hasMany(LeaveEntitlement::class);
    }

    public function memoSignatory()
    {
        return $this->hasMany(MemoSignatory::class);
    }

    public function memoConforme()
    {
        return $this->hasMany(MemoRecipient::class);
    }

    public function scheduler()
    {
        return $this->position->department->schedulers()->first();
    }

    public function timecards()
    {
        return $this->hasMany(Timecard::class);
    }

    public function payslips()
    {
        return $this->hasMany(Payslip::class);
    }

    public function approver()
    {
        return $this->hasOne(Approver::class);
    }

    public function isApprover()
    {
        if ($this->approver) {
            return true;
        }
        return false;
    }

    public function forSignatureMemo()
    {
        return $this->memoSignatory()->whereNull('date_signed')->get();
    }

    public function forConformationMemo()
    {
        $memosForConforme = $this->memoConforme;

        $memos = [];
        foreach ($memosForConforme as $mfc) {
            if ($mfc->memo->isSigned() AND $mfc->conform == 0) {
                $memos[$mfc->memo->id] = $mfc->memo->id;
            }
        }

        return $memos;
    }

    public function scopeOrderByLastName($query)
    {
        return $query->leftJoin('users', 'employees.user_id', '=', 'users.id')->orderBy('users.last_name');
    }

    public function scopeOrderByFirstName($query)
    {
        return $query->leftJoin('users', 'employees.user_id', '=', 'users.id')->orderBy('users.first_name');
    }

    public function scopeActive($query)
    {
        return $query->where('users.is_active', 1);
    }

    public function calculateWithHoldingTax($monthly = null, $taxableIncome)
    {
        $dependentsCount = $this->dependents()->count();

        if ($monthly != null) {
            $taxTable = WithholdingTax::where('deduction_schedule', 'monthly')
                ->where('dependents', $dependentsCount)
                ->whereRaw("'$taxableIncome' BETWEEN min AND max")
                ->first();

            $diff = $taxableIncome - $taxTable->min;
            $excess = $diff * $taxTable->percent;

            return $taxTable->base_tax + $excess;
        }
        
        $taxTable = WithholdingTax::where('deduction_schedule', 'semi-monthly')
            ->where('dependents', $dependentsCount)
            ->whereRaw("'$taxableIncome' BETWEEN min AND max")
            ->first();

        $diff = $taxableIncome - $taxTable->min;
        $excess = $diff * $taxTable->percent;

        return $taxTable->base_tax + $excess;
    }

    public function updatePersonalInfo($request)
    {
        $this->position_id = $request->position_id;
        $this->mobile_number = $request->mobile_number;
        $this->home_number = $request->home_number;
        $this->email_address = $request->email_address;
        $this->nickname = $request->nickname;
        $this->birthdate = Carbon::parse($request->birthdate);
        $this->birthplace = $request->birthplace;
        $this->sex = $request->sex;
        $this->address = $request->address;
        $this->city = $request->city;
        $this->province = $request->province;
        $this->zip_code = $request->zip_code;
        $this->mobile_number = $request->mobile_number;
        $this->home_number = $request->home_number;
        $this->email_address = $request->email_address;
        $this->civil_status = $request->civil_status;
        $this->citizenship = $request->citizenship;
        $this->religion = $request->religion;
        $this->height = $request->height;
        $this->weight = $request->weight;
        $this->build = $request->build;
        $this->eyes = $request->eyes;
        $this->blood_type = $request->blood_type;
        $this->sss = $request->sss;
        $this->tin = $request->tin;
        $this->philhealth = $request->philhealth;
        $this->pagibig = $request->pagibig;
        $this->save();

        return $this;
    }

    public function updateDirectFamily($request)
    {
        $this->father_name = $request->father_name;
        $this->father_occupation = $request->father_occupation;
        $this->mother_name = $request->mother_name;
        $this->mother_occupation = $request->mother_occupation;
        $this->spouse_name = $request->spouse_name;
        $this->spouse_occupation = $request->spouse_occupation;

        return $this;
    }

    public function updateEmergency($request)
    {
        $this->contact_person = $request->contact_person;
        $this->cp_number = $request->cp_number;
        $this->cp_address = $request->cp_address;
        $this->cp_language_spoken = $request->cp_language_spoken;

        return $this;
    }

    public function signature()
    {
        $signature = asset('assets/images/signed.png');
        if ($this->signature) {
            $signature = asset('uploads/signatures/' . $this->signature);
        }
        return $signature;
    }

    public static function createEmployee($request)
    {        
        $employeeData = [
            // 'user_id' => $user->id,
            'position_id' => $request->position_id,
            'nickname' => $request->nickname,
            'address' => $request->address,
            'city' => $request->city,
            'province' => $request->province,
            'zip_code' => $request->zip_code,
            'mobile_number' => $request->mobile_number,
            'home_number' => $request->home_number,
            'email_address' => $request->email_address,
            'birthdate' => $request->birthdate,
            'birthplace' => $request->birthplace,
            'sex' => $request->sex,
            'civil_status' => $request->civil_status,
            'citizenship' => $request->citizenship == 'Filipino' ? $request->citizenship : $request->citizenship_other,
            'religion' => $request->religion,
            'height' => $request->height,
            'weight' => $request->weight,
            'build' => $request->build,
            'eyes' => $request->eyes,
            'blood_type' => $request->blood_type,
            'sss' => $request->sss == '' ? null : $request->sss,
            'tin' => $request->tin == '' ? null : $request->tin,
            'philhealth' => $request->philhealth == '' ? null : $request->philhealth,
            'pagibig' => $request->pagibig == '' ? null : $request->pagibig,

            'father_name' => $request->father_name,
            'father_occupation' => $request->father_occupation,
            'mother_name' => $request->mother_name,
            'mother_occupation' => $request->mother_occupation,

            'spouse_name' => $request->spouse_name,
            'spouse_occupation' => $request->spouse_occupation,

            'contact_person' => $request->contact_person,
            'cp_number' => $request->cp_number,
            'cp_address' => $request->cp_address,
            'cp_language_spoken' => $request->cp_language_spoken,

            'date_hired' => Carbon::parse($request->date_hired),
            'basic_income' => $request->basic_income,
            'monthly_work_days' => $request->monthly_work_days
        ];
        $employee = Employee::create($employeeData);
        
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => $request->employment_status,
            'updated_by' => Auth::user()->employee->id
        ]);

        return $employee;
    }

    public function canFileLeave()
    {
        $entitledLeaves = $this->entitledLeave()->where('year', Carbon::now()->year)->get();
        if (count($entitledLeaves) > 0) {
            return true;
        }
        return false;
    }

    public function checkLeaveCredits($leaveTypeId = null)
    {
        if ($leaveTypeId == 0) {
            return 10;
        }
        
        $leaveTypeId = $leaveTypeId == null ? $this->id : $leaveTypeId;
        $entitledLeaves = $this->entitledLeave()
            ->where('year', Carbon::now()->year)
            ->where('leave_type_id', $leaveTypeId)
            ->get();
        
        $credits = 0;
        if (count($entitledLeaves) > 0) {
            foreach ($entitledLeaves as $entitledLeave) {
                $credits += $entitledLeave->credits;
            }
        }        

        $approvedLeaves = $this->approvedLeavesCount($leaveTypeId);

        return $credits - $approvedLeaves;
    }

    public function approvedLeavesCount($leaveTypeId)
    {
        $count = 0;
        $leaveApplications = $this->leaveApplications;
        foreach ($leaveApplications as $leaveApplication) {
            if ($leaveApplication->approved() AND $leaveApplication->leave_type_id == $leaveTypeId) {
                $count += $leaveApplication->number_of_days;
            }
        }

        return $count;
    }

    public function forApprovalLeaves()
    {
        $forApprovalLeaves = LeaveApplication::query()
            ->whereRaw('first_approver = ' . $this->id . ' AND is_first_approved < 0')
            ->orWhereRaw('final_approver = ' . $this->id . ' AND is_final_approved < 0')
            ->get();

        return $forApprovalLeaves;
    }

    public function approvedLeaves()
    {
        $approvedLeaves = LeaveApplication::query()
            ->whereRaw('first_approver = ' . $this->id)
            ->orWhereRaw('final_approver = ' . $this->id)
            ->get();

        return $approvedLeaves;
    }

    public function isLeaveSignatory($leaveId)
    {
        $leaveApplication = LeaveApplication::findOrFail($leaveId);
        
        if ($leaveApplication->isApprover($this->id)) {
            return true;
        }

        return false;        
    }

    public function managedEmployees()
    {
        $managedDepartments = $this->managedDepartments;

        $positions = [];
        $count = 0;
        foreach ($managedDepartments as $department) {
            foreach ($department->department->positions as $position) {
                $positions[$count] = $position->id;
                $count++;
            }
        }

        $employees = Employee::whereIn('position_id', $positions)->get();

        $es = [];
        $count = 0;
        foreach ($employees as $employee) {
            $es[$count] = $employee->id;
            $count++;
        }

        return $es;
    }

    public function memoNotificationCount()
    {
        // get for signature
        $forSignatureMemoCount = $this->forSignatureMemo()->count();

        // get unconformed memo
        $unconformedMemoCount = count($this->forConformationMemo());

        return $forSignatureMemoCount + $unconformedMemoCount;
    }
    
}
