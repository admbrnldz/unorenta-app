<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class PayslipLeave extends Model
{
    
    protected $fillable = [
        'payslip_id',
        'leave_application_id',
        'days'
    ];

    public function payslip()
    {
        return $this->belongsTo(Payslip::class);
    }

    public function leaveApplication()
    {
        return $this->belongsTo(LeaveApplication::class);
    }

}
