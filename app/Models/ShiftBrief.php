<?php

namespace UnoRenta\Models;

use Auth;
use UnoRenta\Models\ShiftBrief;
use Illuminate\Database\Eloquent\Model;

class ShiftBrief extends Model
{
    
    protected $fillable = [
        'employee_id', 'month', 'year', 'created_by'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(Employee::class, 'created_by', 'id');
    }

    public function shifts()
    {
        return $this->hasMany(Shift::class);
    }

    public static function createShiftBrief($employeeId, $request)
    {
        $shiftBriefData = [
            'employee_id' => $employeeId,
            'month' => $request->month,
            'year' => $request->year,
            'created_by' => Auth::user()->employee->id
        ];
        $shiftBrief = ShiftBrief::create($shiftBriefData);

        return $shiftBrief;
    }

}
