<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentCategory extends Model
{

    protected $fillable = [
        'name',
        'order'
    ];

    public function assessments()
    {
        return $this->hasMany(AssessmentGuide::class);
    }

}
