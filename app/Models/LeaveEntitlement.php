<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveEntitlement extends Model
{
    
    protected $fillable = [
        'employee_id',
        'leave_type_id',
        'year',
        'credits',
    ];

    public function leaveType()
    {
        return $this->belongsTo(LeaveType::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

}
