<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	
	public $timestamps = false;

    protected $fillable = [
        'permission_category_id',
        'key', 
        'name'
    ];

    public function category()
    {
        return $this->belongsTo(PermissionCategory::class);
    }
	
}
