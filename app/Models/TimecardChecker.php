<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class TimecardChecker extends Model
{
    
    public $timestamps = false;

    protected $fillable = [
        'employee_id', 'checker_id'
    ];

}
