<?php

namespace UnoRenta\Models;

use Auth;
use Gate;
use UnoRenta\Models\User;
use UnoRenta\Models\UserPermission;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'display_photo',
        'notification_tone',
        'is_active',
        'email',
        'password',
        'username'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    public function permissions()
    {
        return $this->hasMany(UserPermission::class);
    }

    public function allowedTo($permission)
    {
        $permission = Permission::where('key', str_replace('_', '-', $permission))->first();
        if ($permission) {
            if ($permissions = $this->permissions()->where('permission_id', $permission->id)->count() > 0) {
                return true;
            }
        }
        return false;
    }

    public function name($formal = null)
    {
        if ($formal) {
            return $this->last_name . ', ' . $this->first_name . ' ' . substr($this->middle_name, 0, 1) . '.';
        }
        return $this->first_name . ' ' . $this->last_name;
    }

    public function dp()
    {
        $dp = $this->display_photo;
        if ($dp === 'no-dp.png') {
            return asset('assets/images/' . $dp);
        }
        else {
            return asset('uploads/dp/' . $dp);
        }
    }

    public function position($positions)
    {
        foreach ($positions as $position) {
            if ($this->employee->position->name == $position) {
                return true;
            }
        }
        return false;
    }

    public function isApprover()
    {
        return $this->employee->isApprover();
    }

    public static function createUser($request)
    {
        $userData = [
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'username' => $request->username,
            'password' => bcrypt($request->password)
        ];
        $user = User::create($userData);

        if ($request->hasFile('display_photo')) {

            $destinationPath = 'uploads/dp';
            $extension = $request->file('display_photo')->getClientOriginalExtension();
            $fileName = $user->id . '_' . time() . '.' . $extension;
            $request->file('display_photo')->move($destinationPath, $fileName);

            $user->display_photo = $fileName;
            $user->save();

        }
        
        return $user;
    }

    public function updateUserSettings($request)
    {
        if ($request->user_id == Auth::user()->id AND !Gate::denies('edit_account_settings', Auth::user())) {

            $this->username = $request->username;            
            if ($request->password != '') {
                $this->password = bcrypt($request->password);
            }
            $this->save();
            return $this;

        }

        $this->first_name = $request->first_name;
        $this->middle_name = $request->middle_name;
        $this->last_name = $request->last_name;
        $this->username = $request->username;
        
        if ($request->password != '') {
            $this->password = bcrypt($request->password);
        }

        if ($request->is_active) {
            if ($this->is_active == 0) {
                $this->employee->status = 1; // back to probi
                $this->employee->save();
            }
            $this->is_active = 1;            
        }
        if ($request->is_active == 0) {
            $this->is_active = 0;
            $this->employee->status = 3;
            $this->employee->save();
        }

        $this->save();
        return $this;
    }

}
