<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
	
	public $timestamps = false;

    protected $fillable = [
        'permission_id', 'user_id'
    ];

    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

}
