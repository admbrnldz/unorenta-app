<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    
    protected $fillable = [
        'interview_id',
        'assessment_guide_id',
        'rating',
        'comments',
    ];

    public function assessmentGuide()
    {
        return $this->belongsTo(AssessmentGuide::class);
    }

}
