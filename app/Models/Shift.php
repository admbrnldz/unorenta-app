<?php

namespace UnoRenta\Models;

use Auth;
use Carbon\Carbon;
use UnoRenta\Models\Shift;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    
    protected $fillable = [
        'shift_brief_id', 
        'shift_from',
        'shift_to',
        'is_rd'
    ];

    public function timecard()
    {
        return $this->hasOne(Timecard::class);
    }

    public function shiftBrief()
    {
        return $this->belongsTo(ShiftBrief::class);
    }

    public function employee()
    {
        return $this->shiftBrief->employee;
    }

    public static function scheduleShift($request)
    {
        $shift = Shift::firstOrNew([
            'id' => $request->shift_id
        ]);
        $shift->employee_id = $request->employee_id;
        $shift->created_by = Auth::user()->employee->id;
        $shift->date = $request->date;
        $shift->shift_from = $request->shift_from;
        $shift->shift_to = $request->shift_to;

        return $shift;
    }

}
