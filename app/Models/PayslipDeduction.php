<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class PayslipDeduction extends Model
{
    
    protected $fillable = [
        'payslip_id',
        'scheduled_deduction_id',
        'deduction_type',
        'name',
        'key',
        'amount'
    ];

    public function payslip()
    {
        return $this->belongsTo(Payslip::class);
    }

    public function scheduledDeduction()
    {
        return $this->belongsTo(ScheduledDeduction::class);
    }

}
