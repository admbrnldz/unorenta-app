<?php

namespace UnoRenta\Models;

use Auth;
use Carbon\Carbon;
use UnoRenta\Models\LeaveComment;
use Illuminate\Database\Eloquent\Model;

class LeaveComment extends Model
{
    
    protected $fillable = [
        'leave_application_id',
        'author_id',
        'comment'
    ];

    public function leave()
    {
        return $this->belongsTo(LeaveApplication::class);
    }

    public function author()
    {
        return $this->belongsTo(Employee::class, 'author_id', 'id');
    }

    public static function addComment($request)
    {
        $leaveCommentData = [
            'leave_application_id' => $request->leave_application_id,
            'author_id' => Auth::user()->employee->id,
            'comment' => formatCommentText($request->comment),
            'posted_at' => Carbon::now()
        ];
        $leaveComment = LeaveComment::create($leaveCommentData);

        return $leaveComment;
    }

}
