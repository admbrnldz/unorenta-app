<?php

namespace UnoRenta\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
    
    protected $fillable = [
        'employee_id',
        'first_name',
        'last_name',
        'middle_name',
        'birthdate',
        'birthplace',
        'relationship'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function name($full = false)
    {
        if ($full) {
            return $this->last_name . ', ' . $this->last_name . ' ' . substr($this->middle_name, 0, 1);
        }
        return $this->first_name . ' ' . $this->last_name;
    }

    public static function saveDependent($request)
    {
        $dependent = Dependent::create([
            'employee_id' => $request->employee_id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'relationship' => $request->relationship,
            'birthdate' => Carbon::parse($request->birthdate),
            'birthplace' => $request->birthplace
        ]);

        return $dependent;
    }

    public function updateDependent($request)
    {
        $this->first_name = $request->first_name;
        $this->middle_name = $request->middle_name;
        $this->last_name = $request->last_name;
        $this->relationship = $request->relationship;
        $this->birthdate = $request->birthdate;
        $this->birthplace = $request->birthplace;

        return $this;
    }

}
