<?php

namespace UnoRenta\Models;

use Carbon\Carbon;
use UnoRenta\Models\Applicant;
use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
	
	protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'contact_number',
        'email',
        'skype',
        'is_hired',
        'is_flagged',
        'flag_start',
        'duration'
    ];

    public function interview()
    {
        return $this->hasOne(Interview::class);
    }

    public function documents()
    {
        return $this->hasMany(ApplicantDocument::class);
    }

    public function isInterviewed()
    {
        if ($this->interview != null) {
            return true;
        }
        return false;
    }

    public function flagDays()
    {
        $flagStart = Carbon::parse($this->flag_start);
        $now = Carbon::now();
        $daysAgo = $now->diffInDays($flagStart);

        return $daysAgo;
    }

    public function name($full = false)
    {
        if ($full) {
            return $this->last_name . ', ' . $this->first_name . ' ' . substr($this->middle_name, 0, 1) . '.';
        }
        return $this->first_name . ' ' . $this->last_name;
    }
	
    public static function saveApplicant($request)
    {
        $applicant = Applicant::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'contact_number' => $request->contact_number  == '' ? null : $request->contact_number,
            'email' => $request->email == '' ? null : $request->email,
            'skype' => $request->skype == '' ? null : $request->skype
        ]);
        return $applicant;
    }

    public function updateApplicant($request)
    {
        $this->first_name = $request->first_name;
        $this->middle_name = $request->middle_name;
        $this->last_name = $request->last_name;
        $this->contact_number = $request->contact_number  == '' ? null : $request->contact_number;
        $this->email = $request->email == '' ? null : $request->email;
        $this->skype = $request->skype == '' ? null : $request->skype;
    }

    public function hired()
    {
        $this->is_hired = 1;
        $this->save();

        return $this;
    }

}
