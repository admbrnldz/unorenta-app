<?php

namespace UnoRenta\Models;

use Auth;
use Carbon\Carbon;
use UnoRenta\Models\MemoComment;
use Illuminate\Database\Eloquent\Model;

class MemoComment extends Model
{
    
    protected $fillable = [
        'memo_id',
        'author_id',
        'comment'
    ];

    public function memo()
    {
        return $this->belongsTo(Memo::class);
    }

    public function author()
    {
        return $this->belongsTo(Employee::class, 'author_id', 'id');
    }

    public static function addComment($request)
    {
        $memoCommentData = [
            'memo_id' => $request->memo_id,
            'author_id' => Auth::user()->employee->id,
            'comment' => formatCommentText($request->comment),
            'posted_at' => Carbon::now()
        ];
        $memoComment = MemoComment::create($memoCommentData);

        return $memoComment;
    }

}
