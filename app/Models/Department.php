<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Department;
use UnoRenta\Models\Scheduler;

class Department extends Model
{
	
    protected $fillable = [
        'name'
    ];

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function schedulers()
    {
        return $this->hasMany(Scheduler::class);
    }

    public function supervisorPosition()
    {
        return $this->belongsTo(Position::class, 'supervisor', 'id');
    }

    public function employeeCount()
    {
        return Employee::query()
            ->join('positions', 'employees.position_id', '=', 'positions.id')
            ->join('departments', 'positions.department_id', '=', 'departments.id')
            ->where('departments.id', $this->id)
            ->count();
    }

    public function employees()
    {
        return Employee::query()
            ->leftJoin('positions', 'employees.position_id', '=', 'positions.id')
            ->leftJoin('departments', 'positions.department_id', '=', 'departments.id')
            ->where('departments.id', $this->id)
            ->get();
    }

    public static function createDepartment($request)
    {
        $departmentData = [
            'name' => $request->name,
        ];
        $department = Department::create($departmentData);

        foreach ($request->schedulers as $scheduler) {
            Scheduler::create([
                'department_id' => $department->id,
                'employee_id' => $scheduler
            ]);
        }

        return $department;
    }

    public static function updateDepartment($id, $request)
    {
        $department = Department::findOrFail($id);
        $departmentData = [
            'name' => $request->name
        ];
        $department->update($departmentData);

        Scheduler::where('department_id', $department->id)->delete();

        foreach ($request->schedulers as $scheduler) {
            Scheduler::create([
                'department_id' => $department->id,
                'employee_id' => $scheduler
            ]);
        }

        return $department;
    }

}
