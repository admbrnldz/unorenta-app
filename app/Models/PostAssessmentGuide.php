<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class PostAssessmentGuide extends Model
{
    
    protected $fillable = [
        'question',
        'order'
    ];

}
