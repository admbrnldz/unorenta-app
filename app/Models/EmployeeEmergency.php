<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmergency extends Model
{

    protected $fillable = [
        'employee_id',
        'name',
        'contact_number',
        'address',
        'language_spoken'
    ];

}
