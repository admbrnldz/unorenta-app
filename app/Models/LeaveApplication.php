<?php

namespace UnoRenta\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model
{

    protected $fillable = [
        'employee_id',
        'leave_type_id',
        'number_of_days',
        'time_cycle',
        'date_from',
        'date_to',
        'reason', 
        'first_approver',
        'is_first_approved',
        'first_date_signed',
        'final_approver',
        'is_final_approved',
        'final_date_signed',
        'without_pay'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function firstApprover()
    {
        return $this->belongsTo(Employee::class, 'first_approver', 'id');   
    }

    public function finalApprover()
    {
        return $this->belongsTo(Employee::class, 'final_approver', 'id');   
    }

    public function leaveType()
    {
        return $this->belongsTo(LeaveType::class);
    }

    public function comments()
    {
        return $this->hasMany(LeaveComment::class);
    }

    public function approved()
    {
        if ($this->is_final_approved > 0) {
            return true;
        }
        return false;
    }

    public static function processLeave($request)
    {
        $leave = new self();

        $leave->employee_id = $request->employee_id;
        $leave->leave_type_id = $request->leave_type_id;
        $leave->number_of_days = $request->number_of_days;
        $leave->date_from = $request->date_from;
        $leave->date_to = $request->date_to;

        if ($request->number_of_days > 1) {
            $leave->date_to = Carbon::parse($request->date_from)->addDay($request->number_of_days - 1);
        } elseif ($request->number_of_days == 1) {
            $leave->date_to = $request->date_from;
        }

        if ($request->leave_period == 'Half Day') {
            $leave->number_of_days = '.5';
            $leave->date_to = $request->date_from;
            $leave->time_cycle = $request->time_cycle;
        } else {
            $leave->time_cycle = null;
        }
        
        $leave->reason = $request->reason;
        $leave->first_approver = $request->first_approver;
        $leave->final_approver = $request->final_approver;

        if ($leave->leave_type_id == 0) {
            $leave->without_pay = 1;
        }

        $leave->save();

        return $leave;
    }

    public function isApprover($employeeId)
    {
        if (
            $this->first_approver == $employeeId OR 
            $this->final_approver == $employeeId
        ) {
            return true;
        }
        return false;
    }

    public function hasApproved($employeeId)
    {
        if (
            $this->final_approver == $employeeId AND 
            $this->is_final_approved >= 0
        ) {
            return true;
        }
        return false;
    }






    /*

    public function isUserASignatory()
    {
        $loa = LeaveApplication::query()
            ->where('id', $this->id)
            ->where('first_approver', Auth::user()->employee->id)
            ->orWhere('final_approver', Auth::user()->employee->id)
            ->first();

        if ($loa) {
            return true;
        }

        return false;
    }
    
    public static function processLeave($request)
    {
        $leave = new LeaveApplication;
        $leave->employee_id = $request->employee_id;
        $leave->leave_type_id = $request->leave_type_id;

        $leave->number_of_days = $request->number_of_days;
        $leave->date_from = $request->date_from;
        $leave->date_to = $request->date_to;

        if ($request->number_of_days > 1) {
            $leave->date_to = Carbon::parse($request->date_from)->addDay($request->number_of_days - 1);
        }

        if ($request->leave_period == 'Half Day') {
            $leave->number_of_days = '.5';
            $leave->date_to = $request->date_from;
            $leave->time_cycle = $request->time_cycle;
        }
        else {
            $leave->time_cycle = null;
        }
        
        $leave->reason = $request->reason;
        $leave->first_approver = $request->first_approver;
        $leave->final_approver = $request->final_approver;

        return $leave;
    }

    public function isApprover($employeeId)
    {
        if ($this->first_approver == $employeeId OR $this->final_approver == $employeeId) {
            return true;
        }
        return false;
    }

    public function hasApproved($employeeId)
    {
        if ($this->first_approver == $employeeId AND $this->is_first_approved >= 0) {
            return true;
        }
        if ($this->final_approver == $employeeId AND $this->is_final_approved >= 0) {
            return true;
        }        
        return false;
    }
    */

}
