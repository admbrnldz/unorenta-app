<?php

namespace UnoRenta\Models;

use Carbon\Carbon;
use UnoRenta\Models\Holiday;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    
    protected $fillable = [
        'name', 
        'description', 
        'date',
        'holiday_category_id'
    ];

    public function day()
    {
        return date('d', strtotime($this->date));
    }

    public function month()
    {
        return date('M', strtotime($this->date));
    }

    public function category()
    {
        return $this->belongsTo(HolidayCategory::class, 'holiday_category_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(HolidayComment::class);
    }

    public static function saveHoliday($request)
    {
        $holiday = Holiday::firstOrNew([
            'id' => $request->holiday_id
        ]);
        $holiday->date = Carbon::parse($request->date);
        $holiday->name = $request->name;
        $holiday->description = $request->description;
        $holiday->holiday_category_id = $request->category_id;

        return $holiday;
    }

    public function updateHoliday($request)
    {
        $this->date = $request->date;
        $this->name = $request->name;
        $this->description = $request->description;
        $this->save();

        return $this;
    }

}
