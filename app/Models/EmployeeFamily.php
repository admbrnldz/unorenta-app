<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeFamily extends Model
{
    
    protected $fillable = [
        'employee_id',
        'name',
        'occupation',
        'relationship'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

}
