<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentGuide extends Model
{
    
    protected $fillable = [
        'question',
        'assessment_category_id',
        'order'
    ];

    public function category()
    {
        return $this->belongsTo(AssessmentCategory::class, 'assessment_category_id', 'id');
    }

}
