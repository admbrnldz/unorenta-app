<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionCategory extends Model
{

    public $timestamps = false;
    
    protected $fillable = [
        'name'
    ];

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }

}
