<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatus extends Model
{
    
    protected $table = 'employment_status_logs';

    protected $fillable = [
        'employee_id',
        'employment_status',
        'updated_by'
    ];

}
