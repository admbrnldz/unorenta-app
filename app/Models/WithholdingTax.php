<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class WithholdingTax extends Model
{
    
    /**
    * No timestamp
    */
    public $timestamps = false;

    /**
    * Table name
    */
    protected $table = 'withholding_tax_table';

    /**
    * Fillable attributes
    */
    protected $fillable = [
        'deduction_schedule',
        'dependents',
        'min',
        'max',
        'base_tax',
        'percent'
    ];

}
