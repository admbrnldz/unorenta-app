<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;
use UnoRenta\Models\EmployeeEducation;

class EmployeeEducation extends Model
{

    protected $fillable = [
        'employee_id',
        'level',
        'school_name',
        'school_address',
        'course_degree',
        'year_graduated'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public static function saveEducation($request)
    {
        $educData = [
            'employee_id' => $request->employee_id,
            'level' => $request->level,
            'school_name' => $request->school_name,
            'school_address' => $request->school_address,
            'course_degree' => $request->course_degree,
            'year_graduated' => $request->year_graduated
        ];
        $education = EmployeeEducation::create($educData);

        return $education;
    }

    public function updateEducation($request)
    {
        $this->level = $request->level;
        $this->school_name = $request->school_name;
        $this->school_address = $request->school_address;
        $this->course_degree = $request->course_degree;
        $this->year_graduated = $request->year_graduated;
        $this->save();

        return $this;
    }

}
