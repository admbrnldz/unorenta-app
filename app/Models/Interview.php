<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    
    public $timestamps = false;

    protected $fillable = [
        'applicant_id',
        'interviewer',
        'date_interview'
    ];

    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }

    public function theInterviewer()
    {
        return $this->belongsTo(Employee::class, 'interviewer', 'id');
    }

    public function assessments()
    {
        return $this->hasMany(Assessment::class);
    }

    public function postAssessments()
    {
        return $this->hasMany(PostAssessment::class);
    }

}
