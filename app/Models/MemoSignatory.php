<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class MemoSignatory extends Model
{

	public $timestamps = false;

    protected $fillable = [
        'memo_id', 'employee_id', 'complimentary_closing', 'date_signed'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
	
}
