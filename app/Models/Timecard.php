<?php

namespace UnoRenta\Models;

use Auth;
use Carbon\Carbon;
use UnoRenta\Models\Timecard;
use Illuminate\Database\Eloquent\Model;

class Timecard extends Model
{
	
	protected $fillable = [
        'employee_id',
		'shift_id',        
        'time_in',
        'ti_signed_by',
        'time_out',
        'to_signed_by',
        'late_minutes',
        'undertime_minutes',
        'is_absent',
        'absent_reason',
        'overtime_minutes'
	];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }

    public function breaks()
    {
        return $this->hasMany(Breaktime::class);
    }

    public function totalBreak()
    {
        $breaks = $this->breaks()->whereNotNull('break_start')->whereNotNull('break_end')->get();
        $totalMinutes = 0;
        if (count($breaks) > 0) {
            foreach ($breaks as $break) {
                $breakStart = Carbon::parse($break->break_start);
                $breakEnd = Carbon::parse($break->break_end);
                $totalMinutes += $breakStart->diffInMinutes($breakEnd);
            }
        }

        return $totalMinutes;
    }

    public function totalHoursWorked()
    {
        $ti = Carbon::parse($this->time_in);
        $to = Carbon::parse($this->time_out);

        return $ti->diffInMinutes($to) / 60;
    }

    public static function absentTimecard($request)
    {
        $timecard = Timecard::firstOrNew([
            'id' => $request->timecard_id
        ]);
        $timecard->employee_id = $request->employee_id;
        $timecard->signed_by = Auth::user()->employee->id;
        $timecard->date = $request->date;
        $timecard->time_in = '00:00:00';
        $timecard->time_out = '00:00:00';
        $timecard->late_minutes = 0;
        $timecard->is_absent = 1;
        $timecard->absent_reason = $request->absent_reason;

        return $timecard;
    }

    public static function presentTimecard($request)
    {
        $timecard = Timecard::firstOrNew([
            'id' => $request->timecard_id
        ]);
        $timecard->employee_id = $request->employee_id;
        $timecard->signed_by = Auth::user()->employee->id;
        $timecard->date = $request->date;
        $timecard->time_in = $request->shift_from;
        $timecard->time_out = $request->shift_to;
        $timecard->late_minutes = 0;
        $timecard->is_absent = 0;
        $timecard->absent_reason = '';

        return $timecard;
    }

    public static function lateTimecard($request)
    {
        $timecard = Timecard::firstOrNew([
            'id' => $request->timecard_id
        ]);
        $timecard->employee_id = $request->employee_id;
        $timecard->signed_by = Auth::user()->employee->id;
        $timecard->date = $request->date;
        $timecard->time_in = $request->shift_from;
        $timecard->time_out = $request->shift_to;
        $timecard->late_minutes = $request->late_minutes;
        $timecard->is_absent = 0;
        $timecard->absent_reason = '';

        return $timecard;
    }

    public static function timeIn($employeeId, $shiftId)
    {
        $timecard = Timecard::create([
            'employee_id' => $employeeId,
            'shift_id' => $shiftId,
            'time_in' => Carbon::now()->toDateTimeString()
        ]);

        return $timecard;
    }

    public function scopeForApprovalTimeIn($query)
    {
        return $query->whereNotNull('time_in')
            ->whereNull('ti_signed_by')
            ->where('is_absent', 0)
            ->orderBy('time_in');
    }

    public function scopeForApprovalTimeOut($query)
    {
        return $query->whereNotNull('time_in')
            ->whereNotNull('time_out')
            ->whereNull('to_signed_by')
            ->where('is_absent', 0)
            ->orderBy('time_out');
    }

}
