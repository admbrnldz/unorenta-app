<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class PostAssessment extends Model
{
    
    protected $fillable = [
        'interview_id',
        'post_assessment_guide_id',
        'comments'
    ];

    public function postAssessmentGuide()
    {
        return $this->belongsTo(PostAssessmentGuide::class);
    }

}
