<?php

namespace UnoRenta\Models;

use Carbon\Carbon;
use UnoRenta\Models\Holiday;
use Illuminate\Database\Eloquent\Model;

class Payslip extends Model
{
    
    protected $fillable = [
        'payroll_batch_id',
        'employee_id',
        'basic_pay',
        'monthly_work_days',
        'lt',
        'ab',
        'ut',
        'nd',
        'rdot',
        'sh',
        'shrd',
        'rh',
        'rhrd',
        'dh',
        'dhrd'
    ];

    public function payrollBatch()
    {
        return $this->belongsTo(PayrollBatch::class);
    }

    public function timecards()
    {
        return $this->hasMany(Timecard::class);
    }

    public function deductions()
    {
        return $this->hasMany(PayslipDeduction::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function leaves()
    {
        return $this->hasMany(PayslipLeave::class);
    }

    public function ratePerHour()
    {
        return ($this->basic_pay * 2) / ($this->monthly_work_days * 8);
    }

    public function hoursWorked()
    {
        return count($this->timecards) * 8;
    }

    // public function lwopDeduction()
    // {
    //     return $this->lwop > 0 ? ($this->lwop / 60) * $this->ratePerHour() : 0;
    // }

    public function ltDeduction()
    {
        return $this->lt > 0 ? ($this->lt / 60) * $this->ratePerHour() : 0;
    }

    public function abDeduction()
    {
        return $this->ab > 0 ? ($this->ab / 60) * $this->ratePerHour() : 0;
    }

    public function utDeduction()
    {
        return $this->ut > 0 ? ($this->ut / 60) * $this->ratePerHour() : 0;
    }

    public function ndPayment()
    {
        // + 10% of rate/hour
        return $this->nd > 0 ? ($this->nd / 60) * ($this->ratePerHour() * .1) : 0;
    }

    public function rdotPayment()
    {
        // rest day/overtime (rdot)
        // + 30% of rate/hour
        return $this->rdot > 0 ? ($this->rdot / 60) * ($this->ratePerHour() * .3) : 0;
    }

    public function shPayment()
    {
        // speacial holiday
        // + 30% of rate/hour
        return $this->sh > 0 ? ($this->sh / 60) * ($this->ratePerHour() * .3) : 0;
    }

    public function shrdPayment()
    {
        // speacial holiday + rest day/overtime
        // + 50% of rate/hour
        return $this->shrd > 0 ? ($this->shrd / 60) * ($this->ratePerHour() * .5) : 0;
    }

    public function rhPayment()
    {
        // regular holiday
        // + 100% of rate/hour
        return $this->rh > 0 ? ($this->rh / 60) * ($this->ratePerHour() * 1) : 0;
    }

    public function rhrdPayment()
    {
        // speacial holiday + rest day/overtime
        // + 160% of rate/hour
        return $this->rhrd > 0 ? ($this->rhrd / 60) * ($this->ratePerHour() * 1.6) : 0;
    }

    public function dhPayment()
    {
        // double holiday
        // + 200% of rate/hour
        return $this->rh > 0 ? ($this->rh / 60) * ($this->ratePerHour() * 2) : 0;
    }

    public function dhrdPayment()
    {
        // double holiday + rest day/overtime
        // + 290% of rate/hour
        return $this->rhrd > 0 ? ($this->rhrd / 60) * ($this->ratePerHour() * 2.9) : 0;
    }

    public function rdothPayment()
    {
        return $this->rdotPayment() + $this->shPayment() + $this->shrdPayment() + $this->rhPayment() + $this->rhrdPayment() + $this->dhPayment() + $this->dhrdPayment();
    }

    public function getGrossPay()
    {
        return $this->basic_pay + $this->ndPayment() + $this->rdothPayment() + $this->leaveWithPayAmount();
    }

    public function getDeductionsBeforeTax()
    {
        $deductions = $this->deductions()
            ->where('deduction_type', 1)
            ->get();

        $deductionBeforeTax = 0;
        foreach ($deductions as $deduction) {
            $deductionBeforeTax += $deduction->amount;
        }

        $deductionBeforeTax += $this->leaveWithoutPayAmount();
        $deductionBeforeTax += $this->ltDeduction();
        $deductionBeforeTax += $this->abDeduction();
        $deductionBeforeTax += $this->utDeduction();

        return $deductionBeforeTax;
    }

    public function getTaxableIncome()
    {
        return $this->getGrossPay() - $this->getDeductionsBeforeTax();
    } 

    public function getWithholdingTax()
    {
        return calculateWithHoldingTax(
            'semi-monthly', 
            $this->getTaxableIncome(),
            $this->employee->dependents()->count()
        );
    }

    public function getDeductionsAfterTax()
    {
        $deductions = $this->deductions()
            ->where('deduction_type', 2)
            ->get();

        $deductionsAfterTax = 0;
        foreach ($deductions as $deduction) {
            $deductionsAfterTax += $deduction->amount;
        }

        return $deductionsAfterTax;
    }

    public function getTotalDeductionsAfterTax()
    {
        return $this->getWithholdingTax() + $this->getDeductionsAfterTax();
    }

    public function getNetPay()
    {
        return $this->getTaxableIncome() - $this->getTotalDeductionsAfterTax();
    }

    public function scopeOrderByLastName($query)
    {
        return $query->join('users', 'employees.user_id', '=', 'users.id')->orderBy('users.last_name');
    }

    public function leaveWithoutPay()
    {
        $leaveWithoutPay = LeaveApplication::query()
            ->whereBetween('date_from', [$this->payrollBatch->from(), $this->payrollBatch->to()])
            ->get();

        return $leaveWithoutPay;
    }

    public function leaveWithoutPayDays()
    {
        $days = 0;
        foreach ($this->leaveWithoutPay() as $leave) {
            $days += $leave->number_of_days;
        }

        return $days;
    }

    public function leaveWithPayDaysCount()
    {
        $days = 0;
        foreach ($this->leaves as $l) {
            if ($l->leaveApplication->without_pay == 0) {
                $days += $l->days;
            }
        }

        return $days;
    }

    public function leaveWithPayAmount()
    {
        return $this->leaveWithPayDaysCount() * 8 * $this->ratePerHour();
    }

    public function leaveWithoutPayDaysCount()
    {
        $days = 0;
        foreach ($this->leaves as $l) {
            if ($l->leaveApplication->without_pay == 1) {
                $days += $l->days;
            }
        }

        return $days;
    }

    public function leaveWithoutPayAmount()
    {
        return $this->leaveWithoutPayDaysCount() * 8 * $this->ratePerHour();
    }























    // public function nightDifferentialMinutes()
    // {
    //     $nd = 0;
    //     foreach ($this->timecards as $tc) {
    //         $nd += getNightDifferential($tc->time_in, $tc->time_out);
    //     }
    //     return $nd;
    // }

    // public function nightDifferential()
    // {
    //     $nd = ($this->nightDifferentialMinutes() / 60) * ($this->ratePerHour() * .1);
    //     return $nd;
    // }

    // public function holiday()
    // {
    //     return 0;
    // }

    // public function holidayMinutes()
    // {
    //     return 0;
    // }

    // public function holidaysCovered()
    // {
    //     $hs = [];

    //     foreach ($this->timecards as $tc) {

    //         $time_in = Carbon::parse($tc->time_in);
    //         $time_out = Carbon::parse($tc->time_out);

    //         $ti = $time_in->copy()->toDateString();
    //         $to = $time_out->copy()->toDateString();

    //         // check if holiday
    //         $holidays = Holiday::whereRaw("date BETWEEN '$ti' AND '$to'")->get();

    //         if (count($holidays) > 0) {
    //             foreach ($holidays as $holiday) {

    //                 $holidayFrom = Carbon::parse($holiday->date);
    //                 $holidayTo = $holidayFrom->copy()->addDay();

    //                 if ($time_in >= $holidayFrom AND $time_out < $holidayTo) {
    //                     if ($holiday->holiday_category_id > 1) {
    //                         if (isset($hs[strtolower($holiday->category->name)])) {
    //                             $hs[strtolower($holiday->category->name)] += $time_in->diffInMinutes($time_out);
    //                         } else {
    //                             $hs[strtolower($holiday->category->name)] = $time_in->diffInMinutes($time_out);
    //                         }
                            
    //                     }                        
    //                 } elseif ($time_in > $holidayFrom AND $time_out > $holidayTo) {
    //                     if ($holiday->holiday_category_id > 1) {
    //                         if (isset($hs[strtolower($holiday->category->name)])) {
    //                             $hs[strtolower($holiday->category->name)] += $time_in->diffInMinutes($time_out) - $holidayTo->diffInMinutes($time_out);
    //                         } else {
    //                             $hs[strtolower($holiday->category->name)] = $time_in->diffInMinutes($time_out) - $holidayTo->diffInMinutes($time_out);
    //                         }                            
    //                     }
    //                 } elseif ($time_in < $holidayFrom AND $time_out > $holidayFrom AND $time_out < $holidayTo) {
    //                     if ($holiday->holiday_category_id > 1) {
    //                         if (isset($hs[strtolower($holiday->category->name)])) {
    //                             $hs[strtolower($holiday->category->name)] += $holidayFrom->diffInMinutes($time_out);
    //                         } else {
    //                             $hs[strtolower($holiday->category->name)] = $holidayFrom->diffInMinutes($time_out);
    //                         }
    //                     }
    //                 }

    //             }
    //         }

    //     }

    //     return $hs;
    // }

    // public function overtime()
    // {
    //     return 0;
    // }

    // public function overtimeMinutes()
    // {
    //     return 0;
    // }

    // public function late()
    // {
    //     $late = 0;
    //     if ($this->lateMinutes() > 0) {
    //         $late = ($this->lateMinutes() / 60) * $this->ratePerHour();
    //     }
    //     return $late;
    // }

    // public function lateMinutes()
    // {
    //     $late = 0;
    //     foreach ($this->timecards as $tc) {
    //         $late += $tc->late_minutes;
    //     }
    //     return $late;
    // }

    // public function undertime()
    // {
    //     $undertime = 0;
    //     if ($this->undertimeMinutes() > 0) {
    //         $undertime = ($this->undertimeMinutes() / 60) * $this->ratePerHour();
    //     }
    //     return $undertime;
    // }

    // public function undertimeMinutes()
    // {
    //     $undertime = 0;
    //     foreach ($this->timecards as $tc) {
    //         $undertime += $tc->undertime_minutes;
    //         if ($tc->is_absent) {
    //             $undertime += (8 * 60);
    //         }
    //     }
    //     return $undertime;
    // }

    // // public function getGrossPay()
    // // {
    // //     return $this->basic_pay + $this->nightDifferential() + $this->holiday() + $this->overtime();
    // // }

    // // public function getDeductionsBeforeTax()
    // // {
    // //     $deductions = $this->deductions()
    // //         ->where('deduction_type', 1)
    // //         ->get();

    // //     $deductionBeforeTax = 0;
    // //     foreach ($deductions as $deduction) {
    // //         $deductionBeforeTax += $deduction->amount;
    // //     }

    // //     $deductionBeforeTax += $this->late();
    // //     $deductionBeforeTax += $this->undertime();

    // //     return $deductionBeforeTax;
    // // }

    // // public function getTaxableIncome()
    // // {
    // //     return $this->getGrossPay() - $this->getDeductionsBeforeTax();
    // // }

    // // public function getDeductionsAfterTax()
    // // {
    // //     $deductions = $this->deductions()
    // //         ->where('deduction_type', 2)
    // //         ->get();

    // //     $deductionsAfterTax = 0;
    // //     foreach ($deductions as $deduction) {
    // //         $deductionsAfterTax += $deduction->amount;
    // //     }

    // //     return $deductionsAfterTax;
    // // }

    // // public function getWithholdingTax()
    // // {
    // //     return calculateWithHoldingTax('semi-monthly', $this->getTaxableIncome(), $this->employee->dependents()->count());
    // // }

    // // public function getNetPay()
    // // {
    // //     return $this->getTaxableIncome() - ($this->getWithholdingTax() + $this->getDeductionsAfterTax());
    // // }

    

}
