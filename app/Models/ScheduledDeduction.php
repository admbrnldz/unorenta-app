<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduledDeduction extends Model
{
    
    protected $fillable = [
        'employee_id',
        'name',
        'amount',
        'months_payable',
        'before_tax',
        'term',
        'half'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function payments()
    {
        return $this->hasMany(PayslipDeduction::class);
    }

    public function paid()
    {
        $installmentCount = $this->term == 'semi-monthly' ? $this->months_payable * 2 : $this->months_payable;
        if ($this->payments()->count() == $installmentCount) {
            return true;
        }
        return false;
    }

}
