<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class HolidayCategory extends Model
{
    
    protected $fillable = [
        'name'
    ];

}
