<?php

namespace UnoRenta\Models;

use Auth;
use UnoRenta\Models\Memo;
use UnoRenta\Models\MemoRecipient;
use UnoRenta\Models\MemoSignatory;
use Illuminate\Database\Eloquent\Model;

class Memo extends Model
{
	
    protected $fillable = [
        'title', 
        'content', 
        'allow_discussions'
    ];

    public function recipients()
    {
        return $this->hasMany(MemoRecipient::class);
    }

    public function signatories()
    {
        return $this->hasMany(MemoSignatory::class);
    }

    public function comments()
    {
        return $this->hasMany(MemoComment::class);
    }

    public function getSignatories()
    {
        $sigees = [];
        $count = 0;
        foreach ($this->signatories as $signatory) {
            $sigees[$count] = $signatory->employee_id;
            $count++;
        }

        return $sigees;
    }

    public function isSignatory($employeeId = null)
    {
        $employeeId = $employeeId == null ? Auth::user()->employee->id : $employeeId;
        if (in_array($employeeId, $this->getSignatories())) {
            return true;
        }

        return false;
    }

    public function getRecipients()
    {
        $recipients = [];
        $count = 0;
        foreach ($this->recipients as $recipient) {
            $recipients[$count] = $recipient->employee_id;
            $count++;
        }

        return $recipients;
    }

    public function isRecipient($employeeId = null)
    {
        $employeeId = $employeeId == null ? Auth::user()->employee->id : $employeeId;
        if (in_array($employeeId, $this->getRecipients())) {
            return true;
        }

        return false;
    }

    public function isSigned()
    {
        $signatories = $this->signatories;
        $signatoryCount = count($signatories);
        $signatureCount = 0;
     
        foreach ($signatories as $signatory) {
            if ($signatory->date_signed != null) {
                $signatureCount++;
            }
        }

        if ($signatoryCount == $signatureCount) {
            return true;
        }

        return false;
    }

    public function conformed($employeeId)
    {
        $recipient = $this->recipients()->where('employee_id', $employeeId)->first();

        if ($recipient AND $recipient->conform) {
            return true;
        }

        return false;
    }

    public function conformedEmployees($count = 7)
    {
        return [
            'list' => $this->recipients()
                ->where('conform', 1)
                ->whereNotIn('employee_id', [Auth::user()->employee->id])
                ->orderBy('date_conform')
                ->take($count)
                ->get(),
            'offset' => $this->recipients()->where('conform', 1)->count() - $count
        ];
    }

    public static function broadcastMemo($request)
    {

        // create memo
        $memoData = [
            'title' => $request->title,
            'content' => $request->content,
            'allow_discussions' => $request->allow_discussions ? 1 : 0
        ];
        $memo = Memo::create($memoData);

        /**
        * Include signatory/ies in recipients
        */
        $r = [];
        $count = 0;
        foreach ($request->signatories as $signatory) {
            MemoSignatory::create([
                'memo_id' => $memo->id,
                'employee_id' => $signatory['employeeid'],
                'complimentary_closing' => $signatory['comp_closing']
            ]);
            MemoRecipient::create([
                'memo_id' => $memo->id,
                'employee_id' => $signatory['employeeid']
            ]);
            $r[$count] = $signatory['employeeid'];
            $count++;
        }

        /**
        * If broadcast to all
        */
        if ($request->to_all) {

            /**
            * Get all employees except the signatories
            */
            $employees = Employee::whereNotIn('id', $r)->get();
            foreach ($employees as $e) {
                MemoRecipient::create([
                    'memo_id' => $memo->id,
                    'employee_id' => $e->id
                ]);
            }

        } else {

            if (count($request->recipients) > 0) {

                foreach ($request->recipients as $e) {
                    MemoRecipient::create([
                        'memo_id' => $memo->id,
                        'employee_id' => $e
                    ]);
                }

            }

        }

        return $memo;
    }

    public function updateMemo($request)
    {
        $memoData = [
            'title' => $request->title,
            'content' => $request->content,
            'allow_discussions' => $request->allow_discussions ? 1 : 0
        ];
        $this->update($memoData);

        /**
        * Clear recipients and signatories
        */
        MemoRecipient::where('memo_id', $this->id)->delete();
        MemoSignatory::where('memo_id', $this->id)->delete();

        /**
        * Include signatory/ies in recipients
        */
        $r = [];
        $count = 0;
        foreach ($request->signatories as $signatory) {
            MemoSignatory::create([
                'memo_id' => $this->id,
                'employee_id' => $signatory['employeeid'],
                'complimentary_closing' => $signatory['comp_closing']
            ]);
            MemoRecipient::create([
                'memo_id' => $this->id,
                'employee_id' => $signatory['employeeid']
            ]);
            $r[$count] = $signatory['employeeid'];
            $count++;
        }

        /**
        * If broadcast to all
        */
        if ($request->to_all) {

            // get all employees except the signatories
            $employees = Employee::whereNotIn('id', $r)->get();
            foreach ($employees as $e) {
                MemoRecipient::create([
                    'memo_id' => $this->id,
                    'employee_id' => $e->id
                ]);
            }

        } else {

            if (count($request->recipients) > 0) {

                foreach ($request->recipients as $e) {
                    MemoRecipient::create([
                        'memo_id' => $this->id,
                        'employee_id' => $e
                    ]);
                }

            }

        }

        return $this;
    }

}
