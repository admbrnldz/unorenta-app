<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class HolidayComment extends Model
{
    
    protected $fillable = [
        'holiday_id',
        'author_id',
        'comment'
    ];

    public function holiday()
    {
        return $this->belongsTo(Holiday::class);
    }

    public function author()
    {
        return $this->belongsTo(Employee::class, 'author_id', 'id');
    }

}
