<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;
use UnoRenta\Models\TimecardChecker;

class Notification extends Model
{
    
    protected $fillable = [
        'recipient_id', 'caption', 'read'
    ];

    public static function timeTrackingNotification($timecard)
    {
        $checkers = TimecardChecker::where('employee_id', $timecard->employee_id)->get();

        foreach ($checkers as $checker) {
            Notification::create([
                'recipient_id' => $checker->checker_id,
                'caption' => $timecard->employee_id . ' logged in for work.'
            ]);
        }
    }

    public function unread($query)
    {
        return $this->query->where('ready', '<>', 1)->count();
    }

}
