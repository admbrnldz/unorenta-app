<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class Breaktime extends Model
{
    
    public $timestamps = false;

    protected $fillable = [
        'timecard_id',
        'break_start',
        'break_end'
    ];

    public function timecard()
    {
        return $this->belongsTo(Timecard::class);
    }
    
}
