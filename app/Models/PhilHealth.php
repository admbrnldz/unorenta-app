<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class PhilHealth extends Model
{
    
    /**
    * No timestamp
    */
    public $timestamps = false;

    /**
    * Table name
    */
    protected $table = 'philhealth_contribution_table';

    /**
    * Fillable attributes
    */
    protected $fillable = [
        'min',
        'max',
        'ee',
        'er'
    ];
    
}
