<?php

namespace UnoRenta\Models;

use UnoRenta\Models\Department;
use Illuminate\Database\Eloquent\Model;

class Approver extends Model
{
    
    protected $fillable = [
        'employee_id',
        'department_id'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function departments($id)
    {
        $depts = [];

        $approvers = $this->where('employee_id', $id)->get();
        foreach ($approvers as $approver) {
            $depts[$approver->department_id] = $approver->department_id;
        }

        $departments = Department::whereIn('id', $depts)->get();

        return $departments;
    }

    public function scopeOrderByLastName($query)
    {
        return $query->leftJoin('employees', 'approvers.employee_id', '=', 'employees.id')
            ->leftJoin('users', 'employees.user_id', '=', 'users.id')
            ->orderBy('users.last_name');
    }

}
