<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class SSS extends Model
{

    /**
    * No timestamp
    */
    public $timestamps = false;

    /**
    * Table name
    */
    protected $table = 'sss_contribution_table';

    /**
    * Fillable attributes
    */
    protected $fillable = [
        'min',
        'max',
        'er',
        'ee',
        'ec'
    ];

}
