<?php

namespace UnoRenta\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantDocument extends Model
{
    
    protected $fillable = [
        'applicant_id',
        'filename',
        'description'
    ];

    public function applicant()
    {
        return $this->belongsTo(Applicant::class);
    }

    public function file()
    {
        return asset('uploads/documents/' . $this->applicant_id . '/' . $this->filename);
    }

}
