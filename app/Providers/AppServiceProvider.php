<?php

namespace UnoRenta\Providers;

use Auth;
use Request;
use Session;
use Carbon\Carbon as Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {

            $view->with('currentUser', Auth::user());
            $view->with('today', Carbon::today());
            $view->with('now', Carbon::now());
            $view->with('currentYear', Carbon::now()->year);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
