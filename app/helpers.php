<?php

use Carbon\Carbon;
use UnoRenta\Models\SSS;
use UnoRenta\Models\Shift;
use UnoRenta\Models\Timecard;
use UnoRenta\Models\PhilHealth;
use UnoRenta\Models\WithholdingTax;

function getMonths()
{
    return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
}

function getTimeOptions()
{
    return ['01:00:00', '02:00:00', '03:00:00', '04:00:00', '05:00:00', '06:00:00', '07:00:00', '08:00:00', '09:00:00', '10:00:00', '11:00:00', '12:00:00', 
            '13:00:00', '14:00:00', '15:00:00', '16:00:00', '17:00:00', '18:00:00', '19:00:00', '20:00:00', '21:00:00', '22:00:00', '23:00:00', '24:00:00'];
}

function getWeekLabels()
{
    return ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
}

function getEducationalLevels()
{
    return ['Elementary', 'High School', 'Undergrate', 'Vocational', 'Graduate Studies'];
}

function getCivilStatus()
{
    return [
        'Single',
        'Married',
        'Annulled/Divorced',
        'Widowed',
        'Separated'
    ];
}

function getDefaultPermissions()
{
    return [
        2,  // view profile
        5,  // view departments & positions
        9,  // view holidays
        13  // view memos
    ];
}

function getYears($year = null)
{
    if ($year == null) {
        $year = date('Y', strtotime(Carbon::today()));
    }
    return [$year - 1, $year, $year + 1];
}

function getTimecard($id, $from)
{
    return Timecard::where('employee_id', $id)
                    ->where('date', $from)
                    ->first();
}

function processDate($date, $withTime = false)
{
    $d = Carbon::parse($date);
    if ($withTime) {
        return $d->format('M j, Y - h:iA');
    }
    return $d->format('M j, Y');
}

function formatShortDate($date) {
    return Carbon::parse($date)->format('M j');   
}

function formatDate($date, $withYear = false)
{
    if ($withYear) {
        return Carbon::parse($date)->format('F j, Y');    
    }
    return Carbon::parse($date)->format('F j');
}

function processTime($time, $withMinute = true)
{
    $t = Carbon::parse($time);
    if ($withMinute) {
        return $t->format('h:iA');    
    }
    return $t->format('ga');
}

function fullDate($date)
{
    $d = Carbon::parse($date);
    return $d->format('F j, Y');
}

function processDateForDB($date)
{
    $d = Carbon::parse($date);
    return $d->format('Y-m-d');
}

function parseTime($time)
{
    return Carbon::parse($time)->toTimeString();
}

function getTimeDifference($from, $to)
{
    $f = explode(':', $from);
    $t = explode(':', $to);

    $difference = 0;
    $timeFrom = $f[0];
    $timeTo = $t[0];

    while ($timeFrom != $timeTo) {
        $timeFrom++;
        $difference++;
        if ($timeFrom > 24) {
            $timeFrom = 1;
        }
    }

    return $difference;
}

function drawCalendar($month, $year)
{
    $running_day = date('w', mktime(0, 0, 0, $month, 1, $year));
    $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
    $days_in_this_week = 1;
    $day_counter = 0;
    ?>
    <input type="hidden" name="days_count" value="<?php echo $days_in_month; ?>">
    <table class="table-calendar">
        <thead>
            <tr class="calendar-row">
                <th class="calendar-day-head">
                    <?php echo implode('</th><th class="calendar-day-head">', getWeekLabels()); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr class="calendar-row">

                <?php for ($x = 0; $x < $running_day; $x++): ?>
                    <td class="calendar-day-np"></td>
                <?php $days_in_this_week++; endfor; ?>

                <?php for ($list_day = 1; $list_day <= $days_in_month; $list_day++): ?>

                    <td class="calendar-day">
                        <div class="calendar-checkbox">
                            <label>
                                <input type="checkbox" name="dayoff[]" id="" value="<?php echo $list_day; ?>" />
                                <span><?php echo $list_day; ?></span>
                            </label>
                        </div>
                    </td>
                    <?php if ($running_day == 6): ?>
                        </tr>
                        <?php if(($day_counter + 1) != $days_in_month): ?>
                        <tr class="calendar-row">
                        <?php endif; $running_day = -1; $days_in_this_week = 0; ?>
                    <?php endif; ?>

                    <?php $days_in_this_week++; $running_day++; $day_counter++; ?>

                <?php endfor; ?>

                <?php if ($days_in_this_week > 1 AND $days_in_this_week < 8): ?>
                    <?php for ($x = 1; $x <= (8 - $days_in_this_week); $x++): ?>
                        <td class="calendar-day-np"></td>
                    <?php endfor; ?>
                <?php endif; ?>

            </tr>
        </tbody>
    </table>
    <?php
}

function formatCommentText($string)
{
    $lines = explode(PHP_EOL, $string);

    $result = '<p>';
    for ($i=0; $i < count($lines); $i++) {
        if ($lines[$i] == '' AND $i != 0) {
            if ($lines[$i - 1] == '') {
                continue;
            }
            $result .= '</p><p>';
        }
        else {
            $result .= $lines[$i];
        }
    }
    $result .= '</p>';

    return $result;
}

function getShiftDetails($shiftBriefId, $date)
{
    return Shift::query()
        ->where('shift_brief_id', $shiftBriefId)
        ->where('shift_from', $date)
        ->first();
}

function activeWhen($array)
{

    $pUrl = parse_url(Request::url());
    $paths = explode('/', $pUrl['path']);

    foreach ($array as $arr) {
        if ($arr == $paths[1]) {
            return true;
        }
    }

    return false;
}

function strposa($haystack, $needles = [], $offset=0)
{
    $chr = array();
    foreach($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
    }
    if(empty($chr)) return false;
    return min($chr);
}

function getNightDifferential($from, $to)
{
    $from = Carbon::parse($from);
    $to = Carbon::parse($to);
    $countStart = $from->copy()->hour(22)->minute(0)->second(0);
    $countEnd = $countStart->copy()->addHours(8);

    $nightDiff = 0;
    $minutesWorked = $from->diffInMinutes($to);

    if ($from < $countStart AND $to < $countEnd AND $to > $countStart) {
        $nightDiff = $minutesWorked - $countStart->diffInMinutes($from);
    }
    if ($from > $countStart AND $to > $countEnd) {
        $nightDiff = $minutesWorked - $to->diffInMinutes($countEnd);
    }
    if ($from->hour <= 6 AND $from < $countStart) {
        $nightDiff = $from->diffInMinutes($from->copy()->hour(6));
    }
    if ($from == $countStart) {
        $nightDiff = $minutesWorked;
    }
    if ($nightDiff < 0) {
        $nightDiff = 0;
    }

    return $nightDiff;
}

function getSSSPremium($basicIncome)
{
    $sss = SSS::whereRaw("'$basicIncome' BETWEEN min AND max")->first();
    
    return $sss->ee;
}

function getPhilHealthPremium($basicIncome)
{
    $ph = PhilHealth::whereRaw("'$basicIncome' BETWEEN min AND max")->first();

    return $ph->ee;
}

function calculateWithHoldingTax($schedule, $taxableIncome, $dependentsCount)
{    
    $taxTable = WithholdingTax::where('deduction_schedule', $schedule)
        ->where('dependents', $dependentsCount)
        ->whereRaw("'$taxableIncome' BETWEEN min AND max")
        ->first();

    $diff = $taxableIncome - $taxTable->min;
    $excess = $diff * $taxTable->percent;

    return $taxTable->base_tax + $excess;
}

function calculateHolidayPay($minutes, $ratePerHour) {
    return $minutes / 60 * ($ratePerHour * 3);
}

function dayTypes()
{
    return [
        'ND' => 1.1,
        'RDOT' => 1.3,
        'SH' => 1.3,
        'SHRD' => 1.5,
        'RH' => 2,
        'RHRD' => 2.6,
        'DH' => 3,
        'DHRD' => 3.9
    ];
}

function toPdf($html, $filename, $output=true)
{
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML($html);

    if ($output) {
        $pdf->save(public_path().'/temp/'.$filename.".pdf");
    } else {
        return $pdf->stream($filename.".pdf");
    }
}

function getTones()
{
    $dir = 'assets/tones';
    $files = scandir($dir);
    unset($files[0]);
    unset($files[1]);

    $tones = [];
    foreach ($files as $file) {
        $fileName = substr($file, 0, (strlen($file) - 4));
        $tones[strtolower($fileName)] = ucfirst($fileName);
    }

    return $tones;
}

function getEmploymentStatus($excluding = null)
{
    $statuses = [
        1 => 'Probationary',
        2 => 'Contractual',
        3 => 'Project-based',
        4 => 'Regular',
        5 => 'Resigned',
        6 => 'Terminated'
    ];
    if ($excluding != null) {
        foreach ($excluding as $id) {
            unset($statuses[$id]);
        }
    }
    return $statuses;
}

function getER($amount, $premiumType)
{
    if ($premiumType === 'sss') {
        $sss = SSS::where('ee', $amount)->first();
        if ($sss) {
            return $sss->er;
        }
    } elseif ($premiumType === 'philhealth') {
        $philhealth = PhilHealth::where('ee', $amount)->first();
        if ($philhealth) {
            return $philhealth->er;
        }
    }

    return 0;
}