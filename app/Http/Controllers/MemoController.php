<?php

namespace UnoRenta\Http\Controllers;

use DB;
use Gate;
use Auth;
use Input;
use Carbon\Carbon;
use UnoRenta\Models\Memo;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Employee;
use UnoRenta\Models\MemoComment;
use UnoRenta\Models\MemoRecipient;
use UnoRenta\Models\MemoSignatory;
use UnoRenta\Http\Requests\BroadcastMemoRequests;
use UnoRenta\Http\Requests\AddMemoCommentRequests;

class MemoController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function viewMemos()
    {
        if (Gate::denies('view_memos', Auth::user())) {
            abort(403);
        }

        $memos = Memo::query()
            ->orderBy('created_at', 'desc')
            ->get();

        return view('pages.memos.list', compact(
            'memos'
        ));
    }

    public function broadcastMemo()
    {
        if (Gate::denies('broadcast_memo', Auth::user())) {
            abort(403);
        }
        
        $employees = Employee::all();

        return view('pages.memos.broadcast',
            compact('employees')
        );
    }

    public function broadcastMemoSave(BroadcastMemoRequests $request)
    {
        if (Gate::denies('broadcast_memo', Auth::user())) {
            abort(403);
        }

        $memo = Memo::broadcastMemo($request);

        return redirect()->route('memos.details', ['memoId' => $memo->id])
            ->withStatus($memo->title . ' successfully broadcasted.');

    }

    public function viewMemoDetails($memoId)
    {
        $memo = Memo::findOrFail($memoId);

        /**
        * regular denial
        */
        if (Gate::denies('view_memos', Auth::user())) {
            abort(403);
        }

        /**
        * if memo is not yet signed, 
        * allow only signatory/ies or users that can broadcast a memo to view the memo
        */
        if (!$memo->isSigned() AND !$memo->isSignatory() AND Gate::denies('broadcast_memo', Auth::user())) {
            abort(403);
        }

        /**
        * if signed but user is not a recipient
        */
        if ($memo->isSigned() and !$memo->isRecipient()) {
            abort(403);
        }

        /**
        * seen!!
        */
        if ($memo->isSigned() AND ($memo->isRecipient() OR $memo->isSignatory())) {
            $recipient = $memo->recipients()->where('employee_id', Auth::user()->employee->id)->first();
            $recipient->seen();
        }

        $employees = Employee::query()
            ->whereNotIn('id', $memo->getRecipients())
            ->whereNotIn('id', $memo->getSignatories())
            ->get();

        $recipients = $memo->recipients()
            ->orderBy('date_conform', 'desc')
            ->orderBy('date_last_seen', 'desc')
            ->get();

        if (Input::has('sneak')) {
            if (Input::get('sneak')) {
                $response = [
                    'view' => view('pages.memos.sneak-details', compact(
                        'memo', 'employees', 'recipients'
                    ))->render()
                ];
                return json_encode($response);
            }            
        }

        return view('pages.memos.details', compact(
            'memo', 'employees', 'recipients'
        ));
    }

    public function conform()
    {
        $memo = Memo::findOrFail($this->request->memo_id);

        if (!$memo->isSignatory() AND !$memo->isRecipient()) {
            abort(403);
        }

        $memoRecipient = MemoRecipient::conform($this->request);        

        $response = [
            'dp' => Auth::user()->dp(),
            'profileUrl' => route('employees.profile', ['employeeId' => Auth::user()->employee->id])
        ];

        return json_encode($response);
    }

    public function sign($memoId)
    {
        $memo = Memo::findOrFail($memoId);

        if (!in_array(Auth::user()->employee->id, $memo->getSignatories())) {
            abort(403);
        }

        $ms = MemoSignatory::query()
            ->where('memo_id', $memo->id)
            ->where('employee_id', Auth::user()->employee->id)
            ->first();

        if ($ms->date_signed == null) {
            $ms->update(['date_signed' => Carbon::now()]);
        }

        $response = [
            'signature' => Auth::user()->employee->signature(),
            'id' => Auth::user()->employee->id
        ];

        return json_encode($response);
    }

    public function addComment(AddMemoCommentRequests $request)
    {
        $memo = Memo::findOrFail($request->memo_id);

        if (!$memo->isSignatory() AND !$memo->isRecipient()) {
            if (Gate::denies('broadcast_memo', Auth::user())) {
                abort(403);
            }            
        }

        $comment = MemoComment::addComment($request);

        $response = [
            'view' => view('includes.newcomment', compact('comment'))->render()
        ];

        return json_encode($response);
    }

    public function editMemo($memoId)
    {
        if (Gate::denies('edit_memo', Auth::user())) {
            abort(403);
        }

        $memo = Memo::findOrFail($memoId);
        $employees = Employee::all();

        return view('pages.memos.edit', 
            compact('memo', 'employees')
        );
    }

    public function editMemoSave($memoId, BroadcastMemoRequests $request)
    {
        if (Gate::denies('edit_memo', Auth::user())) {
            abort(403);
        }

        $memo = Memo::findOrFail($memoId);
        $memo->updateMemo($request);        

        return redirect()->route('memos.details', ['memoId' => $memo->id])
            ->withStatus($memo->title . ' successfully updated.');
    }

    public function addRecipient($memoId)
    {
        $memo = Memo::findOrFail($memoId);

        $r = [];
        $count = 0;
        $view = '';
        foreach ($this->request->recipients as $recipient) {
            $mr = MemoRecipient::create([
                'memo_id' => $memo->id,
                'employee_id' => $recipient
            ]);
            $r[$count] = $recipient;
            $count++;

            $view .= '<tr id="r-' . $mr->id . '"><td class="table-dp-td">' .
            '<div class="dp-handler" style="background:url(' . $mr->employee->user->dp() . ') center center no-repeat;background-size:cover;">' .
            '<a href="' . route('employees.profile', ['employeeId' => $mr->employee->id]) . '"></a>' .
            '</div></td><td class="table-highlight">' .
            '<a href="' . route('employees.profile', ['employeeId' => $mr->employee->id]) . '">' . $mr->employee->name() . '</a>' .
            '</td><td>-</td><td>-</td><td class="ta-right"><a href="' . route('memos.removerecipient') . '/' . $mr->id . '" class="remove-recipient"><i class="fa fa-close fa-fw"></i></a></td></tr>';
        }

        $response = [
            'r' => $r,
            'view' => $view
        ];

        return json_encode($response);
    }

    public function removeRecipient($recipientId)
    {
        $recipient = MemoRecipient::findOrFail($recipientId);
        $id = $recipient->id;
        $recipient->delete();

        $option = '<option value="' . $recipient->employee->id . '">' . $recipient->employee->name() . '</option>';

        $response = [
            'done' => true,
            'id' => $id,
            'option' => $option
        ];

        return json_encode($response);
    }

    public function deleteMemo($memoId)
    {
        $memo = Memo::findOrFail($memoId);
        
        if ($memo->isSigned()) {
            abort(404);   
        }

        $memo->recipients()->delete();
        $memo->signatories()->delete();
        $memo->comments()->delete();
        $memo->delete();

        return redirect()->route('memos')
            ->withStatus('Memo successfully deleted!');
    }

}
