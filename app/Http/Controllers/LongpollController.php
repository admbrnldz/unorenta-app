<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Input;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\MemoComment;
use UnoRenta\Models\LeaveComment;
use UnoRenta\Models\MemoRecipient;
use UnoRenta\Models\HolidayComment;

class LongpollController extends Controller
{

    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }
    
    public function memoComments($memoId)
    {

        $latestComment = MemoComment::query()
            ->where('memo_id', $memoId)
            ->orderBy('id', 'desc')
            ->first();

        $latestNotificationTime = 0;
        if ($latestComment) {
            $latestNotificationTime = strtotime($latestComment->created_at);
        }

        if (Input::has('ts')) {
            $timeStamp = (int) Input::get('ts');
            if ($timeStamp == 0) {
                $timeStamp = $latestNotificationTime;
            }
        }

        while ($latestNotificationTime <= $timeStamp) {
            usleep(100000);
            clearstatcache();
            $latestComment = MemoComment::query()
                ->where('memo_id', $memoId)
                ->orderBy('id', 'desc')
                ->first();
            if ($latestComment) {
                $latestNotificationTime = strtotime($latestComment->created_at);
            }            
        }

        $newCommentCount = MemoComment::query()
            ->where('memo_id', $memoId)
            ->count();

        $comment = $latestComment;
        $response = [
            'url' => $this->request->url(),
            'timestamp' => $latestNotificationTime,
            'userId' => $latestComment->author_id,
            'newCommentCount' => $newCommentCount,
            'view' => view('includes.newcomment', compact('comment'))->render()
        ];

        return json_encode($response);

    }

    public function memoSeenCount($memoId)
    {
        $seenCount = MemoRecipient::query()
            ->where('memo_id', $memoId)
            ->where('seen', 1)
            ->count();

        $latestCount = $seenCount;

        if (Input::has('sc')) {
            $lastCount = (int) Input::get('sc');
            if ($lastCount == 0) {
                $lastCount = $latestCount;
            }
        }

        while ($latestCount <= $seenCount) {
            usleep(100000);
            clearstatcache();
            $latestCount = MemoRecipient::query()
                ->where('memo_id', $memoId)
                ->where('seen', 1)
                ->count();
            $lastCount = $latestCount;
        }

        $recentlyWatching = MemoRecipient::query()
            ->where('memo_id', $memoId)
            ->where('seen', 1)
            ->orderBy('date_last_seen', 'desc')
            ->first();

        $response = [
            'url' => $this->request->url(),
            'newCount' => $lastCount,
            'lastSeenDate' => processDate($recentlyWatching->date_last_seen, true),
            'employeeId' => $recentlyWatching->employee_id
        ];

        return json_encode($response);
    }

    public function memoConformCount($memoId)
    {
        $conformCount = MemoRecipient::query()
            ->where('memo_id', $memoId)
            ->where('conform', 1)
            ->count();

        $latestCount = $conformCount;

        if (Input::has('cc')) {
            $lastCount = (int) Input::get('cc');
            if ($lastCount == 0) {
                $lastCount = $latestCount;
            }
        }

        while ($latestCount <= $conformCount) {
            usleep(100000);
            clearstatcache();
            $latestCount = MemoRecipient::query()
                ->where('memo_id', $memoId)
                ->where('conform', 1)
                ->count();
            $lastCount = $latestCount;
        }

        $recentlyConformed = MemoRecipient::query()
            ->where('memo_id', $memoId)
            ->where('conform', 1)
            ->orderBy('date_conform', 'desc')
            ->first();

        $response = [
            'url' => $this->request->url(),
            'newCount' => $lastCount,
            'lastConformDate' => processDate($recentlyConformed->date_conform, true),
            'employeeId' => $recentlyConformed->employee_id
        ];

        return json_encode($response);
    }

    public function leaveComments($loaId)
    {

        $latestComment = LeaveComment::query()
            ->where('leave_application_id', $loaId)
            ->orderBy('id', 'desc')
            ->first();

        $latestNotificationTime = 0;
        if ($latestComment) {
            $latestNotificationTime = strtotime($latestComment->created_at);
        }

        if (Input::has('ts')) {
            $timeStamp = (int) Input::get('ts');
            if ($timeStamp == 0) {
                $timeStamp = $latestNotificationTime;
            }
        }

        while ($latestNotificationTime <= $timeStamp) {
            usleep(100000);
            clearstatcache();
            $latestComment = LeaveComment::query()
                ->where('leave_application_id', $loaId)
                ->orderBy('id', 'desc')
                ->first();
            if ($latestComment) {
                $latestNotificationTime = strtotime($latestComment->created_at);
            }            
        }

        $comment = $latestComment;
        $response = [
            'url' => $this->request->url(),
            'timestamp' => $latestNotificationTime,
            'userId' => $latestComment->author_id,
            'view' => view('includes.newcomment', compact('comment'))->render()
        ];

        return json_encode($response);

    }

    public function holidayComments($holidayId)
    {

        $latestComment = HolidayComment::query()
            ->where('holiday_id', $holidayId)
            ->orderBy('id', 'desc')
            ->first();

        $latestNotificationTime = 0;
        if ($latestComment) {
            $latestNotificationTime = strtotime($latestComment->created_at);
        }

        if (Input::has('ts')) {
            $timeStamp = (int) Input::get('ts');
            if ($timeStamp == 0) {
                $timeStamp = $latestNotificationTime;
            }
        }

        while ($latestNotificationTime <= $timeStamp) {
            usleep(100000);
            clearstatcache();
            $latestComment = HolidayComment::query()
                ->where('holiday_id', $holidayId)
                ->orderBy('id', 'desc')
                ->first();
            if ($latestComment) {
                $latestNotificationTime = strtotime($latestComment->created_at);
            }            
        }

        $comment = $latestComment;
        $response = [
            'url' => $this->request->url(),
            'timestamp' => $latestNotificationTime,
            'userId' => $latestComment->author_id,
            'view' => view('includes.newcomment', compact('comment'))->render()
        ];

        return json_encode($response);

    }

}
