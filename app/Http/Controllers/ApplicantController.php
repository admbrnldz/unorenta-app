<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Gate;
use Input;
use Carbon\Carbon;
use UnoRenta\Models\User;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Position;
use UnoRenta\Models\Applicant;
use UnoRenta\Models\Interview;
use UnoRenta\Models\Assessment;
use UnoRenta\Models\PostAssessment;
use UnoRenta\Models\EmployeeStatus;
use UnoRenta\Models\UserPermission;
use UnoRenta\Models\AssessmentGuide;
use UnoRenta\Models\ApplicantDocument;
use UnoRenta\Models\AssessmentCategory;
use UnoRenta\Models\PostAssessmentGuide;
use UnoRenta\Http\Requests\ApplicantRequest;
use UnoRenta\Http\Requests\HireApplicantRequests;

class ApplicantController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /*
     * Get List
     */
    public function getApplicants()
    {
        if (Gate::denies('view_applicants', Auth::user())) {
            abort(403);
        }

        $activeMenu1 = 'active';
        $activeMenu2 = '';
        $activeMenu3 = '';
        $applicants = Applicant::query()
            ->orderBy('last_name')
            ->where('is_flagged', 0)
            ->where('is_hired', 0)
            ->orderBy('last_name')
            ->get();

        if (Input::has('a')) {
            $a = Input::get('a');
            if ($a == 'flagged') {
                $applicants = Applicant::query()
                    ->orderBy('last_name')
                    ->where('is_flagged', 1)
                    ->where('is_hired', 0)
                    ->orderBy('last_name')
                    ->get();
                $activeMenu1 = '';
                $activeMenu2 = 'active';
            } elseif ($a == 'hired') {
                $applicants = Applicant::query()
                    ->orderBy('last_name')
                    ->where('is_flagged', 0)
                    ->where('is_hired', 1)
                    ->orderBy('last_name')
                    ->get();
                $activeMenu1 = '';
                $activeMenu3 = 'active';
            }
        }

        $flaggedCount = Applicant::query()
            ->where('is_flagged', 1)
            ->where('is_hired', 0)
            ->count();
        $hiredCount = Applicant::query()
            ->where('is_flagged', 0)
            ->where('is_hired', 1)
            ->count();

        return view('pages.applicants.list', compact(
            'applicants', 'flaggedCount', 'hiredCount', 'activeMenu1', 'activeMenu2', 'activeMenu3'
        ));
    }

    public function applicantDetails($applicantId)
    {
        if (Gate::denies('view_applicants', Auth::user())) {
            abort(403);
        }

        $applicant =  Applicant::findOrFail($applicantId);
        $employee = null;
        if ($applicant->is_hired) {
            $user = User::query()
                ->where('first_name', $applicant->first_name)
                ->where('last_name', $applicant->last_name)
                ->first();
            $employee = $user->employee;
        }

        return view('pages.applicants.details', compact(
            'applicant', 'employee'
        ));
    }

    public function applicantQuickView($applicantId)
    {
        if (Gate::denies('view_applicants', Auth::user())) {
            abort(403);
        }

        $applicant =  Applicant::findOrFail($applicantId);

        $response = [
            'view' => view('pages.applicants.sneak-details', compact('applicant'))->render()
        ];

        return json_encode($response);
    }

    /*
     * Add Applicant
     */
    public function addApplicant()
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $response = [
            'title' => 'Add Applicant',
            'view' => view('pages.applicants.add')->render()
        ];

        return json_encode($response);
    }

    public function addApplicantSave(ApplicantRequest $request)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::saveApplicant($request);

        return redirect()->route('applicants')
            ->withStatus($applicant->name() . ' successfully added.');
    }

    /*
     * Edit Applicant
     */
    public function editApplicant($applicantId)
    {
        if (Gate::denies('edit_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);

        $interviewed = false;
        if ($applicant->interview) {
            $interviewed = true;
        }

        $response = [
            'title' => 'Edit Applicant',
            'view' => view('pages.applicants.edit', compact('applicant'))->render(),
            'interviewed' => $interviewed
        ];

        return json_encode($response);
    }

    public function editApplicantSave($applicantId, ApplicantRequest $request)
    {
        if (Gate::denies('edit_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);
        $applicant->updateApplicant($request);
        $applicant->save();

        if ($applicant->interview) {
            return redirect()->route('applicants.interview.details', [
                    'interviewId' => $applicant->interview->id
                ])->withStatus($applicant->name() . ' successfully added.');
        }

        return redirect()->route('applicants')
            ->withStatus($applicant->name() . ' successfully added.');
    }

    /*
     * Delete Applicant
     */
    public function deleteApplicant($applicantId)
    {
        if (Gate::denies('delete_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);
        $name = $applicant->name();
        $applicant->delete();

        return redirect()->route('applicants')
            ->withStatus($name . ' successfully removed.');   
    }

    /*
     * Assessments
     */
    public function getAssessments()
    {
        if (Gate::denies('add_interview_questions', Auth::user())) {
            abort(403);
        }

        $categories = AssessmentCategory::orderBy('order')->get();
        $postAssessmentGuides = PostAssessmentGuide::orderBy('order')->get();

        return view('pages.assessments.lists', compact(
            'categories', 'postAssessmentGuides'
        ));
    }

    public function updateInterviewQuestions()
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $data = json_decode($this->request->data);

        $catOrder = 1;
        foreach ($data as $category) {
            $cat = AssessmentCategory::find($category->catId);
            $cat->order = $catOrder;
            $cat->save();
            if (isset($category->children)) {

                $order = 1;
                foreach ($category->children as $question) {
                    $question = AssessmentGuide::find($question->id);
                    $question->assessment_category_id = $cat->id;
                    $question->order = $order;
                    $question->save();
                    $order++;
                }

            }
            $catOrder++;
        }

        $response = [
            'data' => $data
        ];
        return json_encode($response);
    }

    /*
     * Add Assessment Category
     */
    public function addAssessmentCategory()
    {
        if (Gate::denies('add_interview_questions', Auth::user())) {
            abort(403);
        }

        $response = [
            'title' => 'Add Assessment Category',
            'view' => view('pages.assessments.category.add')->render()
        ];
        return json_encode($response);
    }

    public function addAssessmentCategorySave()
    {
        if (Gate::denies('add_interview_questions', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'category_name' => 'required'
        ]);

        $lastOrder = AssessmentCategory::max('order');
        $lastOrder++;

        $category = AssessmentCategory::create([
            'name' => $this->request->category_name,
            'order' => $lastOrder
        ]);

        return redirect()->route('assessments')
            ->withStatus($category->name . ' successfully added.');
    }

    /*
     * Edit Assessment Category
     */
    public function editAssessmentCategory($categoryId)
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $category = AssessmentCategory::findOrFail($categoryId);

        $response = [
            'title' => 'Edit Assessment Category',
            'view' => view('pages.assessments.category.edit', compact('category'))->render()
        ];

        return json_encode($response);
    }

    public function editAssessmentCategorySave($categoryId)
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'category_name' => 'required'
        ]);

        $category = AssessmentCategory::findOrFail($categoryId);
        $category->name = $this->request->category_name;
        $category->save();

        return redirect()->route('assessments')
            ->withStatus($category->name . ' successfully updated.');
    }

    /*
     * Delete Assessment Category
     */
    public function deleteAssessmentCategory($categoryId)
    {
        if (Gate::denies('delete_interview_questions', Auth::user())) {
            abort(403);
        }

        AssessmentCategory::findOrFail($categoryId)->delete();

        $questions = AssessmentGuide::where('assessment_category_id', $categoryId)->get();
        foreach ($questions as $question) {
            AssessmentGuideAnswer::where('interview_assessment_id', $question->id)->delete();
        }
        AssessmentGuide::where('assessment_category_id', $categoryId)->delete();

        return redirect()->route('assessments')
            ->withStatus('Category successfully removed.');
    }

    /*
     * Add Assessment Guide
     */
    public function addAssessmentGuideQuestion()
    {
        if (Gate::denies('add_interview_questions', Auth::user())) {
            abort(403);
        }

        $categories = AssessmentCategory::orderBy('name')->get();

        $response = [
            'title' => 'Add Assessment Guide',
            'view' => view('pages.assessments.assessment.add', compact('categories'))->render()
        ];

        return json_encode($response);
    }

    public function addAssessmentGuideQuestionSave()
    {
        if (Gate::denies('add_interview_questions', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'question' => 'required',
            'category_id' => 'required'
        ]);

        $lastOrder = AssessmentGuide::query()
            ->where('assessment_category_id', $this->request->category_id)
            ->max('order');
        $lastOrder++;

        $assessmentQuestion = AssessmentGuide::create([
            'question' => $this->request->question,
            'assessment_category_id' => $this->request->category_id,
            'order' => $lastOrder
        ]);

        return redirect()->route('assessments')
            ->withStatus('Assessment guide question successfully added.');
    }

    /*
     * Edit Assessment Guide
     */
    public function editAssessmentGuideQuestion($assessmentId)
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $question = AssessmentGuide::findOrFail($assessmentId);        
        $categories = AssessmentCategory::orderBy('name')->get();

        $response = [
            'title' => 'Edit Assessment Guide',
            'view' => view('pages.assessments.assessment.edit', compact('categories', 'question'))->render()
        ];

        return json_encode($response);
    }

    public function editAssessmentGuideQuestionSave($assessmentId)
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'question' => 'required',
            'category_id' => 'required'
        ]);

        $assessmentQuestion = AssessmentGuide::findOrFail($assessmentId);
        $assessmentQuestion->question = $this->request->question;
        $assessmentQuestion->assessment_category_id = $this->request->category_id;
        $assessmentQuestion->save();

        return redirect()->route('assessments')
            ->withStatus('Assessment question successfully added.');
    }

    /*
     * Delete Assessment Guide
     */
    public function deleteAssessmentGuideQuestion($assessmentId)
    {
        if (Gate::denies('delete_interview_questions', Auth::user())) {
            abort(403);
        }

        $question = AssessmentGuide::findOrFail($assessmentId);
        Assessment::where('assessment_guide_id', $question->id)->delete();
        $question->delete();

        return redirect()->route('assessments')
            ->withStatus('Assessment guide successfully removed.');
    }

    /*
     * Update Post Assessment Guide Order
     */
    public function updatePostAssessmentQuestionsOrder()
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $data = json_decode($this->request->data);

        $assessmentOrder = 1;
        foreach ($data as $pa) {
            $postAssessment = PostAssessmentGuide::find($pa->pagId);
            $postAssessment->order = $assessmentOrder;
            $postAssessment->save();
            $assessmentOrder++;
        }

        $response = [
            'data' => $data
        ];
        return json_encode($response);
    }

    /*
     * Add Assessment Category
     */
    public function addPostAssessmentGuideQuestion()
    {
        if (Gate::denies('add_interview_questions', Auth::user())) {
            abort(403);
        }

        $response = [
            'title' => 'Add Post-Assessment Guide',
            'view' => view('pages.assessments.post-assessment.add')->render()
        ];

        return json_encode($response);
    }

    public function addPostAssessmentGuideQuestionSave()
    {
        if (Gate::denies('add_interview_questions', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'question' => 'required'
        ]);

        $lastOrder = PostAssessmentGuide::max('order');
        $lastOrder++;

        $postAssessmentGuide = PostAssessmentGuide::create([
            'question' => $this->request->question,
            'order' => $lastOrder
        ]);

        return redirect()->route('assessments')
            ->withStatus('Post assessmeng guide successfully added.');
    }

    /*
     * Edit Assessment Category
     */
    public function editPostAssessmentGuideQuestion($assessmentId)
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $postAssessmentGuide = PostAssessmentGuide::findOrFail($assessmentId);

        $response = [
            'title' => 'Post Post-Assessment Guide',
            'view' => view('pages.assessments.post-assessment.edit', compact('postAssessmentGuide'))->render()
        ];

        return json_encode($response);
    }

    public function editPostAssessmentGuideQuestionSave($assessmentId)
    {
        if (Gate::denies('edit_interview_questions', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'question' => 'required'
        ]);

        $postAssessmentGuide = PostAssessmentGuide::findOrFail($assessmentId);
        $postAssessmentGuide->question = $this->request->question;
        $postAssessmentGuide->save();

        return redirect()->route('assessments')
            ->withStatus('Post assessment guide successfully updated.');
    }

    /**
    * Delete Assessment Guide
    */
    public function deletePostAssessmentGuideQuestion($assessmentId)
    {
        if (Gate::denies('delete_interview_questions', Auth::user())) {
            abort(403);
        }

        $question = PostAssessmentGuide::findOrFail($assessmentId);
        PostAssessment::where('post_assessment_guide_id', $question->id)->delete();
        $question->delete();

        return redirect()->route('assessments')
            ->withStatus('Post assessment guide successfully removed.');
    }

    /**
    * Interview Applicant
    */
    public function interviewApplicant($applicantsId)
    {
        if (Gate::denies('interview_applicants', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantsId);
        $categories = AssessmentCategory::orderBy('order')->get();
        $postAssessmentGuides = PostAssessmentGuide::orderBy('order')->get();
        $startTime = Carbon::now();

        return view('pages.applicants.interview.interview-page', compact(
            'applicant', 'categories', 'startTime', 'postAssessmentGuides'
        ));
    }

    public function interviewApplicantSave($applicantId)
    {
        if (Gate::denies('interview_applicants', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);

        $interview = Interview::create([
            'applicant_id' => $applicant->id,
            'interviewer' => Auth::user()->employee->id,
            'date_interview' => $this->request->date_interview
        ]);

        foreach ($this->request->assessment as $assessmentGuideId => $assessment) {
            Assessment::create([
                'interview_id' => $interview->id,
                'assessment_guide_id' => $assessmentGuideId,
                'rating' => $assessment['rating'],
                'comments' => $assessment['comments']
            ]);
        }

        foreach ($this->request->postassessment as $postAssessmengGuideId => $assessment) {
            PostAssessment::create([
                'interview_id' => $interview->id,
                'post_assessment_guide_id' => $postAssessmengGuideId,
                'comments' => $assessment['comments']
            ]);
        }

        return redirect()->route('applicants.interview.details', ['interviewId' => $interview->id])
            ->withStatus('Interview with ' . $applicant->name() . ' successfully done!');
    }

    public function interviewDetails($interviewId)
    {
        if (Gate::denies('interview_applicants', Auth::user())) {
            abort(403);
        }

        $interview = Interview::findOrFail($interviewId);

        $employee = null;
        if ($interview->applicant->is_hired) {
            $user = User::query()
                ->where('first_name', $interview->applicant->first_name)
                ->where('last_name', $interview->applicant->last_name)
                ->first();
            $employee = $user->employee;
        }

        return view('pages.applicants.interview.interview-details', compact(
            'interview', 'employee'
        ));
    }

    public function hireApplicant($applicantId)
    {
        if (Gate::denies('interview_applicants', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);
        $positions = Position::all();

        if (!$applicant->isInterviewed() OR $applicant->is_flagged) {
            abort(404);
        }

        // generate username
        $username = strtolower(str_replace(' ', '', $applicant->first_name));
        $userSearch = User::where('username', $username)->get();
        if (count($userSearch) > 0) {
            $username .= $applicant->id;
        }

        return view('pages.applicants.hire', compact(
            'applicant', 'positions', 'username'
        ));
    }

    public function hireApplicantSave($applicantId, HireApplicantRequests $request)
    {
        if (Gate::denies('interview_applicants', Auth::user())) {
            abort(403);
        }
        
        // update applicant
        $applicant = Applicant::findOrFail($applicantId);
        $applicant->hired();

        // parse birthday
        // check if 18yo and above
        $birthdate = Carbon::parse($request->birthdate);
        if ($birthdate->age < 18) {
            return redirect($request->url())
                ->withErrors('Age must be 18 and above.')
                ->withInput();
        }

        // create employee
        $citizenship = $request->citizenship == 'Filipino' ? $request->citizenship : $request->citizenship_other;
        $nickname = $request->nickname == '' ? $request->first_name : $request->nickname;
        $today = Carbon::today();
        $employee = Employee::create([
            'position_id' => $request->position_id,
            'basic_income' => $request->basic_income,
            'monthly_work_days' => $request->monthly_work_days,
            'nickname' => ucwords($nickname),
            'birthdate' => $birthdate,
            'birthplace' => $request->birthplace,
            'sex' => $request->sex,
            'address' => $request->address,
            'city' => $request->city,
            'province' => $request->province,
            'zip_code' => $request->zip_code,
            'mobile_number' => $request->mobile_number,
            'home_number' => $request->home_number,
            'email_address' => $request->email_address,
            'civil_status' => $request->civil_status,
            'citizenship' => $citizenship,
            'religion' => $request->religion,
            'height' => $request->height,
            'weight' => $request->weight,
            'build' => $request->build,
            'eyes' => $request->eyes,
            'blood_type' => $request->blood_type,
            'sss' => $request->sss == '' ? null : $request->sss,
            'tin' => $request->tin == '' ? null : $request->tin,
            'philhealth' => $request->philhealth == '' ? null : $request->philhealth,
            'pagibig' => $request->pagibig == '' ? null : $request->pagibig,
            'date_hired' => $today
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => $request->status,
            'updated_by' => Auth::user()->employee->id
        ]);

        // create user
        $pw = $request->password == '' ? 12345 : $request->password;
        $user = User::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'username' => $request->username,
            'password' => bcrypt($pw)
        ]);

        // user permissions
        $permissions = UserPermission::where('user_id', $user->id)->get();
        if (count($permissions) > 0) { $permissions->delete(); }
        foreach (getDefaultPermissions() as $permissionId):
            UserPermission::create([
                'permission_id' => $permissionId,
                'user_id' => $user->id
            ]);
        endforeach;

        $employee->user_id = $user->id;
        $employee->employee_uid = $today->year . '-' . sprintf('%04d', $employee->id);
        $employee->save();

        return redirect()->route('employees.profile', ['employeeId' => $employee->id])
            ->withStatus($employee->name() . ' is now hired!');
    }

    public function applicantDocuments($applicantId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);
        $employee = null;
        if ($applicant->is_hired) {
            $user = User::query()
                ->where('first_name', $applicant->first_name)
                ->where('last_name', $applicant->last_name)
                ->first();
            $employee = $user->employee;
        }

        return view('pages.applicants.documents', compact(
            'applicant', 'employee'
        ));
    }

    public function uploadDocument($applicantId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);

        $response = [
            'title' => 'Upload Document<span>' . $applicant->name() . '</span>',
            'view' => view('pages.applicants.documents-upload', compact('applicant'))->render()
        ];

        return json_encode($response);
    }

    public function uploadDocumentSave($applicantId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);

        if ($this->request->hasFile('document')) {

            $destinationPath = 'uploads/documents/' . $applicant->id;
            $extension = $this->request->file('document')->getClientOriginalExtension();
            $fileName = $applicant->id . '_' . time() . '.' . $extension;
            $this->request->file('document')->move($destinationPath, $fileName);

            ApplicantDocument::create([
                'applicant_id' => $applicant->id,
                'filename' => $fileName,
                'description' => $this->request->description
            ]);

        }

        return redirect()->route('applicants.documents', ['applicantId' => $applicant->id])
            ->withStatus('Successfully uploaded document/file for ' . $applicant->name());
    }

    public function removeDocument($documentId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $document = ApplicantDocument::findOrFail($documentId);
        $applicant = $document->applicant;
        $document->delete();

        return redirect()->route('applicants.documents', ['applicantId' => $applicant->id])
            ->withStatus('Successfully removed document/file from ' . $applicant->name());
    }

    public function editDocument($documentId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $document = ApplicantDocument::findOrFail($documentId);

        $response = [
            'title' => 'Edit Document<span>' . $document->applicant->name() . '</span>',
            'view' => view('pages.applicants.documents-edit', compact('document'))->render()
        ];

        return json_encode($response);
    }

    public function editDocumentSave($documentId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $document = ApplicantDocument::findOrFail($documentId);
        $document->description = $this->request->description;
        $document->save();

        return redirect()->route('applicants.documents', ['applicantId' => $document->applicant->id])
            ->withStatus('Successfully updated document/file of ' . $document->applicant->name());
    }

    public function flagApplicant($applicantId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);

        $response = [
            'title' => 'Flag ' . $applicant->first_name,
            'view' => view('pages.applicants.flag', compact('applicant'))->render()
        ];

        return json_encode($response);
    }

    public function flagApplicantSave($applicantId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }

        $applicant = Applicant::findOrFail($applicantId);
        $applicant->is_flagged = 1;
        $applicant->flag_start = Carbon::parse($this->request->flag_start);
        $applicant->duration = $this->request->duration;
        $applicant->save();

        return redirect()->route('applicants.details', ['applicantId' => $applicant->id])
            ->withStatus($applicant->first_name . ' was flagged!');
    }

    public function unflagApplicant($applicantId)
    {
        if (Gate::denies('add_applicant', Auth::user())) {
            abort(403);
        }
        
        $applicant = Applicant::findOrFail($applicantId);

        $applicant->is_flagged = 0;
        $applicant->flag_start = null;
        $applicant->duration = null;
        $applicant->save();

        return redirect()->route('applicants.details', ['applicantId' => $applicant->id])
            ->withStatus($applicant->first_name . ' successfully unflagged!');
    }

}
