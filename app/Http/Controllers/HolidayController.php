<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Gate;
use Input;
use Session;
use Carbon\Carbon;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Holiday;
use UnoRenta\Models\HolidayComment;
use UnoRenta\Models\HolidayCategory;
use UnoRenta\Http\Requests\SaveHolidayRequest;

class HolidayController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getHolidays()
    {
        $showViewButton = true;
        $activeCat = 0;

        $holidays = Holiday::query()
            ->where('date', '>=', Carbon::today())
            ->orderBy('date')
            ->get();

        if (Input::has('cat')) {
            $activeCat = Input::get('cat');
            $holidays = Holiday::query()
                ->where('date', '>=', Carbon::today())
                ->where('holiday_category_id', $activeCat)
                ->orderBy('date')
                ->get();

            $showViewButton = false;
        }

        $categories = HolidayCategory::all();

        return view('pages.holidays.list', compact(
            'holidays', 'categories', 'showViewButton', 'activeCat'
        ));
    }

    public function viewDetails($holidayId)
    {
        if (Gate::denies('view_holidays', Auth::user())) {
            abort(403);
        }

        $holiday = Holiday::findOrFail($holidayId);

        return view('pages.holidays.details',
            compact('holiday')
        );
    }

    public function addHoliday()
    {
        if (Gate::denies('add_holiday', Auth::user())) {
            abort(403);
        }

        $categories = HolidayCategory::all();

        $response = [
            'title' => 'Add Holiday',
            'view' => view('pages.holidays.add', compact('categories'))->render()
        ];

        return json_encode($response);
    }

    public function addHolidaySave(SaveHolidayRequest $request)
    {
        if (Gate::denies('add_holiday', Auth::user())) {
            abort(403);
        }

        $holiday = Holiday::saveHoliday($request);
        $holiday->save();

        Session::flash(
            'status',
            $holiday->name . ' successfully added.'
        );

        return json_encode(['redirect' => route('holidays.dashboard')]);
    }

    public function deleteHoliday($holidayId)
    {
        if (Gate::denies('delete_holiday', Auth::user())) {
            abort(403);
        }

        $holiday = Holiday::findOrFail($holidayId);
        $holiday->delete();

        return redirect()->route('holidays.dashboard')
            ->withStatus('Holiday successfully deleted.');
    }

    public function editHoliday($holidayId)
    {
        if (Gate::denies('edit_holiday', Auth::user())) {
            abort(403);
        }

        $holiday = Holiday::findOrFail($holidayId);

        $response = [
            'title' => 'Edit Holiday',
            'view' => view('pages.holidays.edit', compact('holiday'))->render()
        ];

        return json_encode($response);
    }

    public function editHolidaySave($holidayId, SaveHolidayRequest $request)
    {
        if (Gate::denies('edit_holiday', Auth::user())) {
            abort(403);
        }

        $holiday = Holiday::findOrFail($holidayId);
        $holiday->updateHoliday($request);

        return redirect()->route('holidays.details', ['holidayId' => $holiday->id])
            ->withStatus($holiday->name . ' successfully updated.');
    }

    public function addComment()
    {
        if (Gate::denies('view_holidays', Auth::user())) {
            abort(403);
        }

        $comment = HolidayComment::create([
            'holiday_id' => $this->request->holiday_id,
            'author_id' => Auth::user()->employee->id,
            'comment' => formatCommentText($this->request->comment)
        ]);

        $response = [
            'view' => view('includes.newcomment', compact('comment'))->render()
        ];

        return json_encode($response);
    }

    public function pastHolidays()
    {
        if (Gate::denies('view_holidays', Auth::user())) {
            abort(403);
        }
        
        $showViewButton = true;
        $activeCat = 0;

        $holidays = Holiday::query()
            ->where('date', '<', Carbon::today())
            ->orderBy('date', 'desc')
            ->get();

        if (Input::has('cat')) {
            $activeCat = Input::get('cat');

            $holidays = Holiday::query()
                ->where('date', '<', Carbon::today())
                ->where('holiday_category_id', $activeCat)
                ->orderBy('date', 'desc')
                ->get();

            $showViewButton = false;
        }

        $categories = HolidayCategory::all();

        return view('pages.holidays.past-holidays', compact(
            'holidays', 'categories', 'showViewButton', 'activeCat'
        ));
    }

}
