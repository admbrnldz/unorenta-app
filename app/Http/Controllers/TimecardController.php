<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Gate;
use Session;
use Carbon\Carbon;
use UnoRenta\Models\Shift;
use Illuminate\Http\Request;
use UnoRenta\Http\Requests;
use UnoRenta\Models\Timecard;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Breaktime;

class TimecardController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function timeIn()
    {
        $employeeId = Auth::user()->employee->id;
        $shiftId = $this->request->shift_id;

        $timecard = Timecard::query()
            ->where('employee_id', $employeeId)
            ->where('shift_id', $shiftId)
            ->first();

        if ($timecard == null) {            
            $timecard = Timecard::timeIn($employeeId, $shiftId);
            Session::put('timecard', $timecard);
        }

        $response = [
            'user' => Auth::user()->name(),
            'type' => 'time_in',
            'timecard' => $timecard
        ];

        return json_encode($response);
    }

    public function timeOut()
    {
        $employeeId = Auth::user()->employee->id;
        $shiftId = $this->request->shift_id;

        $timecard = Timecard::findOrFail($this->request->timecard_id);
        $timecard->time_out = Carbon::now();
        $timecard->save();

        $response = [
            'type' => 'time_out',
            'timecard' => Timecard::findOrFail($this->request->timecard_id)
        ];

        return json_encode($response);
    }

    public function takeBreak($paramId)
    {
        $timestamp = Carbon::now()->toDateTimeString();
        $onBreak = false;

        if (Session::has('breaktime')) {
            /**
            * Stop break
            */
            $breaktime = Breaktime::findOrFail($paramId);
            $breaktime->break_end = $timestamp;
            $breaktime->save();

            Session::forget('breaktime');
        } else {
            /**
            * Start break
            */
            $timecard = Timecard::findOrFail($paramId);
            $breaktime = Breaktime::create([
                'timecard_id' => $timecard->id,
                'break_start' => $timestamp
            ]);
            Session::put('breaktime', $breaktime);
            $onBreak = true;
        }

        $view = '<section class="break-notification" data-url="' . route('timecards.break') . '/' . $breaktime->id . '">' .
            '<strong>break ka po. click dineh para resume sa work!</strong></section>';
        
        $response = [
            'timestamp' => $timestamp,
            'onBreak' => $onBreak,
            'view' => $view,
            'totalBreak' => $breaktime->timecard->totalBreak()
        ];

        return json_encode($response);
    }

    public function getForApprovalTimecards()
    {
        if (Gate::denies('approve_attendance', Auth::user())) {
            abort(403);
        }

        $managedEs = Auth::user()->employee->managedEmployees();
        $now = Carbon::now();

        /**
        * Time In
        */
        $forApprovalTimeIn = Timecard::forApprovalTimeIn()->get();

        foreach ($forApprovalTimeIn as $ti) {
            $index = array_search($ti->employee_id, $managedEs);
            if ($index) {
                unset($managedEs[$index]);
            }
        }

        /**
        * Time Out
        */
        $forApprovalTimeOut = Timecard::forApprovalTimeOut()->get();

        foreach ($forApprovalTimeOut as $to) {
            $index = array_search($to->employee_id, $managedEs);
            if ($index) {
                unset($managedEs[$index]);
            }
        }

        /**
        * Get Employees Shifts
        */
        $managedEmployees = Employee::whereIn('id', $managedEs)->get();

        if (Auth::user()->position(['HR Officer'])) {
            $managedEmployees = Employee::all();
        }

        $employeesShifts = [];
        $count = 0;
        foreach ($managedEmployees as $employee) {
            $employeesShifts[$count]['employee'] = $employee;

            $shiftBrief = $employee->shiftBriefs()
                ->where('month', $now->copy()->month)
                ->where('year', $now->copy()->year)
                ->first();

            $employeesShifts[$count]['shift'] = null;
            $employeesShifts[$count]['nextShift'] = false;
            if ($shiftBrief) {
                $shift = $shiftBrief->shifts()
                    ->whereRaw("'$now' BETWEEN shift_from AND shift_to")
                    ->first();
                if (!$shift) {
                    $twentyFourHoursFromNow = $now->copy()->addDay();
                    $nextShift = $shiftBrief->shifts()
                        ->whereRaw("shift_from BETWEEN '$now' AND '$twentyFourHoursFromNow'")
                        ->first();
                    if ($nextShift) {
                        $employeesShifts[$count]['shift'] = $nextShift;
                        $employeesShifts[$count]['nextShift'] = true;
                    }                    
                } else {
                    $employeesShifts[$count]['shift'] = $shift;
                }
            }

            $count++;
        }

        return view('pages.timecards.for-approval', compact(
            'forApprovalTimeIn', 'forApprovalTimeOut', 'employeesShifts'
        ));
    }

    public function absentTimecard($shiftId)
    {
        $shift = Shift::findOrFail($shiftId);

        $response = [
            'title' => 'Warning!',
            'view' => view('pages.timecards.absent', compact('shift'))->render()
        ];

        return json_encode($response);
    }

    public function absentTimecardSave($shiftId)
    {
        $shift = Shift::findOrFail($shiftId);
        $employeeId = $shift->employee()->id;
        $timecard = Timecard::create([
            'shift_id' => $shift->id,
            'employee_id' => $employeeId,
            'is_absent' => 1,
            'absent_reason' => $this->request->reason
        ]);

        $response = [
            'employeeId' => $employeeId
        ];

        return json_encode($response);
    }    

    public function approveTimeIn($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);

        $shiftFrom = Carbon::parse($timecard->shift->shift_from);
        $timeIn = Carbon::parse($timecard->time_in);

        $diff = 0;
        if ($shiftFrom < $timeIn) {
            $diff = $shiftFrom->diffInMinutes($timeIn);
        }

        $timecard->late_minutes = $diff > 10 ? $diff : 0;
        $timecard->ti_signed_by = Auth::user()->employee->id;
        $timecard->save();

        $response = [
            'timecard' => $timecard,
            'type' => 'timeIn',
            'request' => $this->request->all(),
            'timeDisplay' => processTime($timecard->time_in)
        ];

        return json_encode($response);
    }

    public function approveTimeOut($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);

        $shiftTo = Carbon::parse($timecard->shift->shift_to);
        $timeOut = Carbon::parse($timecard->time_out);

        $diff = 0;
        if ($shiftTo > $timeOut) {
            $diff = $shiftTo->diffInMinutes($timeOut);    
        }

        $timecard->undertime_minutes = $diff;
        $timecard->to_signed_by = Auth::user()->employee->id;
        $timecard->save();

        $response = [
            'timecard' => $timecard,
            'type' => 'timeOut',
            'request' => $this->request->all(),
            'timeDisplay' => processTime($timecard->time_out)
        ];

        return json_encode($response);
    }

    public function modifyTimecard($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);

        $response = [
            'title' => 'Modify Timecard<span>' . $timecard->employee->name() . '</span>',
            'view' => view('pages.timecards.modify', compact('timecard'))->render()
        ];

        return json_encode($response);
    }

    public function modifyTimecardSave($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);
        
        // new time in
        $timeIn = Carbon::parse($timecard->time_in);
        $timeIn->hour = $this->request->time_cycle_ti == 'PM' ? $this->request->ti_hour + 12 : $this->request->ti_hour;
        $timeIn->minute = $this->request->ti_minute;

        // new late
        $shiftFrom = Carbon::parse($timecard->shift->shift_from);
        $late = 0;
        if ($shiftFrom < $timeIn) {
            $late = $shiftFrom->diffInMinutes($timeIn);    
        }

        // new time out
        $timeOut = Carbon::parse($timecard->time_out);
        $timeOut->hour = $this->request->time_cycle_to == 'PM' ? $this->request->to_hour + 12 : $this->request->to_hour;
        $timeOut->minute = $this->request->to_minute;

        // new undertime
        $shiftTo = Carbon::parse($timecard->shift->shift_to);
        $undertime = 0;
        if ($shiftTo > $timeOut) {
            $undertime = $shiftTo->diffInMinutes($timeOut);    
        }

        // update timecard
        $timecard->time_in = $timeIn;
        $timecard->time_out = $timeOut;
        $timecard->late_minutes = $late;
        $timecard->undertime_minutes = $undertime;
        $timecard->save();

        return redirect()->route('timecards.approval')
            ->withStatus('Timecard successfully updated!');
    }

    public function allTimecards()
    {
        if (Gate::denies('approve_attendance', Auth::user())) {
            abort(403);
        }
        
        $managedEs = Auth::user()->employee->managedEmployees();

        $timecards = Timecard::query()
            ->whereIn('employee_id', $managedEs)
            ->take(50)
            ->get();

        return view('pages.timecards.all', compact(
            'timecards'
        ));
    }

    public function modifyTimeIn($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);
        $shiftFrom = Carbon::parse($timecard->shift->shift_from);
        $timeIn = Carbon::parse($timecard->time_in);
        $lateMinutes = 0;
        $timeDiff = $timeIn->diffInMinutes($shiftFrom);
        if ($timeIn > $shiftFrom AND $timeDiff > 10) {
            $lateMinutes = $timeDiff;
        }

        $response = [
            'title' => 'Timecard: Time In',
            'view' => view('pages.timecards.modify-ti', compact(
                'timecard', 'lateMinutes'
            ))->render()
        ];

        return json_encode($response);
    }

    public function modifyTimeInSave($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);

        // new time in
        $timeIn = Carbon::parse($timecard->time_in);
        $timeIn->hour = $this->request->time_cycle_ti == 'PM' ? $this->request->ti_hour + 12 : $this->request->ti_hour;
        $timeIn->minute = $this->request->ti_minute;

        // new late
        $shiftFrom = Carbon::parse($timecard->shift->shift_from);
        $late = 0;
        if ($shiftFrom < $timeIn) {
            $late = $shiftFrom->diffInMinutes($timeIn);    
        }

        $timecard->time_in = $timeIn;
        $timecard->ti_signed_by = Auth::user()->employee->id;
        $timecard->late_minutes = $late;
        $timecard->save();

        return redirect()->route('timecards.approval')
            ->withStatus('Timecard approved!');
    }

    public function rejectTimeIn($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);
        $employeeName = $timecard->employee->nickname;
        $timecard->delete();

        return redirect()->route('timecards.approval')
            ->withErrors('Time out of ' . $employeeName . ' was rejected!');
    }

    public function modifyTimeOut($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);
        $shiftTo = Carbon::parse($timecard->shift->shift_to);
        $timeOut = Carbon::parse($timecard->time_out);
        $overtimeMinutes = 0;
        if ($timeOut > $shiftTo) {            
            $overtimeMinutes = $shiftTo->diffInMinutes($timeOut);
        }

        $response = [
            'title' => 'Timecard: Time Out',
            'view' => view('pages.timecards.modify-to', compact(
                'timecard', 'overtimeMinutes'
            ))->render()
        ];

        return json_encode($response);
    }

    public function modifyTimeOutSave($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);

        // new time out
        $timeOut = Carbon::parse($timecard->time_out);
        $timeOut->hour = $this->request->time_cycle_to == 'PM' ? $this->request->to_hour + 12 : $this->request->to_hour;
        $timeOut->minute = $this->request->to_minute;

        // new undertime
        $shiftTo = Carbon::parse($timecard->shift->shift_to);
        $undertime = 0;
        if ($shiftTo > $timeOut) {
            $undertime = $shiftTo->diffInMinutes($timeOut);    
        }

        $timecard->time_out = $timeOut;
        $timecard->to_signed_by = Auth::user()->employee->id;
        $timecard->undertime_minutes = $undertime;
        if ($this->request->approve_overtime != null) {
            $timecard->overtime_minutes = $this->request->overtime_minutes;
        }
        $timecard->save();

        return redirect()->route('timecards.approval')
            ->withStatus('Timecard approved!');
    }

    public function rejectTimeOut($timecardId)
    {
        $timecard = Timecard::findOrFail($timecardId);
        $timecard->time_out = null;
        $timecard->save();

        return redirect()->route('timecards.approval')
            ->withErrors('Time out of ' . $timecard->employee->nickname . ' was rejected!');
    }

}
