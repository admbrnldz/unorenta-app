<?php

namespace UnoRenta\Http\Controllers;

use Gate;
use Auth;
use Input;
use Session;
use Carbon\Carbon;
use UnoRenta\Models\User;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Position;
use UnoRenta\Models\Dependent;
use UnoRenta\Models\LeaveType;
use UnoRenta\Models\UserPermission;
use UnoRenta\Models\EmployeeStatus;
use UnoRenta\Models\EmployeeFamily;
use UnoRenta\Models\LeaveEntitlement;
use UnoRenta\Models\EmployeeEmergency;
use UnoRenta\Models\EmployeeEducation;
use UnoRenta\Models\EmployeeScholarship;
use UnoRenta\Http\Requests\DependentRequests;
use UnoRenta\Http\Requests\EducationRequests;
use UnoRenta\Http\Requests\SaveEmployeeRequests;
use UnoRenta\Http\Requests\PersonalInformationRequests;

class EmployeeController extends Controller
{

    private $request = null;
    
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function viewEmployees()
    {
        if (Gate::denies('view_employees', Auth::user())) {
            abort(403);
        }

        $employees = Employee::query()
            ->orderByLastName()
            ->select('employees.*')
            ->get();

        return view('pages.employees.list',
            compact('employees')
        );
    }

    public function viewEmployeeProfile($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);
        $positions = Position::orderBy('name')->get();

        $dependent = null;
        if (Input::has('did')) {
            $dependent = Dependent::find(Input::get('did'));
        }

        $education = null;
        if (Input::has('eid')) {
            $education = EmployeeEducation::find(Input::get('eid'));
        }

        return view('pages.employees.profile', 
            compact('employee', 'positions', 'dependent', 'education')
        );
    }

    public function addEmployee()
    {
        if (Gate::denies('add_employee', Auth::user())) {
            abort(403);
        }

        $positions = Position::orderBy('name')->get();
        $leaveTypes = LeaveType::all();

        return view('pages.employees.add', compact(
            'positions', 'leaveTypes'
        ));
    }

    public function addEmployeeSave(SaveEmployeeRequests $request)
    {
        if (Gate::denies('add_employee', Auth::user())) {
            abort(403);
        }       

        $birthdate = Carbon::parse($request->birthdate);
        if ($birthdate->age < 18) {
            return redirect($request->url())
                ->withErrors('Age must be 18 and above.')
                ->withInput();
        }
        $employee = Employee::createEmployee($request);
        $user = User::createUser($request);
        $employee->user_id = $user->id;
        $employee->save();

        // user permissions
        $permissions = UserPermission::where('user_id', $user->id)->get();
        if (count($permissions) > 0) { $permissions->delete(); }
        foreach (getDefaultPermissions() as $permissionId):
            UserPermission::create([
                'permission_id' => $permissionId,
                'user_id' => $user->id
            ]);
        endforeach;

        if ($request->employment_status == 4) {
            $leaves = $request->leave;
            foreach ($leaves as $leaveTypeId => $leaveMeta) {
                LeaveEntitlement::create([
                    'employee_id' => $employee->id,
                    'leave_type_id' => $leaveTypeId,
                    'year' => $leaveMeta['year'],
                    'credits' => $leaveMeta['credits']
                ]);
            }
        }

        return redirect()->route('employees')
            ->withStatus($user->name() . ' successfully added');
    }

    /**
     * Update Personal Info
     */
    public function updatePersonalInfo($employeeId)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $employee = Employee::findOrFail($employeeId);
        $positions = Position::all();

        $response = [
            'title' => 'Edit Personal Information<span>' . $employee->name() . '</span>',
            'view' => view('pages.employees.personal-info.edit', compact(
                'employee', 'positions'
            ))->render()
        ];

        return json_encode($response);
    }

    public function updatePersonalInfoSave($employeeId, PersonalInformationRequests $request)
    {
        if (Gate::denies('add_employee', Auth::user())) {
            abort(403);
        }

        $employee = Employee::findOrFail($employeeId);
        $employee->updatePersonalInfo($request);

        Session::flash('status', $employee->name() . ' personal information updated successfully!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $employee->id])
        ];
        return json_encode($response);
    }

    public function addDirectFamily($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $opts = ['father', 'mother', 'spouse'];
        foreach ($employee->families as $family) {
            foreach ($opts as $key => $opt) {
                if ($opt == $family->relationship) {
                    unset($opts[$key]);
                }
            }
        }

        $response = [
            'title' => 'Add Direct Family<span>' . $employee->name() . '</span>',
            'view' => view('pages.employees.direct-family.add', compact(
                'employee', 'opts'
            ))->render()
        ];

        return json_encode($response);
    }

    public function addDirectFamilySave()
    {
        $employee = Employee::findOrFail($this->request->employee_id);

        $this->validate($this->request, [
            'name' => 'required',
            'relationship' => 'required'
        ]);

        foreach ($employee->families as $family) {
            if ($family->relationship == strtolower($this->request->relationship)) {
                Session::flash('error', $employee->name() . ' already has a ' . ucfirst($this->request->relationship));
                $response = [
                    'redirect' => route('employees.profile', ['employeeId' => $employee->id])
                ];
                return json_encode($response);
            }
        }

        $family = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $this->request->name,
            'occupation' => $this->request->occupation,
            'relationship' => $this->request->relationship
        ]);

        Session::flash('status', $family->name . ' added successfully!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $employee->id])
        ];
        
        return json_encode($response);
    }

    /**
     * Update Direct Family
     */
    public function updateDirectFamily($familyId)
    {
        $family = EmployeeFamily::findOrFail($familyId);

        $response = [
            'title' => 'Edit Direct Family<span>' . $family->employee->name() . '</span>',
            'view' => view('pages.employees.direct-family.edit', compact(
                'family'
            ))->render()
        ];

        return json_encode($response);
    }

    public function updateDirectFamilySave($familyId)
    {
        $family = EmployeeFamily::findOrFail($familyId);

        $this->validate($this->request, [
            'name' => 'required'
        ]);

        $family->name = $this->request->name;
        $family->occupation = $this->request->occupation;
        $family->save();

        Session::flash('status', $family->employee->name() . ' direct family updated successfully!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $family->employee->id])
        ];
        return json_encode($response);
    }

    public function deleteDirectFamily($familyId)
    {
        $family = EmployeeFamily::findOrFail($familyId);
        $name = $family->name;
        $employeeId = $family->employee->id;
        $family->delete();

        Session::flash('status', $name . ' direct family successfully removed!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $employeeId])
        ];
        return json_encode($response);
    }

    /**
     * Update Emergency Contact Person
     */
    public function updateEmergency($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $response = [
            'title' => 'Edit Emergency Contact Person<span>' . $employee->name() . '</span>',
            'view' => view('pages.employees.emergency.edit', compact('employee'))->render()
        ];

        return json_encode($response);
    }

    public function updateEmergencySave($employeeId)
    {
        $this->validate($this->request, [
            'contact_person' => 'required',
            'cp_number' => 'required',
            'cp_address' => 'required'
        ]);

        $employee = Employee::findOrFail($employeeId);
        $contactPerson = EmployeeEmergency::firstOrCreate([
            'employee_id' => $employeeId
        ]);
        $contactPerson->name = $this->request->contact_person;
        $contactPerson->contact_number = $this->request->cp_number;
        $contactPerson->address = $this->request->cp_address;
        $contactPerson->language_spoken = $this->request->cp_language_spoken;
        $contactPerson->save();

        Session::flash('status', $employee->name() . ' emergency contact informatin updated successfully!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $employee->id])
        ];
        
        return json_encode($response);
    }

    /**
     * Add Dependent
     */
    public function addDependent($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $dependentsCount = $employee->dependents()->count();
        if ($dependentsCount == 4) {
            $response = [
                'title' => 'Sorry...',
                'view' => '<div class="ta-center">' . $employee->nickname . ' already have 4 dependents.</div>',
                'className' => 'modal-warning'
            ];
        } else {
            $response = [
                'title' => 'Add Dependent<span>' . $employee->name() . '</span>',
                'view' => view('pages.employees.dependents.add', compact('employee'))->render(),
                'className' => ''
            ];
        }

        return json_encode($response);
    }

    public function addDependentSave(DependentRequests $request)
    {
        $employee = Employee::findOrFail($request->employee_id);

        $birthdate = Carbon::parse($request->birthdate);
        if ($birthdate->age > 21) {

            Session::flash('error', 'The dependent is over 21 years old. Sorry.');
            $response = [
                'redirect' => route('employees.profile', ['employeeId' => $employee->id])
            ];
            return json_encode($response);

        }

        if ($employee->dependents()->count() > 4) {

            Session::flash('error', $employee->name() . ' already has a 4 dependents.');
            $response = [
                'redirect' => route('employees.profile', ['employeeId' => $employee->id])
            ];
            return json_encode($response);

        }

        $dependent = Dependent::saveDependent($request);
        $dependent->save();

        Session::flash('status', $dependent->name() . ' successfully added as dependent!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $employee->id])
        ];

        return json_encode($response);
    }

    /**
     * Update Dependent
     */
    public function updateDependent($dependentId)
    {
        $dependent = Dependent::findOrFail($dependentId);

        $response = [
            'title' => 'Update Dependent<span>' . $dependent->employee->name() . '</span>',
            'view' => view('pages.employees.dependents.edit', compact('dependent'))->render()
        ];

        return json_encode($response);
    }

    public function updateDependentSave($dependentId)
    {
        $dependent = Dependent::findOrFail($dependentId);
        $dependent->updateDependent($this->request);
        $dependent->save();

        return redirect()->route('employees.profile', ['employeeId' => $this->request->employee_id])
            ->withStatus($dependent->name() . ' successfully updated!');
    }

    /**
     * Delete Dependent
     */
    public function deleteDependent($dependentId)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $dependent = Dependent::findOrFail($dependentId);
        $employeeId = $dependent->employee_id;
        $name = $dependent->name();

        $dependent->delete();

        return redirect()->route('employees.profile', ['employeeId' => $employeeId])
            ->withStatus($name . ' successfully removed as dependent.');
    }

    /**
     * Add Educational Attainment
     */
    public function addEducation($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $response = [
            'title' => 'Add Educational Attainment<span>' . $employee->name() . '</span>',
            'view' => view('pages.employees.education.add', compact('employee'))->render()
        ];

        return json_encode($response);
    }

    public function addEducationSave(EducationRequests $request)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $education = EmployeeEducation::saveEducation($request);

        Session::flash('status', 'Graduated at ' . $education->school_name . ' last ' . $education->year_graduated . '.');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $request->employee_id])
        ];
        return json_encode($response);
    }

    /**
     * Update Educational Attainment
     */
    public function updateEducation($educationId)
    {
        $education = EmployeeEducation::findOrFail($educationId);

        $response = [
            'title' => 'Update Educational Attainment<span>' . $education->employee->name() . '</span>',
            'view' => view('pages.employees.education.edit', compact('education'))->render()
        ];

        return json_encode($response);
    }

    public function updateEducationSave(EducationRequests $request)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $education = EmployeeEducation::findOrFail($request->education_id);
        $education->updateEducation($request);

        Session::flash('status', $education->school_name . ' information successfully updated.');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $request->employee_id])
        ];

        return json_encode($response);
    }

    /**
     * Delete Educational Attainment
     */
    public function deleteEducation($educationId)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }
        
        $education = EmployeeEducation::findOrFail($educationId);
        $employeeId = $education->employee_id;
        $name = $education->school_name;

        $education->delete();

        return redirect()->route('employees.profile', ['employeeId' => $employeeId])
            ->withStatus($name . ' successfully removed.');
    }

    /**
     * Add Scholarship
     */
    public function addScholarship($employeeId)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $employee = Employee::findOrFail($employeeId);

        $response = [
            'title' => 'Add Scholarship<span>' . $employee->name() . '</span>',
            'view' => view('pages.employees.scholarships.add', compact('employee'))->render()
        ];

        return json_encode($response);
    }

    public function addScholarshipSave()
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'description' => 'required',
            'grantor' => 'required'
        ]);

        $scholarship = EmployeeScholarship::saveScholarship($this->request);

        Session::flash('status', 'Scholarship successfully added!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $this->request->employee_id])
        ];

        return json_encode($response);
    }

    /**
     * Update Scholarship
     */
    public function updateScholarship($scholarshipId)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $scholarship = EmployeeScholarship::findOrFail($scholarshipId);

        $response = [
            'title' => 'Update Scholarship<span>' . $scholarship->employee->name() . '</span>',
            'view' => view('pages.employees.scholarships.edit', compact('scholarship'))->render()
        ];

        return json_encode($response);
    }

    public function updateScholarshipSave()
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $this->validate($this->request, [
            'description' => 'required',
            'grantor' => 'required'
        ]);

        $scholarship = EmployeeScholarship::findOrFail($this->request->scholarship_id);
        $scholarship->updateScholarship($this->request);

        Session::flash('status', 'Scholarship successfully updated!');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $this->request->employee_id])
        ];

        return json_encode($response);
    }

    /**
     * Delete Scholarship
     */
    public function deleteScholarship($scholarshipId)
    {
        if (Gate::denies('edit_employee', Auth::user())) {
            abort(403);
        }

        $scholarship = EmployeeScholarship::findOrFail($scholarshipId);
        $employeeId = $scholarship->employee_id;
        $name = $scholarship->description;

        $scholarship->delete();

        return redirect()->route('employees.profile', ['employeeId' => $employeeId])
            ->withStatus($name . ' successfully removed.');
    }

    /**
     * Update Employment Details
     */
    public function updateEmploymentDetails($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $employmentStatuses = getEmploymentStatus([$employee->currentStatus(true)]);

        $response = [
            'title' => 'Update Employment Details<span>' . $employee->name() . '</span>',
            'view' => view('pages.employees.employment-details.edit', compact(
                'employee', 'employmentStatuses'
            ))->render()
        ];

        return json_encode($response);
    }

    public function updateEmploymentDetailsSave($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => $this->request->status,
            'updated_by' => Auth::user()->employee->id
        ]);

        // deactive account automatically if status > 4 (resigned/terminated)
        if ($this->request->status > 4) {
            $employee->user->is_active = 0;
            $employee->user->save();
        } else {
            $employee->user->is_active = 1;
            $employee->user->save();
        }

        Session::flash('status', $employee->name() . ' employment details successfully updated.');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $employee->id])
        ];

        return json_encode($response);
    }

    /**
     * Regularize Employee
     */
    public function regularize($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);
        $leaveTypes = LeaveType::all();

        $response = [
            'title' => 'Regularize ' . $employee->user->first_name,
            'view' => view('pages.employees.employment-details.regularize', compact(
                'employee', 'leaveTypes'
            ))->render()
        ];

        return json_encode($response);
    }

    public function regularizeSave($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $leaves = $this->request->leave;
        foreach ($leaves as $leaveTypeId => $leaveMeta) {
            LeaveEntitlement::create([
                'employee_id' => $employee->id,
                'leave_type_id' => $leaveTypeId,
                'year' => $leaveMeta['year'],
                'credits' => $leaveMeta['credits']
            ]);
        }

        $employee->status = 2;
        $employee->date_regular = Carbon::now();
        $employee->save();

        return redirect()->route('employees.profile', ['employeeId' => $employee->id])
            ->withStatus($employee->name() . ' is now a regular employee.');
    }

    /**
    * Regularize Employee
    */
    public function resign($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);
        $employee->user->is_active = 0;
        $employee->user->save();

        $employee->position_id = 1;
        $employee->status = 3;
        $employee->date_resigned = Carbon::now();
        $employee->save();

        return redirect()->route('employees.profile', ['employeeId' => $employee->id])
            ->withStatus($employee->name() . ' is now an inactive employee.');
    }

    /**
    * Upload Employee Signature
    */
    public function uploadSignature($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        // unlink/delete existing employee's signature
        $fileLocation = 'uploads/signatures/' . $employee->signature;
        if ($employee->signature != null AND file_exists($fileLocation)) {
            unlink($fileLocation);
        }

        // upload new signature
        if ($this->request->hasFile('signature_file')) {

            $destinationPath = 'uploads/signatures';
            $extension = $this->request->file('signature_file')->getClientOriginalExtension();
            $fileName = $employee->id . '_' . time() . '.' . $extension;
            $this->request->file('signature_file')->move($destinationPath, $fileName);

            $employee->signature = $fileName;
            $employee->save();

        }
        

        $view = '<div class="panel-heading-edit"><a id="remove-signature" title="Remove Signature" href="' . route('employees.removesignature') . '/' . $employee->id . '"><i class="fa fa-close"></i></a></div>';

        $response = [
            'newSignature' => $employee->signature(),
            'remover' => $view
        ];

        return json_encode($response);
    }

    public function removeSignature($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);        
        $signatureFileName = $employee->signature;
        unlink('uploads/signatures/' . $signatureFileName);
        $employee->signature = null;
        $employee->save();

        $response = [
            'done' => true
        ];
        return json_encode($response);
    }

    public function employeesRates()
    {
        if (!Auth::user()->allowedTo('view_employee_rate')) {
            abort(403);
        }


        $employees = Employee::orderByLastName()->get();

        return view('pages.employees.rates', compact('employees'));
    }

    public function updateRate($employeeId)
    {
        if (!Auth::user()->allowedTo('edit_employee_rate')) {
            abort(403);
        }

        $employee = Employee::findOrFail($employeeId);

        $response = [
            'title' => 'Update Rate<span>' . $employee->name(true) . '</span>',
            'view' => view('pages.employees.rates-update', compact('employee'))->render()
        ];

        return json_encode($response);
    }

    public function updateRateSave($employeeId)
    {
        if (!Auth::user()->allowedTo('edit_employee_rate')) {
            abort(403);
        }

        $this->validate($this->request, [
            'basic_income' => 'required',
            'monthly_work_days' => 'required'
        ]);

        $employee = Employee::findOrFail($employeeId);
        $employee->basic_income = $this->request->basic_income;
        $employee->monthly_work_days = $this->request->monthly_work_days;
        $employee->save();

        Session::flash('status', 'Employee\'s rate successfully updated.');
        $response = [
            'redirect' => route('employees.rates')
        ];

        return json_encode($response);
    }

}
