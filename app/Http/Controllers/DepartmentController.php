<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Gate;
use Session;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Position;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Department;
use UnoRenta\Http\Requests\SaveDepartmentRequests;

class DepartmentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        view()->share('departments', Department::all());
        view()->share('employees', Employee::all());
    }

    public function viewDepartments()
    {
        if (Gate::denies('view_departments_positions', Auth::user())) {
            abort(403);
        }

        $departments = Department::query()
            ->orderBy('name')
            ->get();
        return view('pages.departments.list', compact('departments'));
    }

    public function createDepartment()
    {
        if (Gate::denies('add_department_position', Auth::user())) {
            abort(403);
        }

        $response = [
            'title' => 'Add Department',
            'view' => view('pages.departments.create')->render()
        ];

        return json_encode($response);
    }

    public function createDepartmentSave(SaveDepartmentRequests $request)
    {
        if (Gate::denies('add_department_position', Auth::user())) {
            abort(403);
        }

        $department = Department::createDepartment($request);
        $department->save();

        Session::flash('status', $department->name . ' successfully created.');
        $response = [
            'redirect' => route('departments')
        ];

        return json_encode($response);
    }

    public function editDepartment($departmentId)
    {
        if (Gate::denies('edit_department_position', Auth::user())) {
            abort(403);
        }

        $department = Department::find($departmentId);

        $response = [
            'title' => 'Edit Department',
            'view' => view('pages.departments.edit', compact('department'))->render()
        ];

        return json_encode($response);
    }

    public function editDepartmentSave($departmentId, SaveDepartmentRequests $request)
    {
        if (Gate::denies('edit_department_position', Auth::user())) {
            abort(403);
        }

        $department = Department::updateDepartment($departmentId, $request);
        $department->save();

        Session::flash('status', $department->name . ' successfully updated.');
        $response = [
            'redirect' => route('departments')
        ];

        return json_encode($response);
    }

}
