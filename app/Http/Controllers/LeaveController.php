<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Gate;
use Session;
use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use UnoRenta\Http\Requests;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Approver;
use UnoRenta\Models\LeaveType;
use UnoRenta\Models\Department;
use UnoRenta\Models\LeaveComment;
use UnoRenta\Models\LeaveApplication;
use UnoRenta\Models\LeaveEntitlement;
use UnoRenta\Http\Requests\AddLeaveCommentRequests;
use UnoRenta\Http\Requests\LeaveApplicationRequests;

class LeaveController extends Controller
{

    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }
    
    public function getDashboard()
    {
        // leave na for approval
        $forApprovalLeaves = Auth::user()->employee->forApprovalLeaves();

        // user's leave applications
        $yourLeaves = LeaveApplication::query()
            ->where('employee_id', Auth::user()->employee->id)
            ->get();

        return view('pages.loa.dashboard', compact(
            'forApprovalLeaves', 'yourLeaves'
        ));
    }

    public function signedLeaveApplications()
    {
        if (!Auth::user()->isApprover()) {
            abort(403);
        }
        
        $title = 'Signed Leave Applications';
        $leaveApplications = Auth::user()->employee->approvedLeaves();

        return view('pages.loa.list', compact(
            'leaveApplications', 'title'
        ));
    }

    public function fileLeave()
    {
        $dept = Auth::user()->employee->position->department->id;
        $approvers = Approver::where('department_id', $dept)
            ->get();

        $response = [
            'title' => 'File Leave of Absence',
            'view' => view('pages.loa.file', compact('approvers'))->render()
        ];

        return json_encode($response);
    }   

    public function fileLeaveSave(LeaveApplicationRequests $request)
    {
        // check leave credits
        $employee = Auth::user()->employee;
        $leaveTypeName = '';
        if ($request->leave_type_id > 0) {
            $leaveType = LeaveType::findOrFail($request->leave_type_id);
            $leaveTypeName = $leaveType->name;
        }        

        $creditsRemaining = $employee->checkLeaveCredits($request->leave_type_id);

        if ($creditsRemaining >= $request->number_of_days) {
            $leave = LeaveApplication::processLeave($request);
            Session::flash(
                'status', 
                'Leave of Absence has been submitted. Please wait for the approvers to approve your application.'
            );
        } else {
            Session::flash(
                'error', 
                'Sorry, you only have ' . $creditsRemaining . ' ' . $leaveTypeName . '  credits remaining.'
            );
        }

        $response = [
            'redirect' => route('loa.dashboard')
        ];

        return json_encode($response);
    }

    public function getLeaveCredits()
    {
        $employee = Employee::findOrFail($this->request->employee_id);
        $creditsRemaining = 10;
        if ($this->request->leave_type_id > 0) {
            $creditsRemaining = $employee->checkLeaveCredits($this->request->leave_type_id);
        }        

        $response = [
            'remainingLeave' => $creditsRemaining
        ];

        return json_encode($response);
    }

    public function detailsLeave($loaId)
    {
        $leave = LeaveApplication::findOrFail($loaId);

        return view('pages.loa.details',
            compact('leave')
        );
    }

    public function approveLeave($loaId)
    {
        $leave = LeaveApplication::findOrFail($loaId);

        if (
            $leave->first_approver != $this->request->approver_id AND
            $leave->final_approver != $this->request->approver_id
        ) {
            abort(403);
        }

        if (
            $leave->first_approver == $this->request->approver_id AND 
            $leave->is_first_approved < 0 // para once lang jud maapprove
        ) {
            $leave->is_first_approved = 1;
            $leave->first_date_signed = Carbon::now();
        }
        if (
            $leave->final_approver == $this->request->approver_id AND 
            $leave->is_final_approved < 0 // para once lang jud maapprove
        ) {
            $leave->is_final_approved = 1;
            $leave->final_date_signed = Carbon::now();
        }

        $leave->save();

        $response = [
            'status' => 'approved',
            'leaveId' => $leave->id,
            'approverId' => $this->request->approver_id
        ];

        return json_encode($response);
    }

    public function approveWithoutPayLeave($loaId)
    {
        $leave = LeaveApplication::findOrFail($loaId);

        if (Auth::user()->employee->id == $leave->final_approver) {
            abort(403);
        }
        
        $leave->is_final_approved = 1;
        $leave->final_date_signed = Carbon::now();
        $leave->leave_type_id = 0;
        $leave->without_pay = 1;
        $leave->save();

        $employee = Employee::findOrFail($this->request->approver_id);
        LeaveComment::create([
            'leave_application_id' => $leave->id,
            'author_id' => $employee->id,
            'comment' => 'Approved without pay :)'
        ]);

        $response = [
            'status' => 'approved',
            'leaveId' => $leave->id,
            'approverId' => $this->request->approver_id
        ];

        return json_encode($response);
    }

    public function disapproveLeave($loaId)
    {
        $leave = LeaveApplication::findOrFail($loaId);

        $response = [
            'title' => 'Disapprove Leave Application<span> ' . $leave->employee->name() . ' </span>',
            'view' => view('pages.loa.disapprove', compact('leave'))->render()
        ];

        return json_encode($response);
    }

    public function disapproveLeaveSave($loaId)
    {
        $leave = LeaveApplication::find($loaId);

        if (
            $leave->first_approver != $this->request->approver_id AND
            $leave->final_approver != $this->request->approver_id
        ) {
            abort(403);
        }

        if (
            $leave->first_approver == $this->request->approver_id AND 
            $leave->is_first_approved < 0
        ) {
            $leave->is_first_approved = 0;
            $leave->first_date_signed = Carbon::now();
        }
        if (
            $leave->final_approver == $this->request->approver_id AND 
            $leave->is_final_approved < 0
        ) {
            $leave->is_final_approved = 0;
            $leave->final_date_signed = Carbon::now();
        }

        $leave->save();

        if ($this->request->comment != '') {
            LeaveComment::addComment($this->request);
        }
        
        $response = [
            'status' => 'disapproved',
            'leaveId' => $leave->id,
            'approverId' => $this->request->approver_id
        ];

        return json_encode($response);
    }

    public function sign($approver, $status, $loaId)
    {
        $loa = LeaveApplication::findOrFail($loaId);

        if (!Auth::user()->employee->isLeaveSignatory($loa->id)) {
            abort(403);
        }

        if ($status == 'a') { $s = 1; } elseif ($status == 'd') { $s = 0; }

        if ($approver == 'first') {            
            $loa->update([
                'is_first_approved' => $s,
                'first_date_signed' => Carbon::now()
            ]);
        } elseif ($approver == 'final') {
            $loa->update([
                'is_final_approved' => $s,
                'final_date_signed' => Carbon::now()
            ]);
        }

        $response = [
            'signature' => Auth::user()->employee->signature(),
            'status' => $s,
            'who' => $approver
        ];

        return json_encode($response);
    }

    public function addComment(AddLeaveCommentRequests $request)
    {
        $loa = LeaveApplication::findOrFail($request->leave_application_id);
        $comment = LeaveComment::addComment($request);

        $response = [
            'view' => view('includes.newcomment', compact('comment'))->render()
        ];

        return json_encode($response);
    }

    /**
    * Add Leave Type
    */
    public function addLeaveType()
    {
        $response = [
            'title' => 'Add Leave Type',
            'view' => view('pages.loa.types.add')->render()
        ];

        return json_encode($response);
    }

    public function addLeaveTypeSave()
    {
        $this->validate($this->request, [
            'name' => 'required|unique:leave_types,name'
        ]);

        $leaveType = LeaveType::create([
            'name' => $this->request->name
        ]);

        Session::flash(
            'status',
            $leaveType->name . ' successfully added.'
        );

        $response = [
            'redirect' => route('loa.entitlements')
        ];

        return json_encode($response);
    }

    /**
    * Edit Leave Type
    */
    public function editLeaveType($leaveTypeId)
    {
        $leaveType = LeaveType::findOrFail($leaveTypeId);

        $response = [
            'title' => 'Add Leave Type',
            'view' => view('pages.loa.types.edit', compact('leaveType'))->render()
        ];

        return json_encode($response);
    }

    public function editLeaveTypeSave($leaveTypeId)
    {
        $leaveType = LeaveType::findOrFail($leaveTypeId);

        $this->validate($this->request, [
            'name' => 'required'
        ]);

        $leaveType->name = $this->request->name;
        $leaveType->save();

        return redirect()->route('loa.entitlements')
            ->withStatus($leaveType->name . ' successfully updated.');
    }

    /**
    * Delete Leave Type
    */
    public function deleteLeaveType($leaveTypeId)
    {
        $leaveType = LeaveType::findOrFail($leaveTypeId);
        $name = $leaveType->name;
        $leaveType->delete();

        return redirect()->route('loa.entitlements')
            ->withStatus($name . ' successfully deleted.');
    }

    /**
    * Entitle Leave
    */
    public function leaveEntitlements()
    {
        if (Gate::denies('entitle_leave_applications', Auth::user())) {
            abort(403);
        }
        
        $leaveTypes = LeaveType::all();
        $employees = Employee::query()
            ->active()
            ->orderByLastName()
            ->get();

        return view('pages.loa.entitlements.dashboard', compact(
            'leaveTypes', 'employees'
        ));
    }

    public function entitleLeave($employeeId)
    {
        $leaveTypes = LeaveType::all();
        $employee = Employee::findOrFail($employeeId);

        $redirect = 'entitlements';
        if ($this->request->where === 'profile') {
            $redirect = 'profile';
        }

        $response = [
            'title' => 'Entitle Leave<span>' . $employee->name() . '</span>',
            'view' => view('pages.loa.entitlements.entitle', compact(
                'employee', 'leaveTypes', 'redirect'
            ))->render()
        ];

        return json_encode($response);
    }

    public function entitleLeaveSave($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $this->validate($this->request, [
            'leave_type_id' => 'required|integer',
            'year' => 'required|integer',
            'credits' => 'required|integer'
        ]);

        /**
        * Error Trap
        */
        $lookup = LeaveEntitlement::query()
            ->where('employee_id', $employeeId)
            ->where('leave_type_id', $this->request->leave_type_id)
            ->where('year', $this->request->year)
            ->get();

        if (count($lookup) > 0) {

            $leaveType = LeaveType::find($this->request->leave_type_id);

            if ($this->request->where === 'profile') {

                Session::flash(
                    'error',
                    $leaveType->name . ' for ' . $this->request->year . ' was already entitled to ' . $employee->name() . '.'
                );
                return json_encode(['redirect' => route('employees.profile', ['employeeId' => $employee->id])]);
            }

            Session::flash(
                'error',
                $leaveType->name . ' for ' . $this->request->year . ' was already entitled to ' . $employee->name() . '.'
            );
            return json_encode(['redirect' => route('loa.entitlements')]);

        }

        // zero all previous credits
        $entitledLeaves = $employee->entitledLeave()
            ->where('leave_type_id', $this->request->leave_type_id)
            ->get();
        foreach ($entitledLeaves as $el) {
            $el->credits = 0;
            $el->save();
        }

        $entitledLeave = LeaveEntitlement::create([
            'employee_id' => $employee->id,
            'leave_type_id' => $this->request->leave_type_id,
            'year' => $this->request->year,
            'credits' => $this->request->credits
        ]);

        if ($this->request->where === 'profile') {
            Session::flash(
                'status',
                $entitledLeave->leaveType->name . ' successfully entitled to ' . $employee->name() . '.'
            );
            return json_encode(['redirect' => route('employees.profile', ['employeeId' => $employee->id])]);
        }

        Session::flash(
            'status',
            $entitledLeave->leaveType->name . ' successfully entitled to ' . $employee->name() . '.'
        );
        return json_encode(['redirect' => route('loa.entitlements')]);
    }

    public function entitledLeavesList($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        $response = [
            'title' => 'Leave Entitlements<span>' . $employee->name() . '</span>',
            'view' => view('pages.loa.entitlements.entitled-list', compact('employee'))->render()
        ];

        return json_encode($response);
    }

    public function removeEntitledLeave($entitlementId)
    {
        $entitledLeave = LeaveEntitlement::findOrFail($entitlementId);
        $employeeName = $entitledLeave->employee->user->first_name;
        $leaveType = $entitledLeave->leaveType->name;
        $entitledLeave->delete();

        return redirect()->route('loa.entitlements')
            ->withStatus($leaveType . ' successfully removed from ' . $employeeName . '.');
    }

    public function leaveApprovers()
    {
        $approvers = Approver::query()
            ->join('employees as ee', 'approvers.employee_id', '=', 'ee.id')
            ->orderByLastName()
            ->groupBy('employee_id')
            ->select('approvers.*')
            ->get();

        return view('pages.loa.approvers.list', compact(
            'approvers'
        ));
    }

    public function addLeaveApprover()
    {
        $employees = Employee::all();
        $departments = Department::all();

        $response = [
            'title' => 'Add Approver',
            'view' => view('pages.loa.approvers.add', compact(
                'employees', 'departments'
            ))->render()
        ];

        return json_encode($response);
    }

    public function addLeaveApproverSave()
    {
        $this->validate($this->request, [
            'approver' => 'required',
            'departments' => 'required'
        ]);

        foreach ($this->request->departments as $department) {
            Approver::create([
                'employee_id' => $this->request->approver,
                'department_id' => $department
            ]);
        }
        
        Session::flash('status', 'Successfully added approver.');
        $response = [
            'redirect' => route('loa.approvers')
        ];

        return json_encode($response);
    }

}