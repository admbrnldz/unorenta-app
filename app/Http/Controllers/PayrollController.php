<?php

namespace UnoRenta\Http\Controllers;

use PDF;
use Auth;
use Input;
use Session;
use Carbon\Carbon;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Payslip;
use UnoRenta\Models\Holiday;
use UnoRenta\Models\Timecard;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Department;
use UnoRenta\Models\PayrollBatch;
use UnoRenta\Models\PayslipLeave;
use UnoRenta\Models\LeaveApplication;
use UnoRenta\Models\PayslipDeduction;
use UnoRenta\Models\ScheduledDeduction;

class PayrollController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function payrollDashboard()
    {
        return view('pages.accounting.payroll.dashboard');
    }

    public function batch()
    {
        if (!Auth::user()->allowedTo('view_payroll_batch')) {
            abort(403);
        }

        $payrollBatches = PayrollBatch::all();

        return view('pages.accounting.payroll.batch.list', compact(
            'payrollBatches'
        ));
    }

    public function batchDetails($batchId)
    {
        if (!Auth::user()->allowedTo('view_payroll_batch')) {
            abort(403);
        }

        $payrollBatch = PayrollBatch::findOrFail($batchId);

        $payslips = $payrollBatch->payslips()
            ->join('employees', 'payslips.employee_id', '=', 'employees.id')
            ->orderByLastName()
            ->select('payslips.*')
            ->get();

        return view('pages.accounting.payroll.batch.details', compact(
            'payrollBatch', 'payslips'
        ));
    }

    public function fetchTimecards()
    {
        if (!Auth::user()->allowedTo('generate_payslip')) {
            abort(403);
        }

        $year = Input::get('year');
        $month = Input::get('month');
        $half = Input::get('half');

        if ($half == 1) {
            $from = Carbon::create($year, $month - 1, 21)->startOfDay();
            $to = Carbon::create($year, $month, 5)->endOfDay();
        } elseif ($half == 2) {
            $from = Carbon::create($year, $month, 6)->startOfDay();
            $to = $from->copy()->day(20)->endOfDay();
        }

        $timecards = Timecard::query()
            ->whereRaw("time_in BETWEEN '$from' AND '$to'")
            ->whereNull('payslip_id')
            ->select('employee_id')
            ->distinct()
            ->get();

        $es = [];
        foreach ($timecards as $tc) {
            $es[$tc->employee_id] = $tc->employee_id;
        }

        $employees = Employee::whereIn('employees.id', $es)->orderByLastName()->get();
        if ($this->request->department > 0) {
            $employees = Employee::query()
                ->leftJoin('positions', 'employees.position_id', '=', 'positions.id')
                ->where('positions.department_id', $this->request->department)
                ->whereIn('employees.id', $es)
                ->orderByLastName()
                ->select('employees.*')
                ->get();
        }

        $noData = false;
        $table = '<table class="table selectlist"><thead><tr><th style="width:24px;">' .
            '<input class="checkboxes sl-checkbox-main sl-checkboxes-ctrl" type="checkbox">' .
            '</th><th colspan="2">Employee</th><th>Position</th>' .
            '<th class="ta-right">Timecards</th></tr></thead><tbody>';

        if (count($employees) > 0) {
            foreach ($employees as $e) {
                $timecardsCount = $e->timecards()->whereRaw("(time_in BETWEEN '$from' AND '$to')")->count();
                $table .= '<tr>' . 
                    '<td class="tbl-checkbox"><input class="employees-id checkboxes sl-checkbox" value="' . $e->id . '" type="checkbox"></td>' .
                    '<td class="table-dp-td">' .
                        '<div class="dp-handler" style="background:url(' . $e->dp() . ') center center no-repeat;background-size:cover;">' .
                        '<a href="' . route('employees.profile', ['employeeId' => $e->id]) . '"></a>' .
                        '</div>' .
                    '</td>' .
                    '<td class="table-highlight">' .
                        '<a href="' . route('employees.profile', ['employeeId' => $e->id]) . '">' . $e->name(true) . '</a>' .
                    '</td>' .
                    '<td>' . $e->position->name . '</td>' .
                    '<td class="ta-right">' . $timecardsCount . '</td>' .
                    '</tr>';
            }
        } else {
            $table .= '<tr><td colspan="9" class="ta-center">No data</td></tr>';
            $noData = true;
        }

        $table .= '</tbody></table>';

        $response = [
            'table' => $table,
            'employees' => $employees,
            'noData' => $noData
        ];

        return json_encode($response);
    }

    public function createPayrollBatch()
    {
        if (!Auth::user()->allowedTo('generate_payslip')) {
            abort(403);
        }

        $year = $this->request->year;
        $month = $this->request->month;
        $half = $this->request->half;

        if ($half == 1) {
            $from = Carbon::create($year, $month - 1, 21)->startOfDay();
            $to = Carbon::create($year, $month, 5)->endOfDay();
        } elseif ($half == 2) {
            $from = Carbon::create($year, $month, 6)->startOfDay();
            $to = $from->copy()->day(20)->endOfDay();
        }

        $payrollBatch = PayrollBatch::create([
            'year' => $year,
            'month' => $month,
            'half' => $half,
            'generated_by' => Auth::user()->employee->id
        ]);

        $firstEmployee = Employee::findOrFail($this->request->firstEmployee);

        $response = [
            'done' => true,
            'payrollBatch' => $payrollBatch->id,
            'firstEmployee' => $firstEmployee->nickname
        ];

        return json_encode($response);
    }

    public function generatePayroll()
    {
        if (!Auth::user()->allowedTo('generate_payslip')) {
            abort(403);
        }

        $employees = Employee::orderByLastName()->get();
        $departments = Department::all();

        return view('pages.accounting.payroll.generate', compact(
            'employees', 'departments'
        ));
    }

    public function generatePayrollSave()
    {
        if (!Auth::user()->allowedTo('generate_payslip')) {
            abort(403);
        }
        
        $employees = $this->request->employees;

        // find employee and payroll batch
        $employee = Employee::findOrFail($this->request->employeeId);
        $payrollBatch = PayrollBatch::findOrFail($this->request->batchId);

        $basicIncome = $employee->basic_income;
        $payrollBasicIncome = $basicIncome / 2;
        $hourlyRate = $basicIncome / ($employee->monthly_work_days * 8);

        // get timecards
        $year = $this->request->year;
        $month = $this->request->month;
        $half = $this->request->half;

        if ($half == 1) {
            $from = Carbon::create($year, $month - 1, 21)->startOfDay();
            $to = Carbon::create($year, $month, 5)->endOfDay();
        } elseif ($half == 2) {
            $from = Carbon::create($year, $month, 6)->startOfDay();
            $to = $from->copy()->day(20)->endOfDay();
        }

        $timecards = $employee->timecards()
            ->whereRaw("time_in BETWEEN '$from' AND '$to'")
            ->whereNull('payslip_id')
            ->get();

        // $deductions['premium']['SSS'] = 0;
        // $deductions['premium']['PhilHealth'] = 0;
        // $deductions['premium']['Pagibig'] = 0;
        if ($from->day > 20) {

            // 1st half
            // compute sss premium
            $deductions['premium']['SSS'] = getSSSPremium($payrollBasicIncome);

        } elseif ($from->day > 5 AND $to->day < 21) {

            // 2nd half
            // compute philhealth and pagibig premium
            $deductions['premium']['PhilHealth'] = getPhilHealthPremium($payrollBasicIncome);
            $deductions['premium']['Pagibig'] = 100;

        }

        // calculate LT, AB, UT, ND, RDOT, SH, SHRD, RH, RHRD, DH, DHRD
        $_lt = 0;
        $_ab = 0;
        $_ut = 0;
        $_nd = 0;
        $_rdot = 0;
        $_sh = 0;
        $_shrd = 0;
        $_rh = 0;
        $_rhrd = 0;
        $_dh = 0;
        $_dhrd = 0;
        foreach ($timecards as $timecard) {
            
            $time_in = Carbon::parse($timecard->time_in);
            $time_out = Carbon::parse($timecard->time_out);
            $ti = $time_in->copy()->toDateString();
            $to = $time_out->copy()->toDateString();

            /*
            * Late / Tardiness
            * Undertime / Absences
            */
            $_lt += $timecard->late_minutes;
            $_ut += $timecard->undertime_minutes;
            if ($timecard->is_absent) {
                $_ab += (8 * 60);
            }

            /*
            * Night Differential
            */
            $tffn = $timecard->shift->shift_from;
            $ttfn = $timecard->shift->shift_to;
            $_nd += getNightDifferential($tffn, $ttfn);

            $shiftFrom = Carbon::parse($timecard->shift->shift_from);
            if (
                $shiftFrom->between($shiftFrom->copy()->subDay()->hour(22), $shiftFrom->copy()->hour(6)) OR
                $shiftFrom->between($shiftFrom->copy()->hour(22), $shiftFrom->copy()->addDay()->hour(6))
            ) {
                $_nd -= $timecard->late_minutes;
            }
            $shiftTo = Carbon::parse($timecard->shift->shift_to);
            if (
                $shiftTo->between($shiftTo->copy()->subDay()->hour(22), $shiftTo->copy()->hour(6)) OR
                $shiftTo->between($shiftTo->copy()->hour(22), $shiftTo->copy()->addDay()->hour(6))
            ) {
                $_nd -= $timecard->undertime_minutes;
            }

            /*
            * Rest Day
            */
            $isRd = false;
            if ($timecard->shift->is_rd) {
                $isRd = true;
                $_rdot += (8 * 60);
            }
            $_rdot -= ($timecard->late_minutes + $timecard->undertime_minutes);

            /*
            * Overtime
            */
            $_rdot += $timecard->overtime_minutes;

            /*
            * Holidays
            */
            $holidays = Holiday::whereRaw("date BETWEEN '$ti' AND '$to'")->get();

            if (count($holidays) > 0) {
                foreach ($holidays as $holiday) {

                    $holidayFrom = Carbon::parse($holiday->date);
                    $holidayTo = $holidayFrom->copy()->addDay();

                    if ($shiftFrom >= $holidayFrom AND $shiftTo < $holidayTo) {
                        $duration = $shiftFrom->diffInMinutes($shiftTo);
                    } elseif ($shiftFrom > $holidayFrom AND $shiftTo > $holidayTo) {
                        $duration = $shiftFrom->diffInMinutes($shiftTo) - $holidayTo->diffInMinutes($shiftTo);
                    } elseif ($shiftFrom < $holidayFrom AND $shiftTo > $holidayFrom AND $shiftTo < $holidayTo) {
                        $duration = $holidayFrom->diffInMinutes($shiftTo);
                    }

                    if ($holiday->category->name == 'Regular') {
                        $_rh = $duration;
                        if ($isRd) {
                            $_rh -= $duration;
                            $_rhrd += $duration;
                        }
                    } elseif ($holiday->category->name == 'Special') {
                        $_sh = $duration;
                        if ($isRd) {
                            $_sh -= $duration;
                            $_shrd += $duration;
                        }
                    }
                    
                    if ($isRd AND $holiday->holiday_category_id > 1) {
                        $_rdot -= $duration;
                    }

                }
            }

        }

        // create payslip
        $payslip = Payslip::create([
            'payroll_batch_id' => $payrollBatch->id,
            'employee_id' => $employee->id,
            'basic_pay' => $payrollBasicIncome,
            'monthly_work_days' => $employee->monthly_work_days,

            'lt' => $_lt,
            'ab' => $_ab,
            'ut' => $_ut,
            'nd' => $_nd,
            'rdot' => $_rdot,
            'sh' => $_sh,
            'shrd' => $_shrd,
            'rh' => $_rh,
            'rhrd' => $_rhrd,
            'dh' => $_dh,
            'dhrd' => $_dhrd
        ]);

        // link timecards to payslip !!
        foreach ($timecards as $timecard) {
            $timecard->payslip_id = $payslip->id;
            $timecard->save();
        }

        $dayFrom = $payslip->payrollBatch->half == 1 ? 20 : 6;
        $dayTo = $payslip->payrollBatch->half == 1 ? 5 : 21;
        $monthFrom = $payslip->payrollBatch->half == 1 ? $payslip->payrollBatch->month - 1 : $payslip->payrollBatch->month;
        $cutoffFrom = Carbon::create($payslip->payrollBatch->year, $monthFrom, $dayFrom)->startOfDay();
        $cutoffTo = Carbon::create($payslip->payrollBatch->year, $payslip->payrollBatch->month, $dayTo)->endOfDay();

        // leave applications
        $leaveApplications = $employee->leaveApplications()
            ->whereRaw("((date_from BETWEEN '$cutoffFrom' AND '$cutoffTo') OR (date_to BETWEEN '$cutoffFrom' AND '$cutoffTo'))")
            ->get();

        foreach ($leaveApplications as $loa) {

            $leaveFrom = Carbon::parse($loa->date_from)->startOfDay();
            $leaveTo = Carbon::parse($loa->date_to)->endOfDay();
            $days = 0;
            if ($leaveFrom < $cutoffFrom AND $leaveTo > $cutoffFrom) {
                $days = $leaveTo->day - $cutoffFrom->copy()->subSecond()->day;
            } elseif ($leaveFrom < $cutoffTo AND $leaveTo > $cutoffTo) {
                $days = $cutoffTo->day - $leaveFrom->copy()->subSecond()->day;
            } else {
                $days = $loa->number_of_days;
            }

            // catch all leave applications
            if ($days > 0) {
                PayslipLeave::create([
                    'payslip_id' => $payslip->id,
                    'leave_application_id' => $loa->id,
                    'days' => $days
                ]);
            }

        }

        // get scheduled deductions
        $scheduledDeductions = ScheduledDeduction::where('employee_id', $this->request->employeeId)->get();
        foreach ($scheduledDeductions as $sd) {
            if (!$sd->paid()) {
                if (
                    ($sd->term == 'monthly' AND $sd->half == 2 AND $from->day > 5 AND $to->day < 21) OR
                    ($sd->term == 'monthly' AND $sd->half == 1 AND $from->day > 20) OR
                    $sd->term == 'semi-monthly'
                ) {

                    $payableAmount = $sd->amount / $sd->months_payable;
                    if ($sd->term == 'semi-monthly') {
                        $payableAmount = $sd->amount / ($sd->months_payable * 2);
                    }
                    
                    PayslipDeduction::create([
                        'payslip_id' => $payslip->id,
                        'scheduled_deduction_id' => $sd->id,
                        'deduction_type' => $sd->before_tax,
                        'name' => $sd->name,
                        'key' => strtolower(str_replace(' ', '', $sd->name)),
                        'amount' => $payableAmount
                    ]);
                    
                }
            }
        }

        // add deductions to table           
        foreach ($deductions['premium'] as $key => $value) {
            PayslipDeduction::create([
                'payslip_id' => $payslip->id,
                'deduction_type' => 1, // before tax
                'name' => $key,
                'key' => strtolower($key),
                'amount' => $value
            ]);
        }

        // get who's the next employee to poll
        $current = array_search($employee->id, $employees);
        $percent = $this->request->count / count($employees) * 100;

        $next = 0;
        $done = true;
        $employeeName = '';
        if (($current + 1) < count($employees)) {
            $next = $current + 1;
            $done = false;
            $nextEmployee = Employee::findOrFail($employees[$next]);
            $employeeName = $nextEmployee->nickname;            
        }

        $response = [
            'done' => $done,
            'current' => $employee->id,
            'next' => $employees[$next],
            'employeeName' => $employeeName,
            'count' => $this->request->count + 1,
            'percent' => $percent,
            'leaveApplications' => $leaveApplications
        ];

        if ($done) {
            Session::flash('status', 'Payslips successfuly generated.');
            $response['redirect'] = route('payroll.batch.details', ['batchId' => $payrollBatch->id]);
        }

        return json_encode($response);
    }

    public function payrollBatchToPDF($payrollBatchId)
    {
        if (!Auth::user()->allowedTo('generate_payslip')) {
            abort(403);
        }
        
        $payrollBatch = PayrollBatch::findOrFail($payrollBatchId);
        $payslips = $payrollBatch->payslips()
            ->join('employees', 'payslips.employee_id', '=', 'employees.id')
            ->orderByLastName()
            ->select('payslips.*')
            ->get();

        $filename = 'Payroll Batch ' . $payrollBatch->id . '-' . time();
        $html = view('pages.accounting.payroll.batch.pdf', compact(
            'payrollBatch', 'filename', 'payslips'
        ))->render();

        return toPdf($html, $filename, false);
    }

}
