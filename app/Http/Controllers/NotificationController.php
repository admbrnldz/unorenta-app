<?php

namespace UnoRenta\Http\Controllers;

use Illuminate\Http\Request;
use UnoRenta\Http\Requests;
use Input;
use Auth;
use UnoRenta\Models\Notification as Notification;

class NotificationController extends Controller
{
    
    public $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function checkNotifications()
    {

        $user = Auth::user();
        $notification = Notification::where('recipient_id', $user->id)
                                      ->orderBy('id', 'desc')
                                      ->first();
        $latestNotificationTime = 0;
        if ($notification) {
            $latestNotificationTime = strtotime($notification->created_at);   
        }

        if (Input::has('ts')) {
            $timeStamp = (int) Input::get('ts');
            if ($timeStamp == 0) {
                $timeStamp = $latestNotificationTime;
            }
        }        

        while ($latestNotificationTime <= $timeStamp) {
            usleep(100000);
            clearstatcache();
            $latestNotificationTime = $this->timeStampLastNotification(Auth::user());
        }

        $count = Notification::where('recipient_id', $user->id)
                               ->where('read', 0)
                               ->count();

        $data = [
            'rawTime' => $timeStamp,
            'latestNotificationTime' => $latestNotificationTime,
            'timeStamp' => $latestNotificationTime,
            'user' => Auth::user()->id,
            'count' => $count
        ];

        return json_encode($data);
    }

    public function timeStampLastNotification($user)
    {
        $notification = Notification::where('recipient_id', $user->id)
                                      ->orderBy('id', 'desc')
                                      ->first();
        if ($notification) {
            return strtotime($notification->created_at);
        }
        return 0;
    }

}
