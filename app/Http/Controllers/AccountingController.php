<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Session;
use Illuminate\Http\Request;
use UnoRenta\Models\Employee;
use UnoRenta\Models\ScheduledDeduction;

class AccountingController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getDashboard()
    {
        return view('pages.accounting.dashboard');
    }

    public function deductions()
    {
        if (!Auth::user()->allowedTo('view_other_deductions')) {
            abort(403);
        }

        $deductions = ScheduledDeduction::all();

        return view('pages.accounting.deductions.list', compact(
            'deductions'
        ));
    }

    public function addScheduledDeduction()
    {
        if (!Auth::user()->allowedTo('add_other_deductions')) {
            abort(403);
        }

        $employees = Employee::all();

        $response = [
            'title' => 'Add Scheduled Deductions',
            'view' => view('pages.accounting.deductions.add-sd', compact(
                'employees'
            ))->render()
        ];

        return json_encode($response);
    }

    public function addScheduledDeductionSave()
    {
        if (!Auth::user()->allowedTo('add_other_deductions')) {
            abort(403);
        }
        
        $half = $this->request->term == 'monthly' ? 'required' : '';

        $this->validate($this->request, [
            'employees' => 'required',
            'name' => 'required',
            'amount' => 'required',
            'months_payable' => 'required',
            'term' => 'required',
            'half' => $half,
        ]);

        $beforeTax = $this->request->before_tax == null ? 2 : $this->request->before_tax;

        foreach ($this->request->employees as $employeeId) {
            ScheduledDeduction::create([
                'employee_id' => $employeeId,
                'name' => $this->request->name,
                'amount' => $this->request->amount,
                'months_payable' => $this->request->months_payable,
                'before_tax' => $beforeTax,
                'term' => $this->request->term,
                'half' => $this->request->half
            ]);
        }

        Session::flash('status', 'Deductions successfully added.');

        $response = [
            'redirect' => route('payroll.deductions')
        ];

        return json_encode($response);
    }

    public function editDeduction($deductionId = 0)
    {
        $response = [
            'title' => 'Edit Mandatory Deductions',
            'view' => view('pages.accounting.payroll.mandatory-deductions.edit')->render()
        ];

        return json_encode($response);
    }

    public function deductionDetails($deductionId)
    {
        $deduction = ScheduledDeduction::findOrFail($deductionId);

        $response = [
            'view' => view('pages.accounting.deductions.details', compact('deduction'))->render()
        ];

        return json_encode($response);
    }

    public function usersDeductions()
    {
        $deductions = ScheduledDeduction::query()
            ->where('employee_id', Auth::user()->employee->id)
            ->get();

        return view('pages.accounting.deductions.for-employee-list', compact('deductions'));
    }

}
