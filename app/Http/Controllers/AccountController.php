<?php

namespace UnoRenta\Http\Controllers;

use Gate;
use Auth;
use Session;
use UnoRenta\Models\User;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\UserPermission;
use UnoRenta\Models\PermissionCategory;
use UnoRenta\Http\Requests\UserSettingsRequests;

class AccountController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getSettings($userId)
    {
        if (Gate::denies('edit_account_settings', Auth::user())) {
            if (Auth::user()->id != $userId) {
                abort(403);
            }
        }

        $user = User::findOrFail($userId);
        $categories = PermissionCategory::all();

        $userPermissions = [];
        $count = 0;
        
        foreach ($user->permissions as $up) {
            $userPermissions[$count] = $up->permission_id;
            $count++;
        }

        return view('pages.accounts.settings',
            compact('user', 'userPermissions', 'categories')
        );
    }

    public function saveSettings(UserSettingsRequests $request)
    {
        if (Gate::denies('edit_account_settings', Auth::user())) {
            if ($request->user_id != Auth::user()->id) {
                abort(403);
            }
        }

        $user = User::findOrFail($request->user_id);
        $user->updateUserSettings($request);

        return redirect()->route('account.settings', ['userId' => $user->id])
            ->withStatus('Updated na po successfully.');
    }

    public function checkUsername()
    {
        $available = true;
        $username = $this->request->username;

        if ($username == '') {
            $available = false;
        }

        $user = User::where('username', $username)
            ->where('id', '<>', $this->request->user_id)
            ->get();
        if (count($user) > 0) {
            $available = false;
        }

        $response = [
            'available' => $available
        ];

        return json_encode($response);
    }

    public function uploadDisplayPhoto($userId)
    {        
        $user = User::findOrFail($userId);

        // remove old display photo
        $fileLocation = 'uploads/dp/' . $user->display_photo;
        if ($user->display_photo != null AND file_exists($fileLocation)) {
            unlink($fileLocation);
        }

        // upload new display photo
        if ($this->request->hasFile('dp_file')) {

            $destinationPath = 'uploads/dp';
            $extension = $this->request->file('dp_file')->getClientOriginalExtension();
            $fileName = $user->id . '_' . time() . '.' . $extension;
            $this->request->file('dp_file')->move($destinationPath, $fileName);

            $user->display_photo = $fileName;
            $user->save();

        }

        $response = [
            'newDP' => $user->dp()
        ];

        return json_encode($response);
    }

    public function removeDisplayPhoto($userId)
    {
        if (Gate::denies('edit_account_settings', Auth::user())) {
            if ($userId != Auth::user()->id) {
                abort(403);
            }
        }

        $user = User::findOrFail($userId);
        $user->display_photo = 'no-dp.png';
        $user->save();

        return redirect()->route('employees.profile', ['employeeId' => $user->employee->id])
            ->withStatus('Naremove na po.');
    }

    public function updatePermissions($userId)
    {
        $user = User::findOrFail($userId);

        UserPermission::where('user_id', $user->id)->delete();

        foreach ($this->request->permissions as $permissionId) {
            UserPermission::create([
                'permission_id' => $permissionId,
                'user_id' => $user->id
            ]);
        }       

        return redirect()->route('account.settings', ['userId' => $user->id])
            ->withStatus('User permissions updated successfully!');
    }

    public function deactivateAccount($userId)
    {
        if (Gate::denies('edit_account_settings', Auth::user())) {
            abort(403);
        }

        $user = User::findOrFail($userId);
        $user->employee->position_id = 1;
        $user->employee->status = 3;
        $user->employee->save();
        $user->is_active = 0;
        $user->save();

        Session::flash('error', $user->name() . ' is now inactive and placed in Unassigned position.');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $user->employee->id])
        ];

        return json_encode($response);
    }

    public function reactivateAccount($userId)
    {
        if (Gate::denies('edit_account_settings', Auth::user())) {
            abort(403);
        }

        $user = User::findOrFail($userId);
        $user->employee->position_id = 1;
        $user->employee->status = 1; // back to probi
        $user->employee->save();
        $user->is_active = 1;
        $user->save();

        Session::flash('status', $user->name() . ' is now active but in probationary status.');
        $response = [
            'redirect' => route('employees.profile', ['employeeId' => $user->employee->id])
        ];

        return json_encode($response);
    }

    public function updateTone($userId)
    {
        $user = User::findOrFail($userId);
        $user->notification_tone = $this->request->tone;
        $user->save();

        $newTone = '';
        if ($user->notification_tone != '') {
            $newTone = asset('assets/tones') . '/' . $user->notification_tone;
        }

        $response = [
            'newTone' => $newTone,
            'toneString' => $user->notification_tone
        ];

        return json_encode($response);
    }

}
