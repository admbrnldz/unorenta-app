<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Session;
use Carbon\Carbon;
use UnoRenta\Models\Shift;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Timecard;
use UnoRenta\Models\ShiftBrief;
use UnoRenta\Models\UserPermission;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDashboard()
    {
        $today = Carbon::today();
        $employeeId = Auth::user()->employee->id;
        $shiftToday = null;
        $shifts = null;
        // fetch undone timecard
        $timecard = Timecard::query()
            ->whereNull('time_out')
            ->whereNull('to_signed_by')
            ->where('employee_id', $employeeId)
            ->first();

        $shiftBrief = ShiftBrief::query()
            ->where('month', $today->month)
            ->where('employee_id', $employeeId)
            ->first();

        if ($shiftBrief) {
            $shifts = $shiftBrief->shifts;

            $today = Carbon::today();
            $now = Carbon::now();
            $nowPlusHour = $now->copy()->addHour();

            $shiftToday1 = $shiftBrief->shifts()
                ->whereRaw("'$nowPlusHour' BETWEEN shift_from AND shift_to")
                ->first();

            $shiftToday2 = $shiftBrief->shifts()
                ->whereRaw("'$now' BETWEEN shift_from AND shift_to")
                ->first();

            if ($shiftToday1) {
                $shiftToday = $shiftToday1;
            } elseif ($shiftToday2) {
                $shiftToday = $shiftToday2;
            }

            if ($shiftToday) {
                $timecard = $shiftToday->timecard;
            }

        }

        Session::put('timecard', $timecard);

        /**
        * Breaktime
        */
        $breaktime = null;
        if ($timecard) {
            if (count($timecard->breaks) > 0) {
                $breaktime = $timecard->breaks()->whereNotNull('break_start')->whereNull('break_end')->first();
            }
        }
        Session::put('breaktime', $breaktime);

        $entitledLeaves = Auth::user()->employee
            ->entitledLeave()
            ->where('year', Carbon::now()->year)
            ->select('id', 'leave_type_id')
            ->get();

        /**
        * Latest Payslip
        */
        $latestPayslip = Auth::user()->employee->payslips()->orderBy('id', 'desc')->first();

        return view('pages.dashboard', compact(
            'shiftBrief', 'shiftToday', 'shiftToday2', 'timecard', 'entitledLeaves',
            'latestPayslip'
        ));
    }
    
}
