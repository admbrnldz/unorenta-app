<?php

namespace UnoRenta\Http\Controllers;

use Gate;
use Auth;
use Session;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Position;
use UnoRenta\Models\Department;
use UnoRenta\Http\Requests\SavePositionRequests;

class PositionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        view()->share('positions', Position::all());
    }

    public function addPosition()
    {
        if (Gate::denies('add_department_position', Auth::user())) {
            abort(403);
        }

        $vars['departments'] = Department::all();

        $response = [
            'title' => 'Add Position',
            'view' => view('pages.positions.create', $vars)->render()
        ];

        return json_encode($response);
    }

    public function addPositionSave(SavePositionRequests $request)
    {
        if (Gate::denies('add_department_position', Auth::user())) {
            abort(403);
        }

        $position = Position::addPosition($request);
        $position->save();

        Session::flash('status', $position->name . ' successfully added.');
        $response = [
            'redirect' => route('departments')
        ];

        return json_encode($response);
    }

    public function editPosition($positionId)
    {
        if (Gate::denies('edit_department_position', Auth::user())) {
            abort(403);
        }

        $position = Position::findOrFail($positionId);
        $departments = Department::all();

        $response = [
            'title' => 'Edit Position',
            'view' => view('pages.positions.edit', compact('position', 'departments'))->render()
        ];

        return json_encode($response);
    }

    public function editPositionSave($positionId, SavePositionRequests $request)
    {
        if (Gate::denies('edit_department_position', Auth::user())) {
            abort(403);
        }

        $position = Position::findOrFail($positionId);
        $position->updatePosition($request);

        Session::flash('status', $position->name . ' successfully updated.');
        $response = [
            'redirect' => route('departments')
        ];

        return json_encode($response);
    }

}
