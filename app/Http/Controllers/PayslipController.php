<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Payslip;

class PayslipController extends Controller
{

    private $request = null;
    
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getPayslips()
    {
        $payslips = Auth::user()->employee->payslips;

        return view('pages.accounting.payslips.list', compact(
            'payslips'
        ));
    }

    public function payslipDetails($payslipId)
    {
        $payslip = Payslip::findOrFail($payslipId);

        $response = [
            'view' => view('pages.accounting.payslips.sneak-details', compact(
                'payslip'
            ))->render()
        ];

        return json_encode($response);
    }

    public function payslipTimecards($payslipId)
    {
        $payslip = Payslip::findOrFail($payslipId);

        return view('pages.accounting.payslips.timecards', compact(
            'payslip'
        ));
    }
    
}
