<?php

namespace UnoRenta\Http\Controllers;

use Gate;
use Auth;
use Input;
use Session;
use Carbon\Carbon;
use UnoRenta\Models\Shift;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Timecard;
use UnoRenta\Models\Employee;
use UnoRenta\Models\ShiftBrief;
use UnoRenta\Http\Requests\SaveShiftRequests;
use UnoRenta\Http\Requests\GenerateShiftRequests;

class ShiftController extends Controller
{
    
    private $request = null;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getEmployees()
    {
        $managedDepartments = Auth::user()->employee->managedDepartments;

        $positions = [];
        $count = 0;
        foreach ($managedDepartments as $department) {
            foreach ($department->department->positions as $position) {
                $positions[$count] = $position->id;
                $count++;
            }
        }

        $employees = Employee::query()
            ->whereIn('position_id', $positions)
            ->orderByLastName()
            ->get();

        return $employees;
    }

    public function manageShifts()
    {
        if (Gate::denies('add_shift', Auth::user())) {
            abort(403);
        }

        $employees = $this->getEmployees();

        return view('pages.shifts.manage',
            compact('employees')
        );
    }

    public function generateShifts($employeeId)
    {
        $employeesId = explode(',', $employeeId);

        if (count($employeesId) == 1) {
            Employee::findOrFail($employeesId[0]);
        }

        $employees = $this->getEmployees();

        $es = [];
        $count = 0;
        foreach ($employees as $e) {
            $es[$count] = $e->id;
            $count++;
        }

        foreach ($employeesId as $id) {
            if (!in_array($id, $es)) {
                abort(404);
            }
        }

        $today = Carbon::today();
        $month = date('n', strtotime($today));
        $year = date('Y', strtotime($today));

        return view('pages.shifts.generate',
            compact('employees', 'month', 'year', 'employeesId')
        );
    }

    public function generateShiftsSave(GenerateShiftRequests $request)
    {
        $creator = Auth::user()->employee->id;
        $employees = Employee::findOrFail($request->employee_id);

        $employeeIds = implode('-', $request->employee_id);

        $month = $request->month;
        $year = $request->year;
        $shift_from = $request->shift_from;
        $shift_to = $request->shift_to;

        // check if shift already exists
        $es = [];
        foreach ($employees as $employee):
            $shiftBrief = ShiftBrief::query()
                ->where('employee_id', $employee->id)
                ->where('month', $month)
                ->where('year', $year)
                ->first();

            if ($shiftBrief) {
                array_push($es, $employee->user->name() . ' already have a shift for the month of ' . date('F', mktime(0, 0, 0, $month, 1, $year)));
            }
        endforeach;

        if (count($es) > 0) {
            return redirect(url('shifts/generate/' . $employeeIds))->withErrors($es);
        }

        foreach ($employees as $employee):

            // create shift brief
            $shiftBrief = ShiftBrief::createShiftBrief($employee->id, $request);

            // create shift
            for ($i=1; $i <= $request->days_count; $i++):

                if ($request->dayoff != null) {
                    if (in_array($i, $request->dayoff)) {
                        continue;
                    }
                }

                $from = explode(':', $shift_from);
                $to = explode(':', $shift_to);

                $start = $from[0];
                $hours = 0;
                while ($start != $to[0]) {
                    $start++;
                    if ($start == 25) {
                        $start = 1;
                    }
                    $hours++;
                }

                $shift_start = Carbon::parse($year . '-' . $month . '-' . $i . ' ' . $from[0] . ':00:00');

                Shift::create([
                    'shift_brief_id' => $shiftBrief->id,
                    'shift_from' => $shift_start,
                    'shift_to' => $shift_start->copy()->addHours($hours)
                ]);

            endfor;

        endforeach;

        return redirect()->route('shifts.manage');
    }

    public function drawCalendar($date)
    {
        $dd = explode('-', $date);
        $month = $dd[0];
        $year = $dd[1];

        if (Input::has('shiftBriefId')) {

            $shiftBrief = ShiftBrief::findOrFail(Input::get('shiftBriefId'));
            return response()->view('pages.shifts.calendar-brief', 
                compact('month', 'year', 'shiftBrief')
            );

        }

        return response()->view('pages.shifts.calendar', 
            compact('month', 'year')
        );
    }

    public function submitShift()
    {
        $data = [
            'view' => route('shifts.form', [
                'employee_id' => $this->request->employee_id, 
                'date' => $this->request->date
            ])
        ];
        return json_encode($data);
    }

    public function submitShiftView()
    {
        $employee_id = Input::get('employee_id');
        $from = explode('-', Input::get('date'));
        $vars['fromDate'] = Carbon::create($from[0], $from[1], $from[2], 0, 0, 0);

        $vars['yesterday'] = false;
        if (strtotime($vars['fromDate']) < strtotime(Carbon::today())) {
            $vars['yesterday'] = true;
        }

        $vars['employee'] = Employee::find($employee_id);
        $vars['shift'] = Shift::where('employee_id', $employee_id)
                            ->where('date', $vars['fromDate'])
                            ->first();
                            
        $vars['timecard'] = null;
        if (Input::get('timecard') != '') {
            $vars['timecard'] = Timecard::find(Input::get('timecard'));
        }

        return view('pages.shifts.submit', $vars);
    }

    public function submitShiftViewSave(SaveShiftRequests $request)
    {
        $shift = Shift::scheduleShift($request);
        $shift->save();

        $response = [
            'route' => route('shifts.manage'),
            'shift' => $shift,
            'type' => 'shift'
        ];

        return json_encode($response);
    }

    public function shiftBriefDetails($briefId)
    {
        $shiftBrief = ShiftBrief::findOrFail($briefId);

        if (Gate::denies('view_shifts', Auth::user())) {
            if (Auth::user()->employee->id != $shiftBrief->employee_id) {
                abort(403);
            }
        }

        return view('pages.shifts.brief',
            compact('shiftBrief')
        );
    }

    public function shiftsList($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);

        return view('pages.shifts.list', 
            compact('employee')
        );
    }

    public function deleteShiftBrief($shiftBriefId)
    {
        ShiftBrief::findOrFail($shiftBriefId)->delete();

        Session::flash('status', 'Shift brief delete successfully!');

        $response = [
            'route' => route('shifts.manage')
        ];

        return json_encode($response);
    }

    public function getShiftInfo($shiftId, $employeeId, $shiftBriefId)
    {
        $now = Carbon::now();
        $employee = Employee::findOrFail($employeeId);

        $date = '';
        if (Input::has('d')) {
            $date = Input::get('d');
            $shiftDate = Carbon::parse($date);
        }
        if ($shiftId == 0) {
            $shift = null;  
            $duration = 'Rest Day';
        } else {
            $shift = Shift::findOrFail($shiftId);
            $duration = processDate($shift->shift_from, true) . ' &mdash; ' . processDate($shift->shift_to, true);
        }

        if ($shiftDate < $now) {

            $response = [
                'title' => 'Sorry...',
                'view' => '<div class="ta-center">Unable to edit this shift.</div>',
                'className' => 'modal-warning',
                'past' => true
            ];

        } else {

            $response = [
                'title' => $employee->name() . '<span>' . $duration . '</span>',
                'view' => view('pages.shifts.modal-shift-modify', compact(
                    'shift', 'shiftBriefId', 'date'
                ))->render(),
                'past' => false
            ];

        }

        return json_encode($response);
    }

    public function updateShift($shiftId)
    {
        $shift = Shift::findOrFail($shiftId);

        $shiftFrom = Carbon::parse($this->request->shift_from_date . ' ' . $this->request->shift_from_time);
        $diff = getTimeDifference($this->request->shift_from_time, $this->request->shift_to_time);
        $shiftTo = $shiftFrom->copy()->addHours($diff);

        $shift->shift_from = $shiftFrom;
        $shift->shift_to = $shiftTo;
        $shift->is_rd = $this->request->is_rd == null ? 0 : 1;
        $shift->update();

        $shiftBriefId = $shift->shift_brief_id;

        $response = [
            'redirect' => route('shifts.brief', ['briefId' => $shiftBriefId])
        ];

        return json_encode($response);
    }

    public function deleteShift($shiftId)
    {
        $shift = Shift::findOrFail($shiftId);
        $shiftBriefId = $shift->shift_brief_id;
        $shift->delete();

        $response = [
            'redirect' => route('shifts.brief', ['briefId' => $shiftBriefId])
        ];

        return json_encode($response);
    }

    public function createShift()
    {
        $shiftFrom = Carbon::parse($this->request->shift_from_date . ' ' . $this->request->shift_from_time);
        $diff = getTimeDifference($this->request->shift_from_time, $this->request->shift_to_time);
        $shiftTo = $shiftFrom->copy()->addHours($diff);

        $data = [
            'shift_brief_id' => $this->request->shift_brief_id,
            'shift_from' => $shiftFrom,
            'shift_to' => $shiftTo
        ];
        Shift::create($data);
        
        $response = [
            'redirect' => route('shifts.brief', ['briefId' => $this->request->shift_brief_id])
        ];

        return json_encode($response);
    }

    public function shiftDetails($shiftId)
    {
        $shift = Shift::findOrFail($shiftId);

        $response = [
            'title' => $shift->shiftBrief->employee->name() . '<span>' . processDate($shift->shift_from, true) . ' - '. processDate($shift->shift_to, true) . '</span>',
            'view' => view('pages.shifts.shift-details', compact('shift'))->render()
        ];

        return json_encode($response);
    }

}
