<?php

namespace UnoRenta\Http\Controllers;

use Auth;
use Input;
use Carbon\Carbon;
use UnoRenta\Http\Requests;
use Illuminate\Http\Request;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Department;
use UnoRenta\Models\PayrollBatch;
use UnoRenta\Models\PayslipDeduction;

class ReportController extends Controller
{
    
    public function __construct()
    {
        if (!Auth::user()->allowedTo('view_reports')) {
            abort(403);
        }
    }

    public function getReportDashboard()
    {
        return view('pages.reports.dashboard');
    }

    public function getContributionReport()
    {
        $employees = Employee::query()
            ->orderByLastName()
            ->get();

        return view('pages.reports.contributions.list', compact(
            'employees'
        ));
    }

    public function getContributionReportDetails($premiumType, $employeeId)
    {
        $year = Carbon::now()->year;
        if (Input::has('y')) {
            $year = Input::get('y');    
        }
        
        $employee = Employee::findOrFail($employeeId);

        $years = PayrollBatch::query()
            ->groupBy('year')
            ->select('payroll_batches.year')
            ->get();

        $payslips = $employee->payslips()
            ->join('payroll_batches', 'payslips.payroll_batch_id', '=', 'payroll_batches.id')
            ->where('payroll_batches.year', $year)
            ->select('payslips.*')
            ->get();

        $paymentIds = [];
        foreach ($payslips as $payslip) {
            $payment = $payslip->deductions()->where('key', strtolower($premiumType))->first();
            if ($payment) {
                $paymentIds[$payment->id] = $payment->id;
            }
        }

        $payments = PayslipDeduction::query()
            ->join('payslips', 'payslip_deductions.payslip_id', '=', 'payslips.id')
            ->join('payroll_batches', 'payslips.payroll_batch_id', '=', 'payroll_batches.id')
            ->whereIn('payslip_deductions.id', $paymentIds)
            ->orderBy('payroll_batches.month')
            ->select('payslip_deductions.*')
            ->get();

        return view('pages.reports.contributions.details', compact(
            'years', 'year', 'employee', 'payments', 'premiumType'
        ));
    }

    public function exportContributionsToPDF($premiumType, $year, $employeeId)
    {        
        $employee = Employee::findOrFail($employeeId);

        $years = PayrollBatch::query()
            ->groupBy('year')
            ->select('payroll_batches.year')
            ->get();

        $payslips = $employee->payslips()
            ->join('payroll_batches', 'payslips.payroll_batch_id', '=', 'payroll_batches.id')
            ->where('payroll_batches.year', $year)
            ->select('payslips.*')
            ->get();

        $paymentIds = [];
        foreach ($payslips as $payslip) {
            $payment = $payslip->deductions()->where('key', strtolower($premiumType))->first();
            if ($payment) {
                $paymentIds[$payment->id] = $payment->id;
            }
        }

        $payments = PayslipDeduction::query()
            ->join('payslips', 'payslip_deductions.payslip_id', '=', 'payslips.id')
            ->join('payroll_batches', 'payslips.payroll_batch_id', '=', 'payroll_batches.id')
            ->whereIn('payslip_deductions.id', $paymentIds)
            ->orderBy('payroll_batches.month')
            ->select('payslip_deductions.*')
            ->get();

        $filename = $year . ' ' . strtoupper($premiumType) . ' Contributions of ' . $employee->name();
        $html = view('pages.reports.contributions.pdf', compact(
            'years', 'year', 'employee', 'payments', 'premiumType', 'filename'
        ))->render();

        return toPdf($html, $filename, false);
    }

    public function getWithholdingTaxReport()
    {
        $employees = Employee::query()
            ->orderByLastName()
            ->get();

        return view('pages.reports.withholding-tax.list', compact(
            'employees'
        ));
    }

    public function getWithholdingTaxReportDetails($employeeId)
    {
        $year = Carbon::now()->year;
        if (Input::has('y')) {
            $year = Input::get('y');
        }

        $employee = Employee::findOrFail($employeeId);

        $years = PayrollBatch::query()
            ->groupBy('year')
            ->select('payroll_batches.year')
            ->get();

        $payslips = $employee->payslips()
            ->join('payroll_batches', 'payslips.payroll_batch_id', '=', 'payroll_batches.id')
            ->where('payroll_batches.year', $year)
            ->orderBy('payroll_batches.month')
            ->orderBy('payroll_batches.half')
            ->select('payslips.*')
            ->get();

        return view('pages.reports.withholding-tax.details', compact(
            'years', 'year', 'employee', 'payslips'
        ));
    }

    public function getWithholdingTaxReportToPDF($year, $employeeId)
    {
        $year = Carbon::now()->year;
        if (Input::has('y')) {
            $year = Input::get('y');
        }

        $employee = Employee::findOrFail($employeeId);

        $years = PayrollBatch::query()
            ->groupBy('year')
            ->select('payroll_batches.year')
            ->get();

        $payslips = $employee->payslips()
            ->join('payroll_batches', 'payslips.payroll_batch_id', '=', 'payroll_batches.id')
            ->where('payroll_batches.year', $year)
            ->orderBy('payroll_batches.month')
            ->orderBy('payroll_batches.half')
            ->select('payslips.*')
            ->get();

        $filename = $year . ' Withholding Tax of ' . $employee->name();
        $html = view('pages.reports.withholding-tax.pdf', compact(
            'years', 'year', 'employee', 'payslips', 'filename'
        ))->render();

        return toPdf($html, $filename, false);
    }

    public function getTardinessReport()
    {
        $employees = Employee::query()
            ->orderByLastName()
            ->get();

        $theYear = Carbon::now()->year;
        $theMonth = Carbon::now()->month;
        if (Input::has('y')) {
            $theYear = Input::get('y');
        }
        if (Input::has('m')) {
            $theMonth = Input::get('m');
        }

        $dateFrom = Carbon::create($theYear, $theMonth, 1)->startOfDay();
        $dateTo = $dateFrom->copy()->endOfMonth()->endOfDay();

        return view('pages.reports.tardiness.list', compact(
            'employees', 'dateFrom', 'dateTo', 'theYear', 'theMonth'
        ));
    }

    public function getTardinessReportToPdf($month, $year)
    {
        $employees = Employee::query()
            ->orderByLastName()
            ->get();

        $andy = Employee::findOrFail(4);

        $dateFrom = Carbon::create($year, $month, 1)->startOfDay();
        $dateTo = $dateFrom->copy()->endOfMonth()->endOfDay();

        $filename = 'Tardiness Report - ' . getMonths()[$month - 1] . ' ' . $year;
        $html = view('pages.reports.tardiness.pdf', compact(
            'employees', 'dateFrom', 'dateTo', 'year', 'month', 'filename', 'andy'
        ))->render();

        return toPdf($html, $filename, false);
    }

    public function getAbsencesReport()
    {
        $employees = Employee::query()
            ->orderByLastName()
            ->get();

        $theYear = Carbon::now()->year;
        $theMonth = Carbon::now()->month;
        if (Input::has('y')) {
            $theYear = Input::get('y');
        }
        if (Input::has('m')) {
            $theMonth = Input::get('m');
        }

        $dateFrom = Carbon::create($theYear, $theMonth, 1)->startOfDay();
        $dateTo = $dateFrom->copy()->endOfMonth()->endOfDay();

        return view('pages.reports.absences.list', compact(
            'employees', 'dateFrom', 'dateTo', 'theYear', 'theMonth'
        ));
    }

    public function getAbsencesReportToPdf($month, $year)
    {
        $employees = Employee::query()
            ->orderByLastName()
            ->get();

        $andy = Employee::findOrFail(4);

        $dateFrom = Carbon::create($year, $month, 1)->startOfDay();
        $dateTo = $dateFrom->copy()->endOfMonth()->endOfDay();

        $filename = 'Tardiness Report - ' . getMonths()[$month - 1] . ' ' . $year;
        $html = view('pages.reports.absences.pdf', compact(
            'employees', 'dateFrom', 'dateTo', 'year', 'month', 'filename', 'andy'
        ))->render();

        return toPdf($html, $filename, false);
    }

}
