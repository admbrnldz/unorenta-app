<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if (Auth::check() AND Auth::user()->is_active) {
        return redirect()->route('dashboard');
    } else {
        return view('landing');
    }
});

Route::auth();
Route::get('/logout', 'Auth\AuthController@logout');

Route::get('/testing', function() {
    return view('testing');
});

Route::get('/about', function() {
    return view('about');
});

Route::get('/inactive', function() {
    if (Auth::check()) {
        return redirect()->route('dashboard');
    }
    return view('pages.accounts.inactive');
});

Route::get('/dashboard', [
    'as' => 'dashboard',
    'uses' => 'HomeController@getDashboard'
]);

/**
 * --------------------------------------------------------------------------
 * Account
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'account'], function () {

    Route::get('/settings/{userId?}', [
        'as' => 'account.settings',
        'uses' => 'AccountController@getSettings',
    ]);

    Route::post('/settings/save', [
        'as' => 'account.settings.save',
        'uses' => 'AccountController@saveSettings',
    ]);

    Route::get('/checkusername', [
        'as' => 'account.check.username',
        'uses' => 'AccountController@checkUsername',
    ]);

    Route::post('/uploaddp/{userId?}', [
        'as' => 'account.uploaddp',
        'uses' => 'AccountController@uploadDisplayPhoto',
    ]);

    Route::get('/removedp/{userID?}', [
        'as' => 'account.removedp',
        'uses' => 'AccountController@removeDisplayPhoto',
    ]);

    Route::post('/update-permissions/{userId?}', [
        'as' => 'account.update.permissions',
        'uses' => 'AccountController@updatePermissions',
    ]);

    Route::post('/deactivate/{userID?}', [
        'as' => 'account.deactivate',
        'uses' => 'AccountController@deactivateAccount',
    ]);
    Route::post('/reactivate/{userID?}', [
        'as' => 'account.reactivate',
        'uses' => 'AccountController@reactivateAccount',
    ]);

    Route::post('/update-tone/{userID?}', [
        'as' => 'account.updatetone',
        'uses' => 'AccountController@updateTone',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Notifications
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'notification'], function () {

    Route::get('/check', [
        'as' => 'notification.check',
        'uses' => 'NotificationController@checkNotifications',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Departments
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'departments'], function () {

    Route::get('/', [
        'as' => 'departments',
        'uses' => 'DepartmentController@viewDepartments',
    ]);

    Route::get('/create', [
        'as' => 'departments.create',
        'uses' => 'DepartmentController@createDepartment',
    ]);

    Route::post('/create', [
        'as' => 'departments.create',
        'uses' => 'DepartmentController@createDepartmentSave',
    ]);

    Route::get('/edit/{departmentId?}', [
        'as' => 'departments.edit',
        'uses' => 'DepartmentController@editDepartment',
    ]);

    Route::post('/edit/{departmentId?}', [
        'as' => 'departments.edit',
        'uses' => 'DepartmentController@editDepartmentSave',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Positions
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'positions'], function () {

    Route::get('/add', [
        'as' => 'positions.add',
        'uses' => 'PositionController@addPosition',
    ]);

    Route::post('/add', [
        'as' => 'positions.add',
        'uses' => 'PositionController@addPositionSave',
    ]);

    Route::get('/edit/{positionId?}', [
        'as' => 'positions.edit',
        'uses' => 'PositionController@editPosition',
    ]);

    Route::post('/edit/{positionId?}', [
        'as' => 'positions.edit',
        'uses' => 'PositionController@editPositionSave',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Employees
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'employees'], function () {

    Route::get('/', [
        'as' => 'employees',
        'uses' => 'EmployeeController@viewEmployees',
    ]);

    Route::get('/profile/{employeeId?}', [
        'as' => 'employees.profile',
        'uses' => 'EmployeeController@viewEmployeeProfile',
    ]);

    Route::get('/add', [
        'as' => 'employees.add',
        'uses' => 'EmployeeController@addEmployee',
    ]);

    Route::post('/add', [
        'as' => 'employees.add',
        'uses' => 'EmployeeController@addEmployeeSave',
    ]);

    Route::get('/update-personal-info/{employeeId?}', [
        'as' => 'employees.update.personal-info',
        'uses' => 'EmployeeController@updatePersonalInfo',
    ]);
    Route::post('/update-personal-info/{employeeId?}', [
        'as' => 'employees.update.personal-info',
        'uses' => 'EmployeeController@updatePersonalInfoSave',
    ]);

    Route::get('/add-direct-family/{employeeId?}', [
        'as' => 'employees.add.direct-family',
        'uses' => 'EmployeeController@addDirectFamily',
    ]);
    Route::post('/add-direct-family', [
        'as' => 'employees.add.direct-family',
        'uses' => 'EmployeeController@addDirectFamilySave',
    ]);

    Route::get('/update-direct-family/{familyId?}', [
        'as' => 'employees.update.direct-family',
        'uses' => 'EmployeeController@updateDirectFamily',
    ]);
    Route::post('/update-direct-family/{familyId?}', [
        'as' => 'employees.update.direct-family',
        'uses' => 'EmployeeController@updateDirectFamilySave',
    ]);

    Route::get('/delete-direct-family/{familyId?}', [
        'as' => 'employees.delete.direct-family',
        'uses' => 'EmployeeController@deleteDirectFamily',
    ]);

    Route::get('/update-emergency/{employeeId?}', [
        'as' => 'employees.update.emergency',
        'uses' => 'EmployeeController@updateEmergency',
    ]);
    Route::post('/update-emergency/{employeeId?}', [
        'as' => 'employees.update.emergency',
        'uses' => 'EmployeeController@updateEmergencySave',
    ]);

    Route::get('/update-employmentdetails/{employeeId?}', [
        'as' => 'employees.update.employmentdetails',
        'uses' => 'EmployeeController@updateEmploymentDetails',
    ]);
    Route::post('/update-employmentdetails/{employeeId?}', [
        'as' => 'employees.update.employmentdetails',
        'uses' => 'EmployeeController@updateEmploymentDetailsSave',
    ]);

    Route::get('/fetch', [
        'as' => 'employees.fetch',
        'uses' => 'EmployeeController@fetchEmployees',
    ]);

    Route::get('/regularize/{employeeId?}', [
        'as' => 'employees.regularize',
        'uses' => 'EmployeeController@regularize',
    ]);
    Route::post('/regularize/{employeeId?}', [
        'as' => 'employees.regularize',
        'uses' => 'EmployeeController@regularizeSave',
    ]);

    Route::get('/resign/{employeeId?}', [
        'as' => 'employees.resign',
        'uses' => 'EmployeeController@resign',
    ]);

    Route::post('/upload-signature/{employeeId?}', [
        'as' => 'employees.uploadsignature',
        'uses' => 'EmployeeController@uploadSignature',
    ]);
    Route::get('/remove-signature/{employeeId?}', [
        'as' => 'employees.removesignature',
        'uses' => 'EmployeeController@removeSignature',
    ]);

    Route::get('/rates', [
        'as' => 'employees.rates',
        'uses' => 'EmployeeController@employeesRates',
    ]);

    Route::get('/rates/update/{employeeId?}', [
        'as' => 'employees.updaterate',
        'uses' => 'EmployeeController@updateRate',
    ]);
    Route::post('/rates/update/{employeeId?}', [
        'as' => 'employees.updaterate',
        'uses' => 'EmployeeController@updateRateSave',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Dependents
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'dependents'], function () {

    Route::get('/add/{employeeId?}', [
        'as' => 'dependent.add',
        'uses' => 'EmployeeController@addDependent',
    ]);
    Route::post('/add', [
        'as' => 'dependent.add',
        'uses' => 'EmployeeController@addDependentSave',
    ]);

    Route::get('/update/{dependentId?}', [
        'as' => 'dependent.update',
        'uses' => 'EmployeeController@updateDependent',
    ]);
    Route::post('/update/{dependentId?}', [
        'as' => 'dependent.update',
        'uses' => 'EmployeeController@updateDependentSave',
    ]);

    Route::get('/delete/{dependentId?}', [
        'as' => 'dependent.delete',
        'uses' => 'EmployeeController@deleteDependent',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Education
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'education'], function () {

    Route::get('/add/{employeeId?}', [
        'as' => 'education.add',
        'uses' => 'EmployeeController@addEducation',
    ]);
    Route::post('/add', [
        'as' => 'education.add',
        'uses' => 'EmployeeController@addEducationSave',
    ]);

    Route::get('/update/{educationId?}', [
        'as' => 'education.update',
        'uses' => 'EmployeeController@updateEducation',
    ]);
    Route::post('/update', [
        'as' => 'education.update',
        'uses' => 'EmployeeController@updateEducationSave',
    ]);

    Route::get('/delete/{educationId?}', [
        'as' => 'education.delete',
        'uses' => 'EmployeeController@deleteEducation',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Shifts
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'shifts'], function () {

    Route::get('/', [
        'as' => 'shifts',
        'uses' => 'ShiftController@viewShifts',
    ]);

    Route::get('/manage', [
        'as' => 'shifts.manage',
        'uses' => 'ShiftController@manageShifts',
    ]);

    Route::get('/generate/{employeeId?}', [
        'as' => 'shifts.generate',
        'uses' => 'ShiftController@generateShifts'
    ]);    

    Route::post('/generate', [
        'as' => 'shifts.generate',
        'uses' => 'ShiftController@generateShiftsSave'
    ]);

    Route::get('/drawcalendar/{date?}', [
        'as' => 'shifts.drawcalendar',
        'uses' => 'ShiftController@drawCalendar'
    ]);

    Route::post('/form', [
        'as' => 'shifts.form',
        'uses' => 'ShiftController@submitShift',
    ]);

    Route::get('/form', [
        'as' => 'shifts.form',
        'uses' => 'ShiftController@submitShiftView',
    ]);

    Route::post('/form', [
        'as' => 'shifts.form',
        'uses' => 'ShiftController@submitShiftViewSave',
    ]);

    Route::get('/brief/{briefId?}', [
        'as' => 'shifts.brief',
        'uses' => 'ShiftController@shiftBriefDetails'
    ]);

    Route::get('/brief/delete/{briefId?}', [
        'as' => 'shifts.brief.delete',
        'uses' => 'ShiftController@deleteShiftBrief'
    ]);

    Route::get('/list/{employeeId?}', [
        'as' => 'shifts.list',
        'uses' => 'ShiftController@shiftsList',
    ]);

    Route::get('/i/{shiftId?}/{employeeId?}/{shiftBriefId?}', [
        'as' => 'shifts.getinfo',
        'uses' => 'ShiftController@getShiftInfo',
    ]);

    Route::post('/update/{shiftId?}', [
        'as' => 'shifts.update',
        'uses' => 'ShiftController@updateShift',
    ]);

    Route::post('/delete/{shiftId?}', [
        'as' => 'shifts.delete',
        'uses' => 'ShiftController@deleteShift',
    ]);

    Route::post('/create', [
        'as' => 'shifts.create',
        'uses' => 'ShiftController@createShift',
    ]);

    Route::get('/details/{shiftIf?}', [
        'as' => 'shifts.details',
        'uses' => 'ShiftController@shiftDetails',
    ]);
    
});

/**
 * --------------------------------------------------------------------------
 * Timecards
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'timecards'], function () {

    Route::post('/timein', [
        'as' => 'timecards.timein',
        'uses' => 'TimecardController@timeIn',
    ]);

    Route::post('/timeout', [
        'as' => 'timecards.timeout',
        'uses' => 'TimecardController@timeOut',
    ]);

    Route::post('/present', [
        'as' => 'timecards.present',
        'uses' => 'TimecardController@presentTimecard',
    ]);

    Route::post('/late', [
        'as' => 'timecards.late',
        'uses' => 'TimecardController@lateTimecard',
    ]);

    Route::get('/approval', [
        'as' => 'timecards.approval',
        'uses' => 'TimecardController@getForApprovalTimecards',
    ]);

    Route::post('/approve/timein/{timecardId?}', [
        'as' => 'timecards.approvetimein',
        'uses' => 'TimecardController@approveTimeIn',
    ]);

    Route::post('/approve/timeout/{timecardId?}', [
        'as' => 'timecards.approvetimeout',
        'uses' => 'TimecardController@approveTimeOut',
    ]);

    Route::post('/break/{paramId?}', [
        'as' => 'timecards.break',
        'uses' => 'TimecardController@takeBreak',
    ]);

    Route::get('/absent/{shiftId?}', [
        'as' => 'timecards.absent',
        'uses' => 'TimecardController@absentTimecard',
    ]);
    Route::post('/absent/{shiftId?}', [
        'as' => 'timecards.absent',
        'uses' => 'TimecardController@absentTimecardSave',
    ]);

    Route::get('/modify/{timecardId?}', [
        'as' => 'timecards.modify',
        'uses' => 'TimecardController@modifyTimecard',
    ]);
    Route::post('/modify/{timecardId?}', [
        'as' => 'timecards.modify',
        'uses' => 'TimecardController@modifyTimecardSave',
    ]);

    Route::get('/all', [
        'as' => 'timecards.all',
        'uses' => 'TimecardController@allTimecards',
    ]);

    Route::get('/modify/to/{timecardId?}', [
        'as' => 'timecards.modifytimeout',
        'uses' => 'TimecardController@modifyTimeOut',
    ]);
    Route::post('/modify/to/{timecardId?}', [
        'as' => 'timecards.modifytimeout',
        'uses' => 'TimecardController@modifyTimeOutSave',
    ]);

    Route::get('/reject/to/{timecardId?}', [
        'as' => 'timecards.rejecttimeout',
        'uses' => 'TimecardController@rejectTimeOut',
    ]);

    Route::get('/modify/ti/{timecardId?}', [
        'as' => 'timecards.modifytimein',
        'uses' => 'TimecardController@modifyTimeIn',
    ]);
    Route::post('/modify/ti/{timecardId?}', [
        'as' => 'timecards.modifytimein',
        'uses' => 'TimecardController@modifyTimeInSave',
    ]);

    Route::get('/reject/ti/{timecardId?}', [
        'as' => 'timecards.rejecttimein',
        'uses' => 'TimecardController@rejectTimeIn',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Leave
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'loa'], function () {

    Route::get('/', [
        'as' => 'loa.dashboard',
        'uses' => 'LeaveController@getDashboard',
    ]);

    Route::get('/signed', [
        'as' => 'loa.signed',
        'uses' => 'LeaveController@signedLeaveApplications',
    ]);

    Route::get('/file', [
        'as' => 'loa.file',
        'uses' => 'LeaveController@fileLeave',
    ]);

    Route::post('/file', [
        'as' => 'loa.file',
        'uses' => 'LeaveController@fileLeaveSave',
    ]);

    Route::post('/checkcredits', [
        'as' => 'loa.checkcredits',
        'uses' => 'LeaveController@getLeaveCredits',
    ]);

    Route::get('/details/{loaId}', [
        'as' => 'loa.details',
        'uses' => 'LeaveController@detailsLeave',
    ]);

    Route::post('/approve/{loaId}', [
        'as' => 'loa.approve',
        'uses' => 'LeaveController@approveLeave',
    ]);
    
    Route::post('/approvewp/{loaId}', [
        'as' => 'loa.approvewp',
        'uses' => 'LeaveController@approveWithoutPayLeave',
    ]);

    Route::get('/disapprove/{loaId}', [
        'as' => 'loa.disapprove',
        'uses' => 'LeaveController@disapproveLeave',
    ]);
    Route::post('/disapprove/{loaId?}', [
        'as' => 'loa.disapprove',
        'uses' => 'LeaveController@disapproveLeaveSave',
    ]);

    Route::post('/sign/{approver?}/{status?}/{loaId?}', [
        'as' => 'loa.sign',
        'uses' => 'LeaveController@sign',
    ]);

    Route::post('/comment/add', [
        'as' => 'loa.comment.add',
        'uses' => 'LeaveController@addComment',
    ]);

    Route::get('/entitlements', [
        'as' => 'loa.entitlements',
        'uses' => 'LeaveController@leaveEntitlements',
    ]);

    Route::get('/type/add', [
        'as' => 'loa.type.add',
        'uses' => 'LeaveController@addLeaveType',
    ]);
    Route::post('/type/add', [
        'as' => 'loa.type.add',
        'uses' => 'LeaveController@addLeaveTypeSave',
    ]);

    Route::get('/type/edit/{leaveTypeId?}', [
        'as' => 'loa.type.edit',
        'uses' => 'LeaveController@editLeaveType',
    ]);
    Route::post('/type/edit/{leaveTypeId?}', [
        'as' => 'loa.type.edit',
        'uses' => 'LeaveController@editLeaveTypeSave',
    ]);

    Route::get('/type/delete/{leaveTypeId?}', [
        'as' => 'loa.type.delete',
        'uses' => 'LeaveController@deleteLeaveType',
    ]);

    Route::get('/entitle-leave/{employeeId?}', [
        'as' => 'loa.entitle',
        'uses' => 'LeaveController@entitleLeave',
    ]);
    Route::post('/entitle-leave/{employeeId?}', [
        'as' => 'loa.entitle',
        'uses' => 'LeaveController@entitleLeaveSave',
    ]);

    Route::get('/entitlements/list/{employeeId?}', [
        'as' => 'loa.entitlements.list',
        'uses' => 'LeaveController@entitledLeavesList',
    ]);

    Route::get('/entitlements/remove/{entitlementId?}', [
        'as' => 'loa.remove-entitled-leave',
        'uses' => 'LeaveController@removeEntitledLeave',
    ]);

    Route::get('/approvers', [
        'as' => 'loa.approvers',
        'uses' => 'LeaveController@leaveApprovers',
    ]);

    Route::get('/approvers/add', [
        'as' => 'loa.approvers.add',
        'uses' => 'LeaveController@addLeaveApprover',
    ]);
    Route::post('/approvers/add', [
        'as' => 'loa.approvers.add',
        'uses' => 'LeaveController@addLeaveApproverSave',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Holidays
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'holidays'], function () {

    Route::get('/', [
        'as' => 'holidays.dashboard',
        'uses' => 'HolidayController@getHolidays',
    ]);

    Route::get('/add', [
        'as' => 'holidays.add',
        'uses' => 'HolidayController@addHoliday',
    ]);

    Route::post('/add', [
        'as' => 'holidays.add',
        'uses' => 'HolidayController@addHolidaySave',
    ]);

    Route::get('/details/{holidayId?}', [
        'as' => 'holidays.details',
        'uses' => 'HolidayController@viewDetails',
    ]);

    Route::get('/delete/{holidayId}', [
        'as' => 'holidays.delete',
        'uses' => 'HolidayController@deleteHoliday',
    ]);

    Route::get('/edit/{holidayId?}', [
        'as' => 'holidays.edit',
        'uses' => 'HolidayController@editHoliday',
    ]);

    Route::post('/edit/{holidayId?}', [
        'as' => 'holidays.edit',
        'uses' => 'HolidayController@editHolidaySave',
    ]);

    Route::post('/comment/add', [
        'as' => 'holidays.comment.add',
        'uses' => 'HolidayController@addComment',
    ]);

    Route::get('/past-holidays', [
        'as' => 'holidays.past',
        'uses' => 'HolidayController@pastHolidays',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Memoranda
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'memos'], function () {

    Route::get('/', [
        'as' => 'memos',
        'uses' => 'MemoController@viewMemos',
    ]);

    Route::get('/broadcast', [
        'as' => 'memos.broadcast',
        'uses' => 'MemoController@broadcastMemo',
    ]);
    Route::post('/broadcast', [
        'as' => 'memos.broadcast',
        'uses' => 'MemoController@broadcastMemoSave',
    ]);

    Route::get('/details/{memoId?}', [
        'as' => 'memos.details',
        'uses' => 'MemoController@viewMemoDetails',
    ]);

    Route::post('/sign/{memoId?}', [
        'as' => 'memos.sign',
        'uses' => 'MemoController@sign',
    ]);

    Route::post('/conform', [
        'as' => 'memos.conform',
        'uses' => 'MemoController@conform',
    ]);

    Route::post('/comment/add', [
        'as' => 'memos.comment.add',
        'uses' => 'MemoController@addComment',
    ]);

    Route::get('/edit/{memoId?}', [
        'as' => 'memos.edit',
        'uses' => 'MemoController@editMemo',
    ]);

    Route::post('/edit/{memoId?}', [
        'as' => 'memos.edit',
        'uses' => 'MemoController@editMemoSave',
    ]);

    Route::post('/addrecipient/{memoId?}', [
        'as' => 'memos.addrecipient',
        'uses' => 'MemoController@addRecipient',
    ]);

    Route::get('/removerecipient/{recipientId?}', [
        'as' => 'memos.removerecipient',
        'uses' => 'MemoController@removeRecipient',
    ]);

    Route::get('/delete/{memoId?}', [
        'as' => 'memos.delete',
        'uses' => 'MemoController@deleteMemo',
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Accounting
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'accounting'], function () {

    Route::get('/', [
        'as' => 'accounting.dashboard',
        'uses' => 'AccountingController@getDashboard'
    ]);


    /**
    * Payroll
    */
    Route::get('/payroll', [
        'as' => 'payroll.dashboard',
        'uses' => 'PayrollController@payrollDashboard'
    ]);

    Route::get('/payroll/generate', [
        'as' => 'payroll.generate',
        'uses' => 'PayrollController@generatePayroll'
    ]);
    
    Route::post('/payroll/generate', [
        'as' => 'payroll.generate',
        'uses' => 'PayrollController@generatePayrollSave'
    ]);

    Route::get('/payroll/batch', [
        'as' => 'payroll.batch',
        'uses' => 'PayrollController@batch'
    ]);

    Route::post('/payroll/batch/create', [
        'as' => 'payroll.batch.create',
        'uses' => 'PayrollController@createPayrollBatch'
    ]);

    Route::get('/payroll/fetch-timecards', [
        'as' => 'payroll.fetch-timecards',
        'uses' => 'PayrollController@fetchTimecards'
    ]);

    Route::get('/payroll/batch/details/{batchId?}', [
        'as' => 'payroll.batch.details',
        'uses' => 'PayrollController@batchDetails'
    ]);

    Route::get('/payroll/batch/pdf/{batchId?}', [
        'as' => 'payroll.batch.pdf',
        'uses' => 'PayrollController@payrollBatchToPDF'
    ]);

    /**
    * Deductions
    */
    Route::get('/deductions', [
        'as' => 'deductions',
        'uses' => 'AccountingController@usersDeductions'
    ]);

    Route::get('/payroll/deductions', [
        'as' => 'payroll.deductions',
        'uses' => 'AccountingController@deductions'
    ]);

    Route::get('/payroll/deductions/add', [
        'as' => 'payroll.deductions.add',
        'uses' => 'AccountingController@addScheduledDeduction'
    ]);
    Route::post('/payroll/deductions/add', [
        'as' => 'payroll.deductions.add',
        'uses' => 'AccountingController@addScheduledDeductionSave'
    ]);

    Route::get('/payroll/deductions/edit/{deductionId?}', [
        'as' => 'payroll.deductions.edit',
        'uses' => 'AccountingController@editDeduction'
    ]);

    Route::get('/payroll/deductions/details/{deductionId?}', [
        'as' => 'payroll.deductions.detils',
        'uses' => 'AccountingController@deductionDetails'
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Payslip
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'payslips'], function () {

    Route::get('/', [
        'as' => 'payslips',
        'uses' => 'PayslipController@getPayslips'
    ]);

    Route::get('/details/{payslipId?}', [
        'as' => 'payslips.details',
        'uses' => 'PayslipController@payslipDetails'
    ]);

    Route::get('/details/timecards/{payslipId?}', [
        'as' => 'payslips.timecards',
        'uses' => 'PayslipController@payslipTimecards'
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Applicants
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'applicants'], function () {

    Route::get('/', [
        'as' => 'applicants',
        'uses' => 'ApplicantController@getApplicants'
    ]);

    Route::get('/details/{applicantId?}', [
        'as' => 'applicants.details',
        'uses' => 'ApplicantController@applicantDetails'
    ]);

    Route::get('/quickview/{applicantId?}', [
        'as' => 'applicants.quickview',
        'uses' => 'ApplicantController@applicantQuickView'
    ]);

    Route::get('/add', [
        'as' => 'applicants.add',
        'uses' => 'ApplicantController@addApplicant'
    ]);
    Route::post('/add', [
        'as' => 'applicants.add',
        'uses' => 'ApplicantController@addApplicantSave'
    ]);

    Route::get('/edit/{applicantsId?}', [
        'as' => 'applicants.edit',
        'uses' => 'ApplicantController@editApplicant'
    ]);
    Route::post('/edit/{applicantsId?}', [
        'as' => 'applicants.edit',
        'uses' => 'ApplicantController@editApplicantSave'
    ]);

    Route::get('/delete/{applicantsId?}', [
        'as' => 'applicants.delete',
        'uses' => 'ApplicantController@deleteApplicant'
    ]);

    Route::get('/interview/{applicantsId?}', [
        'as' => 'applicants.interview',
        'uses' => 'ApplicantController@interviewApplicant'
    ]);
    Route::post('/interview/{applicantsId?}', [
        'as' => 'applicants.interview',
        'uses' => 'ApplicantController@interviewApplicantSave'
    ]);

    Route::get('/interview/details/{interviewId?}', [
        'as' => 'applicants.interview.details',
        'uses' => 'ApplicantController@interviewDetails'
    ]);

    Route::get('/hire/{applicantId?}', [
        'as' => 'applicants.hire',
        'uses' => 'ApplicantController@hireApplicant'
    ]);
    Route::post('/hire/{applicantId?}', [
        'as' => 'applicants.hire',
        'uses' => 'ApplicantController@hireApplicantSave'
    ]);

    Route::get('/documents/{applicantId?}', [
        'as' => 'applicants.documents',
        'uses' => 'ApplicantController@applicantDocuments'
    ]);

    Route::get('/documents/upload/{applicantId?}', [
        'as' => 'applicants.documents.upload',
        'uses' => 'ApplicantController@uploadDocument'
    ]);
    Route::post('/documents/upload/{applicantId?}', [
        'as' => 'applicants.documents.upload',
        'uses' => 'ApplicantController@uploadDocumentSave'
    ]);

    Route::get('/documents/edit/{documentId?}', [
        'as' => 'applicants.documents.edit',
        'uses' => 'ApplicantController@editDocument'
    ]);
    Route::post('/documents/edit/{documentId?}', [
        'as' => 'applicants.documents.edit',
        'uses' => 'ApplicantController@editDocumentSave'
    ]);

    Route::get('/documents/remove/{documentId?}', [
        'as' => 'applicants.documents.remove',
        'uses' => 'ApplicantController@removeDocument'
    ]);

    Route::get('/flag/{applicantId?}', [
        'as' => 'applicants.flag',
        'uses' => 'ApplicantController@flagApplicant'
    ]);
    Route::post('/flag/{applicantId?}', [
        'as' => 'applicants.flag',
        'uses' => 'ApplicantController@flagApplicantSave'
    ]);

    Route::get('/unflag/{applicantId?}', [
        'as' => 'applicants.unflag',
        'uses' => 'ApplicantController@unflagApplicant'
    ]);

});

/**
 * --------------------------------------------------------------------------
 * Assessment / Assessment Guide
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'assessments'], function () {

    Route::get('/', [
        'as' => 'assessments',
        'uses' => 'ApplicantController@getAssessments'
    ]);

    Route::post('/assessment-guide/update', [
        'as' => 'assessments.assessmentguide.update',
        'uses' => 'ApplicantController@updateInterviewQuestions'
    ]);

    Route::get('/category/add', [
        'as' => 'assessments.category.add',
        'uses' => 'ApplicantController@addAssessmentCategory'
    ]);
    Route::post('/category/add', [
        'as' => 'assessments.category.add',
        'uses' => 'ApplicantController@addAssessmentCategorySave'
    ]);

    Route::get('/category/edit/{categoryId?}', [
        'as' => 'assessments.category.edit',
        'uses' => 'ApplicantController@editAssessmentCategory'
    ]);
    Route::post('/category/edit/{categoryId?}', [
        'as' => 'assessments.category.edit',
        'uses' => 'ApplicantController@editAssessmentCategorySave'
    ]);

    Route::get('/category/delete/{categoryId?}', [
        'as' => 'assessments.category.delete',
        'uses' => 'ApplicantController@deleteAssessmentCategory'
    ]);

    Route::get('/assessment-guide/add', [
        'as' => 'assessments.assessmentguide.add',
        'uses' => 'ApplicantController@addAssessmentGuideQuestion'
    ]);
    Route::post('/assessment-guide/add', [
        'as' => 'assessments.assessmentguide.add',
        'uses' => 'ApplicantController@addAssessmentGuideQuestionSave'
    ]);

    Route::get('/assessment-guide/edit/{assessmentId?}', [
        'as' => 'assessments.assessmentguide.edit',
        'uses' => 'ApplicantController@editAssessmentGuideQuestion'
    ]);
    Route::post('/assessment-guide/edit/{assessmentId?}', [
        'as' => 'assessments.assessmentguide.edit',
        'uses' => 'ApplicantController@editAssessmentGuideQuestionSave'
    ]);

    Route::get('/assessment-guide/delete/{assessmentId?}', [
        'as' => 'assessments.assessmentguide.delete',
        'uses' => 'ApplicantController@deleteAssessmentGuideQuestion'
    ]);

    Route::post('/post-assessment-guide/update', [
        'as' => 'assessments.postassessmentguide.update',
        'uses' => 'ApplicantController@updatePostAssessmentQuestionsOrder'
    ]);

    Route::get('/post-assessment-guide/add', [
        'as' => 'assessments.postassessmentguide.add',
        'uses' => 'ApplicantController@addPostAssessmentGuideQuestion'
    ]);
    Route::post('/post-assessment-guide/add', [
        'as' => 'assessments.postassessmentguide.add',
        'uses' => 'ApplicantController@addPostAssessmentGuideQuestionSave'
    ]);

    Route::get('/post-assessment-guide/edit/{assessmentId?}', [
        'as' => 'assessments.postassessmentguide.edit',
        'uses' => 'ApplicantController@editPostAssessmentGuideQuestion'
    ]);
    Route::post('/post-assessment-guide/edit/{assessmentId?}', [
        'as' => 'assessments.postassessmentguide.edit',
        'uses' => 'ApplicantController@editPostAssessmentGuideQuestionSave'
    ]);

    Route::get('/post-assessment-guide/delete/{assessmentId?}', [
        'as' => 'assessments.postassessmentguide.delete',
        'uses' => 'ApplicantController@deletePostAssessmentGuideQuestion'
    ]);

});


/**
 * --------------------------------------------------------------------------
 * Assessment / Assessment Guide
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'reports'], function () {

    Route::get('/', [
        'as' => 'reports.dashboard',
        'uses' => 'ReportController@getReportDashboard'
    ]);

    Route::get('/contributions', [
        'as' => 'reports.contributions',
        'uses' => 'ReportController@getContributionReport'
    ]);

    Route::get('/contributions/{contributionType}/{employeeId}', [
        'as' => 'reports.contributions.details',
        'uses' => 'ReportController@getContributionReportDetails'
    ]);

    Route::get('/contributions/pdf/{contributionType}/{year}/{employeeId}', [
        'as' => 'reports.contributions.pdf',
        'uses' => 'ReportController@exportContributionsToPDF'
    ]);

    Route::get('/withholding-tax', [
        'as' => 'reports.withholdingtax',
        'uses' => 'ReportController@getWithholdingTaxReport'
    ]);

    Route::get('/withholding-tax/{employeeId}', [
        'as' => 'reports.withholdingtax.details',
        'uses' => 'ReportController@getWithholdingTaxReportDetails'
    ]);

    Route::get('/withholding-tax/pdf/{year}/{employeeId}', [
        'as' => 'reports.withholdingtax.pdf',
        'uses' => 'ReportController@getWithholdingTaxReportToPDF'
    ]);

    Route::get('/tardiness', [
        'as' => 'reports.tardiness',
        'uses' => 'ReportController@getTardinessReport'
    ]);

    Route::get('/tardiness/pdf/{month}/{year}', [
        'as' => 'reports.tardiness.pdf',
        'uses' => 'ReportController@getTardinessReportToPdf'
    ]);

    Route::get('/absences', [
        'as' => 'reports.absences',
        'uses' => 'ReportController@getAbsencesReport'
    ]);

    Route::get('/absences/pdf/{month}/{year}', [
        'as' => 'reports.absences.pdf',
        'uses' => 'ReportController@getAbsencesReportToPdf'
    ]);

});


/**
 * --------------------------------------------------------------------------
 * Long Polling
 * --------------------------------------------------------------------------
 */
Route::group(['prefix' => 'longpoll'], function () {

    /**
    * Memo
    */
    Route::get('/memo-comments/{memoId?}', [
        'as' => 'longpoll.memocomments',
        'uses' => 'LongpollController@memoComments'
    ]);
    Route::get('/memo-seen-count/{memoId?}', [
        'as' => 'longpoll.memoseencount',
        'uses' => 'LongpollController@memoSeenCount'
    ]);
    Route::get('/memo-conform-count/{memoId?}', [
        'as' => 'longpoll.memoconformcount',
        'uses' => 'LongpollController@memoConformCount'
    ]);

    Route::get('/leave-comments/{loaId?}', [
        'as' => 'longpoll.leavecomments',
        'uses' => 'LongpollController@leaveComments'
    ]);

    Route::get('/holiday-comments/{holidayId?}', [
        'as' => 'longpoll.holidaycomments',
        'uses' => 'LongpollController@holidayComments'
    ]);

});