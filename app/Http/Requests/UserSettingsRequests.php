<?php

namespace UnoRenta\Http\Requests;

use UnoRenta\Http\Requests\Request;
use Auth;
use Gate;

class UserSettingsRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $first_name = 'required';
        $last_name = 'required';
        $is_active = 'required';

        if (Gate::denies('edit_account_settings', Auth::user())) {
            $first_name = '';
            $last_name = '';
            $is_active = '';
        }

        return [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => 'required',
            'is_active' => $is_active
        ];
        
    }
}
