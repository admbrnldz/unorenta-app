<?php

namespace UnoRenta\Http\Requests;

use UnoRenta\Http\Requests\Request;
use Auth;

class AddLeaveCommentRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'leave_application_id' => 'required',
            'comment' => 'required'
        ];
    }
}
