<?php

namespace UnoRenta\Http\Requests;

use Auth;
use UnoRenta\Http\Requests\Request;

class PersonalInformationRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required',
            'city' => 'required',
            'province' => 'required',
            'zip_code' => 'required',
            'birthdate' => 'required',
            'sex' => 'required',
            'civil_status' => 'required'
        ];
    }
}
