<?php

namespace UnoRenta\Http\Requests;

use UnoRenta\Http\Requests\Request;
use Auth;

class AddMemoCommentRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'memo_id' => 'required',
            'comment' => 'required'
        ];
    }
}
