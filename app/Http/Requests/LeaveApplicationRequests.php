<?php

namespace UnoRenta\Http\Requests;

use UnoRenta\Http\Requests\Request;
use Auth;

class LeaveApplicationRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $time_cycle = '';
        if (Request::get('leave_period') == 'Half Day') {
            $time_cycle = 'required';
        }

        $number_of_days = '';
        if (Request::get('leave_period') == 'Whole Day') {
            $number_of_days = 'required';
        }

        return [
            'leave_type_id' => 'required',
            'leave_period' => 'required',
            'number_of_days' => $number_of_days,
            'time_cycle' => $time_cycle,
            'date_from' => 'required',
            'reason' => 'required',
            'first_approver' => 'required',
            'final_approver' => 'required'
        ];
    }

}
