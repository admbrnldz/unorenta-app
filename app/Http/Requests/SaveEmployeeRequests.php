<?php

namespace UnoRenta\Http\Requests;

use UnoRenta\Http\Requests\Request;
use Auth;

class SaveEmployeeRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $username = '';
        $password = '';
        if (!Request::has('employee_id')) {
            $username = 'required|unique:users,username';
            $password = 'required';
        }

        return [
            // user
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'username' => $username,
            'password' => $password,
            
            'position_id' => 'required',
            'birthdate' => 'required',
            'sex' => 'required',

            // address
            'address' => 'required',
            'city' => 'required',
            'province' => 'required',
            'zip_code' => 'required',

            'basic_income' => 'required|numeric',
            'monthly_work_days' => 'required|numeric',
            'date_hired' => 'required|date',
            'employment_status' => 'required',

            'mobile_number' => 'required',
            'home_number' => 'required',
            'email_address' => 'required',
            'civil_status' => 'required',
            'citizenship' => 'required',

            'sss' => 'unique:employees,sss',
            'tin' => 'unique:employees,tin',
            'philhealth' => 'unique:employees,philhealth',
            'pagibig' => 'unique:employees,pagibig',
        ];
    }
}
