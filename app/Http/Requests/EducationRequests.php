<?php

namespace UnoRenta\Http\Requests;

use UnoRenta\Http\Requests\Request;
use Auth;

class EducationRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $course_degree = 'required';
        if (Request::get('level') == 'Elementary' || Request::get('level') == 'High School') {
            $course_degree = '';
        }
        return [
            'employee_id' => 'required',
            'level' => 'required',
            'school_name' => 'required',
            'school_address' => 'required',
            'course_degree' => $course_degree,
            'year_graduated' => 'required'
        ];
    }
}
