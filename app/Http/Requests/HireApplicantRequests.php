<?php

namespace UnoRenta\Http\Requests;

use Auth;
use UnoRenta\Http\Requests\Request;

class HireApplicantRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $citizenshipOther = '';
        if (Request::get('citizenship') == 'Others') {
            $citizenshipOther = 'required';
        }
        return [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:users,username',
            'position_id' => 'required|integer',
            'status' => 'required|integer',
            'basic_income' => 'required|numeric',
            'monthly_work_days' => 'required|numeric',
            'address' => 'required',
            'city' => 'required',
            'citizenship_other' => $citizenshipOther,
            'province' => 'required',
            'mobile_number' => 'required',
            'email_address' => 'required|email',
            'birthdate' => 'required|date',
            'sex' => 'required'
        ];
    }
}
