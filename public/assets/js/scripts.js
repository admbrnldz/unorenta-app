$(function() {

    $('.update-family').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href'),
                targetForm = $(this).attr('data-target'),
                deleteUrl = $(this).attr('data-delete');
            $.get( url ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Update',
                            className: 'btn-primary',
                            callback: function() {
                
                                var form = $(targetForm),
                                    url = form.attr('action'),
                                    data = form.serialize();

                                $.post( url, data ).success(function(r) {
                                    r = $.parseJSON(r);
                                    bootbox.hideAll();
                                    setTimeout(function() {
                                        window.location = r.redirect;
                                    }, 500);
                                }).error(function(r) {
                                    var errors = r.responseJSON;
                                    var html = '';
                                    $.each(errors, function(index, value) {
                                        html += '<li>' + value[0] + '</li>';
                                    });
                                    $('#modal-error-handler').fadeIn().find('ul').html(html);
                                });
                                return false;
                                
                            }
                        },
                        'Delete' : {
                            className: 'btn-danger',
                            callback: function() {

                                $.get( deleteUrl ).done(function(r) {
                                    r = $.parseJSON(r);
                                    window.location = r.redirect;
                                });

                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });  
            })

        });
    });

    $('.tone-option').each(function() {
        $(this).on('click', function() {
            var form = $('#update-tone-form'),
                url = form.attr('action'),
                data = form.serialize();
            $.post( url, data ).done(function(r) {
                r = $.parseJSON(r);
                if (r.newTone != '') {
                    tone = r.toneString;
                    $.playSound(r.newTone);
                }                
            });
        });
    });

    $(document).on('click', '#flag-applicant', function(e) {
        e.preventDefault();
        sneak.out();
        var url = $(this).attr('href');
        $.get( url ).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                className: 'modal-warning',
                buttons: {
                    success: {
                        label: 'Flag',
                        className: 'btn-danger',
                        callback: function() {
                            $('#flag-applicant-form').submit();
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $('.sneak').on('click', function(e) {
        e.preventDefault();
        sneak.go($(this).attr('href'));
    });

    $('#deactivate-account').on('click', function(e) {
        e.preventDefault();
        var form = $('#deactivate-account-form'),
            url = form.attr('action'),
            data = form.serialize();

        bootbox.dialog({
            title: 'Warning!',
            message: '<div class="ta-center">Setting this account to <strong><u>inactive</u></strong> will set his/her position into <strong><i>"Unassigned"</i></strong>.<br />Are you sure you want to deactive this account?</div>',
            className: 'modal-warning',
            buttons: {
                success: {
                    label: 'Deactive',
                    className: 'btn-danger',
                    callback: function() {

                        $.post( url, data ).done(function(r) {
                            r = $.parseJSON(r);
                            window.location = r.redirect;
                        });
                        return false;
                    }
                },
                'Cancel' : {
                    className: 'btn-default'
                }
            }
        });       
        
    });

    $('#reactivate-account').on('click', function(e) {
        e.preventDefault();
        var form = $('#reactivate-account-form'),
            url = form.attr('action'),
            data = form.serialize();
            
        $.post( url, data ).done(function(r) {
            r = $.parseJSON(r);
            window.location = r.redirect;
        });
    });

    $('#dp-upload').on('click', function(e) {
        e.preventDefault();
        $('.upload-profile').click();
    });

    $('#dp-remove').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div class="ta-center">Are you sure you want to remove the display photo?</div>',
            className: 'modal-warning',
            buttons: {
                success: {
                    label: 'Yes, Remove',
                    className: 'btn-danger',
                    callback: function() {

                        window.location = url;

                    }
                },
                'Cancel' : {
                    className: 'btn-default'
                }
            }
        });
    });

    $('.add-holiday').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href'),
                targetForm = $(this).attr('data-form');
            $.get(url).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Add',
                            className: 'btn-primary',
                            callback: function() {

                                var form = $(targetForm),
                                    url = form.attr('action'),
                                    data = form.serialize();

                                $.post( url, data ).success(function(r) {
                                    r = $.parseJSON(r);
                                    bootbox.hideAll();
                                    setTimeout(function() {
                                        window.location = r.redirect;
                                    }, 500);
                                }).error(function(r) {
                                    var errors = r.responseJSON;
                                    var html = '';
                                    $.each(errors, function(index, value) {
                                        html += '<li>' + value[0] + '</li>';
                                    });
                                    $('#modal-error-handler').fadeIn().find('ul').html(html);
                                });
                                return false;

                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    var count = 1;
    $('.form-control-file').each(function() {
        $(this).attr('data-parent', 'fcfw-' + count);
        $(this).wrap('<div id="fcfw-' + count + '" class="form-control-file-wrapper"></div>');
        $(this).after('<div class="form-file-placeholder" data-target="#fcfw-' + count + '"><i class="fa fa-paperclip"></i><span>Choose file</span></div>');
        count++;
    });

    $('.form-control-file').each(function() {
        $(this).on('change', function() {
            var filename = $(this).val().split('\\');
            console.log(filename[2]);
            $('div[data-target="#' + $(this).attr('data-parent') + '"').find('span').html(filename[2]);
        });
    });

    $(document).on('click', '.form-file-placeholder', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-target');
        $(target).find('input[type="file"]').click();
    });

    $('.entitle-leave').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('href'),
                targetForm = $(this).attr('data-target-form'),
                where = $(this).attr('data-where');
            $.get( url, { 'where': where } ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    buttons: {
                        success: {
                            label: 'Entitle',
                            className: 'btn-primary',
                            callback: function() {

                                var form = $(targetForm),
                                    url = form.attr('action'),
                                    data = form.serialize();

                                $.post( url, data ).success(function(r) {
                                    r = $.parseJSON(r);
                                    bootbox.hideAll();
                                    setTimeout(function() {
                                        window.location = r.redirect;
                                    }, 500);                                
                                }).error(function(r) {
                                    var errors = r.responseJSON;
                                    var html = '';
                                    $.each(errors, function(index, value) {
                                        var count = value.length;
                                        for (var i = 0; i < count; i++) {
                                            html += '<li>' + value[i] + '</li>';
                                        }                                        
                                    });
                                    $('#modal-error-handler').fadeIn().find('ul').html(html);
                                });
                                return false;

                            }
                        },
                        'Cancel' : {
                            className: 'btn-default'
                        }
                    }
                });
            });
        });
    });

    $('#file-leave').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            targetForm = $(this).attr('data-form');
        $.get(url).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                buttons: {
                    success: {
                        label: 'File',
                        className: 'btn-primary',
                        callback: function() {
                            var form = $(targetForm),
                                url = form.attr('action'),
                                data = form.serialize();

                            $.post( url, data ).success(function(r) {
                                r = $.parseJSON(r);
                                bootbox.hideAll();
                                setTimeout(function() {
                                    window.location = r.redirect;
                                }, 500);                                
                            }).error(function(r) {
                                var errors = r.responseJSON;
                                var html = '';
                                $.each(errors, function(index, value) {
                                    html += '<li>' + value[0] + '</li>';
                                });
                                $('#modal-error-handler').fadeIn().find('ul').html(html);
                            });
                            return false;
                        }
                    },
                    'Cancel' : {
                        className: 'btn-default'
                    }
                }
            });
        })
    });

    $('.btp').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, 500);
        });
    });

    $('aside').perfectScrollbar();

    $(document).on('click', '.navigatable', function(e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        if (url != null) {
            window.location = url;
        }
    })

    $('.modable').each(function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            
        });
    });

    var aside = {
        aside: $('aside'),
        html: '<div id="aside-overlay"></div>',
        show: function() {
            if (this.aside.hasClass('show')) {
                this.hide();
            } else {
                this.aside.addClass('show');
                $(this.html).insertAfter(this.aside);
            }
        },
        hide: function() {
            this.aside.removeClass('show');
            $('#aside-overlay').remove();
        }
    }

    $('#aside-trigger').on('click', function(e) {
        e.preventDefault();
        aside.show();
    });

    $(document).on('click', '#aside-overlay', function(e) { aside.hide(); });

    $(document).on('click', '.bootbox', function(e) { bootbox.hideAll(); });
    $(document).on('click', '.modal-dialog', function(e) { e.stopPropagation(); });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
    * time-tracking
    */
    $('#record-time-trigger').on('click', function(e) {
        e.preventDefault();

        var form = $('#tracktime'),
            formData = form.serialize(),
            url = form.attr('action');

        $.post(url, formData, function(data) {
            $('#time-in').text(data.time_in);
            $('#time-out').text(data.time_out);
            $('#record-time-trigger').text(data.label);
            if (data.disabled) {
                $('#record-time-trigger').prop('disabled', true);
            }
            if (data.breakDisabled) {
                $('#take-break-trigger').prop('disabled', true);
            }
            else {
                $('#take-break-trigger').prop('disabled', false);
            }
            console.log(data);
        }, 'json');
    });
    /*
        BREAK
    */
    $('#take-break-trigger').on('click', function(e) {
        e.preventDefault();

        var url = $(this).attr('data-takebreak');

        $.post(url, function(data) {
            console.log(data);
        }, 'json');
    });

    /*
        AUTO-RESIZE TEXTAREA
    */
    autosize($('textarea'));

    /*
        FADE OUT ALERT
    */
    var alert = $('.alert-fadeable');
    alert.delay(5000).animate({
        'margin-top': '-' + (alert.height() + 48) + 'px'
    }, 100);
    alert.delay(50).fadeOut(100);

    $('.pd-content').each(function() {
        $(this).on('click', function() {
            var parentLi = $(this).closest('li');
            parentLi.find('.pd-meta').toggleClass('hidden');
        });
    });

    $('#generate-password').on('click', function(e) {
        e.preventDefault();
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i=0; i < 12; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        $('#' + $(this).attr('data-target')).val(text);
    });

    $('.submit-profile-btn').each(function() {

        $(this).on('click', function(e) {
            e.preventDefault();
        });

    });

    var d = new Date(),
        year = d.getFullYear();

    $('#birthdate').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: (year - 50) + ':' + (year - 18),
        dateFormat: 'MM d, yy'
    });

    $('#guardian').on('change', function() {
        var tf = $('#guardian-name'),
            target = $(this).val();
        if (target != '') {
            tf.val($('#' + target + '-name').val());
        } else {
            tf.val('');
        }
    });

    $('#cp').on('change', function() {
        var tf = $('#contact-person'),
            target = $(this).val();
        if (target != '') {
            tf.val($('#' + target + '-name').val());
        } else {
            tf.val('');
        }
    });

    $('#guardian-same-address, #contact-same-address').on('change', function() {
        var target = $(this).attr('data-target'),
            eAddress = $('#employee-address .form-control'),
            addressValues = {},
            count = 0;

        eAddress.each(function() {
            addressValues[count] = $(this).val();
            count++;
        });

        count = 0;
        if ($(this).prop('checked') == true) {
            $(target + ' .form-control').each(function() {
                $(this).prop('disabled', true).val(addressValues[count]);
                count++;
            });
            $(target + '-catcher').val(1);
        } else {
            $(target + ' .form-control').each(function() {
                $(this).prop('disabled', false).val('');
            });
            $(target + '-catcher').val(0);
        }
    });


    /*
        VERIFY USERNAME
    */
    var typingTimer;
    var doneTypingInterval = 500;

    $('.verify-field').on('keyup', function() {

        var data = {
                'username': $(this).val(),
                'user_id': $(this).attr('data-user-id')
            },
            url = $(this).attr('data-url');

        clearTimeout(typingTimer);

        if ($(this).val) {
            typingTimer = setTimeout(function() {

                $('.field-verifier').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');                
                
                $.get(url, data, function(response) {
                    console.log(response);
                    response = $.parseJSON(response);
                    if (response.available == true) {
                        $('.field-verifier').html('<i class="fa fa-check fa-fw fc-primary"></i>');
                    } else {
                        $('.field-verifier').html('<i class="fa fa-close fa-fw fc-danger"></i>');
                    }
                });

            }, doneTypingInterval);
        }

    });

    $('.upload-profile').on('click', function(e) {
        e.stopPropagation();
        var target = $($(this).attr('data-target'));
        target.click();
    });
    $('#edit-dp').on('change', function() {
        readPhotoURL(this, '.modal-dp', 'cover');
        var form = $($(this).attr('data-target-form')),
            formData = new FormData(form[0]);

        formData.append('image', form.find('input[type=file]')[0].files[0]); 
        $.ajax({
            url: form.attr('action'),
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
        }).done(function(r) {
            r = $.parseJSON(r);
            var style = 'background:url(' + r.newDP + ') center center no-repeat;background-size:cover;';
            $('#e-dp').attr('style', style);
            $('#dp-remove').removeClass('hidden');
        });
    });
    $('#edit-dp, #employee-dp').on('change', function() {
        readPhotoURL(this, '.upload-profile', 'cover');
    });

    $('#citizenship').on('change', function() {
        if ($(this).val() == 'Others') {
            $('#citizenship-other').prop('disabled', false).focus();
        }
        else {
            $('#citizenship-other').prop('disabled', true);   
        }
    });

    $('#message, #comment').on('keyup', function() {
        var postButton = $('#post-comment');
        if ($(this).val() != '') {
            postButton.prop('disabled', false);
            postButton.addClass('btn-primary').removeClass('btn-danger');
        } else {
            postButton.prop('disabled', true);
        }
    });

    /*
        RATING
    */
    $('.rating').each(function() {
        var wrapper = $(this),
            ratingField = wrapper.find('input'),
            ul = wrapper.find('ul'),
            li = ul.find('li');
        li.each(function() {
            var rating = $(this).attr('data-rating');
            $(this).on('mouseover', function() {
                var lis = ul.find('li');
                lis.each(function() {
                    if ($(this).attr('data-rating') <= rating) {
                        $(this).find('.fa').attr('class', 'fa fa-star');
                    } else {
                        $(this).find('.fa').attr('class', 'fa fa-star-o');
                    }
                });
            });
            $(this).on('click', function() {
                ratingField.val(rating);
            });
        });
        $(this).on('mouseout', function() {
            var rating = ratingField.val();
            if (rating == '') {   
                li.each(function() {
                    $(this).find('.fa').attr('class', 'fa fa-star-o');
                });
            } else {
                li.each(function() {
                    if ($(this).attr('data-rating') <= rating) {
                        $(this).find('.fa').attr('class', 'fa fa-star');
                    }
                    else {
                        $(this).find('.fa').attr('class', 'fa fa-star-o');    
                    }
                });
            }
        });
    });

    // update profile photo
    $('.modal-dp').on('click', function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).click();
    });

    $('#dp-file').on('change', function() {
        readPhotoURL(this, '.modal-dp', 'cover');
        var form = $($(this).attr('data-form')),
            formData = new FormData(form[0]);

        formData.append('image', form.find('input[type=file]')[0].files[0]); 
        $.ajax({
            url: form.attr('action'),
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
        }).done(function(r) {
            r = $.parseJSON(r);
            var style = 'background:url(' + r.newDP + ') center center no-repeat;background-size:cover;';
            $('#upanel .upanel-dp').attr('style', style);
        });
    });

    $('#signature-file').on('change', function() {
        var target = '.signature-holder';
        readPhotoURL(this, target, 'contain');
        $(target).removeClass('no-signature').html('');

        var form = $($(this).attr('data-form')),
            formData = new FormData(form[0]);

        formData.append('image', form.find('input[type=file]')[0].files[0]); 
        $.ajax({
            url: form.attr('action'),
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
        }).done(function(r) {
            r = $.parseJSON(r);
            var style = 'background:url(' + r.newSignature + ') center center no-repeat;background-size:contain;';
            $(target).attr('style', style);
            $('#signature-panel .panel-heading').append(r.remover);
        });
        // clear value
        // ready for new upload
        $(this).val('');
    });
    
});

function drawCalendar(dom, targetURL) {
    $.ajax({ url: targetURL }).done(function(response) {
        $(dom).html(response);
    });
}

function drawShiftCalendar(dom, targetURL, shiftBriefId) {
    var data = { 'shiftBriefId': shiftBriefId };
    $.ajax({ url: targetURL, data: data }).done(function(response) {
        $(dom).html(response);
    });
}

function readPhotoURL(input, dom, size) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(dom).attr('style', "background: url(" + e.target.result + ") center center no-repeat;background-size:" + size + ";");
        };
        reader.readAsDataURL(input.files[0]);
    }
}