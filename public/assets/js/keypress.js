$(function() {

   $('body').on('keydown', function(e) {
        var nodename = e.target.nodeName.toLowerCase(),
            key = e.keyCode;
        if (nodename == 'body') {
            if ($('#menu-' + key).length) {
                window.location = $('#menu-' + key).attr('href');
            }            
        }
    }); 

});