(function ($) {

    var checkedCount = 0;

    $.fn.selectlist = function(options) {

        // append css
        $('head').append('<style>' +
            '.selectlist tbody tr{cursor:pointer;}.sl-checkbox-stash{position:relative;}' +
            '.selectlist input[type="checkbox"],.sl-checkbox-stash input[type="checkbox"]{position:absolute;left:-999px;}' +
            '</style>');

        // settings
        var settings = $.extend({

        }, options );

        var controllerCheckbox = this.find('.sl-checkboxes-ctrl'),
            bodyRow = this.find('tbody tr'),
            checkboxCount = this.find('tbody tr .sl-checkbox').length,
            btnHTML = '';

        // enable the default behaviour of anchor tags
        this.find('tbody tr td a, tbody tr td :text').on('click', function(e) {
            e.stopPropagation();
        });

        // hide all checkboxes
        // be sure to include/import the selectlist.css
        this.find('.sl-checkbox').each(function() {
            $(this).wrap('<label class="sl-checkbox-stash"></label>')
        });

        if (checkboxCount > 0) {
            btnHTML = '<button class="sl-controller"></button>';
        }
        else {
            btnHTML = '<button class="sl-controller" disabled></button>';           
        }
        controllerCheckbox.parent().append(btnHTML);

        // declare after appending      
        var controllerButton = this.find('.sl-controller');

        bodyRow.on('click', function() {
            var checkbox = $(this).find('.sl-checkbox');
            if (checkbox.length != 0) {
                $(this).toggleClass('sl-selected');
                if ($(this).hasClass('sl-selected')) {
                    checkbox.prop('checked', true);
                }
                else {
                    checkbox.prop('checked', false);
                }
                
                // count checked
                checkedCount = bodyRow.find('.sl-checkbox:checked').length;
                if (checkedCount < checkboxCount) {
                    controllerCheckbox.prop('checked', false);
                    controllerButton.removeClass('sl-select-all').html(settings.checkLabel);
                }
                else if (checkedCount == checkboxCount) {
                    controllerCheckbox.prop('checked', true);
                    controllerButton.addClass('sl-select-all').html(settings.uncheckLabel);
                }
            }
        });

        this.find('.sl-checkbox').on('click', function() {
            $(this).closest('tr').toggleClass('sl-selected');
            if ($(this).closest('tr').hasClass('sl-selected')) {
                $(this).prop('checked', true);
            }
            else {
                $(this).prop('checked', false);
            }
            
            // count checked
            checkedCount = bodyRow.find('.sl-checkbox:checked').length;
            if (checkedCount < checkboxCount) {
                controllerCheckbox.prop('checked', false);
                controllerButton.removeClass('sl-select-all').html(settings.checkLabel);
            }
            else if (checkedCount == checkboxCount) {
                controllerCheckbox.prop('checked', true);
                controllerButton.addClass('sl-select-all').html(settings.uncheckLabel);
            }
        });

        controllerButton.on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('sl-select-all');
            if ($(this).hasClass('sl-select-all')) {
                controllerCheckbox.prop('checked', true);
                bodyRow.addClass('sl-selected').find('.sl-checkbox').prop('checked', true);
                $(this).html(settings.uncheckLabel);
            }
            else {
                controllerCheckbox.prop('checked', false);
                bodyRow.removeClass('sl-selected').find('.sl-checkbox').prop('checked', false); 
                $(this).html(settings.checkLabel);
            }
        });

        return this;
    }

}(jQuery));
// by Eric Amigo