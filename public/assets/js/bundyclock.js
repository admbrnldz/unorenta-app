$(function() {

    var form = $('#bundy-clock-form'),
        data = form.serialize(),
        url = '';

    $('#time-in, #time-out').on('click', function(e) {
        e.preventDefault();
        url = $(this).attr('data-url');
        bundyClock(url, data);
    });

    $('#takebreak').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        takeBreak(url);
    });

    $(document).on('click', '.break-notification', function(e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        takeBreak(url);
    });

});
function bundyClock(targetUrl, data) {

    $.post({
        url: targetUrl,
        data: data
    }).done(function(response) {
        console.log(response);
        response = $.parseJSON(response);

        if (response.type == 'time_in') {

            var dateTime = response.timecard.time_in,
                theTime = dateTime.split(' '),
                timePart = theTime[1].split(':'),
                h = timePart[0], m = timePart[1];

            var time = processTime(parseInt(h), parseInt(m));

            $('#time-in-clock').html(time);
            $('#time-in').prop('disabled', true);
            $('#time-in').append('<i class="fa fa-circle-o-notch fa-spin fa-fw" title="Waiting for approval"></i>');
            console.log('Timed in at ' + time);

        } else if (response.type = 'time_out') {

            var dateTime = response.timecard.time_out,
                theTime = dateTime.split(' '),
                timePart = theTime[1].split(':'),
                h = timePart[0], m = timePart[1];

            var time = processTime(parseInt(h), parseInt(m));

            $('#time-out-clock').html(time);
            $('#time-out').prop('disabled', true);
            $('#takebreak').prop('disabled', true);
            $('#time-out').append('<i class="fa fa-circle-o-notch fa-spin fa-fw" title="Waiting for approval"></i>');
            console.log('Timed in at ' + time);

        }
    });

}
function takeBreak(url)
{
    $.post( url ).done(function(r) {
        r = $.parseJSON(r);
        if (r.onBreak) {
            $('#break-nofitication-wrap').html(r.view);
            $('#time-out').prop('disabled', true);
            $('#takebreak').prop('disabled', true).find('span').html('On break');
        } else {
            $('#break-nofitication-wrap').html('');
            $('#time-out').prop('disabled', false);
            $('#takebreak').prop('disabled', false).find('span').html(r.totalBreak + 'm');            
            setTimeout(function() {
                var percentage = (r.totalBreak / 60) * 100;
                $('#takebreak').find('.break-progress').css({
                    'width': percentage + '%'
                });
                if (percentage >= 75) {
                    $('#takebreak').removeClass('btn-success').addClass('btn-warning');
                }
                if (percentage >= 90) {
                    $('#takebreak').removeClass('btn-success btn-warning').addClass('btn-danger');
                }
            }, 500);
        }
    });
}
function processTime(hr, min) {
    tc = 'AM';
    if (hr >= 12) { hr -= 12; tc = 'PM'; }
    if (hr == 12) { tc = 'AM'; }
    if (hr == 0) { hr = 12; }
    if (hr < 10) { hr = '0' + hr; }
    if (min < 10) { min = '0' + min; }
    return hr + ':' + min + tc;
}