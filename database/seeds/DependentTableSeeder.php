<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\Dependent as Dependent;

class DependentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Dependent::create([
            'employee_id' => 4,
            'first_name' => 'Ricardo',
            'last_name' => 'Balbastro',
            'middle_name' => 'Sossy',
            'birthdate' => '2004-02-15',
            'relationship' => 'Son'
        ]);

        Dependent::create([
            'employee_id' => 4,
            'first_name' => 'Melanie',
            'last_name' => 'Balbastro',
            'middle_name' => 'Sossy',
            'birthdate' => '2002-08-09',
            'relationship' => 'Daughter'
        ]);

    }
}
