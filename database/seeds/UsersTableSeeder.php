<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use UnoRenta\Models\User;
use Faker\Factory as Faker;
use UnoRenta\Models\Employee;
use UnoRenta\Models\EmployeeStatus;
use UnoRenta\Models\EmployeeFamily;
use UnoRenta\Models\EmployeeEmergency;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        // Anna Victoria De Los Santos 
        $gm = User::create([
            'first_name' => 'Anna Victoria',
            'last_name' => 'De Los Santos',
            'middle_name' => 'N',
            'username' => 'anna',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $gm->id,
            'position_id' => 2,
            'nickname' => $gm->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($gm->first_name) . $gm->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $gm->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $gm->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $gm->last_name,
            // 'contact_person' => $faker->firstName . ' ' . $gm->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 26,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 1
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 1
        ]);

        // gill
        $adminuser = User::create([
            'first_name' => 'Gill',
            'last_name' => 'Besada',
            'middle_name' => 'T',
            'username' => 'gill',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $adminuser->id,
            'position_id' => 3,
            'nickname' => $adminuser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($adminuser->first_name) . $adminuser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $adminuser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $adminuser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $adminuser->last_name,
            // 'contact_person' => $faker->firstName . ' ' . $adminuser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 26,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        // jayson
        $omuser = User::create([
            'first_name' => 'Jayson',
            'last_name' => 'Abello',
            'middle_name' => 'S',
            'username' => 'jayson',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $omuser->id,
            'position_id' => 4,
            'nickname' => $omuser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($omuser->first_name) . $omuser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $omuser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $omuser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $omuser->last_name,
            // 'contact_person' => $faker->firstName . ' ' . $omuser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 21.6667,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        // andy
        $hruser = User::create([
            'first_name' => 'Andy',
            'last_name' => 'Catayas',
            'middle_name' => 'M',
            'username' => 'andy',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $hruser->id,
            'position_id' => 5,
            'nickname' => $hruser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($hruser->first_name) . $hruser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $hruser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $hruser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $hruser->last_name,
            // 'guardian_address' => $address->id,
            // 'contact_person' => $faker->firstName . ' ' . $hruser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 26,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        // johnail
        $accountinguser = User::create([
            'first_name' => 'Johnail',
            'last_name' => 'Balbastro',
            'middle_name' => 'L',
            'username' => 'johnail',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $accountinguser->id,
            'position_id' => 6,
            'nickname' => $accountinguser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($accountinguser->first_name) . $accountinguser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $accountinguser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $accountinguser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $accountinguser->last_name,
            // 'guardian_address' => $address->id,
            // 'contact_person' => $faker->firstName . ' ' . $accountinguser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 26,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        // katerina
        $traineruser = User::create([
            'first_name' => 'Katerina',
            'last_name' => 'Bernales',
            'middle_name' => 'P',
            'username' => 'katerina',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $traineruser->id,
            'position_id' => 7,
            'nickname' => $traineruser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($traineruser->first_name) . $traineruser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $traineruser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $traineruser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $traineruser->last_name,
            // 'guardian_address' => $address->id,
            // 'contact_person' => $faker->firstName . ' ' . $traineruser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 26,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        // shari mae
        $bpatluser = User::create([
            'first_name' => 'Shari Mae',
            'last_name' => 'Juntilla',
            'middle_name' => 'V',
            'username' => 'shari',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $bpatluser->id,
            'position_id' => 8,
            'nickname' => $bpatluser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($bpatluser->first_name) . $bpatluser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $bpatluser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $bpatluser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $bpatluser->last_name,
            // 'guardian_address' => $address->id,
            // 'contact_person' => $faker->firstName . ' ' . $bpatluser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 21.6667,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        // eula
        $icstluser = User::create([
            'first_name' => 'Eula Mae',
            'last_name' => 'Tocao',
            'middle_name' => 'S',
            'username' => 'eula',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $icstluser->id,
            'position_id' => 9,
            'nickname' => $icstluser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($icstluser->first_name) . $icstluser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $icstluser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $icstluser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $icstluser->last_name,
            // 'guardian_address' => $address->id,
            // 'contact_person' => $faker->firstName . ' ' . $icstluser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 21.6667,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        // adrian kline
        $utilityuser = User::create([
            'first_name' => 'Adrian Kline',
            'last_name' => 'Dalaguit',
            'middle_name' => 'G',
            'username' => 'adrian',
            'password' => bcrypt('hello')
        ]);
        $employee = Employee::create([
            'user_id' => $utilityuser->id,
            'position_id' => 10,
            'nickname' => $utilityuser->first_name,
            'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
            'birthplace' => $faker->city,
            'address' => $faker->streetAddress,
            'city' => $faker->city,
            'province' => $faker->country,
            'zip_code' => $faker->postcode,
            'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
            'email_address' => strtolower($utilityuser->first_name) . $utilityuser->id . '@unorenta.app',
            'citizenship' => 'Filipino',
            'height' => rand(140, 172),
            'weight' => rand(45, 80),
            'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-0',
            'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
            'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
            'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
            // 'father_name' => $faker->firstNameMale . ' ' . $utilityuser->last_name,
            // 'father_occupation' => $faker->jobTitle,
            // 'mother_name' => $faker->firstNameFemale . ' ' . $utilityuser->last_name,
            // 'mother_occupation' => $faker->jobTitle,
            // 'guardian_name' => $faker->firstName . ' ' . $utilityuser->last_name,
            // 'guardian_address' => $address->id,
            // 'contact_person' => $faker->firstName . ' ' . $utilityuser->last_name,
            // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
            // 'status' => 2,
            'basic_income' => rand(22000, 32000),
            'monthly_work_days' => 26.0000,
            // 'cp_language_spoken' => 'Filipino, English',
            'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
        ]);
        $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
        // $employee->date_probationary = $employee->date_hired;
        // $employee->date_regular = $employee->date_hired;
        $employee->save();

        $father = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'father'
        ]);
        $mother = EmployeeFamily::create([
            'employee_id' => $employee->id,
            'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
            'occupation' => $faker->jobTitle,
            'relationship' => 'mother'
        ]);
        $contact = EmployeeEmergency::create([
            'employee_id' => $employee->id,
            'name' => $mother->name,
            'contact_number' => $employee->mobile_number,
            'address' => $employee->address()
        ]);

        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 1,
            'updated_by' => 3
        ]);
        EmployeeStatus::create([
            'employee_id' => $employee->id,
            'employment_status' => 4,
            'updated_by' => 3
        ]);

        for ($i=0; $i < 10; $i++) { 

            $firstName = $faker->firstName;
            
            $user = User::create([
                'first_name' => $firstName,
                'last_name' => $faker->lastName,
                'middle_name' => $faker->lastName,
                'username' => strtolower($firstName),
                'password' => bcrypt('hello')
            ]);

            $employee = Employee::create([
                'user_id' => $user->id,
                'position_id' => rand(11, 16),
                'nickname' => $firstName,
                'birthdate' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-22 years'),
                'birthplace' => $faker->city,
                'address' => $faker->streetAddress,
                'city' => $faker->city,
                'province' => $faker->country,
                'zip_code' => $faker->postcode,
                'mobile_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
                'home_number' => '+63 82 ' . rand(305, 321) . ' ' . sprintf('%04d', rand(0, 9999)),
                'email_address' => strtolower($firstName) . $user->id . '@unorenta.app',
                'citizenship' => 'Filipino',
                'height' => rand(140, 172),
                'weight' => rand(45, 80),
                'sss' => sprintf('%02d', rand(0, 9)) . '-' . sprintf('%07d', rand(5000, 999999)) . '-' . rand(0, 9),
                'tin' => rand(100, 300) . '-' . sprintf('%03d', rand(0, 200)) . '-' . sprintf('%03d', rand(0, 200)) . '-000',
                'philhealth' => sprintf('%02d', rand(0, 9)) . '-0000' . sprintf('%04d', rand(0, 9999)) . '0-' . rand(0, 9),
                'pagibig' => rand(1000, 9999) . '-' . rand(1000, 9999) . '-' . rand(15, 99),
                // 'father_name' => $faker->firstNameMale . ' ' . $user->last_name,
                // 'father_occupation' => $faker->jobTitle,
                // 'mother_name' => $faker->firstNameFemale . ' ' . $user->last_name,
                // 'mother_occupation' => $faker->jobTitle,
                // 'guardian_name' => $faker->firstName . ' ' . $user->last_name,
                // 'guardian_address' => $address->id,
                // 'contact_person' => $faker->firstName . ' ' . $user->last_name,
                // 'cp_number' => '+63 9' . rand(22, 33) . ' ' . sprintf('%03d', rand(0, 999)) . ' ' . sprintf('%04d', rand(0, 9999)),
                // 'status' => 2,
                'basic_income' => rand(16000, 22000),
                'monthly_work_days' => 21.6667,
                // 'cp_language_spoken' => 'Filipino, English',
                'date_hired' => $faker->dateTimeBetween($startDate = '-4 months', $endDate = 'now')
            ]);

            $employee->employee_uid = date('Y') . '-' . sprintf('%04d', $employee->id);
            // $employee->date_probationary = $employee->date_hired;
            // $employee->date_regular = $employee->date_hired;
            $employee->save();

            $father = EmployeeFamily::create([
                'employee_id' => $employee->id,
                'name' => $faker->firstNameMale . ' ' . $employee->user->last_name,
                'occupation' => $faker->jobTitle,
                'relationship' => 'father'
            ]);
            $mother = EmployeeFamily::create([
                'employee_id' => $employee->id,
                'name' => $faker->firstNameFemale . ' ' . $employee->user->last_name,
                'occupation' => $faker->jobTitle,
                'relationship' => 'mother'
            ]);
            $contact = EmployeeEmergency::create([
                'employee_id' => $employee->id,
                'name' => $mother->name,
                'contact_number' => $employee->mobile_number,
                'address' => $employee->address()
            ]);

            EmployeeStatus::create([
                'employee_id' => $employee->id,
                'employment_status' => 1,
                'updated_by' => 3
            ]);
            EmployeeStatus::create([
                'employee_id' => $employee->id,
                'employment_status' => 4,
                'updated_by' => 3
            ]);

        }

    }
}
