<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\Permission;
use UnoRenta\Models\PermissionCategory;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // permission categories
        $categories = [
            'Account Module', // 1
            'Employee Module', // 2
            'Leave Module', // 3
            'Memo Module', // 4
            'Attendance Module', // 5
            'Department and Position Module', // 6
            'Holiday Module', // 7
            'Shift Module', // 8
            'Applicant Module', // 9
            'Accounting Module' // 10
        ];
        foreach ($categories as $category) {

            $category = PermissionCategory::create([
                'name' => $category
            ]);

        }
        
        // permissions
        Permission::create([
            'permission_category_id' => 2,
            'key' => 'view-employees',
            'name' => 'View Employees'
        ]);
        Permission::create([
            'permission_category_id' => 2,
            'key' => 'view-profile',
            'name' => 'View Profile'
        ]);
        Permission::create([
            'permission_category_id' => 2,
            'key' => 'add-employee',
            'name' => 'Add Employee'
        ]);
        Permission::create([
            'permission_category_id' => 2,
            'key' => 'edit-employee',
            'name' => 'Edit Employee'
        ]);

        Permission::create([
            'permission_category_id' => 6,
            'key' => 'view-departments-positions',
            'name' => 'View Departments Positions'
        ]);
        Permission::create([
            'permission_category_id' => 6,
            'key' => 'add-department-position',
            'name' => 'Add Department Position'
        ]);
        Permission::create([
            'permission_category_id' => 6,
            'key' => 'edit-department-position',
            'name' => 'Edit Department Position'
        ]);
        Permission::create([
            'permission_category_id' => 6,
            'key' => 'delete-department-position',
            'name' => 'Delete Department Position'
        ]);

        Permission::create([
            'permission_category_id' => 7,
            'key' => 'view-holidays',
            'name' => 'View Holidays'
        ]);
        Permission::create([
            'permission_category_id' => 7,
            'key' => 'add-holiday',
            'name' => 'Add Holiday'
        ]);
        Permission::create([
            'permission_category_id' => 7,
            'key' => 'edit-holiday',
            'name' => 'Edit Holiday'
        ]);
        Permission::create([
            'permission_category_id' => 7,
            'key' => 'delete-holiday',
            'name' => 'Delete Holiday'
        ]);

        Permission::create([
            'permission_category_id' => 4,
            'key' => 'view-memos',
            'name' => 'View Memos'
        ]);
        Permission::create([
            'permission_category_id' => 4,
            'key' => 'broadcast-memo',
            'name' => 'Broadcast Memo'
        ]);
        Permission::create([
            'permission_category_id' => 4,
            'key' => 'edit-memo',
            'name' => 'Edit Memo'
        ]);
        Permission::create([
            'permission_category_id' => 4,
            'key' => 'delete-memo',
            'name' => 'Delete Memo'
        ]);

        Permission::create([
            'permission_category_id' => 1,
            'key' => 'edit-permissions',
            'name' => 'Edit Permissions'
        ]);

        Permission::create([
            'permission_category_id' => 3,
            'key' => 'view-leave-applications',
            'name' => 'View Leave Applications'
        ]);

        Permission::create([
            'permission_category_id' => 8,
            'key' => 'view-shifts',
            'name' => 'View Shifts'
        ]);
        Permission::create([
            'permission_category_id' => 8,
            'key' => 'add-shift',
            'name' => 'Add Shift'
        ]);
        Permission::create([
            'permission_category_id' => 8,
            'key' => 'edit-shifts',
            'name' => 'Edit Shifts'
        ]);

        Permission::create([
            'permission_category_id' => 9,
            'key' => 'view-applicants',
            'name' => 'View Applicants'
        ]);
        Permission::create([
            'permission_category_id' => 9,
            'key' => 'add-applicant',
            'name' => 'Add Applicant'
        ]);
        Permission::create([
            'permission_category_id' => 9,
            'key' => 'delete-applicant',
            'name' => 'Delete Applicant'
        ]);
        Permission::create([
            'permission_category_id' => 9,
            'key' => 'edit-applicant',
            'name' => 'Edit Applicant'
        ]);

        Permission::create([
            'permission_category_id' => 1,
            'key' => 'edit-account-settings',
            'name' => 'Edit Account Settings'
        ]);

        Permission::create([
            'permission_category_id' => 5,
            'key' => 'approve-attendance',
            'name' => 'Approve Attendance'
        ]);

        Permission::create([
            'permission_category_id' => 9,
            'key' => 'add-interview-questions',
            'name' => 'Add Interview Questions'
        ]);
        Permission::create([
            'permission_category_id' => 9,
            'key' => 'edit-interview-question',
            'name' => 'Edit Interview Questions'
        ]);
        Permission::create([
            'permission_category_id' => 9,
            'key' => 'delete-interview-question',
            'name' => 'Delete Interview Questions'
        ]);

        Permission::create([
            'permission_category_id' => 9,
            'key' => 'interview-applicants',
            'name' => 'Interview Applicants'
        ]);

        Permission::create([
            'permission_category_id' => 3,
            'key' => 'entitle-leave-applications',
            'name' => 'Entitle Leave Applications'
        ]);

        /*
            Accounting Permission
            - view employee rate
            - edit employee rate
            - view payroll batch
            - generate payslip
            - view other deductions
            - add other deductions
        */
        Permission::create([
            'permission_category_id' => 10,
            'key' => 'view-employee-rate',
            'name' => 'View Employee Rate'
        ]);

        Permission::create([
            'permission_category_id' => 10,
            'key' => 'edit-employee-rate',
            'name' => 'Edit Employee Rate'
        ]);

        Permission::create([
            'permission_category_id' => 10,
            'key' => 'view-payroll-batch',
            'name' => 'View Payroll Batch'
        ]);

        Permission::create([
            'permission_category_id' => 10,
            'key' => 'generate-payslip',
            'name' => 'Generate Payslip'
        ]);

        Permission::create([
            'permission_category_id' => 10,
            'key' => 'view-other-deductions',
            'name' => 'View Other Deductions'
        ]);

        Permission::create([
            'permission_category_id' => 10,
            'key' => 'add-other-deductions',
            'name' => 'Add Other Deductions'
        ]);

        Permission::create([
            'permission_category_id' => 10,
            'key' => 'view-reports',
            'name' => 'View Reports'
        ]);

    }
}
