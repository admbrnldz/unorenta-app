<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\EmployeeEducation as EmployeeEducation;

class EducationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        EmployeeEducation::create([
            'employee_id' => 4,
            'level' => 'Elementary',
            'school_name' => 'F. Bangoy Central Elementary School',
            'school_address' => 'Sasa, Davao City',
            'course_degree' => '',
            'year_graduated' => '2002',
        ]);

        EmployeeEducation::create([
            'employee_id' => 4,
            'level' => 'High School',
            'school_name' => 'Holy Cross Academy of Sasa',
            'school_address' => 'Sasa, Davao City',
            'course_degree' => '',
            'year_graduated' => '2006',
        ]);

        EmployeeEducation::create([
            'employee_id' => 4,
            'level' => 'College',
            'school_name' => 'University of Mindanao',
            'school_address' => 'Bolton St., Davao City',
            'course_degree' => 'Bachelor of Science in Business Administration - Management Accounting',
            'year_graduated' => '2010',
        ]);

    }
}
