<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\WithholdingTax;

class WithholdingTaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $wts = [
            // no dependents
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 1, 'max' => 2082, 'base_tax' => 0.00, 'percent' => 0],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 2083, 'max' => 2499, 'base_tax' => 0.00, 'percent' => 0.05],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 2500, 'max' => 3332, 'base_tax' => 20.83, 'percent' => 0.10],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 3333, 'max' => 4999, 'base_tax' => 104.17, 'percent' => 0.15],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 5000, 'max' => 7916, 'base_tax' => 354.17, 'percent' => 0.20],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 7917, 'max' => 12499, 'base_tax' => 937.50, 'percent' => 0.25],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 12500, 'max' => 22916, 'base_tax' => 2083.33, 'percent' => 0.30],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 0, 'min' => 22917, 'max' => 1000000, 'base_tax' => 5208.33, 'percent' => 0.32],

            // 1 dependent
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 1, 'max' => 3124, 'base_tax' => 0.00, 'percent' => 0],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 3125, 'max' => 3541, 'base_tax' => 0.00, 'percent' => 0.05],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 3542, 'max' => 4374, 'base_tax' => 20.83, 'percent' => 0.10],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 4375, 'max' => 6041, 'base_tax' => 104.17, 'percent' => 0.15],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 6042, 'max' => 8957, 'base_tax' => 354.17, 'percent' => 0.20],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 8958, 'max' => 13541, 'base_tax' => 937.50, 'percent' => 0.25],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 13542, 'max' => 23957, 'base_tax' => 2083.33, 'percent' => 0.30],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 1, 'min' => 23958, 'max' => 1000000, 'base_tax' => 5208.33, 'percent' => 0.32],

            // 2 dependents
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 1, 'max' => 4166, 'base_tax' => 0.00, 'percent' => 0],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 4167, 'max' => 4582, 'base_tax' => 0.00, 'percent' => 0.05],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 4583, 'max' => 5416, 'base_tax' => 20.83, 'percent' => 0.10],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 5417, 'max' => 7082, 'base_tax' => 104.17, 'percent' => 0.15],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 7083, 'max' => 9999, 'base_tax' => 354.17, 'percent' => 0.20],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 10000, 'max' => 14582, 'base_tax' => 937.50, 'percent' => 0.25],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 14583, 'max' => 24999, 'base_tax' => 2083.33, 'percent' => 0.30],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 2, 'min' => 25000, 'max' => 1000000, 'base_tax' => 5208.33, 'percent' => 0.32],

            // 3 dependents
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 1, 'max' => 5207, 'base_tax' => 0.00, 'percent' => 0],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 5208, 'max' => 5624, 'base_tax' => 0.00, 'percent' => 0.05],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 5625, 'max' => 6457, 'base_tax' => 20.83, 'percent' => 0.10],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 6458, 'max' => 8124, 'base_tax' => 104.17, 'percent' => 0.15],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 8125, 'max' => 11041, 'base_tax' => 354.17, 'percent' => 0.20],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 11042, 'max' => 15624, 'base_tax' => 937.50, 'percent' => 0.25],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 15625, 'max' => 26041, 'base_tax' => 2083.33, 'percent' => 0.30],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 3, 'min' => 26042, 'max' => 1000000, 'base_tax' => 5208.33, 'percent' => 0.32],

            // 4 dependents
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 1, 'max' => 6249, 'base_tax' => 0.00, 'percent' => 0],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 6250, 'max' => 6666, 'base_tax' => 0.00, 'percent' => 0.05],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 6667, 'max' => 7499, 'base_tax' => 20.83, 'percent' => 0.10],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 7500, 'max' => 9166, 'base_tax' => 104.17, 'percent' => 0.15],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 9167, 'max' => 12082, 'base_tax' => 354.17, 'percent' => 0.20],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 12083, 'max' => 16666, 'base_tax' => 937.50, 'percent' => 0.25],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 16667, 'max' => 27082, 'base_tax' => 2083.33, 'percent' => 0.30],
            ['deduction_schedule' => 'semi-monthly', 'dependents' => 4, 'min' => 27083, 'max' => 1000000, 'base_tax' => 5208.33, 'percent' => 0.32],
        ];

        foreach ($wts as $wt) {
            WithholdingTax::create([
                'deduction_schedule' => $wt['deduction_schedule'],
                'dependents' => $wt['dependents'],
                'min' => $wt['min'],
                'max' => $wt['max'],
                'base_tax' => $wt['base_tax'],
                'percent' => $wt['percent']
            ]);
        }

    }
}
