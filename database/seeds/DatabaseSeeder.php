<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UserPermissionTableSeeder::class);
        
        $this->call(DepartmentPositionTablesSeeder::class);
        $this->call(DependentTableSeeder::class);
        $this->call(EducationTableSeeder::class);

        $this->call(ApplicantsTableSeeder::class);
        $this->call(AssessmentCategorySeeder::class);
        $this->call(AssessmentSeeder::class);
        $this->call(PostEvaluationTableSeeder::class);

        $this->call(HolidaysTableSeeder::class);
        $this->call(LeaveSeeder::class);
        $this->call(ShiftsTableSeeder::class);

        $this->call(SSSContributionTableSeeder::class);
        $this->call(PhilHealthContributionTableSeeder::class);
        $this->call(WithholdingTaxTableSeeder::class);
    }
}
