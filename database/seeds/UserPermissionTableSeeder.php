<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\UserPermission as UserPermission;
use UnoRenta\Models\User as User;

class UserPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=1; $i <= 39; $i++) { 
            UserPermission::create([
                'user_id' => 1,
                'permission_id' => $i
            ]);
        }
        
        // gill permissions
        $gillPermissions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 39];
        for ($i=0; $i < count($gillPermissions); $i++) { 
            UserPermission::create([
                'user_id' => 2,
                'permission_id' => $gillPermissions[$i]
            ]);
        }

        // jayson permissions
        $jaysonPermissions = [1, 2, 5, 9, 13, 18, 19, 20, 21, 27];
        for ($i=0; $i < count($jaysonPermissions); $i++) { 
            UserPermission::create([
                'user_id' => 3,
                'permission_id' => $jaysonPermissions[$i]
            ]);
        }

        // andy persmission
        $andyPermissions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 39];
        for ($i=0; $i < count($andyPermissions); $i++) { 
            UserPermission::create([
                'user_id' => 4,
                'permission_id' => $andyPermissions[$i]
            ]);
        }

        // johnnail permissions
        $johnnailPermissions = [1, 2, 5, 9, 13, 18, 19, 33, 34, 35, 36, 37, 38, 39];
        for ($i=0; $i < count($johnnailPermissions); $i++) { 
            UserPermission::create([
                'user_id' => 5,
                'permission_id' => $johnnailPermissions[$i]
            ]);
        }

        // kath permissions
        $kathPermissions = [1, 2, 5, 9, 13];
        for ($i=0; $i < count($kathPermissions); $i++) { 
            UserPermission::create([
                'user_id' => 6,
                'permission_id' => $kathPermissions[$i]
            ]);
        }

        // tls permissions
        $tlPermissions = [1, 2, 5, 9, 13, 18, 19, 20, 21, 27];
        for ($i=0; $i < count($tlPermissions); $i++) { 
            UserPermission::create([
                'user_id' => 7,
                'permission_id' => $tlPermissions[$i]
            ]);
            UserPermission::create([
                'user_id' => 8,
                'permission_id' => $tlPermissions[$i]
            ]);
        }

        // default permissions
        $userCount = User::all()->count();
        $defaltPermissions = [2, 5, 9, 13];
        for ($i=0; $i < count($defaltPermissions); $i++) {
            for ($j=9; $j <= $userCount; $j++) { 
                UserPermission::create([
                    'user_id' => $j,
                    'permission_id' => $defaltPermissions[$i]
                ]);
            }            
        }

    }
}
