<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\PostAssessmentGuide;

class PostEvaluationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [
            'Identify three key elements of excellent customer services.',
            'Identify three implications of poor customer services.',
            'Name two things that you can commit to do to improve customer service in your job.'
        ];
        $count = count($questions);
        for ($i = 1; $i <= $count; $i++) { 
            PostAssessmentGuide::create([
                'question' => $questions[$i - 1],
                'order' => $i
            ]);
        }
    }
}
