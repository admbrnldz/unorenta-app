<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\Department;
use UnoRenta\Models\Position;
use UnoRenta\Models\Scheduler;

class DepartmentPositionTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // administrative services
        $adminServices = Department::create([
            'name' => 'Administrative Services'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 2
        ]);

        // Operation and Training Team
        $operationTrainingTeam = Department::create([
            'name' => 'Operation and Training Team'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $operationTrainingTeam->id,
            'employee_id' => 3
        ]);

        // finance/accounting Services
        $finAccountingServices = Department::create([
            'name' => 'Finance and Accounting Services'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $finAccountingServices->id,
            'employee_id' => 2
        ]);

        // bpa / business process team
        $bpa = Department::create([
            'name' => 'Business Process Team'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $bpa->id,
            'employee_id' => 3
        ]);
        Scheduler::create([
            'department_id' => $bpa->id,
            'employee_id' => 7
        ]);

        // ics / inbound customer support
        $ics = Department::create([
            'name' => 'Property Concierge'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $ics->id,
            'employee_id' => 3
        ]);
        Scheduler::create([
            'department_id' => $ics->id,
            'employee_id' => 8
        ]);

        // data entry team
        $dataEntryTeam = Department::create([
            'name' => 'Data Entry Team'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $dataEntryTeam->id,
            'employee_id' => 2
        ]);

        // dispatch team
        $dispatchTeam = Department::create([
            'name' => 'Dispatch Team'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $dispatchTeam->id,
            'employee_id' => 3
        ]);

        // escalation and pom team
        $ept = Department::create([
            'name' => 'Escalation and Pom Team'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $ept->id,
            'employee_id' => 3
        ]);

        // lead generation team
        $leadGenerationTeam = Department::create([
            'name' => 'Lead Generation Team'
        ]);
        Scheduler::create([
            'department_id' => $adminServices->id,
            'employee_id' => 1
        ]);
        Scheduler::create([
            'department_id' => $leadGenerationTeam->id,
            'employee_id' => 3
        ]);

        // unassigned employee
        $unassigned = Position::create([
            'name' => 'Unassigned',
            'department_id' => $adminServices->id,
        ]);

        $generalManager = Position::create([
            'name' => 'General Manager',
            'department_id' => $adminServices->id,
        ]);

        $administrationManager = Position::create([
            'name' => 'Administration Manager',
            'department_id' => $adminServices->id,
        ]);
        $operationsManager = Position::create([
            'name' => 'Operations Manager',
            'department_id' => $operationTrainingTeam->id,
        ]);
        $hrOfficer = Position::create([
            'name' => 'HR Officer',
            'department_id' => $adminServices->id,
        ]);
        $accounting = Position::create([
            'name' => 'Accounting Staff',
            'department_id' => $finAccountingServices->id,
        ]);
        $trainer = Position::create([
            'name' => 'Trainer/Subject-Matter Expert',
            'department_id' => $operationTrainingTeam->id,
        ]);
        $bpaTeamLead = Position::create([
            'name' => 'BPA Team Lead',
            'department_id' => $bpa->id,
        ]);
        $icsTeamLead = Position::create([
            'name' => 'ICS Team Lead',
            'department_id' => $ics->id,
        ]);
        $utility = Position::create([
            'name' => 'Utility',
            'department_id' => $adminServices->id,
        ]);
        $bpaAssoc = Position::create([
            'name' => 'Business Process Associate',
            'department_id' => $bpa->id,
        ]);
        $icsRep = Position::create([
            'name' => 'ICS Representative',
            'department_id' => $ics->id,
        ]);      
        $dea = Position::create([
            'name' => 'Data Entry Agent',
            'department_id' => $dataEntryTeam->id,
        ]);      
        $dispatcher = Position::create([
            'name' => 'Tookan Dispatcher',
            'department_id' => $dispatchTeam->id,
        ]);       
        $ea = Position::create([
            'name' => 'Executive Assistant',
            'department_id' => $ept->id,
        ]);       
        $lga = Position::create([
            'name' => 'Lead Generation Associate',
            'department_id' => $leadGenerationTeam->id,
        ]);



    }
}
