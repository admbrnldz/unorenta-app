<?php

use UnoRenta\Models\Holiday;
use Illuminate\Database\Seeder;
use UnoRenta\Models\HolidayCategory;

class HolidaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Uncategorized',
            'Regular',
            'Special'
        ];

        foreach ($categories as $cat) {
            HolidayCategory::create([
                'name' => $cat
            ]);
        }

        $holidays = [
            [
                'date' => '2016-01-01',
                'name' => 'New Year\'s Day',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-01-02',
                'name' => 'Special non-working day after New Year',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
            [
                'date' => '2016-02-08',
                'name' => 'Chinese Lunar New Year\'s Day',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
            [
                'date' => '2016-02-25',
                'name' => 'People Power Anniversary',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
            [
                'date' => '2016-03-20',
                'name' => 'March equinox',
                'description' => 'Season',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-03-24',
                'name' => 'Maundy Thursday',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-03-25',
                'name' => 'Good Friday',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-03-26',
                'name' => 'Holy Saturday',
                'description' => 'Observance',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-03-27',
                'name' => 'Easter Sunday',
                'description' => 'Observance',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-04-09',
                'name' => 'The Day of Valor',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-05-01',
                'name' => 'Labor Day',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-05-05',
                'name' => 'Lailatul Isra Wal Mi Raj',
                'description' => 'Common Local holidays',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-05-09',
                'name' => 'National Elections',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
            [
                'date' => '2016-06-12',
                'name' => 'Independence Day',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-06-20',
                'name' => 'June Solstice',
                'description' => 'Season',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-07-08',
                'name' => 'Eidul-Fitar',
                'description' => 'Common Local holidays',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-08-21',
                'name' => 'Ninoy Aquino Day',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
            [
                'date' => '2016-08-29',
                'name' => 'National Heroes Day',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-09-10',
                'name' => 'Id-ul-Adha (Feast of the Sacrifice)',
                'description' => 'Common Local holidays',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-09-11',
                'name' => 'Id-ul-Adha Day 2',
                'description' => 'Common Local holidays',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-09-12',
                'name' => 'Id-ul-Adha Day 3',
                'description' => 'National holiday',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-09-22',
                'name' => 'September equinox',
                'description' => 'Season',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-10-03',
                'name' => 'Amun Jadid',
                'description' => 'Muslim, Common Local holidays',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-11-01',
                'name' => 'All Saints\' Day',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
            [
                'date' => '2016-11-02',
                'name' => 'All Souls\' Day',
                'description' => 'Observance',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-11-30',
                'name' => 'Bonifacio Day',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-12-12',
                'name' => 'Maulid un-Nabi',
                'description' => 'Common Local holidays',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-12-21',
                'name' => 'December Solstice',
                'description' => 'Season',
                'holiday_category_id' => 1
            ],
            [
                'date' => '2016-12-24',
                'name' => 'Christmas Eve',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
            [
                'date' => '2016-12-25',
                'name' => 'Christmas Day',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-12-30',
                'name' => 'Rizal Day',
                'description' => 'Regular Holiday',
                'holiday_category_id' => 2
            ],
            [
                'date' => '2016-12-31',
                'name' => 'New Year\'s Eve',
                'description' => 'Special Non-working Holiday',
                'holiday_category_id' => 3
            ],
        ];

        foreach ($holidays as $holiday) {
            Holiday::create([
                'name' => $holiday['name'],
                'description' => $holiday['description'],
                'date' => $holiday['date'],
                'holiday_category_id' => $holiday['holiday_category_id']
            ]);
        }

        // fake holidays
        Holiday::create([
            'name' => 'Haladay kay malas',
            'description' => 'Malas man gud, holiday na lang.',
            'date' => '2016-09-09',
            'holiday_category_id' => 2
        ]);
        Holiday::create([
            'name' => 'Holodoy kewon',
            'description' => 'This is holodoy!',
            'date' => '2016-09-27',
            'holiday_category_id' => 3
        ]);

    }
}
