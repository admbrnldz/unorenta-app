<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\AssessmentCategory;

class AssessmentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Prior Work Experience',
            'Technical Qualification/Experience',
            'Leadership Ability',
            'Customer Service Skills',
            'Communication Skills',
            'Candidate Enthusiasm',
            'Problem Solver/Analytical',
            'Empathy',
            'Active Listening',
            'Overall Impression and Recommendation'
        ];
        $count = count($categories);
        for ($i = 1; $i <= $count; $i++) { 
            AssessmentCategory::create([
                'name' => $categories[$i - 1],
                'order' => $i
            ]);
        }
    }
}
