<?php

use Illuminate\Database\Seeder;
use UnoRenta\Models\AssessmentGuide;

class AssessmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [
            'Has the candidate acquired necessary skills or qualification through past work experience?',
            'Does the candidate have the technical skills necessary for this position?',
            'Did the candidate demonstrate the leadership skills necessary for the position?',
            'Did the candidate demonstrate the knowledge and skills to create a positive customer experience/interaction necessary for this position?',
            'How were the candidates\'s communication skills during the interview?',
            'How much interest did the candidate show in the position?',
            'Did the candidate have the initiative to solve problem?',
            'Did the candidate show the willingness to help?',
            'Did the candidate listen closely and carefully to every details of each question?',
            'Final comments and recommendations for proceeding with the candidate.'
        ];
        $count = count($questions);
        for ($i = 1; $i <= $count; $i++) { 
            AssessmentGuide::create([
                'question' => $questions[$i - 1],
                'assessment_category_id' => $i,
                'order' => $i
            ]);
        }    }
}
