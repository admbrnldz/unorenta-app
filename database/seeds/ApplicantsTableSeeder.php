<?php

use UnoRenta\Models\Applicant;
use Illuminate\Database\Seeder;

class ApplicantsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$applicants = [
            [
                'first_name' => 'Gene Eric',
                'middle_name' => 'Sosyal',
                'last_name' => 'Amigo',
                'contact_number' => '0933 195 2485',
                'email' => 'ericamigo@email.com',
                'skype' => 'ericamigo'
            ],
            [
                'first_name' => 'Adam Vincent',
                'middle_name' => 'Achup',
                'last_name' => 'Bernaldez',
                'contact_number' => '0975 021 3015',
                'email' => 'adambernaldez@email.com',
                'skype' => 'adambernaldez'
            ],
            [
                'first_name' => 'John Babes',
                'middle_name' => 'Corporation',
                'last_name' => 'Palaca',
                'contact_number' => '0915 230 5973',
                'email' => 'jbpalaca@email.com',
                'skype' => 'jbpalaca'
            ],
        ];
        foreach ($applicants as $applicant):
            Applicant::create([
                'first_name' => $applicant['first_name'],
                'middle_name' => $applicant['middle_name'],
                'last_name' => $applicant['last_name'],
                'contact_number' => $applicant['contact_number'],
                'email' => $applicant['email'],
                'skype' => $applicant['skype']
            ]);
        endforeach;
	}
}
