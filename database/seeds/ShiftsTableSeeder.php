<?php

use Carbon\Carbon;
use UnoRenta\Models\Shift;
use UnoRenta\Models\Timecard;
use UnoRenta\Models\Employee;
use UnoRenta\Models\ShiftBrief;
use Illuminate\Database\Seeder;

class ShiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $employees = Employee::all();
        $now = Carbon::now();
        $theMonth = $now->month;

        for ($i=6; $i < $theMonth; $i++):

            $currentMonth = Carbon::create(2016, $i, 1)->startOfDay();
            $numberOfDays = $currentMonth->format('t');

            foreach ($employees as $employee):

                // $shift
                $h = [9, 17, 1];
                $pick = $h[rand(0, 2)];

                // create shift brief
                $shiftBrief = ShiftBrief::create([
                    'employee_id' => $employee->id,
                    'month' => $currentMonth->month,
                    'year' => $currentMonth->year,
                    'created_by' => 1
                ]);

                for ($j = 1; $j <= $numberOfDays; $j++):

                    $thisDay = Carbon::create($currentMonth->year, $currentMonth->month, $j, 0, 0, 0);

                    if ($thisDay->isWeekend()) continue;

                    // create shift
                    $shiftFrom = $thisDay->copy();
                    $shiftFrom->hour = $pick;
                    
                    $shift = Shift::create([
                        'shift_brief_id' => $shiftBrief->id,
                        'shift_from' => $shiftFrom,
                        'shift_to' => $shiftFrom->copy()->addHours(8),
                        'is_rd' => rand(0, 1)
                    ]);

                    // create timecard
                    $opt = rand(1, 2);
                    $timeIn = null;
                    $late = 0;
                    if ($opt == 1) {
                        $timeIn = Carbon::parse($shift->shift_from)->subMinutes(rand(0, 10));
                    } else if ($opt == 2) {
                        $late = rand(0, 20);
                        $timeIn = Carbon::parse($shift->shift_from)->addMinutes($late);
                    }

                    Timecard::create([
                        'shift_id' => $shift->id,
                        'employee_id' => $employee->id,
                        'time_in' => $timeIn,
                        'ti_signed_by' => 2,
                        'time_out' => $timeIn->copy()->addHours(8)->addMinutes(rand(0, 10)),
                        'to_signed_by' => 2,
                        'late_minutes' => $late > 10 ? $late : 0,
                        'undertime_minutes' => 0
                    ]);

                endfor;

            endforeach;

        endfor;        

    }
}
