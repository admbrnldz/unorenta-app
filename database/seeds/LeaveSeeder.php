<?php

use Carbon\Carbon;
use UnoRenta\Models\Employee;
use UnoRenta\Models\Approver;
use UnoRenta\Models\LeaveType;
use UnoRenta\Models\Department;
use Illuminate\Database\Seeder;
use UnoRenta\Models\LeaveEntitlement;
use UnoRenta\Models\LeaveApplication;

class LeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        LeaveType::create([
            'name' => 'Sick Leave'
        ]);
        LeaveType::create([
            'name' => 'Vacation Leave'
        ]);


        $employees = Employee::all();
        foreach ($employees as $e):
            LeaveEntitlement::create([
                'employee_id' => $e->id,
                'leave_type_id' => 1,
                'year' => 2016,
                'credits' => 15
            ]);
            LeaveEntitlement::create([
                'employee_id' => $e->id,
                'leave_type_id' => 2,
                'year' => 2016,
                'credits' => 15
            ]);
        endforeach;

        // gill can approve
        // -- admin services
        // -- finance
        // -- data entry
        $gillDepartments = [1, 3, 6];
        foreach ($gillDepartments as $dept) {
            Approver::create([
                'employee_id' => 1,
                'department_id' => $dept
            ]);
        }

        // jayson can approve
        // -- operation 2
        // -- bpa 4
        // -- dispatch team 7
        // -- escalation 8
        // -- lead generation 9
        // -- property 5
        $jaysonDepartments = [2, 4, 5, 7, 8, 9];
        foreach ($jaysonDepartments as $dept) {
            Approver::create([
                'employee_id' => 2,
                'department_id' => $dept
            ]);
        }

        // shari 6
        // bpa 4
        Approver::create([
            'employee_id' => 6,
            'department_id' => 4
        ]);

        // eula 7
        // property 5
        Approver::create([
            'employee_id' => 7,
            'department_id' => 5
        ]);

        // andy 3 all
        $departments = Department::all();
        foreach ($departments as $dept) {
            Approver::create([
                'employee_id' => 3,
                'department_id' => $dept->id
            ]);
        }

        // leave application
        LeaveApplication::create([
            'employee_id' => 4,
            'leave_type_id' => 2,
            'number_of_days' => 4,
            'date_from' => '2016-09-19',
            'date_to' => '2016-09-22',
            'reason' => 'boracay po',
            'first_approver' => 3,
            'is_first_approved' => 1,
            'first_date_signed' => Carbon::now(),
            'final_approver' => 1,
            'is_final_approved' => 1,
            'final_date_signed' => Carbon::now()
        ]);
        LeaveApplication::create([
            'employee_id' => 4,
            'leave_type_id' => 1,
            'number_of_days' => .5,
            'time_cycle' => 'AM',
            'date_from' => '2016-09-08',
            'date_to' => '2016-09-08',
            'reason' => 'sakit tiyan po',
            'first_approver' => 3,
            'is_first_approved' => 1,
            'first_date_signed' => Carbon::now(),
            'final_approver' => 1,
            'is_final_approved' => 1,
            'final_date_signed' => Carbon::now()
        ]);
        LeaveApplication::create([
            'employee_id' => 4,
            'leave_type_id' => 0,
            'number_of_days' => 1,
            'date_from' => '2016-09-12',
            'date_to' => '2016-09-12',
            'reason' => 'sakit po, the heart',
            'first_approver' => 3,
            'is_first_approved' => 1,
            'first_date_signed' => Carbon::now(),
            'final_approver' => 1,
            'is_final_approved' => 1,
            'final_date_signed' => Carbon::now(),
            'without_pay' => 1
        ]);        

    }
}
