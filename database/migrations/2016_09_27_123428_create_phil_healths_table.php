<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhilHealthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('philhealth_contribution_table', function (Blueprint $table) {
            $table->double('min', 10, 2);
            $table->double('max', 10, 2);
            $table->double('ee', 10, 2);
            $table->double('er', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('philhealth_contribution_table');
    }
}
