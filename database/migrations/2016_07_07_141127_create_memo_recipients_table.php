<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemoRecipientsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('memo_recipients', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('memo_id');
			$table->integer('employee_id');
			$table->boolean('seen')->default(0);
			$table->datetime('date_last_seen')->nullable();
			$table->boolean('conform')->default(0);
			$table->datetime('date_conform')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('memo_recipients');
	}
}
