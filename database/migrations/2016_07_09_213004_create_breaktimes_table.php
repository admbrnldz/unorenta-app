<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreaktimesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('breaktimes', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('timecard_id');
			$table->datetime('break_start')->nullable();
			$table->datetime('break_end')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('breaktimes');
	}
}
