<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('leave_type_id');
                        
            $table->float('number_of_days');
            $table->string('time_cycle')->nullable();

            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->text('reason');
            
            $table->integer('first_approver');
            $table->integer('is_first_approved')->default(-1);
            $table->timestamp('first_date_signed')->nullable();

            $table->integer('final_approver');
            $table->integer('is_final_approved')->default(-1);
            $table->timestamp('final_date_signed')->nullable();

            $table->smallInteger('without_pay')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leave_applications');
    }
}
