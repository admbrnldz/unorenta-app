<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDependentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dependents', function (Blueprint $table) {
			$table->increments('id');			
			$table->integer('employee_id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('middle_name');
			$table->date('birthdate')->nullable();
			$table->string('birthplace');
			$table->string('relationship');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dependents');
	}
}
