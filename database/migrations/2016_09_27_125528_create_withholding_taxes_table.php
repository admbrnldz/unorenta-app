<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithholdingTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withholding_tax_table', function (Blueprint $table) {
            $table->string('deduction_schedule');
            $table->integer('dependents');
            $table->double('min', 10, 2);
            $table->double('max', 10, 2);
            $table->double('base_tax', 10, 2);
            $table->double('percent', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('withholding_tax_table');
    }
}
