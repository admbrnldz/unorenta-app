<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemoSignatoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('memo_signatories', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('memo_id');
			$table->integer('employee_id');
			$table->string('complimentary_closing');
			$table->datetime('date_signed')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('memo_signatories');
	}
}
