<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applicants', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('middle_name');
			$table->string('contact_number')->unique()->nullable();
			$table->string('email')->unique()->nullable();
			$table->string('skype')->unique()->nullable();
			$table->smallInteger('is_flagged')->default(0);
			$table->date('flag_start')->nullable();
			$table->integer('duration')->nullable();
			$table->smallInteger('is_hired')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applicants');
	}
}
