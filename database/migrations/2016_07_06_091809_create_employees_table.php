<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_uid')->unique();
            $table->integer('user_id');
            $table->integer('position_id');
            $table->string('nickname')->nullable();

            $table->date('birthdate')->nullable();
            $table->string('birthplace')->nullable();
            $table->string('sex')->nullable();

            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('zip_code')->nullable();

            $table->string('mobile_number')->nullable()->unique();
            $table->string('home_number')->nullable();
            $table->string('email_address')->nullable()->unique();

            $table->string('civil_status')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('religion')->nullable();

            $table->double('height', 5, 2)->nullable();
            $table->double('weight', 5, 2)->nullable();
            $table->string('build')->nullable();
            $table->string('eyes')->nullable();
            $table->string('blood_type')->nullable();
            
            $table->string('sss')->nullable()->unique();
            $table->string('tin')->nullable()->unique();
            $table->string('philhealth')->nullable()->unique();
            $table->string('pagibig')->nullable()->unique();
            
            $table->date('date_hired')->nullable();

            $table->double('basic_income', 10, 2);
            $table->double('monthly_work_days', 10, 4);

            $table->string('signature')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
