<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslip_deductions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payslip_id');
            $table->integer('scheduled_deduction_id')->nullable();
            $table->integer('deduction_type')->comment('1 Before Tax, 2 After Tax');
            $table->string('name');
            $table->string('key')->nullable();
            $table->double('amount', 11, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payslip_deductions');
    }
}
