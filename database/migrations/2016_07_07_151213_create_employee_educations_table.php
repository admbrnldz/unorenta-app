<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeEducationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_educations', function (Blueprint $table) {
			$table->increments('id');			
			$table->integer('employee_id');
			$table->string('level');
			$table->string('school_name');
			$table->string('school_address');
			$table->string('course_degree');
			$table->string('year_graduated');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_educations');
	}
}
