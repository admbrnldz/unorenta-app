<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayslipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payroll_batch_id');
            $table->integer('employee_id');
            $table->double('basic_pay', 11, 2);
            $table->integer('monthly_work_days');

            $table->integer('lt')->default(0);
            $table->integer('ab')->default(0);
            $table->integer('ut')->default(0);
            $table->integer('nd')->default(0);
            $table->integer('rdot')->default(0);
            $table->integer('sh')->default(0);
            $table->integer('shrd')->default(0);
            $table->integer('rh')->default(0);
            $table->integer('rhrd')->default(0);
            $table->integer('dh')->default(0);
            $table->integer('dhrd')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payslips');
    }
}
