<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimecardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timecards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('shift_id');
            $table->integer('employee_id');

            $table->dateTime('time_in')->nullable();
            $table->integer('ti_signed_by')->nullable();
            $table->dateTime('time_out')->nullable();
            $table->integer('to_signed_by')->nullable();

            $table->integer('late_minutes')->default(0);
            $table->integer('undertime_minutes')->default(0);
            $table->smallInteger('is_absent')->default(0);
            $table->string('absent_reason')->nullable();

            $table->integer('overtime_minutes')->default(0);

            $table->integer('payslip_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('timecards');
    }
}
